# Capa 5 Sesion

> Comunicacion entre dispositivos de la red 

> Facilita los mecanismos de gestion de sesion entre la comunicacion de capas superiores 

gestion de sesion de capas superiores, comunicacion flujo constante de informacion. 

* full duplex (cuando va en ambos sentidos) 
* half duplex (cuando va alternando los sentidos de info)
  * Saber cuando es nuestro turno


## Servicios

* Control de dialogo 
* Agrupamiento
* Recuperacion 

## Protocolos 

* RPC
  * Datos cifrados durante su transferencia
  * Procedimiento remoto 
* SCP
* ASP 
* H 245
* L2TP

## Seguridad 

Es lo que caracteriza a la capa de sesion, ya que dentro de sus capacidades es la de gestionar la autenticacion y autorizacion del envio o recepcion de informacion a traves de metodos criptoghaficos. 
Puede tener puntos de comprobacion.


