# Scripting primeros pasos 

> Crear y ejecutar scripts 

## Que es un scritp ? 

Es simplemente un archivo de texto que contiene una serie de comandos que el interprete de bash ejecutara en orden.

## Creacion de scripts

1. Crear el script ( Crear archivo llamado ) `script.sh`
2. Editar script 
  * Ejemplo 
  * Primera linea es el `shebang`, indica al sistema que el archivo debe ejecutarse usando bash 
  * Segunda linea es la que imprime el mensaje por pantalla

``` 
#!/bin/bash 

echo "Primer script"
```
3. Hacer el script ejecutable `chmod +x script.sh`
4. Ejecutar Script `./script.sh`
  * Se puede hacer con: `bash script.sh` ( aca no es necesario cambiar el permiso al archivo )

## Shebang 

Es la primera linea de casi todos los scripts de bash, le dice al sistema que interprete debe usar para ejecutar script. Si no se incluye el sistema operativo no sabria como ejecutarlo 

## Comentarios 

Se usan para explicar que hace cada parte del codigo y comienzan con `#`. Los comentarios son ignorados por bash cuando ejecuta el script pero son utiles para hacer que el codigo sea legible 

## Tarea 

Crear siguiente script (llamarlo info-sistema.sh) y ejecutarlo

```
#!/bin/bash
# Mostrar la fecha y hora actuales
echo "Fecha y hora actuales:"
date

# Mostrar el espacio libre en disco
echo "Espacio libre en disco:"
df -h

# Mostrar los usuarios conectados
echo "Usuarios conectados actualmente:"
who
```

## Recomendaciones 

1. Usar Comentarios
2. Prueba los comandos individualmente 
3. Manten simplicidad 
4. Verifica permisos 


## Errores comunes 

* `<<Permission denied>>`
* `<<Command not found>>` 

## Tarea 

1. Crear un nuevo script que imprima un saludo personalizado seguido del directorio actual y la fecha, guarda el script y ejecutalo 
2. Aniade un comentario en el script explicando lo que hace cada comando
