# SQL Operators 

Sometimes, expression with a single condition are not enough to satisfy the user's requirement. 
* Logical Operatators 
  * AND
  * OR 
  * NOT 

## AND Operator 

Take two conditions and returns true or false 

```
condition1 AND condition2

SELECT 1 = 1 AND 'test' = 'test';
```

In MySQL terms, any non-zero value is considered true and it usually returns the value 1 to signify true, 0 is consider false

## OR Operator 

Take 2 conditions 

``` 
SELECT 1 = 1 AND 'test' = 'test';
```

## Not Operaor 

The not operator simply toggles a boolean value , true is converter to false and vice versa

## Symbol Operators 

Operation | Symbol
:--------:|:-------:
AND | &&
OR | \|\|
NOT | ! 

## Multiple Operators precendece 

1. Division (/), Multiplication (\*), modulus (%)
2. Addition (+), Subtraction (-)
3. Comparison ( =,>,<,<=,>=,!=,LIKE)
4. NOT
5. AND
6. OR 


