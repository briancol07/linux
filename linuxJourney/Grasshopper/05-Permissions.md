# Permissions 

## Index 

* [## File Permissions ](#file)
* [## Modifying Permissions ](#modifying)
* [## Ownership Permissions ](#owner)
* [## Umask ](#umask)
* [## Setuid ](#setuid)
  * [### Modifying SUID ](#suid)
* [## Setgid ](#setgid)
  * [### Modifying SGID ](#sgid)
* [## Process Permissions ](#process)
* [## The Sticky bit ](#sticky)
  * [### Modifying SGID ](#mod)

## File Permissions <a name="file"></a>

``` bash 
ls -l 
```
There are four parts first letter and then its group of three bits 

1. Filetype 
  * First letter 
  * d = directory
  * a = regular file 
2. User permissions
3. Group Permissions
4. Other Permissions 

Letter | Represent
-------|----------
 r | Readable
 w | Writable 
 x | Executable(program)
 \- | Empty

## Modifying Permissions <a name="modifying"></a>

Changing permissions can easily be done with chmod 

```bash
chmod u+x myfile
chmod u-x myfile
chmod ug+w
chmod 777 myfile
```

1. Adding permissions(bit on file)
2. Removing permission (bit on file)
3. Adding multiple permission bit 


* 4 read permission
* 2 write permission
* 1 execute permission

## Ownership Permissions <a name="owner"></a>

* Modify user ownership 
  * sudo chown patty myfile 
  * This will set the owner of myfile to patty
* Modify group ownership
  * sudo chgrp whale myfile
  * This will set the group of myfile to whales 
* Modify both 
  * sudo chown patty:whale myfile

## Umask <a name="umask"></a>

This command takes the 3 bit permission set we see in numerical permissins. Instead of adding these permissions though, unmask takes away these permissions 

```bash
umask 021
```

the default umask on most distributions is 022 meaning all user access butno write accesfor group and other users.

## Setuid <a name="setuid"></a>

There are special file permission bits to allow this . 
The Set User ID (SUID), allows a user to run a program as the owner of the program file rather than as themselves 
For example the passwd modify a file owned by root , in the permission set we see a s that bit is the SUID

### Modifying SUID <a name="suid"></a>

* Symbolic way
  * sudo chmod u+s myfile
* Numerical way
  * sudo chmod 4755 myfile
  
As you can see the SUID is denoted by a 4 and pre-pended to the permission set. You may see the SUID denoted as a capital S this means that it still does the same thing, but it does not have execute permissions.

## Setgid <a name="setgid"></a>

There is a set group ID (SGID). this bit allows a program to run as if it was a member of that group


### Modifying SGID <a name="sgid"></a>

* Symbolic way
  * sudo chmod g+s myfile
* Numerical way
  * sudo chmod 2555 myfile 
* The numerical representation fos SGID is 2 

## Process Permissions <a name="process"></a>

There are three UIDS associated with every process. 

* Effective user ID 
  * This UID is used to grant access rights to a process
* Real user ID 
  * his is the ID of the user that launched the process
  * These are used to track down who the user who launched the process is
* Saved user ID 
  * This allows a process to switch between the effective UID and real UID, vice versa.

## The Sticky bit <a name="sticky"></a>

This permission bit, "sticks a file/directory" this means that only the owner or the root user can delete or modify the file

### Modifying SGID <a name="mod"></a>

* Symbolic way
  * sudo chmod +t mydir
* Numerical way
  * sudo chmod 1755 mydir 
* The numerical representation is 1 

You'll see a special permission bit at the end here t, this means everyone can add files, write files, modify files in the /tmp directory, but only root can delete the /tmp directory. 
