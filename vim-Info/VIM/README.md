# VIM 

> 	`(ﾉ◕ヮ◕)ﾉ*:・ﾟ✧` Improve vi 

## Index 

* [## Topics ](#topics)
* [## Links  ](#links)
* [## Modos ](#modos)
* [## Movimientos ](#movimientos)
  * [### Movimientos entre lineas ](#movimientos2)
  * [### Movimiento entre palabras ](#movimientos3)
* [## Salir ](#salir)
* [## Insert ](#insert)
  * [### Reemplazo de palabras ](#reemplazo)
  * [### Borrado ](#borrado)
  * [### Copiado y pegado ](#cope)
* [## Encontrar ](#find)
* [## Visual ](#visual)
* [## Terminal ](#term)
* [## Archivos ](#files)
* [## Comandos para facilitar algunas cosas ](#comandos)
* [## Buffers ](#buffer)
* [## Split ](#split)
  * [### Moverte entre pestanias ](#windows)
  * [### Aumentar tamanio de las ventanas ](#windows2)
  * [### Another topic ](#another-topic)

## Topics <a name="topics"></a>

* [Learn-vim-The-easy-way](./Learn-vim-The-easy-way/README.md)
* [Learn Vimscript the hard way](./Learn-Vimscript-The-hard-way/README.md)

## Links  <a name="links"></a>

* [Presentations](https://www.youtube.com/watch?v=7fIR55kkTwc)
* [Cheat-sheet](https://vim.rtorr.com/)
* YT 
  * [Josh-Brandchaud](https://www.youtube.com/@jbranchaud)
* Reddit
  * [/vim](https://www.reddit.com/r/vim/)

## Modos <a name="modos"></a>

* Insert
* Visual 
* Normal 
  * con esc siempre se accede a ella 
* Terminal 

> NO olvidarse que se pueden hacer combinaciones 

> Tener cuidado con las mayusculas modifica todo

Con el . se puede repetir el comando anterior 
> Anter cualquier duda usar :help \<command\>

## Movimientos <a name="movimientos"></a>

Teclas  | Funcion 
:------:|:-------:
k | Arriba 
j | Abajo 
h | Izquierda 
l | Derecha 

### Movimientos entre lineas <a name="movimientos2"></a>

Teclas  | Funcion 
:------:|:-------:
  g + g | Va al inicio del archivo 
shift + g | Va al final del archivo 
\<num\> + g + g | se dirige a la linea que sea num en el archivo 
ctrl + u | Se mueve una cierta cantidad de lineas hacia arriba
ctrl + d | Se mueve una cierta cantidad de lineas hacia abajo

### Movimiento entre palabras <a name="movimientos3"></a>

Teclas  | Funcion 
:------:|:-------:
g +  j  | Se mueve sobre la misma linea pero un renglon abajo 
g +  k  | Se mueve sobre la misma linea pero un renglon arriba 
 w | Se mueve un word (palabra a la derecha)
 b | Se mueve una palabra hacia la izquierda 

## Salir <a name="salir"></a>

Teclas  | Funcion 
:------:|:-------:
  : + w    | Guarda el archivo
  : + w + q + ! | Guardar y luego salir forzado 
  : + q + ! | Salir sin guardar 
  shift + z + shift + z | Guardar y Salir 
  shift + z + shift + q | Salir sin guardar 
  esc | Salir Modo insert a modo Normal
  ctrl + [ | Funciona como escape (para salir del como insert)

## Insert <a name="insert"></a>

Teclas  | Funcion 
:------:|:-------:
   i    | Inserta donde este el cursor 
shift+i | Inserta al Inicio de la oracion
   a    | Inserta despues del cursor  
shift+a | Inserta al final de la oracion 

### Reemplazo de palabras <a name="reemplazo"></a>

Teclas  | Funcion 
:------:|:-------:
   s    | Borra el caracter siguiente y te permite escribir 
shift+s | Borra la linea entera y accede al modo insert 
  cc    | Idem
shift+c | Idem 
c + shift + 4 ($) | Borra desde donde estes posicionado hasta el final de la linea y entra modo insert
c + w   | Change word (borra palabra y ingresa modo insert) 
c + i + { | Cambia lo que este dentro de los parentesis (funciona tambien con ( y [  , con las comillas tambien )  

* Tambien puede ser como c + 2 + w (change 2 words)
* Puede existir c + a + p (Change a paragraph)
* Existe c + i + p ( change inner paragraph)

### Borrado <a name="borrado"></a>

Teclas  | Funcion
:------:|:-------:
 d + w  | Delete word 
 d + shift + 4 ($) | Borra Desde donde estes al final de la linea
 d + d  | Borra la linea 
 D      | Idem 
 d + t + caracter | Delete till (borra hasta un caracter)
 d + T + caracter | Borra hasta el caracter 
 d + i + { | Borra lo que este dentro de las llaves (funciona tambien con ( y [ , con las comillas tambien  )
x       | Borra caracter 

Otro ejemplo seria d + 2 + f + / y esto borraria la segunda barra que encuentre 


### Copiado y pegado <a name="cope"></a>

> Se puede hacer con el modo visual y luego con "y" se puede copiar

Teclas  | Funcion
:------:|:-------:
  y     | yank (seria copiar)
 y + y  | copiar linea
 p      | Pegar 
shift + p | Si tiene salto de linea, esto pega sobre el cursor 

> Si se borra una linea se guarda en el buffer y se puede pegar 

## Encontrar <a name="find"></a>

> Se puede usar regex para buscar 

Teclas  | Funcion
:------:|:-------:
  / + \<palabra\> | Busca la primer ocurrencia en el archivo 

## Visual <a name="visual"></a>

Teclas  | Funcion
:------:|:-------:
  v | Entra modo visual 
 shift + v | Selecciona toda la linea 
 ctrl + v | Visual block 

En el visual block podes seleccionar varias lineas moviendote con jk luego al precionar shift + i podes modificar varias al mismo tiempo 

## Terminal <a name="term"></a>


Teclas  | Funcion
:------:|:-------:
: + term | se abre una nueva ventana con una terminal
: + r + ! + \<commando\> | Read command in vim (lee el comando y lo pega en el archivo)

Para salir con term se puede hacer con ctrl + n


## Archivos <a name="files"></a>

Teclas  | Funcion
:------:|:-------:
: + e + \<nombre\_archivo\> | Para abrir un archivo en la direccion que estemos o crear un nuevo archivo 

## Comandos para facilitar algunas cosas <a name="comandos"></a>

Para setear los numeros en las lineas 

```bash 
:set nu 
```

Para setear relative numbers 
```bash 
:set relativenumbers 
```

## Buffers <a name="buffer"></a>

Nos permite ver los buffers activos, tanto archivos como terminales abiertos son los que estan en los buffers 

> Pueden estar visibles con split o estar en el background 

```bash 
:buffers 
```
* Cambiar de buffer 
  * :buffer1
    * :b1 
    * 1 es el id del buffer 
* Para terminar buffer 
  * : + b + d + ! 
* Si muestra un no name en buffers , crear uno nuevo
  * : + enew 

## Split <a name="split"></a>

> Para tener varias ventanas 

Si abajo aparece el [+] significa que los datos no estan guardados (en ese buffer)

Teclas  | Funcion
:------:|:-------:
:split | Split Horizontal 
ctrl + w + s | Forma rapida split horizontal
:vsplit | Split vertical 
ctrl + w + v | Forma rapida split vertical 

### Moverte entre pestanias <a name="windows"></a>

Teclas  | Funcion
:------:|:-------:
ctrl + w + j | Moverte entre arriba 
ctrl + w + k | Moverte para abajo 

### Aumentar tamanio de las ventanas <a name="windows2"></a>

Teclas  | Funcion
:------:|:-------:
 ctrl + w + 20 | agrega 20 espacios a la pestania 
 ctrl + w - 20 | quita 20 a la ventana 
 ctrl + w + = | Pone todas las pestanias al mismo tiempo

### Another topic <a name="another-topic"></a>

* Scrolling the Screen Without Moving the Cursor
  * Key Sequences:
    * Ctrl-y → Scroll the screen up without moving the cursor.
    * Ctrl-e → Scroll the screen down without moving the cursor.
* Adjusting Centering Like za
  * To recenter after scrolling:
    * zz → Centers the current line in the middle of the screen.
    * zt → Puts the current line at the top of the screen.
    * zb → Puts the current line at the bottom of the screen.

## Ideas <a name="ideas"></a>

* Select one image from the img folder and paste in the file in the left pane and make the link to that image

`(*・‿・)ノ⌒*:･ﾟ✧`

