# Supercharge your hacking mindset, workflow productivity and checklist

* [ link](https://laby-and-checklist/)

> Approaching a target to hack can fell like climbing a mountain 

## 1. Preparing your workspace 

### Desk Setup 

* Having the tools you need to do the job is vital to the tasks completion 
* If there is too much clutter on your desk or workspace you will distract 
* Add a flow 
  * Incoming taks at your left 
  * Computer in the middle 
  * Completed task are to the right 

### Distractions 

* Putting electronics away would be ideal
* you could use background music. 

### Mindset 

* Start with a good mindset , but what makes a good mindset? 
  * Setting personal goals for your day
  * Remembering your long-term growth is more important than short-term results 
  * Making amission statements 
  * Enjoy the process and enjoy learning 
  * Control your internal language 

## 2. Recon 

### Infrastructura / asset discovery 

* Large scope 
  * Find ASN belonging to target ( amass, asnlookup, metabigor , bgp ) 
  * Review acquisitions 
  * Check registrant relationships 
  * From the above info , gather seed domains for further enumeration
* Medium scope 
  * Enumerate subdomains ( amass or subfinder ) 
  * Subdomain bruteforce ( puredns )
  * Discover subdomain permutation ( gotator or ripgen )
  * Find subdomains with HTTP ports (httpx) 
  * Subdomain takeover checks 
  * SHodan queries agains domains 
  * Recursively search subdomains 
* Technology analysis 
  * Identify cloud assests 
  * Identify web server , technologies and databases 
  * Interesting files 
  * Diretor / parameter fuzzing 
  * Identify waf 
  * etc 

## 3. In-Depth application analysis 

> user management testing 

* Registration
  * Weak password policies 
  * try capture integration request 
  * Fuzz for folders created for new users 
* Authentication 
  * Username enumeration 
  * SQLI 
* Sessions 
  * Test tokens for meaning and predicatability 
  * Insecure transmission of tokens 
  * Logs 
* Profile
* Forgot/reset password
* Input handling 
* Application logic testing 

## 4. Take good notes 

Organizing your thoughts a


## Conclusion 
The goal of the workflow is to get started maybe you add workflows . 




