# Advanced 

## Exercises 

1. Replace the last line of a file with "END".
2. Delete all lines except those between lines 10 and 20.
3. Insert a line before every line containing the word "TODO".
4. Replace multiple spaces with a single space on each line.
5. Delete all lines after a line that contains the word "STOP".
6. Replace a word only on even-numbered lines in a file.
7. Delete all lines that do not contain a digit.
8. Replace the nth occurrence of "foo" with "bar" in a file.
9. Remove trailing whitespace from each line in a text file.
10. Replace words only in lines that match a specific pattern.
