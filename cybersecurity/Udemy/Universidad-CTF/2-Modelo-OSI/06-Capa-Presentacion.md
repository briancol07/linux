# Capa 6 Presentacion 

Dado que cuando se establece una comunicacion cada autor responsable puede tener sin ir mas lejos, sistemas operativos distintos, es cuando entra en juego el protagonismo de esta capa, la de presentacion la cual efectivamente se encarga de hacer una correcta interpretacion de la informacion enviada.
Hacer correcta interpretacion para que ambos SO se puedan entender 

## Servicios 

* La conversion de datos a formatos normalizados, sin importar el SO.
* Comprime los datos para que ocupen el menor numero d bits.
* Cifra los datos 


## Protocolos 

* NCP
  * Network Control Protocol 
  * sobre PP (point to point)
* XDR
  * Externada Data Representation
* AFP
  * Apple Filing Protocol
