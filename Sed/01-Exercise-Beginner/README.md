# Beginner 

## Exercises 

1. Replace all occurrences of "foo" with "bar" in a text file.
2. Delete all lines containing the word "debug" in a file.
3. Insert a line "HEADER" at the beginning of a file.
4. Append a line "FOOTER" at the end of a file.
5. Replace only the first occurrence of "foo" with "bar" on each line.
6. Delete the first line in a text file.
7. Replace "foo" with "bar" only on lines that contain the word "baz".
8. Delete all empty lines in a text file.
9. Replace all occurrences of "http" with "https" in a file.
10. Convert all uppercase letters to lowercase in a file.
