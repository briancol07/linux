# Introduction

* The industry 
* Getting Starded 
* Web vulnerabilities 
* Expert techniques 


## Bug bounty as a service 

* Hackerone
* bugcrowds 

Asset is an appplication/website/product --> Hack

```
Social --> Allow user to interact with each other 
  |    |                     |-> Need to be manage 
  |    |-> Also -> Privileges 
  |            |-> Account Integrity 
  |->  **Insecure Direct Object Reference** (IDORs)
  |-> Information Leak  
  |             \- Not validating user Identity Properly 
  |-> Account take over 
                \- Mismanage User information  
     |------------------|
         - SQL Injection (SQLi)
         - XSS Cross-site scripting 
```

## Herramienta 

BurpSuite

* General Web Application not involve user-user
    * Server side 
    * Technology stack 
    * Network vulnerability
* Mobile Applications 
    * Attack & analysis strategies 
        * Certificate pining bypass mobile reverse engineering cryptography
    * Rooted Device
    * Emulator 
* APIs ( Application Programming Interface ) 
    * Retrieve data 
        * HTTP
        * XML
        * json
* Source Code Executables 
    * Web vulnerabilities 
    * Programming Kills 
    * Code Analysis
    * Cryptography 
    * Reverse Engineering 
* HW and IOT  -> HACK Devices 
    * IOT concepts / Vulnerabilities 
        * Digital signing 
        * Assymetric Encrytion Scheme 
    * Web vulnerabilities 
    * Programming 
    * Reverse Engineering 
    * Wireless hacking 

## Triage 

> Confirmation of Vulnerabilities 

## Asset Code

> Which subdomain, product and app you can hack 

## Vulnerability scope 

> Which vulnerabilities the company will accept as a valid bugs 

## VDPs ( Vulnerability disclosure Program ) 

> Reputation program only 
