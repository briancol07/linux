# Advanced 

## Exercises

1. Search for lines that contain a date in the format YYYY-MM-DD.
2. Find lines that contain a URL in a file.
3. Display lines containing the word "user" with color highlighting.
4. Find all lines that contain more than one occurrence of the word "data".
5. Search for lines that contain a word with exactly 5 characters.
6. Find lines containing "user" but only in files modified in the last 7 days.
7. Display all lines containing "config" but exclude lines with a comment (#).
8. Search for lines containing hexadecimal numbers in a file.
9. Find lines where the word "user" appears twice on the same line.
10. Search for lines containing "user" in a large file and limit the output to 10 lines
