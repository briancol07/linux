# Puertos y Protocolos 

## Index

* [## Introduccion a los puertos de red](### Introduccion a los puertos de red)
* [## Tipos ](#tipos)
* [## Que son los protocolos de comunicacion ? ](#protocolos)
* [## Protocolos comunes y su funcionamiento ](#comunes)
* [## Como Funcionan los puertos y protocolos juntos ](#funcionamiento)
* [## Importancia de la seguridad en puertos y protocolos ](#seguridad)

## Introduccion a los puertos de red 

Los puertos de red son puntos de acceso en un dispositivo qu permiten la entrada y saldia de datos. En terminos de SW, un puerto es una entidad logica que identifica un proceso especifico o un tipo de servicio en un sistema

Cada puerto tiene un numero asociado, llamado "numero de puerto" que va de 0 a 65535. 

## Tipos <a name="tipos"></a>

* Puertos Bien conocidos ( 0 - 1023 )
  * Estos puertos estan reservados para los servicios o protocolos mas comunes. 
  * 80 HTTP
  * 22 SSh 
  * 21 FTP
* Puertos Registrados ( 1024 - 49151 ) 
  * Estos puertos pueden ser registrados por companias o desarolladores para servicios especificos 
  * 3306 Servicios de base de datos MySQL 
* Puertos Dinamicos o Privados ( 49152 - 65535 ) 
  * Estos puertos son utilizados de manera temporal por aplicaciones para estableces conexiones. Se asignan automaticamente y se liberan unavez que la conexion temrina. 

## Que son los protocolos de comunicacion ? <a name="protocolos"></a>

Los protocolos son un conjunto de reglas que definen como los datos deben ser formateados transmitidos y recibidos en una red. Cada protocolo esta diseniado para cumplis con una tarea especifica en el proceso de comunicacion 

## Protocolos comunes y su funcionamiento <a name="comunes"></a>

* HTTP/HTTPS ( puerto 80 y 443 ) 
  * HTTP ( hypertext transfer protocol ) es el protocolo utilizado para la transferencia de paginas web. HTTPS es la version segura que aniade cifrado para proteger la transmision de datos 
* FTP ( Puerto 21 )
  * File transfer protocol es utilizado para la transferencia de archivo entra un cliente y un servidor. A pesar de ser menos seguro que otros metodos, es ampliamente utilizado en entornos que no requiren alta seguridad 
* SSH ( Puerto 22 ) 
  * Secure shell es un protocolo que permite el acceso seguro a un sistema remoto 
  * Proporciona un canal cifraod apra la administracion remota de servidores y la transferencia de archivos 
* Telnet ( Puerto 23 ) 
  * Permite la comunicacion interactiva con un dispositivo remoto. 
  * A diferencia de SSH, no cifra la transmision de datos, lo que lo hace inseguro para su uso en redes abiertas 
* SMTP ( Puerto 25 ) 
  * Simple mail transfer protocol es utilizado para envio de correos electronicos.
  * Funciona de manera similar a FTP, pero esta diseniado para la transmision de mensaje de texto
* DNS ( Puerto 53 )
  * Domain name system traduce nombrs de dominio a direcciones IP. 
  * Este protocolo es crucial para la navegacion de internet 
* SMB/Samba ( Puerto 445 )
  * Server message block es utilizado para compartir archivos, impresoras y otros recursos 
  * Samba es una implementacion que permite a los sistemas unix/linux interactuar con redes windows 
* POP3/IMAP ( Puertos 110 y 143 ) 
  * Post office protocol y Internet message access protocol son para la recepcion de correos electronicos.
  * POP3 descarga los correos desde el servidor a la maquina del usuario 
  * IMAP permite gestionar el correo directamente en el servidor 

## Como Funcionan los puertos y protocolos juntos <a name="funcionamiento"></a>

En una red cuando se quieren comunicar, envia un paquete de datos a una direcicon IP especifica y un numero de puerto correspondiente . Al acceder a un sitio web, tu navegador envia una solicitud HTTP al puerto 80 del servidor. El servidor responde en ese mismo puerto enviando la pagina web de vuelta al navegador 

## Importancia de la seguridad en puertos y protocolos <a name="seguridad"></a>

Los puertos abiertos y los protocolos mal configurados peuden ser puntos de entrada para ataques. Si telnet esta abierto en un servidor expuesto internet, un atacante puede acceder sin cifrado y sin autenticacion segura, comprometiendo la integriadad de la red .

