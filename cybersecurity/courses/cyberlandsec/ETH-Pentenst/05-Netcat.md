# Netcat 

## Index 

* [## Que es netcat? ](#que)
* [## Funcionalidades ](#funcionalidades)
* [## Comandos Basicos de netcat ](#comandos)
* [## ETH ](#eth)

## Que es netcat? <a name="que"></a>

Es una herramienta que permite leer y escribir datos a traves de conexiones de red utilizando el protocolo TCP o UDP. Ademas de su capacidad para establecer conexiones. Tambien puede ser utilizado para escaneo de puertos, transferencia de archivos y como una herramienta de diagnostico 

Para instalarlo en linux `sudo apt-get install netcat`

## Funcionalidades <a name="funcionalidades"></a>

* Conexiones TCP y UDP
  * Esto es util para establecer comunicacion entre dos dispositivos en una red 
* Escaneo de puertos 
  * Para verificar que puertos estan abiertos en un sistema remoto
* Transferencia de archivos
  * Transferir archivos entre dos dispositivos en una red, actuando como un servidor y un cliente
* Redireccion de Puertos 
  * Redirigir el trafico de un puerto a otro , lo que puede ser util en escenarios de tunel de red.

## Comandos Basicos de netcat <a name="comandos"></a>

* Establecer una conexion TPC `nc` 
  * Este comando establece una conexion al puerto especificado en el sistema de destino
* Escuchar un puerto `nc -l -p`
  * Este comando hace que netcat escuche en un puerto especificio esperando conexiones entrantes
* Transferir un archivo `nc -l -p > archivo_recibido.txt`
  * En el dispositivo cliente, ejecuta `nc <archivo_enviado.txt`
* Escaneo de puertos  `nc -zv 20-80`
  * Este comando escanea los puerto 20 a 80 en el sistema de destino y muestra que puertos estan abiertos 

## ETH <a name="eth"></a>

* Troyano de puerta trasera 
  * Permite crear un troyano simple que permite al atacante obtener acceso remoto al sistema comprometido 
  * `nc -l -p -e /bin/bas` esto le da al atacante acceso completo a una shell en el sistema de la vicitma
* Transferencia encubierta de archivos 
  * Los atacantes peuden utilizar netcat para tranferir archivos entre sistemas comprometidos sin ser detectados 
* Escaneo y enumeracion de puertos 
  * Enumerar servicios y peurtos en un sistema objetivo
