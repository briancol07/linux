# Echoing Messages 

* `echo`
* `echom`

## For help 

```
:help echo 
:help echom
```
## Explain 

Both comands send to the screen the thing you write after 

```
:echo "hello world"
:echom "hello world 2"
```

To see the difference between them use `:messages"

echo will not appear and echom yes , this is useful to print output and debug problems. The `echo` output will be delete when the script is done .

## Comments 

with the character `"` not always work.
