# Moviendote por un archivo 

Aprenderemos los movimientos escenciales y como utilizarlos de manera eficiente.

:h motion.txt 

## Navegando por caracteres 

```vim 
h  " Izquierda
j  " Abajo
k  " Arriba
l  " Derecha
```

Se puede agregar a lo siguiente al vimrc

```vim 
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>
```
Tambien hay complemetnos para tratar de romper ese mal habito uno de ellos es vim-hardtime.

## Numeracion relativa 

Se puede agreagar al .vimrc para que queden 

```vim 
set number 
set relativenumber
```

Esto muestra la lihnea actual en la que se encuentra el cursor y los numeros de linea relativos desde la posicion de este 

se pueden sacar con 
```vim 
set norelativenumber
set nonumber
```

## Cuenta tu movimiento 

Como hablamos antes 

> [numero] + Movimiento 

Puedes aplicar esto a todos tus movimientos. Si quieres moverte 9 caracteres a la derecha en vez de presionar la tecla l 9 veces puedes ejecutar 9l.

## Navegacion por palabras 

Vamos a movernos por una unidad de movimiento mayor: palabra.

* Mover cursor al comienzo siguiente palabra 
  * w 
* Al final de la siguiente palabra 
  * e 
* Al comienzo de la palabra anterior 
  * b 
* Al final de la palabra anterior 
  * ge 

* Mover cursor al principio de la palabra 
  * W 
* Al final de la siguiente palabra 
  * E 
* Al comienzo de la palabra previa  
  * B
* y al final de la palabra previa 
  * gE

```vim 
w   "Mueve el cursor hacia adelante al comienzo de la siguiente palabra
W   "Mueve el cursor hacia adelante al comienzo de la siguiente PALABRA
e   "Mueve el cursor hacia adelante una palabra hasta el final de la siguiente palabra
E   " Mueve el cursor hacia adelante una palabra hasta el final de la siguiente PALABRA
b    " Mueve el cursor hacia atrás al principio de la palabra previa
B    " Mueve el cursor hacia atrás al principio de la PALABRA previa
ge   " Mueve el cursor hacia atrás al final de la palabra previa
gE   " Mueve el cursor hacia atrás al final de la PALABRA previa
```

> Una palabra es una secuencia que incluyen todos los caracteres excepto el espacio blanco. 

```vim
:h word 
:h WORD
```

## Navegacion de la linea actual 

Mientras editas a menudo necesitas navegar horizontalmente en la misma lina 

```vim
0     "Ir al primer caracter de la línea actual
^     "Ir al primer caracter que no es un espacio en blanco en la línea actual
g_    "Ir al último caracter que no es un espacio en blanco en la línea actual
$     "Ir al último caracter de la línea actual
n|    "Ir a la columna n en la línea actual0     Ir al primer caracter de la línea actual
^     "Ir al primer caracter que no es un espacio en blanco en la línea actual
g_    "Ir al último caracter que no es un espacio en blanco en la línea actual
$     "Ir al último caracter de la línea actual
n|    "Ir a la columna n en la línea actual
``` 

Tambien puedes realizar una busqueda en la linea actual con f y t (ambos buscan a adelnate). Con ; puedes seguir avanzando al siguiente caso 

```vim
f  "  Busca hacia adelante una coincidencia en la línea actual
F  "  Busca hacia atrás una coincidencia en la línea acual
t  "  Busca hacia adelante una coincidencia en la línea actual, posicionando el cursor antes de la coincidencia
T  "  Busca hacia atrás una coincidencia en la línea actual, posicionando el cursor antes de la coincidencia
;  "  Repite la última búsqueda en la línea actual en la misma dirección
,  "  Repite la última búsqueda en la línea actual en dirección contraria
```

## Navegacion por fase y parrafo 

> Una frase termina con alguno de estos signos de puntuacion ( . ! ? ) seguido por un final de linea, un espacion en blanco o una tabulacion.

```vim 
(    " Salta a la frase previa
)    " Salta a la siguiente frase
```

Por cierto, si tienes problemas con vim porque no considra una frase, frases separados por . seguido de una linea simple quiza estas en el modo 'compatible'. 

```vim
" agregar en vimrc 
:set nocompatible 
```

> Parrafo : Un parrafo comienza depues de cada linea vacia y tambien en cada conjunto de una macro de parrafo especificada por los pares de caracteres en la opcion de parrafos. 

```vim 
{   " Salta al párrafo previo
}   " Salta al párrafo siguiente
```

```vim 
:h sentence 
:h paragraph 
```

## Navegacion entre parejas 

Se puede saltar entre parentesis, llaves, corchetes 

```vim 
%   " Navega de una a otra pareja, normalmente funciona con (), [], {}
```

```vim 
:h %
```
Luego se puede agregar vim-rainbow 

## Navegacion por numero de linea 

Puedes slatar a un numero de linea n con nG. Por ejemplo si quiered saltar a la line 7 utiliza 7G. 

```vim 
gg   " Va a la primera línea
G    " Va a la última línea
nG   " Va a la línea n
n%   " Va al n% del archivo
```

## Navegacion por la ventana 

Para ir rapidamente a la parte superior, central o inferior de tu ventatna puedes utilizar H,M,O,L 

* H = High
* M = Medium 
* L = Low 

```vim 
H    " Ir a la parte superior de la ventana
M    " Ir a la parte media de la ventana
L    " Ir a la parte inferior de la ventana
nH   " Va a la línea n desde la parte superior de la ventana
nL   " Va a la línea n desde la parte inferior de la ventana
```

## Desplazandose 

Para desplazarte por el texto o hacer scroll 

* Pantalla completa 
  * ctrl-f
  * ctrl-b
* Media pantalla 
  * ctrl-d
  * ctrl-u
* Linea a linea 
  * ctrl-e
  * ctrl-y

Command | Accion
--------|--------
ctrl-e | Desplaza el texto hacia arriba una linea
ctrl-d | Desplaza media pantalla hacia arriba
ctrl-f | Desplaza una pantalla completa hacia arriaba el texto 
ctrl-y | Desplaza el texto abajo una linea
ctrl-u | Desplaza media pantalla hacia abajo
ctrl-b | Desplaza una pantalla completa hacia abjoe el texto 

Desplazarte de forma relativa en funcion de la linea acutal donde se encuentre el cursor

Command | Accion
--------|--------
zt | Lleva la linea actual donde esta el cursor cerca de la parte superior de la pantalla
zz | Lleva la linea actual donde esta el cursor a la parte media de la pantalla 
zb | Lleva la linea actual donde esta el cursor cerca de la parte inferior de la pantalla 

## Navegacion por busqueda 

Command | Accion
--------|--------
/ | Busca hacia adelante una coincidencia 
? | Busca hacia atras una coincidencia
n | Repite la ultima busqueda (en la misma direccion)
N | Repite la ultima busqueda (en direccion opuesta)
\* | Buscar texto bajo cursor (adelante)
\# | Buscar texto bajo cursor (atras)
g\*| Busca la palabra bajo el cursor hacia adelante
g\#| Busca la palabra bajo el cursor hacias atras 


### Para resaltar las busquedas
```
:set hlsearch 
:set incsearch
```

### Inhabilitar esa funcion 

```
nohlsearch
```

```
nnoremap <esc><esc> :noh<return><esc>
```

## Marcando la posicion

Se puede guardar la posicion acutal del cursor y poder volver a esa posicion mas tarde. 

command | Accion
--------|--------
ma  | Marca una posicion estableciendo la marca "a" en la posicion alctual del cursor 
\`a | Salta a la line a y columna donde se encuentra "a"
\'a | Salta a la linea donde se encuentra "a"

Con letras minusculas son marcas locales y con Mayusculas son marcas globlaes, cada archivo tiene sus marcas locales. Pero de las globales solo se peude tener un juego y si se ejecuta el comando las sobreescribe. 

para ver las marcas 

```
:marks
```

Command | Accion
--------|--------
'' | Salta hacia atras a la ultima linea donde se encontraba el cursor en el buffer actual antes de saltar 
\`\`| Salta hacia atras al a ultima posicion en el buffer actual a la ultima posicion en elbuffer actual antes de saltar 
\`[ | Salta al comienzo del texto previamente cambiado / pegado 
\`] | Salta al final de ltexto previament cambiado / pegado 
\`< | Salta al comienzo de la ultima seleccion visual
\`> | Salta al final de la ultima seleccion visual
\'0 | Salta hacia atras al ultimo archivo editado cuando salio de vim 


Para ver mas marcas 

```
:h marks
```

## Saltar 

No todos los movimientos cuenta como salto pero los siguientes si 

Command | Accion
--------|--------
\' | Ir a la linea marcada 
\` | Ir a la posicion marcada 
G |  Ir a la linea 
/ | Buscar hacia adelante
? | Buscar hacia atras
n | Repetir ultima busqueda (misma dir)
N | Repetir ultima busqueda (dir opuesta)
% | Encontra la parjea 
( | Ir a la frase anterior 
) | Ir a frase siguiente
{ | Ir al parrafo anterios 
} | Ir al parrafo siguiente 
L | Ir a la ultima linea mostrada en la ventana
M | Ir ala linea media mostrada en la ventana 
H | Ir ala linea superior mostradad en la ventana 
[[ | Ir a la seccon previa 
]] | Ir a la seccion siguiente 
:s | substituir 
:tag | Saltar a la etiqueta de definicion 

Para mas ayuda ver:

```
:h jumps 
:h jumps-motions
```
