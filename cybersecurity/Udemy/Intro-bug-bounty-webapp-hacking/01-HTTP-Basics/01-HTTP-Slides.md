# HTPP Slides

## HTTP Basics Refresher 

Your browser send a request to the web route

* What page ( or endpoint ) to fetch 
  * Get /HTTP/1.1
* What website ( or host) to fetch from 
  * Hostname 
* Browser information including name version and etc
  * User-agent
* What type of data to send/receive
  * Content-type
* Authorizaiton header allowing you to fetch data
  * Authorization 
* What site/page sent you this new page 
  * Referrer  

## Commonly used HTTP Methods 


| Method | Use |
|:---------:|:---------:|
  GET | To fetch data |
  HEAD| Does the same thing as get but doesn't show the full response|
  POST| To create or change data|
  PUT| To replace or modify data |
  DELETE| To delete data |
  OPTIONS| to see communication options (GET,HEAD,POST,PUT,DELETE,ETC)|

## Common response status codes 


| Range | Status |
|:---------:|:---------:|
  200| Successful reponse|
  300| Redirects|
  400| 401:Unauthorized or unauthenticated <br> 403:Forbidden or no access to resources <br> 404: Not found or file doesn't exist <br> 405: Http methos not allowed  |
  500|Internal error where the server doesn't know how to handle the request |
  Tons more | [Status](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status) |


