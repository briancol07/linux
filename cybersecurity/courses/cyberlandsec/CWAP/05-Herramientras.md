# Herramientas para pruebas de seguridad 

* Nos ayudan a : 
  * Descubrir vulnerabilidades
  * Interceptar comunicaciones 
  * Analizar el comportamiento 
* Herramientas  
  * Burpsuite
  * OWASP ZAP
  * Nikto 
 
## Index 

* [## BurpSuite ](#burp)
* [## OWASP ZAP ](#zap)
* [## Nikto ](#nikto)
* [## Otras Herramientas ](#otros)
* [## Actividad](### Actividad)

## BurpSuite <a name="burp"></a>

Plataforma integral que permite intercerptar modificar y escanear peticiones/respuestas http 

* Proxy intercept 
* Scanner 
* Intruder 
* Repeater 

## OWASP ZAP <a name="zap"></a>

Alternativa Burpsuite 

* Escaneo Activo y pasivo 
* Spidering 
  * Rastrear todas las paginas de un sitio web, encontrando entradas oculatas o no 
* Proxy HTTP 
* Fuzzing 
  * Enviar gran cantidad de entradas no esperadas a un campo de un formulario o aun parametro 

## Nikto <a name="nikto"></a>

Realizar escaneos rapidos de servidores web en busca de configuraciones inseguras y vulnerabilidades conocidas 

* Deteccion de vulnerabilidades comunes 
* Compatiblidad con multiples servidores 
* Velociad 

## Otras Herramientas <a name="otros"></a>

* Nmap 
* Metasploit Framework 
  * Plataforma que se utiliza para realizar pruebas de penetracion y explotacion de vulns 
* SQLMap 
  * Detectar vulnerabilidad de SQL 

## Actividad 

* Configura el objetivo de escaneo Damn vulnerable web application 
* Realiza un escaneo activo y revisa vulnerabilidades encontradas 
* Entender cada una de las vulnerabilidades reportadas y como podrias corregirlas 
