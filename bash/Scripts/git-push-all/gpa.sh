#!/bin/bash

git add . 

if [ $# -eq 0 ]; then
  git commit -m "Fast Commit"
elif [ $# -eq 1 ]; then
  git commit -m "$1"
elif [ $# -eq 2 ]; then
  git commit -m "$1" -m "$2"
else
  echo "Wrong ammount of arguments" 
  exit 128
fi

git push 

