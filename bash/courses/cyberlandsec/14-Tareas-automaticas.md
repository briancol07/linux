# Programacion de tareas automaticas 

## Programacion de tareas recurrentes 

Cron es un servicio de sistemas unix y linux que permiten ejecutar comandos o scripts en intervalos regulares ( minutos , horas, dias, semanas o meses ).
Las tareas programadas se definen en el archivo crontab, que contiene las instrucciones sobre cuando y que ejecutar.

## Sintaxis del crontab 

```
* * * * * Comando
30 12 * * * /ruta/al/comando
```

* Cada asterisco representa un valor de tiempo en el sig orden 
  1. Minuto ( 0-59 )
  2. Hora ( 0-23 )
  3. Dia del mes ( 1 - 31 )
  4. Mes ( 1-12 )
  5. Dia de la semana ( 0-7 ) 0 y 7 son domingos 

## Crear o editar Crontab

```
crontab -e
```

## Listar tareas programadas 

```
crontab -l
```

## Programacion de tareas con `at` tarea unica 

Es otra herramienta pero a diferencia de cron, se utiliza par programar tareas que se ejecutan una sola vez en un momento especifico.

```
at hora [fecha]
echo "echo 'Backup completado' >> /ruta/al/archivo_log" | at 10:30 AM tomorrow
```

### Ver tareas programadas 

Mostrara una lista de todas las tareas pendientes programadas por `at`

```
atq
```
### Eliminar una Tarea programada 

Primero hay que sacar el numero de tarea con `atq` y luego con `atrm` seguido del numero de la tarea 

```
atrm 1
```

## Redirigir Salida y errores en tareas programadas 

```
0 2 * * * /ruta/al/script.sh >> /ruta/al/log.txt 2>&1
echo "bash /ruta/al/script.sh >> /ruta/al/log.txt 2>&1" | at 6:00 PM tomorrow
```

## Ejemplo diario Automatico 

```
#!/bin/bash
fecha=$(date +%Y%m%d)
carpeta_respaldo="/ruta/a/respaldo"
destino="/ruta/a/destino"

# Crear archivo tar con la fecha
tar -czf $destino/respaldo_$fecha.tar.gz $carpeta_respaldo

echo "Respaldo completado para el $fecha" >> /ruta/a/logs/respaldo.log
```

## Tarea 

1. Crea un script que realice un respaldo de una carpeta y programalo con `cron` para que se ejecute cada viernes a las 8 pm 
2. Programa una tarea unica `at` para que ejecute un script dentro de 2 horas y redirige la salida de un archivo de log 
