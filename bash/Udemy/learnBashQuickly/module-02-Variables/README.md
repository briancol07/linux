# Module 2 Variables

## Overview

1. Using variables in bash scripts 
2. Variables and Data Types
3. Constant Variables
4. Command Substitutions
5. A smarter "hello world" script 


## What is a variable? 

It's a storage unit, for different types of values.

## Read command 

> Reads input and storage in the variable 

```bash 
read name
```

when you want to reference a variable use **$** 
use doble quotation because it will show the raw text 

## Data types 

bash don't see data types, are all one thing 

```bash 
age=23
charac='c'
year=2000
color='blue'
```

## Constant Variables

Does not change in time. You cant reasing any value to it.

```bash 
readonly PI=3.14159
```
## Command Substitutions 

assign a variable the value of a commmand 

```bash 
# this can do nested commands 
variable=$(command)

# you cant 
variable=`command`
```




