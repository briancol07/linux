# Cyber Attacks 

## What's the difference?

Cybercriminals use many different types of malicious SW or Malware, to carry out attacks. Malware is any code that can be used to steal data, bypass access controls, or cause harm to or compromise a system.


## Viruses 

A virus is atype of computer program that, when executed, replicates and attaches itself to other files, such as a legitimate program, by inserting its own code into it. Some viruses are harmless yet others can be destructive, such as those that modify or delete data. Most viruses require end-user inteaction to initiate activation, and can be written to act on a specific date or time.
Viruses can be spread thorugh removable media such as USB, internet downloads and email attachments. The simple act of opening a file or executing a specific program can trigger a virus. Once a virus is active, it will usually infect other programs on the computer or other computers on the network. Viruses mutate to avoid detection.

## Worms 

A Worm is a malicious SW program that replicates by independently exploiting vulnerabilities in networks. Unlike a virus,which requires a host program to run, worms can run by themselves. Other than the initial infection of the host, they do no require user participation and can spread very quickly over the network, usually slowing it down. 
Worms share similar patterns: they exploit system vulnerabilities, they have a way to propagate themselves and they all contain malicious code (payload) to cause damage to computer systems or networks.

## Troyan horse

A trojan horse is malware that carries out malicious operations by masking its tru intent. It migh appear legitimate but is , in fact, very dangerous. Trojans exploit the privileges of the user who runs them.
Unlike viruses, trojans do not self-replicate but often bind themselves to non-executable files, such as image, audio or vide files, acting as a decoy to harm the systems of unsuspecting users.

## The Logic Bombs 

A logic bomb is a malicious program that waits for a trigger, such as a specified date or database entry,to set off the malicious code. Until this trigger event happens, the logic bom will remain inactive.
Once activated, a logic bomb implements a malicious code that causes harm to a computer in various ways. It can sabotage database records, erase files and attack operating system or applications. Can damage HW and SW .

## Ransomware 

This malware is designed to hold a computer system or the data it contains captive until a payment is made. 
Ransomware usually works by encrypting your data so that you cannot access it. AcCording to ransomware claims, once the ransom is paid via an untraceable payment system, the cybercriminal will supply a program that decrypts the file or sned an unlock code - but in reality, many victims do not gain access to their data even after they have paid. 
Some versions of ransomware can take advantage of specific system vulnerabilities to lock it down. 

## Denial of Service Attacks (DoS) 

Are a type of network attack, that is realitvely simple to conduct, even for an unskilled attacker. They are a major risk as they usually result in some sort of interruption to network service, causing a significant loss of time and money. Even operational technologies, HW or SW that controls physical devices or processes in buildings factories or utility providers, are vulnerable to DoS attacks, which can cause a shutdown in extreme circumstances. 

### Overhelming quantity of traffic 

This is when a network, host or application is sent an enormous amount of data a rate which it cannot handle. This causes a slowdown in transmission or response, or the device or service to crash 

### Maliciously formatted packets 

A packet is a collection of data that flows between a source and a receiver computer or application over a network such as the Internet. When a maliciously formatted packet is sent, the receiver will be unable to handle it. 
For example, if an attacker forwards packets containing errors or improperly formatted packets that cannot be indentified by an application, this will cause the receiving device to run very  slowly or crash .

## Distributed denial of service (DDoS)

Attacks are similar but originate from multiple coodinated sources. 

1. An attacker builds a network (botnet) of infected hosts called zombies, which are controlled by handler systems.
2. The zombie computers constantly scan and infect more hosts, creating more and more zombies. 
3. When ready, the hacker will instruct the handler systems to make the botnet of zombies carry out a DDoS attack. 

## Domain Name System 

There are many essential technical services needed for a network to operate such as routing, addressing and domain naming. These are prime targets for attack. 

### Domain Reputation 

The Domain Name System (DNS) is used by DNS servers to translate a domain name, such as www.cisco.com, into a numerical IP address so that computer can understand it. If a DNS server does not know an IP address, it will ask another DNS server. 

An organization needs to monitor its domain reputation, including its IP address, to help protect agains malicious external domains. 

### DNS spoofing 

DNS spoofing or DNS cache poisoning is an attack in which false data is introduced into a DNS resolver cache  - the temporary database on a computer's operating system that records recen visits to websites and other Internet domains.
These poison attacks exploit a wekness in the DNS SW that cuases the DNS server to redirect traffic for a specific domain to the attacker's computer 

### Domain hijacking 

When an attacker wrongfully gains control of a target's DNS information, they can make unauthorized chages to it. This is known as domain hijacking . 
The most common way of hijacking a domain name is to change the administrator's contract email address through social engineering or by hacking into the administrator's email account. The administrator's email address can be easily found via the WHOIS record for the domain, which is of public record.

### Uniform resource location (URL) 

Is a unique identifier for finding a specific resource on the Internet. Redirecting a URL commonly happens for legitimate purposes.
Instead of taking to one page they will redirect you to a malicious site. 

## Layer 2 Attacks 

Layer 2 refers to the data link layer in the Open Systems Interconnection (OSI) data communication model. 
This layer is used to move data across a linked physical network. IP addresses are mapped to each physical device address (also known as media access control (MAC) address) on the network, using a procedure called address resolution protocol (ARP). 
In ist simples term, the mac address identifies the intended receiver of an IP address sent over the network and ARP resolves IP addresses to MAC addresses for transmitting data. 

### Spoofing 

Spoofing or poisoning, is a type of impersontaion attack that takes advantanges of a trusted relationship between two systems. 

* Mac address spoofing occurs when an attacker disguises their device as a valid one on the network and can therefore bypass the authentication process. 
* ARP spoofing sends spoofed ARP messages across a LAN. This links an attacker's MAC address to the IP address of an authorized device on the network.
* IP spoofing send IP packets from a spoofed source address in order to disguise it. 

### MAC Flooding 

Devices on a network are connected via a network swith by using packet switching to receive and foward data to the destination device. MAC flooding compromises the data transmitted to a device. An attacker floods the network with fake MAC addresses, compromising the security of the network switch.

## Man in The middle  and Man in the mobile attacks 

Attacker can intercept or modify communications between two devices to steal information from or to impersonate one of the devices. 

### MitM (Man-in-the-Middle)

a MitM attack happens when a cybercriminal takes control of a device without the user's knowledge. With this level of access, an attacker can intercept, manipulate and relay false information between the sender and the intended destination. 

### MitMo (Man-in-the-Mobile)

A variation of man-in-the-middle, Mitmo is a type of attack used to take control over a user's mobile device. When infected, the mobile device is instructed to exfiltrate user-sensitive information and send iit to the attackers.
ZeuS is one example of a malware package with MitMo capabilities. It allows attacker to quitly capture two-step verification SMS messages sent to users. 

## Zero-Day attacks 

A zero-day or zero-day threat exploits SW vulnerabilities before they become known or before they are disclosed by the SW vendor. 
A network is extremely vulnerable to attack between the time an exploit is discovered (zero hour) and the time it takes for the SW vendor to develop and release a patch that fixes this exploit.
Defending against such fast-moving attacks requires network security professionals to adopt a more sophisticated and holistic view of any network architecture. 

## Keyboard Logging 

As the name suggests, keyboar logging or keylogging refer to recording or logging every key struck on a computer's keyboard. 
Cybercriminals log keystrokes via SW installed on a computer system or through HW devices that are physically attached to a acomputer, and configure they keylogger SW to send the log file to the criminal. Because it has recorded all keystrokes, this log file can reveal usernames, passwords, websites visited and other sensitive information.
Many anti-spyware suites can detect adn remove unauthorized keyloggers.

## Defending Against attacks 

Organizations can take several steps to defend against various attacks. 

* Configure firewalls to remove any packets from outside the network that have addresses indicating that they originated from inside the network.
* Ensure patches and upgrades are current 
* Distribute the workload across server systems 
* Netwrok devices use internet Control Message protocol (ICMP) packets to send error and control messages, such as wheter or not a device can communicate with another on the network. To prevent DoS and DDoS attacks , organiztions can block external ICMP packets with their firewalls .
