# Applocker 

Es una caracteristica disponible en las versionesenterprise y ultimate de windows 7. Las directivas de applocker son similares a las de restriccion de SW, aunque applocker posee varias ventajas, puedenser aplicado a usuario, cuentras de grupo y la capacidad de aplicar a todas las versiones de un producto.

AppLocker s basa en un servicio denominado identidad de la aplicacion y el tipo de inicio de este servicio esta configurado como manual. 

## Reglas predeterminadas 

Reglas que se deben crear de forma automatica y que permiten el acceso por defecto de windows y archivos de programa, son necesarioas por que AppLocker limita la ejecucion de cualquier aplicacion que no este en una regla de permiso.

## Reglas de bloqueo

Es necesrio agregar una regla de bloqueo solo si otra regla de AppLocker permite una aplicacion, podemos utilizar explicitamente reglas definidas de bloqueo para impedir la ejecucion de aplicaciones que son activadas a traves de las reglas predeterminadas.

## Reglas Ejecutables 

Aplicadas a los archivos exe y com por defecto son reglas de ruta que permiten a todos ejecutar todas las aplicaciones en la carpeta de archivos de programa y la carpeta de Windows 

Las reglas predeterminadas tambine permiten a los administradores ejecutar aplicaciones en cualquier lugar en el equipo. Es necesario utlizar las reglas por defecto, ya que Windows no funcionara corractamente a menos que ciertas aplicacion cubiertas por estas reglas , tengan autorizacion para ejecutar. 

## Reglas de instalacion 

Archivos con extensiones Msi y Msp, por defecto permite a todas utilizar archivos firmados por Windows installer en la carpeta %SystemDrive%'\windows y los miembors del grupo de administradores local para ejecutar cualquier msi o msp. Las reglas predeterminadas permite la instalacion de SW y actualizaciones de SW a traves de Directivas de Grupo. 

## Reglas de comandos 

Incluye los archivos: bat,cmd,vbs y extensiones js. Aunque es posible el uso de reglas de publicador con secuencias de comandos , la mayoria de scripts se crean sobre una base ad-hoc por administradores y rara vez son firmados digitalmente.
Podemos utilizar las reglas de hash con los scripts que son rara vez modificados y reglas de ruta con directorios que contienen secuencias de comandos que se actualizan periodicamente 

## Reglas DLL 

Bibliotecas, archivos dll y extensiones ocx, estas reglas no estan habilitadas de forma predeterminada en AppLocker, es necesario crear una regla para cada Dll utilizada por las aplicaciones aunt que la creacion de las reglas es facil generando reglas automaticamente.

## Reglas de publicador 

A diferencia de una regla de certificados de restriccion de SW, no es necesario obtener un certificado para utilizar una regla editor porque los detalles de la firma digital son extraidos del archivo aplicacion de referencia. 
Si un archio no tiene firma digtal, no se puede restringir ni permitir mediante reglas de AppLcker editor.
Permiten mas flexibilidad que las reglas hash por que se puede especificar no solo una version especifica de un archivo, tambien todas las versiones futuras de ese archivo.

## Reglas Hash 

Permite identificar un archivo binario especifico que no esta firmado digitalmente, podemos utilizar la identificacion por hash.
Podemos utilizar el asistente de creacion de reglas para automatizar la creacion de hash de archivo para todos los archivos una ubicacion especifica.

## Reglas de ruta 

Las regla de ruta permiten definir rutas en los cuales se van a poder ejecutar programas. 
Debemos tener en consideracion que los usuarios con permisos de modificacion sobre los archivos y/o directorios pueden afectar el comportamiento de la politica. Por ejemplo, reemplanzando un archivo que tiene permisos para ser ejecutado por otro archivo totalmente diferente.

## Configurar Excepciones 

Permiten a aplicaciones especificas estar exentas de reglas mas generales. Se pueden utilizar cualquier metodo para especificar una excepcion y el metodo que elija no dependera del tipo de regla que se esta creando. Podemos crear excepciones para las reglas de bloqueo, asi como reglas de permiso.

## Auditoria 

Esto permite comprobar que aplicaciones son afectadas por AppLocker sin llegar a bloquear la ejecucion. Se puede configurar App:cker para auditar normas en lugar de hacerlas cumplir 

* El nombre de regla 
* El SID de usuario atacado o grupo
* Archivo Afectado por que regla y trayectoria
* Si el archivo esta bloqueado o permitido 
* El tipo de Regla (has editor, ruta de acceso o archivo) 
