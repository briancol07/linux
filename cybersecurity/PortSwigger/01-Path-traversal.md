# Path Traversal

Known as directory traversal, These vulnerabilities anables an attacker to read arbitrary files on the server that is running an application 

* Application code and data 
* Credentials for back-end systems 
* Sensitive operating systems files 

## Reading Arbitrary files via path traversal 

Shopping application that display images of items for sale 
Load an image :

```
<img src="/loadImage?filename=218.png">
```

The attacker can request the following URL to retirve `/etc/passwd` 
```
https://insecure-website.com/loadImage?filename=../../../etc/passwd
```

## LAB ( Apprentice )

Search for this request `GET /image?filename=25.jpg`
and replace after the equal for `../../../etc/passwd` and in the response will show all the passwords 

## Commmon Obstacles to exploiting path traversal vulnerabilities 
