# Expert 

## Exersices 

1. Find lines that contain nested parentheses.
2. Search for lines that contain a palindrome word.
3. Find lines where the same word appears twice consecutively.
4. Display lines that contain words ending in "ing" but not "ring".
5. Search for lines containing a pattern like (foo|bar|baz) in a file.
6. Find lines containing a string that looks like a UUID.
7. Search for lines that have repeated sequences of letters (e.g., "abab").
8. Find lines that contain a mix of uppercase and lowercase letters.
9. Display lines that contain a valid IPv6 address.
10. Search for lines containing JavaScript-like variable names in a file.
11. Find lines that contain words with alternating vowels and consonants.
12. Search for lines that contain a balanced pair of square brackets.
13. Display lines that contain words with exactly two vowels.
14. Find lines where words are separated by exactly three spaces.
15. Search for lines that contain a sequence of increasing numbers.
16. Display lines containing a specific pattern within angle brackets (e.g., <pattern>).
17. Find lines where numbers are formatted as currency (e.g., $123.45).
18. Search for lines that match a CSS selector pattern (e.g., #id.class).
19. Find lines that contain words starting and ending with the same letter.
20. Search for lines where the first word is capitalized.
