# Capa 4 Transporte 

Conexion de extremo a extremo y control de flujo de datos.

> Su objetivo es ofrecer un sistema para enviar informacion sin errores a las capas superiores 

Si hay algun error esta capa realizara una repeticion o descartar la comunicacion. Evita congestion o cortes parciales y que llegue de manera correcta (orden de emision).

## Funciones 

* Entrega la informacion de forma fidedigna
* Brinda la informacion en el orden correcto
* Gestiona de forma eficiente el flujo de la comunicacion


## Protocolos

* TCP 
* UDP


## Datos 

Comunicacion bidireccional

Esta capa toma los datos que vienen de la aplicacion, los divide en segmentos y luego los envia a la capa de red.

## Seguridad 

La seguridad en esta capa se aplica desde :

* El cifrado de los datos 
* Autenticacion de todas las partes
* Prevenir la manipulacion que atente contra la ingegridad de datos 
* Evasion de ataques de reinyeccion
  * Una transmicion es retrasnmitida

En esta capa se pueden apreciar protocolos para una comunicacion segura como ser SSL, TLS o SSH.

Un ataque muy conocido es el SYN. Se bloquea la linea y el receptor se queda en espera de una respuesta.Se empieza a saturar y el host queda deshabilitado y ocurre un DoS. 

