# Education and Careers   

## Become a Cybersecurity Guru 

Search in Indeed, ItjobMatch,Monster and Career Builder to get a sense of what kind of jobs are available, all over the world

## Professional Certifications 

### Microsoft Technology Associate (MTA) Security Fundamentals

This certification is aimed at high school and early college student as well as those interested in a career change 

### Palo Alto Network Certified Cybersecurity Associate

This is an entry-level certification for newcomers who are preparing to start their career in the cybersecurity field.

### ISACA CSX Cybersecurity Fundamentals Certificate 

This certification es geared toward recent post-secondary graduates and those interested in a career change. This certificate does not expire or require preiodic recertification

### CompTIA Security + 

This an entry-level security certification that meets the U.S. Department of Defense Directive 8570.01-M requirement which is an importan item for anyone looking to work in IT security for the federal government 

### EC Council Certified Ethical Hacker (CEH)

This certification test your understanding and knowledge of how to look for wraknesses and vulnerabilities in target system usign the same knowledge and tools as a malicious hacker but in a lawful and legitimate manner.

### ISC2 Certified Information Systems Security Professional (CISSP)

This is the most recognizable and popular security certification. In order to take the exam, you need to have at least five years of relevan industry experience.

### Cisco Certified CyberOps Associate 
This certification validate the skills required of associate-level cybersecurity analysts within security operations centers.

## Cybersecurity Career Pathways 

CyberSeek is a tool that provides detailed data about supply and demand in the cybersecurity job market to help close the cybersecurity skills gap.
