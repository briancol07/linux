#!/bin/bash

#Crea una funcion recursiva que calcule la suma de los numero del 1 al n ( donde n sea un argumento pasado a la funcion )  I

sumatorial() {
    if [ $1 -le 1 ]; then
        echo 1
    else
        local anterior=$(sumatorial $(( $1 - 1 )))
        echo $(( $1 + anterior ))
    fi
}


resultado=$(sumatorial 5)
echo "Resultado: $resultado"
