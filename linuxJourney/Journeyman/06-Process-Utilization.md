# Process Utilization 

## Index 

* [## Tracking processes : `top` ](#tracking)
  * [### Processes list that are currently in use ](#use)
* [## Lsof  ](#lsof)
* [## fuser ](#fuser)
* [## Process threads ](#threads)
* [## CPU Monitoring ](#cpu-monitoring)
* [## I/O Monitoring ](#i-o)
* [## Memory Monitoring ](#memory)
* [## Continuous Monitoring ](#monitoring)
* [## Cron Jobs ](#Cron)

## Tracking processes : `top` <a name="tracking"></a>

> The command top

* 1st line: this is the same information you would see if you ran the uptime command 
  1. Current time 
  2. How long the system has been running 
  3. How many users are currently logged on 
  4. System load average 
* 2nd line : Tasks that are running , sleeping, stopped and zombied 
* 3rd line : Cpu Information 
  1. US: user CPU time- Percentage of CPU time spent running users'processes that aren't niced 
  2. Sy: system CPU time - Percentage of CPU time spent runnign the kernel and kernel proocesses 
  3. ni: nice CPU time - Percentage of CPU time spent runnign niced processes
  4. id: CPU idle time - Percentage of CPU time that is spent idle 
  5. wa: I/O wait - Percentage of CPU time that is spent waiting for I/O
    * If this value is low, the problem probably isn't disk or network I/O
  6. hi: Hardware interrupts- Percentage of CPU time spent serving hardware interrupts 
  7. si: Software interrupts- Percentage of CPU time spent serving software interrupts 
  8. st: Steal time 
    * If your are running virtual machines this is the percentage of CPU time that was stolen from you for other tasks 
* 4th and 5th line: Memory usage and Swap usage

### Processes list that are currently in use <a name="use"></a>

1. PID: id of process
2. USER: user that is the owner of the process 
3. PR: Priority of process 
4. NI: The nice value 
5. VIRT: Virtual memory used by the process 
6. RES: Physical memory used from the process 
7. SHR: Shared memory of the process 
8. S: Indicates the status of the process 
  * S = Sleep
  * R = Running
  * Z = zombie 
  * D = uninterruptible 
  * T = stopped 
9. %CPU - This is the percent of CPU used by this process
10. %MEM = Percentage of RAM used by the process 
11. TIME + - Total time of activity of this process
12. COMMAND - Name of the process 

Specify a process ID 

```
top -p 1
```

## Lsof  <a name="lsof"></a>

> Files aren't just text files, images, etc, they are everything on the system, disks, pipes, network sockets devices,etc. 

To see whatis in use by a process you can use the lsof ( List open files ) this will show you a lsit of all the open files and their associated process. 
You can also kill these processes so we can unmount this pesky drive.

## fuser <a name="fuser"></a>

Another way to track a process is the fuser command ( file user ), this will show you information about the proces that is using the file or the file user 

## Process threads <a name="threads"></a>

Threads are very similar to processes, in that they are used to execute the same program, they are often reffered to as lightweight process.

* 1 thread = single-threaded 
* more than one thread multi-threaded

Threads cna share resources among each other easily, making it easir for them to communicate among each other and at times it is more efficient to have a multi-threaded apllication that an multi process application.

The processes are denoted with each PID and underneath the processes are their threads.

## CPU Monitoring <a name="cpu-monitoring"></a>

* Command uptime 

Load averages are good way to see the CPU load on your system.

These numbers represent the average CPU load in 1, 5 and 15 minutes intervals. 

> CPU load is the average number of processes that are waiting to be executed by the CPU 

you can view the amount of cores you have on your ssytem with `cat /proc/cpuinfo`

When observing load average, you have to take the number of cores into account, if you find that your machine is always using an above avera load, there could somehting wrong going on.

## I/O Monitoring <a name="i-o"></a>

We can also monitor CPU usage as well as monitor disk usage with a handy tool knwon as iostat

* First part is the CPU info:
  * %user- show the percentage of CPU utilization that occurred while executing at the user level ( application)
  * %nice - Show the percentage of CPU utilization that occured while executing at the user level with nice priority. user CPU utilization with nice priorities 
  * %system - show the percentage of CPU utilization that occured while executing at the system level ( kernel )
  * %iowait - Show the percentage of time that the cpu or CPUs wre idle during which the system had an outstanding disk I/O request 
  * %steal - Show the percentage of time spent in involuntary wait by the virtual CPU or CPUs while the hypervisor was servicing another virtual processor 
  * %idle - show the percentage of thime the CPU or CPUs were idle and the system did not have an outstanding disk I/O request 
* Second part 
  * tps = Indicates the number of transfers per second that were issued to the device. 
    * A transfer is an I/O request to the device ( indeterminate size ) 
    * Multiple logical request can be combined into a single I/O request to the device 
  * kb\_reads/s - inidcate the amount of data read from the device expressed in kilobytes per second 
  * kb\_wrtn/s - indicates the amount of data read from the device expressed in kilobytes per second 
  * kb\_read - the total number of kilobytes read
  * kb\_wrtn - The total number of kilobytes written


## Memory Monitoring <a name="memory"></a>

Use `vmstat` 

* Fields 
  * Procs 
    * r - Number of processes for run time 
    * b - Number in uninterruptible sleep
  * Memory 
    * swpd : amount of virtual memory used 
    * free : Amount of free memory 
    * buff : Amount of memory used as buffers 
    * cache: Amount of memory used as cache 
  * Swap  
    * si : amount of memory swapped in from disk 
    * so : amount of memory swapped out to disk 
  * io 
    * bi : amount of blocks received in from a block device 
    * bo : Amount of blocks sent out to a block device 
  * system 
    * in : number of interrupts per second 
    * cs : number of context switched per second 
  * CPU 
    * us : tiem spent in user time 
    * sy : time spent in kernel time 
    * id : time spent idle 
    * wa : time spent waiting for IO 

## Continuous Monitoring <a name="monitoring"></a>

* sar 
  * tool that is used to do historical analysis on your system
  * install `sudo apt install sysstat` 
  * Enable field `/etc/default/sysstat`
* Using 
  * `sudo sar -q` list details from the start of today 
  * `sudo sar -r` list the details of memory usage from the start of the day 
  * `sudo sar -P` List the details of CPU usage   
  * To view different day go into `/var/log/sysstat/saXX` xx is the day you want to view
    * `$sar -q /var/log/sysstat/sa02`

## Cron Jobs <a name="Cron"></a>

Service that runs programs for you at whatever time you schedule 

```
30 08 * * * /home/pete/scripts/change_wallpaper
```

* fields 
  * Minute (0-59)
  * Hour  (0-23)
  * Day of the month (1-31)
  * Month (1-12)
  * Day of week (0-7) ( 0 and 7 = sunday)

Asterisk mean to match every value. To create a cronjob, just edit the crontab `crontab -e`
