# Monitoreo de sitemas y logs 

## Monitoreo de logs del sistema

Archivos como `/var/log/syslog` o `/var/log/auth.log` te permiten rastrear la actividad del sistema, detectar intentos de acceso fallidos, errores o coportamientos anomalos 


### Ejemplo 

```
#!/bin/bash

# Archivo de log a monitorear
log_file="/var/log/auth.log"

# Buscar intentos fallidos de autenticación en el log
echo "Monitoreando intentos fallidos de autenticación en $log_file..."
tail -F $log_file | grep --line-buffered "Failed password"
```

## Generar alertas automaticas basadas en eventos sospechosos 

```
#!/bin/bash

log_file="/var/log/auth.log"
alert_file="/var/log/alerta_fallas.log"
max_fallas=5

# Contar los intentos fallidos de inicio de sesión en el archivo de log
fallos=$(grep "Failed password" $log_file | wc -l)

if [ $fallos -ge $max_fallas ]; then
    echo "ALERTA: Se han detectado $fallos intentos fallidos de inicio de sesión." >> $alert_file
    # Aquí puedes agregar un comando para enviar un correo electrónico o notificación
    # echo "Alerta de intentos fallidos" | mail -s "Alerta de Seguridad" admin@ejemplo.com
fi
```
se puede agregar cron 


## Monitorear cpu y memoria 

* TOP 
* HTOP
* vmstat
* mpstat 
* free

```
#!/bin/bash

# Archivo de log para los resultados
log_file="/var/log/monitoreo_recursos.log"

echo "Monitoreo de Recursos - $(date)" >> $log_file

# Monitorear uso de CPU
echo "Uso de CPU:" >> $log_file
mpstat 1 1 | grep "all" >> $log_file

# Monitorear uso de memoria
echo "Uso de Memoria:" >> $log_file
free -m >> $log_file

# Monitorear procesos que más memoria consumen
echo "Procesos con mayor uso de memoria:" >> $log_file
ps aux --sort=-%mem | head -n 5 >> $log_file

echo "----------------------------------------------------" >> $log_file
```

## Monitorear procesos criticos 

Ejemplo con apache 

```
#!/bin/bash

servicio="apache2"
estado=$(systemctl is-active $servicio)

if [ "$estado" != "active" ]; then
    echo "ALERTA: El servicio $servicio no está en ejecución. Reiniciando servicio..." >> /var/log/alerta_servicios.log
    systemctl start $servicio
    echo "$(date) - Servicio $servicio reiniciado" >> /var/log/alerta_servicios.log
else
    echo "$(date) - El servicio $servicio está en ejecución correctamente." >> /var/log/alerta_servicios.log
fi
```

Con `systemctl` para ver si el servicio esta activo ( en ejecucion )

## Tarea

* Crea un script que monitoree el uso de la CPU y memoria y que envie una alerta si la CPU supera el 80% de uso
* Crea un script que monitoree el archivo syslog en busca de errores del sistema y que guarde un log de estos eventos 





