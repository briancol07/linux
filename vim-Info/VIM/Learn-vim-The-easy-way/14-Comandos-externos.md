# Comandos externos 

## El comando Bang

> `!`

1. Leer la salida estandar (STDOUT) de un comando externo dentro del buffer actual
2. Escribir el conteido de tu buffer como la entrada estandar (STDIN) a un comando externo
3. Ejecutar un comando externo desde dentro de vim 

## Leer salida estandar (STDOUT) de un comando dentro de vim 

La sintaxis para leer la salida estandar (STDOUT) de un comando externo dentro del buffer actual es `:r !cmd `

`:r`es el comando de vim para leer, si se utiliza sin el `!` puedes utilizarlo para obtener el contenido deu n archivo 

```
:r archivo1.txt
```

Si se agrega el `!` + comando externo , la salida de ese comando sera insertada dentro del buffer actual 

```
:r! ls
```

Tambien acepta redireccion 

```
:10r !cat archivo1.txt
```

## Escribir el contenido de un buffer en un comando externo 

Ademas de guardar puede utilizar el comando `:w` para pasar el contenido del buffer acutal como la entrada estandar (STDIN) a un comando externo 

```
:w !cmd
```

De manera similar al comando global ( si no se le pasa un rango toma linea actual)  

```
:2w !node
```

> No es es lo mismo `:w! node` que `:w !node`

Con uno escribes en buffer actual en un comando externo y en el otro estas forzando a guardar y dandole el nombre node 

## Ejecutando un comando externo 

Puede ejecutar un comando externo desde dentro de vim con el comando bang 

```
:! cmd
```

## Filtrando textos

Ejemplo 
```
:.!tr '[:lower:]' '[:upper:]'
```
Hay que aclarar el rango si no va a tirar error , lo que hace es donde este parado el cursor lo va a convertir en mayusculas

otro ejemplo 
```
:%!awk "{print $1}" 
```
Elimina la segunda columna de ambas lineas utilizando el comando awk 
