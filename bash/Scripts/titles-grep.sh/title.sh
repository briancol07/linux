#!/bin/bash

while read -r line; do 

  tmp2="("${line%%<*}")"
  tmp1="${line#*name=\"}"
  tmp1="${tmp1%%\"*}"
  sub_string="* [${tmp2}](#${tmp1})" 
  echo "${sub_string}"

done < <(grep "##" "$1")
