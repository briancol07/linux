# DevOps

## Roadmap <a name="Roadmap"></a>

* [Mischa-roadmap](./Mischa-roadmap.md)
* [Nana-roadmap](./Nana-roadmap.md)

## Tools <a name="Tools"></a>

* [Excalidraw](https://docs.excalidraw.com/docs/introduction/development)

## Links <a name="links"></a>

* [Become DevOps 6 months](https://www.youtube.com/watch?v=9vNN_mkRDq0)


