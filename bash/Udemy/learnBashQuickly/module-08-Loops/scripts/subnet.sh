#!/bin/bash 

for i in {0..255}; do 

 host=23.227.36.$i
 ping -c1 $host 1>/dev/null 2>/dev/null

 if [ $? -eq 0 ]; then
    echo "$host has replied"
 else
      echo "$host didn't reply"
 fi
done 



