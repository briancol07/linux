# Layers of TCP IP Model 

> Transmission control Protocol 
> Internet Protocol 

## Internet Protocol Suite 

Conceptual model , send network traffic, 4 layers 

* Application Layer 
    * HTTP, SMTP, DNS 
    * User Application 
* Transport Layer
    * TCP 
    * UDP 
    * Connections between 
    * Ensure correct package 
* Internet Layer 
    * IPv4
    * IPv6
    * Addressing nodes 
* Link layer 
    * Ethernet 
    * PPP
    * Physical Network 
