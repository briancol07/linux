# Variables en bash 

Las variables son un componente esencial en cualquier lenguaje de programacion y bash no es la excepcion.
* Permiten 
  * Almacenar datos temporales 
  * Utilizarlos a lo largo del script 

## Que es una variable 

Una variable es un espacio en la memoria que se utiliza para almacenar un valor, como un numero, una cadena de texto, o incluso un resultado de un comando. Una vez asingado puede utilizar esa variable en diferentes partes. 

* No necesita declaracion de tipo 
  * se determina automaticamente 

## Declaracion y uso 

Para declarar una variable solo se necesita escribir el nombre de la variable, seguido del signo igual `nombre="pepe"`
Para utilizarla, necesitamos agregar un `$` antes del nombre de la variable 

```
echo "Hola $nombre"
```
### Asignar variables de comandos 

Puedes asignar a una variable el resultado de un comando. para esto se usa comillas invertidad `\`` o `$(comando)`

```
fecha=$(date)
echo "La fecha y hora actuales son: $fecha"
```

## Tipos de variables 

1. De texto ( cadneas | strings ) 
2. Numericas 
3. Especiales 
  * `$0` Nombre del script 
  * `$1`...`$n` argumentos Pasados al script 
  * `$#` : EL numero de argumentos
  * `$?`: El codigo de salida del ultimo comando ejecutado 

## Alcance de las variables 

* Locales  : ( Dentro del script o funcion ) en las que fueron definidas 
* Globales : Pueden ser accesibles desde otros scripts o procesos usar keyword `export`

## Modificacion variables 

Una ventaja de las variables es que pueden modificar su valor a lo largo del script por ejemploo 

```
#!/bin/bash
echo "¿Cuál es tu nombre?"
read nombre
echo "Hola, $nombre"
```

El comando read lo que hace es almacena el valor introducido por el usuario en nombre 

## Operadores 

1. Aritmeticos (matematicos ) ` + - * / `
2. Concatenacion de cadenas 
  * Se pueden concatenar utilizando variables sin necesidad de operadores adicionales `union="$str1 $str2"

## Buenas practicas 

1. Nombre descriptivos 
2. Comillas dobles 
  * Cadenas de texto 
3. Eliminacion de espacio antes y despues del `=`
4. Usar `export` con moderacion 

## Ejemplo 

```
#!/bin/bash
echo "Introduce el primer número:"
read numero1
echo "Introduce el segundo número:"
read numero2
suma=$((numero1 + numero2))
echo "La suma de $numero1 y $numero2 es: $suma"
```

## Tarea 

1. Crea un script que pida al usuario su nombre y su edad, almacene esta información en variables, y luego imprima un mensaje personalizado que utilice ambas variables.
2. Experimenta con operadores aritméticos creando un script que multiplique dos números introducidos por el usuario.
