# Web application technologies 1 

## Client side technologies 

* HTML(Hypertext markup language) :
  * Struture and define the content of web pages 
* CSS (cascading style sheets )
  * Define presentation and styling of web pages 
* JS ( Javascript ) 
  * Enables interactivity in the web applications 
* Cookies and local storage 

## Server-side technologies 

* Web server 
  * Receiving and responding to HTTP requests 
  * Host the web application's file ( and all ) 
  * Apache, nginx 
* Application server
  * Runs the business logic of the web application 
  * Processess user request , access databases and perform computation
* Database server 
  * Stores and manages the web application's data  ( mysql, postgresql, mssql , orcale)
* Scripting languages 
  * Ruby, java , python 
    * Handle server side processing  


