# Module 6 Strings 

## Overview 

1. Get string length
2. Concatenating String 
3. Finding substrings
4. Extracting substrings
5. Replacing substrings
6. Deleting Substrings 
7. Converting Uppercase and lowercase letters 


## 1-Get string length

You can do it with numbers or string or anything. 
bash is datatype blind 

```bash
# ${#string}
distro="Ubuntu"
echo ${#distro}
```

## 2-Concatenating String 
  
```bash 

str3=$str1$str2

```


## 3-Finding substrings

If the substring is not in the string it will return 0 otherwise will return the first index where the substring start. 

```bash 
str="Bash is Cool"
word="Cool"

expr index "$str" "$word"
```

## 4-Extracting substrings

You can omit the end so it will take from begining until the end of string 

```bash
# ${string:begin:end}
foss="Fedora is a free operating system"
echo ${foss:0:6}
echo ${foss:4}
``` 

## 5-Replacing substrings

If you want to make the change to the string you must reasing the values.

```bash 
# ${string/old/new}  
# string=${string/old/new}
```
## 6-Deleting Substrings 

With only one slash you will delete the first occurrence, instead you need to use double slash

```bash 
# ${string/substr}
# ${string//substr}
``` 

## 7-Converting Uppercase and lowercase letters 

With **^^** it will go upper , and with **,,** will go lower

1. With one it will turn the first letter to upper or lower
2. With two it will turn all the string 
3. if you put in brakets it will turn that letters

```bash
# ${string^^}
# ${string^}
# ${string^^[jn]}
```
