# Proteccion contra XSS 

## Indice 

* [## Que es Cross-Site scripting ? ](#xss)
* [## Como Funciona? ](#funcionamiento)
* [## Tipos ](#tipos)
* [## Ejemplo ](#ejemplo)
* [## Impacto ](#impacto)
* [## Como Proteger? ](#proteger)

## Que es Cross-Site scripting ? <a name="xss"></a>

Es un tipo de vulnerabilidad de seguridad que permite a un atacante inyectar scritps maliciosos en paginas web que luego son ejecutados por el navegador de los usuarios.

* Robar Cookies 
* Redirigir usuarios a sitios maliciosos 
* Realizar acciones en nombre del usuario sin su consentimiento 

## Como Funciona? <a name="funcionamiento"></a>

Cuando la aplicaicon web no valida o filtra adecuadamente la entrda del usuario antes de mostrarla en una pagina web. 

## Tipos <a name="tipos"></a>

* XSS Almacenado ( Stored XSS ) : 
  * El contenido se almacena en el servidor o bbdd , cada vez que un usuario accede se ejecuta el script malicioso
* XSS Reflejado ( Reflected XSS ) 
  * Es enviado como parte de una solicitud  y el servidor lo refleja directamente en la respuesta.
  * Cuando los datos no son sanitizados 
* XSS basado en DOM ( DOM-based XSS ) 
  * Manipula el dom del lado del cliente, sin que el servidor este directamente involucrado 

## Ejemplo <a name="ejemplo"></a>

`<script>alert('XSS');</script>` 

* [ Link](https://xss-game.appspot.com/)

## Impacto <a name="impacto"></a>

* Robo de cookies 
* Redireccion a sitios maliciosos 
* Modificacion del contenido del sitio 

## Como Proteger? <a name="proteger"></a>

* Validar (Datos sean los esperados)  y sanitizar (elimina caracteres peligrosos) la entrada del usuario 
* Escapar las salidas ( Output Escaping ) 
  * Asegurarse de que cualquier contenido que se muestre en la pagina no pueda ser interpretado como codigo.
* Uso de content security policy ( CSP )
  * Controlar recursos que el navegador puede cargar y ejecutar 
* Proteger las cookies con `HttpOnly` y `Secure` 
  * HttpOnly : impide que javascript acceda a las cookies, lo que protege contra la extracion de cookies mediante scripts maliciosos 
  * Secure asegura que las coookies solo se transmitan atraves de conexiones HTTPS, lo que protege contra interceptcion en redes no seguras 
* Evitar el uso de `eval()` y `innerHTML` 
* 
