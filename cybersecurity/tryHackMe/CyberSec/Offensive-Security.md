# Offensive Security 

This are involves attacking different applications and technologies to discover vulnerabilities.

* For you if:
  * You enjoy understanding how things work
  * You are analytical 
  * you like thiking out of the box

most common job is penetration tester: is an individual that is legally employed by an organisation to find vulnerabilities in their products. A penetration tester usually requiers a broad range of knowledge.

* Web application security 
* Network security
* Use of programming languages to write various scripts 

## Defensive Security 

Involves detecting and stopping these attacks.

* For you if :
  * You are analytical 
  * You enjoy problem solving 

One carrerar under this track is a Security Analyst: Monitor various system in the organisation and detect whether any of thse systems are being attacked.

### Detect Attacks using splunk 

while a security analyst deals with detecting attacks, an Incident Responder is usually brought in once an attack has already occurred. Their main responsibilities include understanding what actons an attacker has taken in the organisation and what the impact of their actios will be. Incident Responders also need to know how underlying technologies work and what potential attacks could be carried out agains a system. They then analyse trace evidence left by an attacker.

### Analyse memory to trace an attacker actions using volatility 

while this is a very specialist role, malware analysiss is quite common when detecting and responding to attacks. Malicious actors would use malicious piecees of software in any stage of thwir attack cycle from gaining access to a system to maintaining persistence. If you can understand what exaclty this malware is doing, you can prevent further abuse and also identify the malicious action.

* Introduction to malware Analysis 
* Researching and identifying malware
* Identifying strings in Malicious applications 

## Vulnerability Searching 

* To exploit especific SW
  * ExploitDB
  * NVD
  * CVE Mitre 

### NVD 

> Keeps track of CVEs (Common Vulnearabilities and Exposures) 

### ExploitDB 

> Contains exploits that can be downloaded anduse straight out of the box. 

Kali comes pre-installed with a tool called "searchsploit" .




