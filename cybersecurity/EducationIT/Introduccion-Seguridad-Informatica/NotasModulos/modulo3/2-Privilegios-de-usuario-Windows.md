# Privilegios de usuario en Windows 

En Windows, los privilegios de cuentas de usuarios se administran mediante grupos. Un grupo de usuarios es una coleccion de cunetas(de usuario) que tienen en comun los mismos derechos de seguridad. A veces, los grupos de usuarios tambine se denominan grupos de seguridad.

Una cuenta de usuario puede ser miembro de mas de un grupo. Los dos grupos de usuarios mas comunes son el grupo de usuarios estandar y el grupo de administradores, pero hay otros (invitados,operadores de backup).

Mi PC/ Computer -> Administrar/Manage -> Grupos/Groups -> Usuarios locales y grupos/ Local Users and Groups.

## Grupos por defecto

* Administradores 
  * Control total 
  * Se Agrega por default
* Usuarios 
  * Ejecutar apps 
  * usar impresoras locales 
  * No pueden compartir directorios ni crear impresoras locales 
