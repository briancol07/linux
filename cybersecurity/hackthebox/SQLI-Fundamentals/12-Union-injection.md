# Union injection 

## Detect number of columns 

### Using ORDER BY 

This first way fo detecting the number of columns is through the ORDER BY function. 
We have to inject a query that sorts the results by a column we specidfied, column1...columnN, until we get an error saying the column specifies does no exist 

For example we can start with `order by 1` sort by the first column and succeed, as the table must have at least one column. then we will do order by 2 ... . until we reach a number that returns an error or the page does not show any output , which means that this column number does not exist

### Using UNION

THe other method is to attempt a union injection with a different number of columns until we successfully get the results back. The first method always return the results until we hit an error, while this method always gives an error until we get a success. 

## Location of injection 

While a query may return multiple columns, the web application may only display some of them.

The benefit of using numbers as our junk data, as it makes it easy to track which columns are printed so we know at which column to place our query.

```
cn' UNION select 1,@@version,3,4-- -
```

