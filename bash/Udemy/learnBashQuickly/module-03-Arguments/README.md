# Module 3 Arguments


## Overview 

1. Passing one argument to a bash script
2. Passing multiple arguments to a bash scripts
3. Getting creative with arguments 
4. Special bash variables 

## 1 

Read the first param is with $1   

## 2 

to passing multiple arguments we use $number, and this number is the n param

script.sh arg1 arg2 arg3 

```bash 

n1=$(wc -l < $1)
n2=$(wc -l < $2)
n3=$(wc -l < $3)

echo "There are $n1 lines in $1"
echo "There are $n2 lines in $2"
echo "There are $n3 lines in $3"

```
## 4


Special Variable | Description
:---------------:|:------------:
$0 | The name of the bash script
$1..$n | The bash scripts arguments 
$$ | The process id of the current shell
$# | The total number of arguments passed to the script
$@ | The value of all arguments passed to the script
$? | The exit status of the las executed command 
$! | The process id of the last executed command 


