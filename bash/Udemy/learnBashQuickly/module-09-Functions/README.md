# Module 9 Functions 

## Overview 

1. Creating Functions 
2. Returning Functions Values
3. Passing Functions Arguments
4. Local and Global variables 
5. Recursive Functions 

## 1-Creating Functions 

### Method 1 
To call the function will be with the name 

```
functin_name (){
  Commands 
}
```

### Method 2 

```
function function_name(){
  Commands
}
```



## 2-Returning Functions Values

> Bash functions doen't return value only return the value of the last command . The exit status of the last command in the function.
if the command make an error it will return a value between 0 and 255.



## 3-Passing Functions Arguments

> In the function to refer to the arguments we will use the $n with n a integer.


## 4. Local and Global variables 

Global scope can be access in any part of the program.
Local scope not accessible from anywhere, only from that function/definitions etc 


## 5. Recursive Functions 

example factorial
