# Information disclosure 

Type of bug 

> When an application fail to properly protect sensitive information 


* Proper access control to files
* Protect source code 
* Hardcoded credentials

## Prevention 

* Audit public code 
* Remove data from services/server 
* Responses 

## Hunting 

> Recon techniques 

1. Attempt path traversal attack 
2. Search the way back machine 
3. Search paste dupmp sites -> pastebin
4. Reconstruct source code, github gists from exposed git directory 
  * Chequin wether a git folder is public 
  * Downloading files 
5. Find infomration in public files 


