# Hacker's Roadmap 2025

## Index

* [## Links](### Links)
* [## Phase 0 ](#0)
* [## Phase 1: Foundations ](#1)
  * [### Certifications ](#cert)
  * [### Learn ](#learn)
* [## Phase 2: Security ](#2)
  * [### Cert ](#Cert2)
  * [### Learn ](#learn2)
* [## Phase 3: Networking ](#networking)
  * [### Cert ](#cert3)
  * [### Learn ](#learn3)
  * [### Phase 3.5 ](#3.5)
* [## Phase 4: Hacking ](#4)
  * [### Certs ](#cert4)
  * [### Learn ](#learn4)
* [## Shortcuts ](#shortcuts)

## Links <a name="links"></a>

* [Video](https://www.youtube.com/watch?v=5xWnmUEi1Qw)

## Phase 0 <a name="0"></a>

* Apply to jobs in IT ( support / technical support )
  * For starting 
* Experience is key 
* Networking ( Social )

## Phase 1: Foundations <a name="1"></a>

### Certifications <a name="cert"></a>

Study the knowledge but the cert give you the proof.

* Comptia A+ 
* Resources 
  * [Professor Messer](https://www.youtube.com/@professormesser)
  * ITPro

### Learn <a name="learn"><a>

> Resume Building moments  

* Python 
* Linux 
* Homelab
  * Router 
  * DNS 
  * Other 

Around 2-3 Months

## Phase 2: Security <a name="2"></a>

> Keep applying to more jobs and networking 

### Cert <a name="Cert2"></a>

* Security + ( CompTia)
 * Knowledge 
 * Checkbox cert 
* Where 
  * [Professor Messer - SYO-501](https://www.youtube.com/watch?v=JU5zkddWits&list=PLG49S3nxzAnnVhoAaL4B6aMFDQ8_gdxAy)
  * ITPro

### Learn <a name="learn2"></a>

* Linux 
  * Over the wire 
* Python
  * Basics 
  * Proyects 
    * Network scanner 
* Homelab 
  * Automate 
  * Fun proyects 

Duration 2-3 months 

## Phase 3: Networking <a name="networking"></a>

> Keep Networking and applying jobs 

### Cert <a name="cert3"></a>

* CCNA (Cisco) (Harder)
  * [Jeremy's IT labs](https://www.youtube.com/watch?v=H8W9oMNSuwo&list=PLxbwE86jKRgMpuZuLBivzlM8s2Dk5lXBQ)
* CompTia Network + (Neutral)
  * Professor messer 
  * ITPro

### Learn <a name="learn3"></a>

* More Python
* Linux 
* Network automation 
* Homelab 
  * Replace home network with other 
  * Monitoring network 

2 to 4 months 

### Phase 3.5 <a name="3.5"></a>

> Get that job 
Put all the effort in doing this 

## Phase 4: Hacking <a name="4"></a>

> Offensive security 

### Certs <a name="cert4"></a>

* Ejpt 
* Pentest+ 
* CPTS Hackthebox 
* OSCP (Gold standard)

### Learn <a name="learn4"></a>

* CTF 
  * PICO

## Shortcuts <a name="shortcuts"></a>

> Good foundation! 

Not need to follow each one 
