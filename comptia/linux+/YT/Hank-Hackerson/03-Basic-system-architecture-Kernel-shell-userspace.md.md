# Basic system architecture Kernel shell userspace


## Kernel <a name="kernel"></a>

* The linux kernel is the core component of a linux-based OS
* It serves a bridge between the hardware and software layers, managing system resources and facilitating communication between the two .
* Key roles (Resource management) 
  * Memory Management ; Allocates ad deallocates memory to processes as needed 
    * Memory allocation: Dividing physical memory into virtual memory segments 
    * Page fault Handling: when a process tries to access a memory page that is not currently in physical memory, and loads missing page from disk 
    * Swapping : If physical memory is scarce, the kernel can swap inactive pages to disk to free up memory for active proscesses 
  * Process Management: Creates, schedules and terminates processes 
    * Process creation and termination (creates , aassings them unique identifiesr and terminates them when they are no needed)
    * Process scheduling 
    * Context switching 
    * Inter-process communciation (IPC) : Facilitates communication between processes, allowing them to share information and synchronize their activities 
  * Device management : Controls access to hardware devices like disk drives, network cards and printers 
    * Device driver management : loads and unloads device drivers which are SO components that interact with specific HW devices 
    * Input/output operation 
    * Interrupt handling 
  * File system management: Manages files systems and provides access to file and directories 
  * Hardware abstraction layer (HAL)
    * Provides a consistent interface for SW to interact with HW, hiding the complexities of different HW architectures 
    * This abstraction allows software to run on various hardware platforms without requiring significant modifications 
  * System calls 
    * Provides a set of functions that allow user-level programs to interact with the kernel and request system services 
    * Create files, reading and writing data and making network connections 
  * Security 
    * User authentication
    * Access control 
      * can be access control lists 
    * Network security
      * Firewall
      * packet filtering 

## Shell <a name="shell"></a>


