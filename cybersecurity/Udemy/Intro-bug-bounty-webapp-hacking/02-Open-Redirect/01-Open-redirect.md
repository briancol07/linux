# Open Redirect 

> Unvalidated redirect & forwards 

Open redirects happen when the web application takes an untrusted input and redirects a user from the web application to untrusted site or resources that will be used further for malicious pruposes 

Sometimes the application may have some security measures in place where the developers define a list of either trusted or untrusted resources. In some cases, you may be able to bypass it, if you fully understand how it works 

## Impact 

Usually low , unless you are using it to escalate othervulnerabilities 


## Example 

Filtering and bypasses 

```
(allowed)
https://example.com/login/?nextPage=https://google.com
(not allowed)
https://example.com/login/?nextPage=https://evilsite.com
(allowed)
https://example.com/login/?nextPage=https://eviIsite.com/?
google.com
```


