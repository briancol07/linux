#!/bin/bash

while [ 1 ]; do
  echo "Menú de Opciones:"
  echo "1. Mostrar la fecha y hora"
  echo "2. Mostrar el espacio en disco"
  echo "3. Mostrar los usuarios conectados"
  echo "4. Salir"
  echo "Elige una opción:"
  read opcion
  
  case $opcion in
      1)
          echo "La fecha y hora actuales son:"
          date
          ;;
      2)
          echo "El espacio en disco es:"
          df -h
          ;;
      3)
          echo "Usuarios conectados actualmente:"
          who
          ;;
      4)
          echo "Saliendo del script..."
          exit 0
          ;;
      *)
          echo "Opción no válida. Por favor, elige una opción entre 1 y 4."
          ;;
  esac
done
