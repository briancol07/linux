# Intro to Command line

The command line interface provides the following advantages 

* No GUI overhead is incurred 
* Virtually any and every task can be accomplished while sitting at the command line 
* You can implement scritps for often-used tasks and series of procedures 
* You can initiate graphical applications directly from the command line. 

## Using a text terminal <a name="text"></a>

A terminal emulator program emulates a standalone terminal within a window on the desktop.

On GNOME desktop enviroments, the gnome-terminal application is used to emulate a text-mode terminal.

* Other terminal
  * xterm
  * Konsole 
  * Terminator 

## Some basic Utilities <a name="utilities "></a>

* `cat` : used to type out a file 
* `head` : Used to show the first few lines of a file 
* `tail` : Used to show the last few lines of a file 
* `man` : used to view documentation 

The use of the pipe symbol (`|`) used to have one program take as input the output of another 

## The command line <a name="command"></a>

* Basic elements 
  * Command 
  * Options 
  * Arguments 

The command is the name of the program or script you are executing. It may be followed by one or more options. However plenty of commands have no options, no arguments, or neither.

## Sudo <a name="sudo"></a>

Provide the user with administrative (admin) privileges when required. Allows users to run programs using the security privileges of another user, generally root (SuperUser) 

On some distributions you need to configure sudo. 

### Steps for setting up and running sudo <a name="setting"></a>

1. You will need to make modifications as the administrative or superuser, root. While sudo will beome the preferred method of doing this, we do not have it set up yet . At the command line prompt, type `su` . You will then be prompted for the root password. 
2. You need to create a configuration file to enable your user accoutn to use sudo. `/etc/sudoers.d` directory with the name of the file athe same as your username ` echo "student ALL=(ALL) ALL" > /etc/sudoers.d/student` In this case the username is student .
3. Finally, some linux distributions will complain if you do not also change permissions on the file by doing `chmod 440 /etc/sudoers.d/student` 

This will ask for password sometimes but you can make it to not require password (Though very insecure) 

## Switching between the GUI and the Command line <a name="switch "></a>

Linux allows you to drop the graphical interface (temporarily or permanently) or to start it up after the system has been running. 
Most linux give an option during installation if you want to choose between desktop or workstation or server

## Virtual Terminals <a name="vt"></a>

Are console sessions that use the entire display and keyboard ouside of a graphical environment. Such terminals are considered "virtual" because, although there can be multiple active terminals, only oneterminal remains visible at a time.

 VT is not the same as a command line terminal window, you can have many of htose visible simultaneously on a graphical desktop.

To switch between VTs press `ctrl-alt-function key` for the VT. 

## Turning off the graphical desktop <a name="off"></a>

You can start and sto pthe graphical desktop in various ways. Example with the systemctl utility. 

```bash
# STOP 
sudo systemctl stop gdm (or sudo telinit 3)
# Restart
sudo systemctl start gdm (or sudo telint 5)
```

## Basic Operations <a name="basic"></a>

* Log in 
* Log out 
* restart
* Shutdown 
* Locate applications 
* Access directories 
* Identify absolute and relative paths 
* Explore filesystem

### Log in and out <a name="log"></a>

An available text terminal will prompt for a username (with the string `login:`) and password. After this you can also connect and log into remote systems by using SSH `ssh studen@remote-server.com`

### Rebooting and shutting down <a name="rebot"></a>

The preferred method is used the shutdown command. This sends a warningmessage, and the prevents furthes users fro mlogging in. The init process will then control shutting down or rebooting the system.

The halt and poweroff commands issue shutdown -h to halt the system, reboot issues shutdown -r and causes the machine to reboot instead of just shutting down. Both rebooting and shutting down from the command line requires superuser (root) access.
When administering a multi-user system, you have the option of notifying all users prior to shutdown 

```
sudo shutdown -h 10:00 "shutting down for scheduled maintenance"
```
### Locating Applications <a name="locate"></a>

* Programs and scripts 
  * live in `/bin, /usr/bin, /sbin, /usr/sbin`
  * or `/opt`
  * or `/usr/local/bin`

One way to locate programs is to employ the whcih utility `which diff`. If which does not find the program, whereis is a good alternative because it looks for packages in a broader range ` whereis diff` 

### Accessing Directories <a name="direct"></a>

By default when you open a terminal shoudl be your home directory `$HOME` (To see that location `echo $HOME`)

Some commands 

Command | Result 
:------:|:------:
pwd | Display the present working directory 
cd ~ or cd | Change to your home directory shortcut name is ~ 
cd .. | Change to parent directory 
cd - | Change to previous working directory 

### Absolute and relative Paths <a name="absolu-rela"></a>

* An absolute pathname begins with the root (`/`) directory and follows the tree, branch by branch, until reaches the desire directory or file 
* A relative pathname starts from the present working directory. Never start with `/`

Multiple slashes between directorias nad file are allowed, but one slash beween elements in the pathname is ignore by the system `////usr//bin => /usr/bin`

Most of the time, it is most conveninet to use relative paths, whichs require less typing usually, you take advantage of the shortcuts provided by : 

* `.` Present directory 
* `..` Parent directory 
* `~` home directory 

### Exploring the FS (Filesystem) <a name="fs"></a>

The tree command is a good way to get a bird's-eye view of the filesystem tree (`tree -d` to just view directories)

Command | Result 
:------:|:------:
cd / | Changes your current directory to the root(`/`) directory or path you supply
ls | List the contents of the present workign directory 
ls -a | List all files, including hidden files and directories (dotfiles: start with `.`)
tree | Dispalys a tree view of the filesystem 

#### Hard links <a name="hard"></a>

The ln utility is used to create hard links and with the -s option soft links, also known as symbolic links or symlinks 

`ln file1 file2` 
`ls -li file1 file2` 

The -i option to ls prints out in the first column the inode number, which is a unique quantity for each file object. This field is the same for both and these files. It is only one file, but it has more than one name associeated with it. As is indicated by the 2 that appears in the ls output. Thus, there was already another object linked to file1 before the command was executed 

Hard links are very useful and they save space, but you have to be careful with their use, sometimes in subtle ways . 
If your remove either file1 or file2 in the exmaple, the inode object (and the remaining file name) will remain, which might be undesirable, as it may lead to subtle errors later if you vreate a file of that name

> Depend on the editor that you will use, they can o cannot retain the link by default, but is is possible that modifying one of the names may break the link and result in the creation of two objects . 

#### Soft links <a name="soft"></a>

Are created with the -s option 

```
ln -s file1 file3
ls -li file1 file3
```

File 3 no longer appears to be a regular file, and it clearly points to file1 and has a different inode number 

Symbolic links take no extra space on the FS (unles their names are very long). They are extremely convenient, as they can easily be modifies to point to different places. An easy way to create a shortcut from your home directory to long pathnames is to create a symbolic link

Unlinke hard links, soft links can point to objects even on different fs partitions, and /or disk and other media. Which may or may not be currently available or even exit. In the case where the link does not point to a currently available or existing object , you obtain a dangling link 

#### Navigating through directory history <a name="history"></a>

For remembering more than just the last directory visited, used `pushd` to change directory instead of cd, this pushes your starign directory onto a list. Using popd will then send you back to those directories, walking in reverse order 


## Working with files <a name="files"></a>

Command | Result 
:------:|:------:
cat | used for viewing files that are not very long, it does not provide any scroll-back
tac | Used to look at a file backwards, starting with the last line 
less | Used to view larger files, provides scroll-back capabilities
tail | Used to print last 10 lines of a file by default. you can change it with -n 15 or just -15 to see 15 lines 
head | The opposite of tails 

### Touch <a name="touch"></a>

Is often used to set or update the access, change, and modify times of files. By default, it resets a file's timestamp to match the current time 

```
touch <filename>
touch -t 12091600 myfile
```
Tyhe option -t allows you to set the data nad timestamp of the file to a specific value

### mkdir - rmdir <a name="mkdir"></a>

* mkdir : Create a directory 
* rmdir : remove a directory 
  * On empty directories 

The directory must be empty or the commnand will fail. To remove a directory and all of its contents you have todo a rm -rf 
### Moving, renaming or removing a file <a name="mov"></a>

* That command is `mv` 
  * Simply rename a file 
  * Move a file to another location while possibly changing its name at the same time 

## Modifying the command line prompt <a name="prompt"></a>

The PS1 variable is the character strign that is displayed as the prompt on the command line. Most distributions set PS1 to a known default values, which is suitable in most cases. However, users may want custom information to show on the command line. 
This could prove useful if you are working in multiple roles and want to be always reminded of who you are and what machine you are on. 

Example you can change to `\u@\h \$` this will show the user and the host.



