# Using Comments 

## Comments 

* `--`
* `#`
* Inline comment `/**/`


> Sometimes the `--` need a spaca and is count as `-- +`

## Auth bypass with comments 

```
SELECT * FROM logins WHERE username='admin'-- ' AND password = 'something';
```

Sometimes you need a closing `)` to modify the query , like this `admin')--` 

```
SELECT * FROM logins WHERE username='admin'-- ' AND password = 'something';
```
