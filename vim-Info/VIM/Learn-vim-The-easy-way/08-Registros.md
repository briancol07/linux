# Registros 

## Diez tipos de registros 

1. El registro sin nombre ("")
2. Los registros enumerados ("0-9)
3. El registro de pequenias eliminaciones ("-)
4. Los registros nominales ("a-z)
5. EL registro de solo lectura (":,".,y "%)
6. El registro de archivo alternativo ("#)
7. El registro de expresiones ("=)
8. Los registros de seleccion (" * y " +)
9. El registro de agujero negro (" _ )
10. El registro del ultimo patro de busqueda (" /) 

## Operadores del registro 

Para utilizar los registros, necesitas primero almacenarlos con operadores. Estos son algunos operadores que almacenan valores en los registros.
Existen s o x pero los siguientes son mas comnunes.

| Tecla     | Uso       |
|:---------:|:---------:|
    y       | yank (copiar)|
    c       | Borrar texto e iniciar el modo insertar |
    d       | Borrar texto|
    p       | Pega el texto despues del cursor 
    P       | Pega el texto antes del cursor 

La sintaxis general para obtener el contenido desde un registro en especifico es "a, donde a es el simbolo del registro. 

## Llamar a los registros desde el modo insertar  

Todo lo que aprendas en este capitulo puede tambien ser ejecutado en el modo insertar. 
Para obtener el texto del registro a, normalmente debes escribir "ap. Pero si estas en el modo insertar, ejecuta: 

```
Ctrl-R a
```

## El registro sin nombre 

Para obetner texto desde el registro sin nombre, escribe ""p. Este almacena el utlimo texto que copiaste, modificaste o borraste. Si haces otra copia, modificando o borrado. Vim automaticamente reeplazara el texto . 

## Los registros numerados

Los registros numerados automaticamente se llenan a si mismos en orden ascendente. Hay 2 registros nmerados diferentes

* 0 : registro copia
* 1-9 : Registro numerados 

### Registro copia 

Si copias una linea entera de texto (yy), vim realmente guasa ese texto en 2 registros 

1. El registro sin nombre (p)
2. El registro de copia ("0p)

Cuando copias un texto diferente, vim remplaza ambos registros, el de copiay el de sin nombre. Cualquier otra operacion (como eliminar) no sera almacenada en el registro 0. Esto puede ser usado para tu provecho por que a menos que no hagas otra copia, el texto copiado permanecera siempre alli, no importa cuantos cambios y borrados hagas

```
ctrl-r 0
```

### Registros numerados 

Cuando cambias o borrasun texto de almenos una linea de largo, este texto se almacenara en un registro numerado de 1 al 9 ordenados por el mas reciente 

```
"1
"2
"3 ... "9
```

Pequenios borrados como un borrado de palabras (dw) o un cabmio de palabra (cw) no se almacena en registros numeraros. Estos se alamacenan en un pqeuqenio registro de borrado ("-).

## El pequenio registro de borrado 

Los cambios o borrados menores a una linea no se almacenana en los registros numerados de 0-9, pero si en el pequenio regitro de borrados ("-).

## Resitros nominales 

Son los mas versatiles de vim. Estos pueden alamacenar textos copiados, modificados y borrados dentro de los registros a-z. 
A diferencia de los anteriores, este no almacena automaticamente el valor sino que tienes que decirle explicitamente a vim el nombre del registro.

Para copia una palabra dentro del registro a, puedes hacerlo con "ayiw.

Para obeterne el texto del registro a, ejecuta "ap. Puedes usar todos los veintiseis caractereds del alfabeto para almacenar textos.

Si quieres aumentar el tamanio del texto nominal sin reemplazarlo puede usat "Ayiw.

## Registros de solo lectura 

| Tecla     | Uso       |
|:---------:|:---------:|
   . | Almancena el ultimo texto insertado 
   : | Almacena el ultimo que fue ejecutado en la lineea de comandos
   % | Almacena el nombre del archivo actual 

Forma de usarlos "%p 

## Registro de archivo alternativo 

En vim, # usualmente representa el archivo alternativo. Un archivo alternativo es el ultimo archivo que abriste. Para insertar el nombre del archivo alternativo "#p.

## Registro de expresiones

Vim tiene un registro de expresiones, "= para evaluar expresiones 

```
"=1+1<Enter>p
desde modo escritura
Ctrl-R =1+1
si quieres aniadirlo a un registro
"=@a
```

## Registro de seleccion

Se pueden utilizar para aceder a textos copiados desde programas externos

* \*"
* "+

Para pegar texto en programas externos 

Para que vim peque el texto ( =\*p o =+p o "\*p o "+p) es un poco incomodo . Para pegar solo con p se podria

```
set clipboard=unnamed
```

## El registro de agujero Negro 

Cada veza que eliminas o cambias un texto, es almacenado en un registro de vim automaticamente. 
Puede utilizar el registro de agujero negro ("\_). Para eliminar una linea y hacer que vim no almacene esa linea eliminada en ningun registro utiliza 
"\_dd

El registro del agujero negro es como el /dev/null de los registros 

## El registro del patron de busqueda 

Para pega tu ultima busqueda con / o ? puedes utilizar el registro del ultimo patron de busqueda ("/).
Para pegar el termino de la ultima buesqueda utiliza "/p. 

## Ver los registros

Para ver el contenido de tus registros utiliza el comando 

```
:register

Para ver el registro "a "1 "-
:register a 1-
```

## Ejecutando un registro 


s registros nominales no son solo para almacenar textos, tambien pueden ejecutar macros mediante @. 

## Limpiando el contenido de un registro

No hay necesidad por que el proximo texto que se almacene bajo el mismo nombre de un registro lo sobreescribira. 
Sin embargo puedes limpiar el contenido rapidamente de cualquier registro grabando la macro vacia (qaq) para el registro a.

Otra alternativa es ejecutar el comando

```
:call setgreg('a', 'hola registro a') 
```

donde a hace referencia al registro a y 'hola registro a' es el nuevo texto que sobreescribira el anterior. 

Otra forma seria: 

``` 
:let @a = ''
```

## Pegando el contenido de un registro 

```
:put a
``` 

Vim mostrara el contenido a bajo la liea actual. Esto se parece mucho a "ap , con la diferencia que este comando pega el contenido despues del cursor y el comando :put pega el contenido del registro en una nueva linea 

```
:put _
```` 
Esto seria insertar una linea en blanco 

Puede combinar esto con el comando global para insertar multiples lineas en blanco donde lo necesites 

```
:g/fin/put _
```

## Resumen

Registos mas importantes 

1. Registro sin nombre
2. Registros nominales 
3. Registros numerados 
