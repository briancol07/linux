# XML External Entity  ( XXE ) 

-> SSRF
-> DoS

> XML: Extensible markup language 

USE: For storin and transporting data 

HTML use it in SAML 

> SAML: Security Assertion Markup language 

## DTD 

XML Contains DTD 

> Document type definition 

* External: loaded from URL 
* Internal : DOCTYPE

## Prevention 

* Liminting capabilities XML parser 
  * Disabling it 
* Create allow list 
* Disable outbound rules 
  * Network traffic 

## Find one 

1. Found XML data entry point 
2. Test for classic XXE
3. Test for blind XXE
4. Embed XXE payloads in different file types
  * You can embebed XML in other files (word) 
5. Test for x include attack 

> You can see LOL payload XML --> billions laugh attack 









