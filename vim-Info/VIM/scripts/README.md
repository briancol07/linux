# Scripts 

## Links 

* [yt-popup](https://www.youtube.com/watch?v=1AOhfnokacE&t=187s)

## IDEAS 

### PopUp

> Cosas en las que podrias usar popup

- [ ] Mostrar n lineas de un archivo ( puedo hacer que se mueva por todo ) 
- [ ] Mostrar `:help` de un comando de vim/bash
- [ ] Mostrar Backlog del dia 
- [ ] Mostrar el archivo de ideas 
- [ ] Mostrar ejecucion de comando o solo comandos por popup

#### Chatgpt 

- [ ] Notificaciones Rápidas: Mostrar mensajes de notificación cuando ciertos eventos ocurren, como la finalización de un proceso largo o la detección de cambios en archivos.
- [ ] Ayuda Contextual: Mostrar documentación o ejemplos rápidos relacionados con el código o comandos que estés escribiendo.
- [ ] Lista de Tareas: Crear una pequeña lista de tareas pendientes en una ventana flotante que puedas revisar sin cambiar de buffer.
- [ ] Cheat Sheets: Tener accesos directos o guías rápidas de comandos en una ventana flotante para consultas rápidas mientras trabajas.
- [ ] Vista Previa de Archivos: Mostrar una vista previa rápida del contenido de un archivo sin tener que abrirlo completamente en un buffer.
- [ ] Resultados de Búsqueda: Mostrar los resultados de búsquedas en un popup para revisarlos sin interrumpir el flujo de trabajo.
- [ ] Terminales Temporales: Ejecutar comandos rápidos y mostrar su salida en una ventana emergente sin necesidad de abrir un buffer de terminal completo.
- [ ] Información de Git: Mostrar el estado actual del repositorio Git, como cambios pendientes o el log de commits recientes.
- [ ] Errores de Sintaxis: Mostrar errores de sintaxis de forma inmediata cuando estés programando, en lugar de tener que navegar a un buffer diferente.
- [ ] Vista Rápida de Variables: Mostrar el valor de variables mientras estás depurando código o escribiendo scripts.
- [ ] Resultados de Consultas SQL: Mostrar los resultados de una consulta SQL directamente en un popup para facilitar la revisión.
- [ ] Mini-Calendario: Mostrar un calendario pequeño en un popup para verificar fechas sin interrumpir tu flujo de trabajo.
- [ ] Historial de Comandos: Mostrar una lista de los últimos comandos ejecutados en la terminal para reutilizarlos fácilmente.
- [ ] Notas Rápidas: Tomar notas temporales o rápidas en un popup sin necesidad de abrir un archivo de notas separado.
- [ ] Snippets de Código: Mostrar una lista de snippets o ejemplos de código reutilizables que puedes copiar fácilmente.
- [ ] Vista Previa de Markdown: Mostrar una vista previa del formato final de un archivo Markdown en un popup mientras lo editas.
- [ ] Resúmenes de Archivos Grandes: Mostrar un resumen o estadísticas de archivos grandes, como el número de líneas, palabras, etc.
- [ ] Ayuda de Atajos de Teclado: Mostrar una lista de los atajos de teclado personalizados o útiles para el contexto actual.
- [ ] Estado del Sistema: Mostrar información sobre el uso de la CPU, la memoria o la red del sistema en un popup mientras trabajas.
- [ ] Recordatorio de Proyectos: Tener una pequeña ventana flotante con recordatorios o metas de los proyectos actuales, visible en todo momento mientras trabajas.


