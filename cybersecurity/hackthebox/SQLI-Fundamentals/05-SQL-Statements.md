# SQL Statements 

## Insert Statement

To add new records toa given table 

```
INSERT INTO table_name VALUES (column1_value, column2_value, column3_value, ...);
```
We can skip filling columns with default values . This can be don by specifying the column names to insert values into a table selectively

## SELECT Statement

TO let us see how to retreice data 

```
SELECT * FROM table_name;
```

* \* Wild card for all
* From is used to denote the table to select from it is posible to vieew dat 

## Drip statement 

> Remove tables and databases from server 

```
 DROP TABLE logins;
```
## Alter Statement 

> To change the name of any table and any of its fields or to delete or add new columns to an existing table 

```
ALTER TABLE logins ADD newColumn INT;
```

* Rename column
* Modify 
* Drop

## Update Statement 

> Update specific records within a table, based on certain conditions 

```
UPDATE table_name SET column1=newvalue1, column2=newvalue2, ... WHERE <condition>;
```


