# Linux System Administration 

## Topics 

* Linux Administration commands 
* Adding a user 
* Changing password for user 
* Deleting a user 
* Creating a group 
* Adding a user in a group 
* Removing a user from a group 
* Deleting a group 

## Overview 

* [1-Types-User-Linux.md](./1-Types-User-Linux.md)
* [2-Basic-Administration-command.md](./2-Basic-Administration-command.md)
* [3-Adding-Deleting-user.md](./3-Adding-Deleting-user.md)
* [4-Adding-Deleting-group.md](./4-Adding-Deleting-group.md)
