# Security Session 1 

* [Article](https://medium.com/@mon.cybersec/security-session-1-9457a0f5bd27)

## index 

* [## Who we are ](#who-we-are)
* [## Sombrero ](#sombreros)
  * [### Fundamentos ](#fundamentos)
* [## Triada CIA ](#CIA)
* [## Agregando otras llaves ](#otras-llaves)
* [## Seguridad general y web ](#web)
* [## Hacking mind set ](#mind-set)
* [## Penetration Testing VS bug bounty hunting](### Penetration Testing VS bug bounty hunting)
* [## Metodologias ](#metodologias)
* [## Buenas Practicas ](#buenas-practicas)

## Who we are <a name="who-we-are"></a>

Los dominios de la ciberseguridad 

* Carreras 
  * [article](https://medium.com/@mon.cybersec/los-roles-en-ciberseguridad-6d27ee277203)
  * [Reference](https://rafeeqrehman.com/wp-content/uploads/2024/03/CISO_MindMap_2024-2.png)

## Sombrero <a name="sombreros"></a>

* Tipos 
  * White hat 
  * Grey hat 
  * Black hat 

White hat hackers son los que tienen permiso para buscar fallas de seguridad en los sistemas y reportarlas con el fin de que se resuelvan ( Empleados o contratados ).

Black hat son los que tienen malas intenciones. Buscan romper la seguridad de los sitemas para robar informacion, modificarla o hacer que deje de funcionar ( ciberdelincuentes ).

En el medio los grey hat que actuan sin permiso pero su intencion es avisar a la empresa sobre las fallas de seguridad que encuentran 

### Fundamentos <a name="fundamentos"></a>

* Los profesionales en seguridad informatica son los responsables de proteger: 
  * Confidencialidad 
  * Integridad 
  * Disponiblidad 
  * Sistemas informaticos que utilizan las empresas 
* Fundamentos
  * Redes 
  * web
  * linux 
  * windos 

## Triada CIA <a name="CIA"></a>

* Confidencialidad:
  * Garantiza que solo las personas o destinatarios previstos peudan acceder a ciertos datos 
  * Garantiza que las personas no autorizadas no pueden acceder a informacion sensible 
  * La llave secreta 
* Integridad:
  * Garantiza que no haya modificaciones no autorizadas en la informacion o los sistemas, ya sea intencionalmente o no 
  * La llave que no se rompe 
* Disponibilidad:
  * Tiene como objetivo garantizar que el sistema o servicio este disponible cuando se necesite 
  * La llave siempre funciona 

## Agregando otras llaves <a name="otras-llaves"></a>

* Autenticidad 
  * llave original
  * Unica y verdadera. 
  * Es como un sello de un documento importante, te asegura que es real 
* No Repudio 
  * La llave que deja huella
  * Crea una firma cada vez que se usa.
  * Asi nadie peude decir que no fue el quien abrio el candado

## Seguridad general y web <a name="web"></a>

* Proteccion de datos sensibles ( data breach )
* Proteger la privacidad de los usuario reputacion y confianza
* Cumplimiento de regulaciones y leyes 
* Garantizar la disponibilidad de los servicios
* Evitar perdidas economicas / entre otros 

## Hacking mind set <a name="mind-set"></a>

Es ponernos en ese lugar que nos va a aportar creatividad, formas de resolver ciertos problemas y dar resultados increibles 

* Curiosidad constante
* Creatividad 
* Pensamiento Critico 
* Etica profesional
* Documentamos 

## Penetration Testing VS bug bounty hunting 

* Alcance 
  * BB
    * Se enfoca en encontrar vulnerabilidades especificas que tengan un impacto demostrable 
    * El alcance suele ser mas amplio
    * Se paga por vulnerabilidad encontrada 
  * Pentesting 
    * Evalua la seguridad general del sistema
    * Alcance definido y limitado por contrato
    * Pago fijo por el servicio completo 
* Metodologia Trabajo
  * BB 
    * Trabajo independiente y competitivo
    * Sin limite de tiempo especifico
    * Mayor libertad en las tecnicas utilizadas 
  * Pentesting 
    * Trabajo estructurado y metodologico
    * Tiempo definido por contrato
    * Metodologias y procedimientos establecidos 
* Reportes y comunicacion
  * BB
    * Reportes individuales por vulnerabilidad 
    * Comunicacion principalmente a traves de plataformas de bb
    * Enfoque en la prueba de concepto ( PoC )
  * Pentesting 
    * Reporte completo y detallado de toda la evaluacion
    * Comunicacion directa con el cliente 
    * Incluye recomendaciones y plan de remediacion 

## Metodologias <a name="metodologias"></a>

* OWASP ( Open Web Application Security Project ) 
* CWE ( Common Weakness enumeration ) 
* SANS TOP 25 y Mitre 
* ISO 27001 
* NIST cybersecurity framework
* CIS Controls 

## Buenas Practicas <a name="buenas-practicas"></a>

1. Parches y actualizaciones de seguridad 
2. Principio de minimo privilegio
3. Codigo seguro ( Validacion y sanitizacion de entrada )
4. Almacenamiento seguro de datos ( encripttar datos en reposo )
5. Autneticacion multifactor ( MFA )
6. Restistro y monitoreo
7. Capacitacion de usuarios 
