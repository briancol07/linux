# Network Technologies in the home 

## LAN Wireless Frequencies 

The wireless technologies most frequently used in home network are in the unlicensed 2.4 to 5 GHz frequency ranges 

Bluetooth is a technology that makes use of 2.4GHz band, low-speed, short range communication, can communicate with many devices at the same time. 

## Wired network Technologies 

The most commnoly implemented wired protocol is the ethernet protocol. Ethernet uses a suite of protocols that allow networkin devices to communicate over a wired LAN connection. 

Directly connected devices use an Ethernet patch cable, usually unshielded twisted pair. these cables can be purchased with the RJ-45 connectors already installed. 

* Categories 
  * 5e 
    * Is the most common wiring used in LAN.
    * The cable is made up of 4 pairs of wires that are twisted to reduce electrical interference 
  * Coaxial Cable 
    * Has an inner wire surrounded by a tubular insulating layer, that is then surrounded by a tubular conducting shield 
    * Most coax cables also have an external insulating sheath or jacket 
  * Fiber-Optic Cable
    * Glass or plastic with a diameter about the same as human hair and it can carry digital information at very high speeds over long distances 
    * Very High bandwidth 

