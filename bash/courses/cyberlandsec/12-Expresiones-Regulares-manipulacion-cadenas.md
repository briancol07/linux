# Expresiones Regulares 

## Que es una expresion regular ? 

Una expresion regular (regex) es un patron que se utiliza para buscar, coincidir o manipular texto. Las expresiones regulares permiten definir busquedas flexibles, como encontrar cadenas que sigan un formato especifico o reemplazar texto en grandes conjuntos de datos.

## Herramientas para expresiones Regulares 

* No tiene soporte nativo 
* Herramientas 
  * grep 
  * sed 
  * awk

## Grep 

Se utiliza par buscar patrones en archivos o en la salida de comandos 

```
grep [opciones] "patrón" archivo
grep "error" /var/log/syslog
```
### Uso de las expresiones regulares 

* `.` Coincide con cualquier caracter 
* `*` Coincide con cero o mas repeticiones del caracter anterior 
* `^` Coincide con el inicio de una linea
* `$` Coincide con el final de una linea 

```
grep "^Warning" /var/log/syslog
```
``` bash 
# Buscar que terminen con un patron 
grep "[0-9]$" archivo.txt
```

## Sed 

Permte buscar y reemplazar y editar texto en archivos.

```
sed 's/patrón/reemplazo/' archivo
sed 's/foo/bar/g' archivo.txt
```
### Ejemplo Reemplazar solo lineas especificas 

```
sed '/error/s/foo/bar/' archivo.txt
```

## AWK 

Analizar y procesar archivos de texto estructurados.

```
awk 'patrón {acción}' archivo
awk '{print $1, $3}' archivo.txt
```
En el segundo ejemplo imprime primera y tercera columna asumiendo que estan separados por espacios 

### Ejemplo : Filtrar por un patron 

```
awk '/error/ {print $1}' archivo.txt
```

## Manipulacion de cadenas en bash 

### Obtener longitud de una cadena 

```
cadena="Hola Mundo"
echo ${#cadena}
```

### Extraccion subcadenas 

```
cadena="Hola Mundo"
echo ${cadena:5:3}
```
### Reemplazo de cadenas 

```
cadena="Hola Mundo"
nueva_cadena=${cadena/Mundo/Amigos}
echo $nueva_cadena
```

## Ejemplo : Analiss de logs 

1. Buscar todas las lineas que contienen errores 
2. Reemplazar la palabra `Error` por alerta
3. Mostrar la hora de cada error 

```
#!/bin/bash

# Archivo de logs
log_file="/var/log/syslog"

# Buscar líneas que contienen la palabra "error"
echo "Buscando líneas con errores:"
grep -i "error" $log_file

# Reemplazar "ERROR" por "ALERTA"
echo "Reemplazando 'ERROR' por 'ALERTA' en las líneas encontradas:"
grep -i "error" $log_file | sed 's/ERROR/ALERTA/g'

# Mostrar la hora de cada error
echo "Mostrando la hora de cada error:"
grep -i "error" $log_file | awk '{print $1, $2}'
```

## Tarea 

1. Crea un scritp que busque todas las direcciones IP en un archivo de texto utilizando una expresion regular 
2. Modifica el script para que reemplace todas las direcciones ip que comiencen con `192.168` por `Redacted`






