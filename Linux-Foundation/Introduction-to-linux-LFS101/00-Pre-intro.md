# Linux 

## Index 

* [## Distros ](#distros)
* [## Redhat (RHEL) Family ](#redhat)
  * [### Key Factors ](#key-factors)
* [## SUSE Family ](#suse)
  * [### Key factors ](#key)
* [## Debian Family ](#debian)
  * [### Key facts ](#facts)

## Distros <a name="distros"></a>

![distros](./img/distros.png)

## Redhat (RHEL) Family <a name="redhat"></a>

* CentOS
* CentOS stream 
* Fedora 
* Oracle linux 

We use CentOS are free to the end user and there is a longer release cycle than fedora.
The difference between the stream and normal CentOS is that Stream get updates before REHL 

### Key Factors <a name="key-factors"></a>

* Fedora serves as an upstream testing platform for RHEL 
* CentOS is a close clone.
* It support multiple hardware platforms 
* It use dnf, the RPM-based package manager.
* Is widely used by enterprises which host their own systems 

## SUSE Family <a name="suse"></a>

The relationship between SUSE ( SUSE linux enterprise server or SLES ) and openSUSE is similar to the one of CentOS and Fedora

### Key factors <a name="key"></a>

* It uses the RPM-based zypper package manager 
* It include the YaST ( yet another setup tool ) application for system administration purposes 

## Debian Family <a name="debian"></a>

Upstream for several other distribution like ubuntu, ubuntu is upstream for Mint.

Debian is a pure open source community project ( not owned by any corporation ) and has a strong focus on stability 

Ubuntu gets most of its package from Debian stable branch, it also has access to a very large software repository. 

> Ubuntu TLS ( Long Term Suppport ) 

### Key facts <a name="facts"></a>

* It uses the DPKG-based APT package manager 
* While ubuntu is build on top of Debian and is GNOME-based under the hood, it differs visually from the interface on standard debian.
