# Introduction 

## Index 

* [## Overview](### Overview)

## Overview <a name="overview"></a>

A netwroks enables two computers to communicate with each other. They use some topologies, mediums  and protocols to facilitate the network.

* Topologies 
  * Mesh 
  * Tree
  * Star 
* Mediums 
  * Ethernet 
  * Fiber 
  * Coax
  * wireless
* Protocols 
  * TCP 
  * UDP 
  * IPX 

Most networks uses a `/24` subnet, so much so that many penetration tester will set this subnet mask ( 255.255.255.0) without checking  

![network](../img/network.png)

The entire internet is based on many subdivided networks. We can imagine networking as the delivery of mail or packages sent by one ocomputer and received by the other 

URL: Uniform resource locator  or FQDN : Fully qualified domain name 

* FQDN
  * Especifies the address
* URL
  * Have more information 

We know the address but not exact geographical location  so there goes our Internet Service provider (ISP)

We send our packet through the router, this packet is fowarded to the ISP.

DMZ: delimitarized zone 
