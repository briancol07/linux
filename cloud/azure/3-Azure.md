# What is Azure?

is cloud computing platform

* Set of services 
  * IaaS (Infrastructe as a service)
  * PaaS (Platform as a Service)
  * SaaS (Software as a Service) 

Virtual machine
web site
web hosting 
machine learning 
iot 

pay as you go 
cloud-based storage
deploy/operate 

azure functions --> create apps with no code 
Azure containers 
Azure Kubernetes Services 
Azure cosmos db
Artificial intelligence and machine learning 
Regional Datacenters --> globaly 
Azure portal create and configurate all 

## What does Azure offer?

* Be ready for the future 
  * Continuous innovation
* Build on your terms
  * You can build how you want and deploy where you want to
* Operate Hybrid seamlessly
  * Integrate and manage your environments
* Trust your cloud 

## what can I do with Azure?

AI and machine-learning services that can naturally communicate with your users through vision,hearing and speech.
Also provide storage solutions that dynamically grow to accommodate massive amounts of data.

## How does azure work

1. Virtualization 
  * hw --> os
  * Hypervisor 

Hypervisor emulate all the functions of a real computer, cpu optimazing the capacity, run multiple vm. In the data center with racks. wach one of this contains a fabric controller and these have an **Orchestrator** . Responsible of what happend in Azure.
User make request via the web api, can be call by many tools. This will select the best options. 

## What is the Azure portal?

> Web-based, unified console that provides an alternative to cammand -line tools.

* You can 
  * build , manage and monitor everything from simple web apps to complex cloud deployments.
  * Create custom dashboards for an organized view of resources 
  * Configure accessibility options for an optimal experience.

It maintains a presence in every Azure datacenter.

## What is Azure  Marketplace? 

help to connect users with microsoft partners, independent software vendors.The solution catalog spans severeal industry categories.
