# Detectify Articles 

## To-Read 

- [ ] [Top 10 most critical CVEs added 2020](https://blog.detectify.com/product-updates/top-10-critical-cves-added-in-2020/)


## DONE 

1. [Creating perfect BB automation](https://labs.detectify.com/ethical-hacking/hakluke-creating-the-perfect-bug-bounty-automation/)
2. [Should you learn to code before hacking?](https://labs.detectify.com/ethical-hacking/should-you-learn-to-code-before-you-learn-to-hack/) 
3. [Determining hacking target](https://labs.detectify.com/ethical-hacking/determining-your-hacking-targets-with-recon-and-automation/)
4. [Supercharge your hacking](https://labs.detectify.com/ethical-hacking/how-to-supercharge-your-hacking-mindset-workflow-productivity-and-checklist/)
5. [Hack with goodfaith](https://labs.detectify.com/ethical-hacking/hack-with-goodfaith-to-automate-and-scale-ethical-hacking/)
6. [Recon strategy](https://blog.detectify.com/industry-insights/streaak-my-recon-techniques-from-2019/)
