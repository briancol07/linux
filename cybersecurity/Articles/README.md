# Articles 

> Here I will make a summary of all the articles that I read .

## links 

* [Detectify](https://labs.detectify.com/)
* [Brevity-in-motion](https://www.brevityinmotion.com/automated-cloud-based-recon)
  * [Automated cloud based recon](https://www.brevityinmotion.com/automated-cloud-based-recon)
* [Thread reader](https://threadreaderapp.com/thread/1838169889330135132.html#google_vignette)
* [Inforsecwriteups-EWPT](https://infosecwriteups.com/ewpt-certification-guide-training-exam-resources-career-851ddf9a1ee8)
* [Subdomain takeover](https://0xpatrik.com/)
* [DarkReading](https://www.darkreading.com)
* [Exploiting and Remediating access control Vulns](https://taeluralexis.com/exploiting-and-remediating-access-control-vulnerabilities/)


