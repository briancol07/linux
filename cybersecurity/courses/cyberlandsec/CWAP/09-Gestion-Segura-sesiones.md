# Gestion Segura de Sesiones 

Una sesion es un mecanismo que permite que un servidor mantenga un seguimiento de la interaccion de un usuario durante un periodo de tiempo

![sesion](./img/sesion.png)

La gestion de sesiones se refiere al proceso de creacion, mantenimiento y finalizacion de estas sesiones. 

## Por que es importante? <a name="importante"></a>

El secuestro de sesiones ( Session hijacking ) es una de las tecnicas mas utilizadas por los atacantes para acceder a cuentas de usuarios sin necesidad de conocer las credenciales.

* Robo de cookies 
* Fijacion de sesion ( Session fixation ) 

## Componentes <a name="componentes"></a>

* Cookies de sesion 
* Duracion de la sesion 
* Regeneracion de la sesion 
* Cierre de la sesion 

## Buenas Practicas <a name="buenas-practicas"></a>

* Uso de cookies seguras 
  * Enviar Session id
  * Secure : Solo HTTPS
    * `Set-Cookie: sessionId=abcdef; Secure`
  * HTTP only : Evitar XSS
    * `Set-Cookie: sessionId=abcdef; HttpOnly`
  * SameSite : Evitar CSRF 
    * `Set-Cookie: sessionId=abcdef; SameSite=Strict` 

## Regeneracion de ID de session <a name="regeneracion"></a>

* Debe ser unico y dificil de adivinar ( Session ID ) 
* Regeneracion despues del inicio de sesion 
  * Para evitar que se reutilicen sesiones antiguas 
  * `session_regenerate_id(true);`

## Control de la duracion de la sesion <a name="duracion"></a>

* Sesiones activas 
  * Expirar automaticametne si el usuario no realiza ninguna accion en un periodo de tiempo especifico 
* Tiempo de vida de la cookie de sesion 
  * Define cuanto tiempo puede persistir la cookie de sesion en el navegador del usuario. 
  * `session_set_cookie_params(0); // Expira al cerrar el navegador` 

## Uso de cifrado para la informacion de la sesion <a name="cifrado"></a>

La informacion de la sesion nunca debe almacenarse en texto plano en la cookie. 

* Cifrado del contenido de la sesion 
  * Si decides almacenar datos en la cookie, utiliza un algoritmo de  cifrado fuerte ( AES )

## Cierre de sesion seguro <a name="cierre"></a>

Es esencial que el ID de sesion se invalide inmediatamente para que no pueda ser reutilizado por un atacante. 

## Control de acceso basado en IP y dispositivo <a name="basado-ip"></a>

Se pueden implementar restricciones de acceso adicionales basadas en Direcciones IP o el dispositivo desde el que el usuario esta accediendo. Una unica IP o Dispositivo 

## Prevencion de ataques comunes <a name="prevencion"></a>

* Ataques de secuestro de sesiones ( Session Hijacking ) 
  * Roba una sesion activa del usuario y utiliza para suplantar su identidad 
  * Usa Cookies : Secure y Httponly 
  * implementa HTTPS 
  * Regenera id de sesion 
* Ataques de fijacion de sesion ( Session fixation 
  * OCurre cuando un atacante fuerza a un usuario a usar un ID de sesion conocido. Al regenerar el id de sesion despues de cada inicio de sesion, se previene este ataque 

![hijack](./img/hijacking.png)
