# Exploring The System 

command | Output
--------|--------
ls      | List Directory Contents
file    | Determine file type 
less    | View file contents 

* ls 
  * we can specify the directory to list 
  * Home = **~** 
  * -l Long format 

``` 
total 12
-rw-rw-r-- 1 brian brian 1501 may 10 23:30 1-What-is-the-Shell.md
-rw-rw-r-- 1 brian brian 1354 may 10 23:57 2-Navigation.md
-rw-rw-r-- 1 brian brian   60 may 11 00:00 3-Exploring-The-System.md
```

## Options And Arguments 

```
command -optionns arguments
ls -lt --reverse 
```

Option | Long Option 
-------|------------
-a | --all 
-A | --almost all
-d | --directory
-F | --classify 
-h | --human-readable
-l |
-r | --reverse 
-S | 
-t | 


## Long listing Fields 

```
-rw-rw-r-- 1 brian brian 1501 may 10 23:30 1-What-is-the-Shell.md
```
* -rw-rw-r--
  1. Type of file 
    * - = file
    * d = directory 
  2. 2 to 4 indicates access rights for the file's owner 
    * w = write
    * r = read 
    * x = execute  
  3. 5 to 7  files's group 
  4. The others for everyone else 
* 1
  * File's number of hard links 
* brian
  * The username of the file's owner
* brian
  * The name of the group wich owns the file 
* 1501
  * Size of the file in bytes 
* may 10 23:30
  * Date and time of the file's last modification 
* 1-What-is-the-Shell.md
  * Name of the file 


## Directories Found on linux 

Directory | Comments 
----------|---------
/         | Where everything begins  
/bin      | Binaries that must be present for the system boot and run 
/boot     | Contain linux kernel and the boot loader  
/dev      | Contains device nodes, list of all the devices it understand  
/etc      | Contains all of the system-wide configuration files
/home     | Each user is given a directory in /home
/lib      | Contains shared librery files used by the core system programs 
/lost+found| It is udes in the case of a pratial recovery 
/media    | Contains the mount point for removable media 
/mnt      | Contains mount points for removable devices that have been mounted manually 
/opt      | Is used to install "optional" sw 
/proc     | It is a virtual file system maintained by the linux kernel 
/root     | Directory for the root account 
/sbin     | System "binaries" , generally reserved for the superuser 
/tmp      | Storage temporary transient files 
/usr      | It contains all the programs and support files used by regular users 
/usr/bin  | Contains the executable programs installed 
/usr/lib  | The shared libraries for /usr/bin
/usr/local| Programs that are not included with your distribution
/usr/sbin | System administration programs 
/usr/share| contains all the shared date used by programs in /usr/bin 
/ust/share/doc| Documentation 
/var      | Where data that is likely to change is stored 
/var/log  | Contains log files , the most useful is /var/log/messages 

## Symbolic Links 

### Soft link

Here is where symbolic links save the day. Let's say we install version 2.6 of “foo,”
which has the filename “foo-2.6” and then create a symbolic link simply called “foo” that
points to “foo-2.6.” This means that when a program opens the file “foo”, it is actually
opening the file “foo-2.6”. Now everybody is happy.

### Hard links 
