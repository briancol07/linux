# SQli - Fundamentals 

## Topics 

* Introduction
* Intro to Databases
* Types of Databases
* Intro to MySQL
* SQL Statements
* Query Results
* SQL Operators
* Intro to SQL Injections
* Subverting Query Logic
* Using Comments
* Union Clause
* Union Injection
* Database Enumeration
* Reading Files
* Writing Files
* Mitigating SQL Injection
* Skills Assessment - SQL Injection Fundamentals

## Links 

* [Module](https://academy.hackthebox.com/module/33/section/177)
