# Protecting your device and network

## Protecting your computing devices 

### Turn the firewall on 

You should use at least one type of firewall (either Sw firewall o HW) to protect your device from unauthorized access. The firewall should be turn on and constantly updated to preven hackers from accessing your personal or organization data .

### Install antivirus and antispyware

To prevent viruses and spyware, you should only ever download sw from trusted websites. However you should always use antivirus to provide another layer of protection.which often includes antispyware, is designed to scan your computer and incoming email for viruses and delte them.

### Manage your operating system and browser 

You should set the securitysetting on your computer and browser to medium level or higher.Also regularly update your computer's operating system, including your web browser and download and install the latest SW patches and security updates from the vendors.

### Set up password protection 

All of your devices should be password protected to preven unaunthorized access. Sensitive or confidential data shoudl be encrypted.

## Wireless network security at home 

Wireless router can be configured so that it doesn't broadcaste the SSID (service set identifier), ths should not be considered adequate security for a wireless network.
You should change default password ,also encrupt wireless communication by enabling wireless security and the WPA2 encryption feature on your wireless router. But be aware, even with WPA2 encryption enabled, a wireless network can still be vulnerable.

KRACK

## Public WI-Fi risks 

It is best not ot access or send any personal information when using public Wi-Fi. You should also use an encrypted VPN service to prevent other from intercepting your information (known as 'eavesdropping') over a public wireless network.

Don't forget aht the bluetoothwireless protocol, can also be exploited 

## A Strong Password 

* Do not use dictinary words or names in any languages
* Do not use common misspellings of dictionary words 
* if possible, use special characters 
* Do not use computer names or account names * use a password with more than ten characters

## Using a Passphrase 

In order to prevent unauthorized access to your devices , you should consider using passphrases instead of passwords.

* Choose a statement that is meaningful to you
* Add special characters 
* The longer the better 
* Avoid common or famous statements 
  * lyrics from popular song 

## Password Guidelines 

NIST ( National Institute of Standards and Technology) 

These guidelines aim to place responsibility for user verification on service providers and ensure a better experience for user overall.

* Passwords should be at leat eight characters, but no mor than 64.
* Common, easily guessed passwords, sich as 'password' or 'abc123' should not be used
* Ther should be no composition rules, sich as having to include lower and uppercase letters and numbers 
* Users should be able to see the password when typing, to help improve accuracy 
* All printing characters and spaces should be allowed
* There should be no password hints
* There should be no password expiration period 
* There should be no knowledge-based authentication, such as having to provide answer to secret questions or verifu transaction history
