# Logging 

## Index 

* [## System Logging ](#logging)
* [## Syslog ](#syslog)
* [## General logging ](#general)
* [## Kernel logging ](#kernel)
* [## Authentication logging ](#auth)
* [## Managing log Files ](#log-files)

## System Logging <a name="logging"></a>

The services, kernel, daemons, etc on your system are constantly doing something, this data is actually sent to be saved on your system in the form of logs.
Usually kept in the `/var`: where we keep our variable data 

There is a service called syslog that sends this information to the system logger.
Syslog actually contains many components, one of the important ones is a daemon running called syslogd ( Newer linux use rsyslogd), that wait for event messages to occur and filler the ones it wants to know about, and depending on what it's supposed to do with that message. 

You will see many applications that write their own logging rules and generate different log files, however in general the format of logs should include timestamp and the event details. 

## Syslog <a name="syslog"></a>

The syslog service manages and sends logs to the system logger. Rsyslog is an advanced version of syslog.
The ouput of all the logs the syslog service collects can be found at `/var/log/syslog` ( except auth messages ) 

To find out what files are maintained by our system logger, look at the configuration files in `/etc/rsyslog.d`

These rules to log files are denoted by the selector on the left column and the action on the right column. 

* Action:
  * Tells us where to send the log information, in a file, console, etc.

You can manually send a log with the `logger` command 

```
logger -s Hello
```

## General logging <a name="general"></a>

Some important logs in `/var/log`

* Two general log files 
  * `/var/log/messages`
    * All non-critical and non-debug messages, includes messages logged during bootup (dmesg), auth, cron, daemon.
  * `/var/log/syslog`
    * This logs everything except auth messages, It's extremely useful for debugging errors on your machine 

## Kernel logging <a name="kernel"></a>

On boot time your systems logs information about the kernel ring buffer. This shows us information about hardware drivers, kernel information and status during bootup and more. 
This log file can befound at `/var/log/dmesg` and gets reset on every boot. you may not actually see any use in it now

Another log you can use to view kernel information is the `/var/log/kern.log` file this logs the kernel information and events on your system.  It also logs dmesg output.

## Authentication logging <a name="auth"></a>

Can be very useful to llok at it you are having issues logging in. `/var/log/auth.log`, This contains system authoriation log, such as user login and the authentication method use. 

## Managing log Files <a name="log-files"></a>

Log files generate lots of data and they store this data on your hard disks, however there are lots of issues with this, for the most part we just want to be able to see newer logs. we also want to manage our disk space efficiently, so how do we do all of this. `logrotate`

The logrotate utility does log management for us. It has a configuraiton file that allow us to specify hwo many and what logs to keep, how to compress our logs to save space and more. 

Is usually run out of cron once a day and the configuration files can be found in `/etc/logrotate.d`( this is the most common one ).
