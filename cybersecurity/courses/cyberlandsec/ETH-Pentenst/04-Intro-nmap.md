# Introduccion a nmap

## Index 

* [## Que es Nmap? ](#nmap)
* [## Tipos de escaneos ](#tipos)
* [## Comandos Basicos ](#comandos)
* [## Interpretacion Resultados ](#resultados)
* [## Ejemplo ](#ejemplo)

## Que es Nmap? <a name="nmap"></a>

> Network mapper 

Es una herramienta de codigo abierto utilizada para la exploracion y auditoria de seguridad de redes.

* Detectar dispositivos en una red
* Identificar servicios y sistemas operativos
* Encontrar posibles vulnerabilidadades en sistemas 

Instalacion `sudo apt-get install nmap`

## Tipos de escaneos <a name="tipos"></a>

* Escaneo SYN (TCP SYN Scan)
  * Escaneo mas comun 
  * Envia un paquete SYN al puerto objetivo
  * Si esta abierto el objetivo responde con un paquete SYN-ACK
* Escaneo TCP Connect (TCP connect scan)
  * Utiliza el sistema operativo del atacante para realizar una conexion completa con el puerto objetivo.
  * Menos sigilo que el escaneo SYN porque realiza una conexion completa
* Escaneo UDP (UDP scan) 
  * Intenta determinar que puertos UDP estan abiertos en el objetivo
  * Mas lento debido a UDP
  * No requiere un intercambio de paquetes para establecer conexion 
* Escaneo de version (Version Detection)
  * Despues de identificar un puerto abierto
  * Puede intentar detectar laversion exacta del servicio que se esta ejecutando

## Comandos Basicos <a name="comandos"></a>

* Escaneo Basico de puertos 
  * Escanea los 1000 puertos mas comunes en la direccion IP especificada 
  * `nmap`
* Escaneo de puertos Especificos 
  * `nmap -p 22,80,443`
  * Esta comando escanea los puertos 22,80,443 en la direccion IP objetivo
* Escaneo completo de puertos (0-65535)
  * `nmap -p-
  * Escanea todos los puertos disponibles en el objetivo
* Deteccion de Sistema Operativo 
  * `nmap -O`
  * Intenta identificar el sistema operativo del objetivo
* Deteccion de Version de servicios 
  * `namp -sV` 
  * Identifica los servicios y versiones en los puertos abiertos 

## Interpretacion Resultados <a name="resultados"></a>

* Puertos Abiertos 
  * Indica los puertos que estan abierto y accesibles desde el exterior. Un puerto abierto podria significar un servicio vulnerable 
* Puertos Filtrados 
  * Estos puertos estan protegidos por un firewall u otra forma de filtrado y no responden a los escaneos
* Servicios Detetados 
  * Nmap mostrara los servicios asociados a cada puerto abierto (EJ HTTP puerto 80)
* Versiones de Software 
  * Cuando se realiza la deteccion de version, nmap intentara identificar la version especifica del software que se ejecuta en un puerto.
  * Esto es crucial para identificar vulnerabilidades conocidas 

## Ejemplo <a name="ejemplo"></a>

* Escanoe de puertos abiertos a una red local
  * `nmap -sP 192.168.1.0/24`
* Escaneo completo de un servidor 
  * `namp -p- -A`
 


