# El comando del punto 

Cuando estas editando un texto, evita todo lo que puedas volver a hacer lo que ya hiciste. En este capitulo, aprenderas a utilizar el comand del punto para volver a realizar los cambios previos de una manera sencilla. 

## Uso

Puedes utilizar el comando del punto simplemente presionando la tecla del signo del punto. 

## Que es un cambio 

Si buscas en vim la definicion del comando del punto (:h . ), veras que menciona que el comando repite el ultimo cambio.

Cada vez que realizas una actualizacion del contenido del buffer actual (aniadir, modificar o eliminar) utilizando cualquier comando del modo normal, estas realizando un cambio. 
Los Comandos : no cuentan como cambios

En vim, los cambios no incluyen los movimientos, porque estos no actualizan el contenido del buffer.o

## Aprender el comando del punto de la manera mas inteligente 

El poder del comando del punto viene dado por poder ahorrarte muchas pulsaciones de teclado simplemente en una, pulsando la tecla del punto. quizas no es un cambiio muy rentable el utlilizar comando del punto como reemplazo para operaciones que solo requieren una pulsacion de tecla como x. 

Cuando este editando, preguntate si la accion que estas a punto de realizar es porpensa de volver a repetirla. 
