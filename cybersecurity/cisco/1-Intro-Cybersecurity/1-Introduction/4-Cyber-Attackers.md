# Cyber Attackers

Attackers are individuals or gorups who attempt to exploit vulnerability for personal or financial gain.

## Types of Attackers 

### Amateurs 

> script kiddies, are just curious others are trying to demostrate their skill and cause harm. While these white hat attackers may use basic tools, their attacks can still have devastating consequences.

### Hackers 

> This group of attackers break into computer system or networks to gian access. Depending on the intent of their break in.

* White 
* Gray
* Black

### White hat attackers

Break into networks or computer systems to identify any weaknesses so that the security of a system or network can be improved.
These break-ins are done with prior permission and any results are reported.

### Gray hat attackers 

May set out to find vulnerabilities in a system but they will only report their findings to the owner of a system. Or they might even publish details about the vulnerability on the internet so that other attackers can exploit it.

### Black hay attackers 

Take advantage of any vulnerability for illegal personal,financial or political gain.

## Internal And External Threats 

### Internal 

Employees , contract staff or trusted partners can accidentally or intentionally:

* Mishandle confidential data
* Facilitate outside attacks by connecting infected USB media into the oraganization's computer system.
* Invite malware onto the organization's network by clicking on malicious emails or websites
* Threaten the operations of internal servers or network infrastructure devices.

### External

Amateurs or skilled attackers outside of the organization can:

* Exploit vulnerabilities in the network
* Gain unauthorized access to computing devices
* Use social engineering to gain unauthorized access to organizational data



