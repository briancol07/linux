# Introduction

Azure = Cloud computing platform 

* Fully Virtualized 
* Cloud-based services
  * Remote storage 
  * Database hosting 
  * Centralized account management 
  * Iot 

## What is Azure Fundamentals ? 

> Serie of six learning path that familiarize you to azure and its many services adn features 

## Why should I take Azure fundamentals?

* Provides you with everything you need to get started.
* Prepare AZ-900
  * Describe cloud concepts 
  * Describe core Azure Services
  * Describe core solutions and management tool on Azure
  * Describe general security and network security features
  * Describe identity, governance privacy and compliance features
  * Describe Azure cost management and Service level Agreements 

