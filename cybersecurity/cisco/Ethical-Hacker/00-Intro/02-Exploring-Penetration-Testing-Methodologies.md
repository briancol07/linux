# Exploring the penetration testing methodologies 

The process of completing a penetration test varies based on many factors. The tool and techniques used to assess the secuirty posture of a network or system also very. The network and systems being evaluated are ofthen highly complex. 

## Why do we need to follow a methodology for penetration testing?

* Show that the methods you plan to use for testing are tried and true 
* Provide Documentation of a specialized procedure.

## Environmental Considerations

### Network Infraesture Test 

This often inclueds the swtiches, routers, firewall and suppporting resources (Authentication, authorization, accounting  (AAA)), IPs 
Penetrtion test on wireless infrastructure may sometimes be inclued int the scope of network infrastructure test.

### Application-Based Test 

Focuse on the testing of secuirity weaknesses in enterprise applications.
* Include 
  * Misconfigurations 
  * Input validation issues 
  * Injectio issues
  * Logic flaws 

> Open web application security project (OWASP)

### Penetration testing in the cloud 

* Cloud Service Providers (CSP)
  * Azure 
  * Amazon web service (AWS)
  * Google cloud platform (GCP)

The responsibility for cloud security deppends on the type of model 

* Software as a Service (SaaS)
* Platform as a Servive (PaaS)
* Infrastructure as a Service (IaaS)

> Service level agreement (SLAs) 

## Note 

* Social engineering 
  * Should be to asses the security awareness program 
  * Social-Engineer Toolkit (SET) 
    * Created by Dave Kennedy 

## Penetration Testing methods

* Unknown-environment (Black box)
* Know-environment (white box)
* Partially know environment (Gray box)

### Unknown-Environment test 

The tester is typically provided only a very limited amount of information. For instance, the tester may be provided only the doamin names and IP addresses that are in scope for a particular target.
The tester would no have prior knowledge of the target's organization and Infrastructure. Also the network support personnel of the target may not be given information about exactly when the test  is taking place. 

### Know-Environment test

The tester starts out with a significant amount of information about the organization and its infrastructure. 
The idea of this type of test is to identify as many security holes as possible. 

### Partially Known Environment Test 

Hybrid approach between unknown and know environment test. The tester may be provided credentials but not full documentation of the network infrastructure. A good approach would be a scope where the tester start on the inside of the network and have access to a client machine. Then they could pivot throughtout the network to determine what the impact of a compromise would be.

## Surveying Different Standards and Methodologies 

### Mitre Atta&ck 

[Mitre]((https://attack.mitre.org)

It's a framework is a resoruce for learning about and adversary's tactics, techniques and procedures (TTPs) Bothe offesive security professionaland incident responders and threat hunting teams use. 
Its a collection of different matrices of tactics, techniques and subtechniques. 

### OWASP WSTG 

[OWASP](https://owasp.org/www-project-web-security-testing-guide/)

The OWASP Web Security Testing Guide (WSTG) is a comprehensive guide focused on web application testing. It is a compilation of many years of wrok . Covers the high-level of web application security testing and digs deeper into the testing methods used. 

* Cross-site scripting (XSS)
* XML external entitiy (XXE)
* Cross-site request forgery (CSEF)
* SQL injection attacks 

### NIST SP 800-115 

[NIST](https://csrc.nist.gov/publications/detail/sp/800-115/final)

Is a document created by the National institute of standards and technology (NIST). Provides organixations with guidelines on planning and conducting information security testing. 

The SP800-42 and SP800-115 is considered an industry standard for penetration testing guidance and is called out in many other industry standards and documents.

### OSSTMM

[ISECOM](https://www.isecom.org/)

The open source security testing methodoloy Manual (OSSTMM), developed by Pete Herzog, has been around a long time. Is a document that lyas out repeatable and consistent security testing. 

* Operational Security Metrics 
* Trust Analysis 
* Work flow 
* Human Security Testing 
* Physical Security Testing 
* Wireless Security Testing 
* Telecommunications Security testing 
* Data Networks Security testing 
* Compliance Regulations
* Reporting with the Security Test Audit Report (STAR)

### PTES

[PTES](http://www.pentest-standard.org/)

The Penetration Testing Execution Standard provides information about types of attacks and methods, and it provides information on the latest tools available to accomplish the testing methods outlines.

* Pre-engagement interactions 
* Intellligence gathering 
* Threat modeling 
* Vulnerability analysis
* Exploitation
* Post-exploitation
* Reporting 

## ISSAF

The information Systems Security Assesment Framework (ISSAF) is another penetration testing methodology similar to the others on this list with some aditional phases.

* Information gathering
* Network mapping 
* Vulnerability identificacion 
* Penetration
* Gaining access and privilege escalation
* Enumerating futher 
* Compromising remote user/sites
* Maintaining access
* Covering the tracks 


