# Procesos y Subprocesos 

## Que es un proceso ? 

Es cualquier programa o comando que se esta ejecutando en el sistema. Cada procesotiene un PID que lo identifica de manera unica. 

Cuando se ejecuta un comando, se crea un proceso para ese comando y una vez que el proceso termina el comando tambien 

## Ejecutar procesos en segundo plano 

```
comando &
```

### Ejemplo 
```
#!/bin/bash
sleep 10 &
echo "El proceso 'sleep' se está ejecutando en segundo plano."
```

## Verificar procesos en segundo plano 

```
jobs 
```

## Traer procesos de fondo al primer plano 

```
fg %n
```
* `%n`: representa el numero del trabajo mostrado por el comando jobs 
* Se puede utilizar solo `fg`

## Detener y continuar procesos 

* Pausar un proceso `ctrl+z`
  * Lo colocara en segundo plano en un estado detenido
* Reanudar un proceso en segundo plano `bg`
  * `bg %n`
* Reanudar un proceso en primer plano `fg`

## Control de proceso 

* Ver procesos con `ps`
  * `ps aux` da una lista mas detallada
* Terminar un proceso con `kill`
  * `kill PID` -> PID es process id 
  * `kill -SIGKILL PID` (Termina inmediatamente el proceso )
    * `SIGTERM` ( Solicitud de terminacion )
    * `SIGHUP` ( Reinicio )

## Control de concurrencia en bash 

Cuando tienes multiples procesos ejecutandose al mismo tiempo, la concurrencia se refiere a la capacidad de manejar varios procesos de manera simultanea sin que se bloqueen entre si

### Ejecutar multiples procesos en segundo plano

```
comando1 & comando2 & comando3 &
```

### Esperar que un proceso termine 

El comando 2 no se ejecutara hasta que termine el comando 1

```
comando1 &
wait
comando2
```

## Ejemplo : Ejecutar tareas en segundo plano 

```
#!/bin/bash

# Ejecutar tareas en segundo plano
echo "Iniciando tarea 1 (sleep 5)..."
sleep 5 &

echo "Iniciando tarea 2 (sleep 3)..."
sleep 3 &

echo "Iniciando tarea 3 (sleep 7)..."
sleep 7 &

# Mostrar los procesos en segundo plano
echo "Procesos en segundo plano:"
jobs

# Esperar a que todos los procesos en segundo plano terminen
wait

echo "Todas las tareas en segundo plano han finalizado."
```

## Tarea de la leccion

1. Crea un script que ejecute tres comandos en segundo plano. Usa Jobs para verificar los procesos en ejecucion y wai para asegurarte de que todos terminen antes de imprimir un mensaje final 
2. Modifica el script para matar uno de los proceso en seugndo plano usando kill


