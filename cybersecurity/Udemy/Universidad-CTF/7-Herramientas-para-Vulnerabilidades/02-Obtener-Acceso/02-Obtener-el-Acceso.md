# Obtener el acceso 

> Todo esto se realiza en la consola de metasploit 

Pueden haber varias trabas que nos bloqueen el trabajo 

## Ver todas las opciones de metasploit

```bash 
help 
```

## Buscar exploit

```bash 
search eternal blue 
# O con numero de identificacion
search <codigoIdentificador>
```

## Usaremos un modulo de metasploit

> Obtenido antes con el search 

```bash 
use <modulo>
```

Pare que pueda conectarse necesita opciones y no todos tienen las mismas opciones 

```bash 
show options
```

## Options

Vamos a setear las opciones 

```bash 
set RHOST <IP>
# para ver mas informacion del exploit 
show info 
```

## Para hacer el exploit 

```bash 
exploit
```
Si nos dice que es vulnerable se continua con la explotacion 

