# Recompilacion de informacion sensible

## Index 

* [## Introduccion a la post-explotacion ](#post)
* [## Extraccion de hashes de contrasenias en SO Windos ](#extraccion)
* [## Extraccion de hashes de contrasenias SO linux ](#linux)
* [## Recompilacion de tokens de autenticacion ](#recompilacion)
* [## Captura de credenciales de SSH y RDP ](#ssh)
* [## Automatizacion de la recompilacion de informacion ](#automatizacion)

* Hashes 
* Credenciales 
* Tokens 

## Introduccion a la post-explotacion <a name="post"></a>

Es la fase que sigue a la obtencion de acceso a un sistema comprometido. El objetivo es maximizar el control sobre el sistema, extraer informacion vlaiosa y/o escalar privilegios 

## Extraccion de hashes de contrasenias en SO Windos <a name="extraccion"></a>

Las credenciales se alamacenan en el archivo SAM ( Security account manager )  y los hashes de las contrasenias se encuentran cifrados. 

* Se pueden extraer usando metasploit
  * `use post/windows/gather/hasdump`
  * `set SESSION`
  * `run`
  * Este modulo extrae los hashes de las contrasenias de todos los usuario y muesttra los resultados por consola 
* Uso de mimikatz
  * Puede extrer hashes/contrasenias texto claro / tokens de kerberos 
  * `sekurlsa::logonpasswords`
  * Este comando lista todas las credenciales almacenadas en el sistema.

## Extraccion de hashes de contrasenias SO linux <a name="linux"></a>

Las contrasenias son almacenadas en el archivo `/etc/shadow` 

* Para descifrar se puede usar john the ripper 
  * `john -wordlist=/ruta/diccionario.txt archivo`
  * Este comando utiliza un diccionario de contrasenias para intentar descrifrar los hashes alamacenados en el archivo 

## Recompilacion de tokens de autenticacion <a name="recompilacion"></a>

Los tokens son esenciales apra acceder a otros recursos sin necesidad de introducir credenciales adicionales

* Extraccion de tokens de kerberso con mimikatz `sekurlsa::tickets`
  * Lista los tokens de kerberos almacenados en la memoria del sistema
  * Pueden ser usados para moverse lateralmente dentro de la red mediante `pass-the-ticket`

## Captura de credenciales de SSH y RDP <a name="ssh"></a>

* Las credenciales ssh suelen almacenarse en el dir `~/.ssh/`
  * Comando para copiar una clave ssh privada `cat ~/.ssh/id_rsa > ssh_key.txt`
  * Comando para utilizar la clave privada para iniciar sesion `ssh -i ssh_key.txt usuario@servidor`
* Credenciales RDP
  * con mimikatz caputrar `sekurlsa::msv`

## Automatizacion de la recompilacion de informacion <a name="automatizacion"></a>

* Con metasploit 
* Empire
  * Powershell empire es una herramienta post-explotacion que automatiza la extraccion de tokens kerberos y credenciales de usuario en Windows 
