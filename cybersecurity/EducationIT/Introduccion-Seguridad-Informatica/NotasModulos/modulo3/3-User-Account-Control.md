# User Account Control (UAC)

Es un componente de seguridad incluido en el sistema operativo. El objetivo es minimizar el uso d privilegios de administrador por partes de los usuarios estandar en los equipos.
Las ventajas principales de trabajar con los privilegios de usuario estandar por defecto, son reducir el numero de llamadas al help dek, asi como minimizar el impacto de virus o malware.

## Funcionamiento 

Durante el proceso de logon se genera el token de acceso del usuario. En la generacion de dicho token se evalua los SIDs y privilegios, de manera que si alguno se considera como elevado se produce el split del token.
Esta funcionalidad se denomina protected admin(PA).Con el split del token incluso los usuarios con privilegios de administracion se comportan por defecto como "usuario estandar", si en algun momento se requiere del uso de los privilegios elevados, se prenstara al usuarioel aviso para utilizar las credenciales administrativas. 

No se nos notificara antes de que se realicen cambion en el equipo. Si ha iniciado sesin como administrador, los programas pueden realizar cambios en el equipo sin que usted lo sepa. 
