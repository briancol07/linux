# Web application security 

## What are web Application ?

* Software program that run on web server and are accesible over the internet through web browsers
* Designed to provide interactive and dynamic functionality to users, allowing them to perform various task, access information and interact with data online 
* Web applications have become an integral part of modern internet usage, and they power a wide range of online services and activities 

> Web Site < Web app 

* WebSite (static)
  * Client side 
  * Less complexity 
* Web APP 
  * Have backend ( SQL + Other things ) 
  * More complex 

## How do web Application work?

* The cliente-server Architecture web applications follow the cliente-server model, where the application's logic and data are hosted on a web server, and users access it using web browsers on their devices 
* User Interface (UI): the user interface of web application is usually HTML + CSS + JS 
* Internet connectivity: Web application require an internet connection for users to access them. Sending reques to server and responding to those. 
  * Can be by IP or DNS ( domain name ) 
* Cross platform 
* Statelessness 
  * HTTP is stateless
  * Web app must manage user sessions and state to remember user interaction 

## Web Application security 

* Critical aspect of cyberseurity taht focuses on protecting web app 
  * Protection of sensitive data 
  * Safeguarding user trust 
* To ensure CIA ( confidentiality integrity and availability ) of data prrocessed 
* Mitigate the risk of unauthorized access, data breaches and service disruptions 

## Importance of web app security 

* Prevention of Financial loss
* Compliance and regulatory Requirements 
  * GDPR
  * HIPAA
  * PCIDSS
* Mitigation of cyber threats 
* Protection against DDoS attack 
* Maitaining Business continuity 
* Preventing defacement and data manipulation

## Best Practices 

* Authentication and authorizacion 
* Input Validation
* Secure communication 
* Secure Coding practices 
* Regular security updates 
* Least privilege principle
* WAF ( web application firewalls ) 
* Session management 
