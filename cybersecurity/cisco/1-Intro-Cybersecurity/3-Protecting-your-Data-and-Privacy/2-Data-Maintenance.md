# Data Maintenance 

## What is Encryption?

The process of converting information into a form in which unauthorized parties cannot read it . Only a trusted, authorized person with the secret key or password can decrypt the data and access it in its original form.

> The encryption itself does not prevent someone from intercepting the data. It can only prevent an unauthorized person from viewing or accessing the content.

## How do you Encrupt your data?

Encrupting file system (EFS) is a windows feature that can encrupt data. It is directly linked to a specific user account and only the user that encrupts the data will be able to access it after it has been encrypted using EFS.

## Back Up your data 

Having backup may prevent the loss of irrepaceable data.

* Storing your data locally means that you have total control of it 
* Secondary location 
* The cloud 

## How do you Delete your data Permanently?

To erase data so that it is no longer recoverable, it must be overwritten with ones and zeroes multiple times, using tools specifically designed to do just that. 

* SDelete (windows)
* Shred (linux)
* Secure Empty trash (Mac)

The only way to be certain that data or files are not recoverable is to physically destroy the hard drive or storage.
