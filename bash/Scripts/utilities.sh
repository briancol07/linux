#!/bin/bash 

rep=$2
one=$1

if [ -z $rep ]; then 
  rep=1
fi 

if [ -z $one ]; then
  one=1
fi 

while [ $rep -gt 0 ]; do
  if [ $1 -eq 0 ]; then
    echo "╔══════════════╗"
    echo "║              ║"
    echo "╚══════════════╝"
  elif [ $1 -eq 1 ]; then 
    echo "┌──────────────┐"
    echo "│              │"
    echo "└──────────────┘"
  elif [ $1 -eq 2 ]; then 
    echo "(╯°□°）╯︵ ┻━┻ "
  elif [ $1 -eq 3 ]; then 
    echo "( ˘ ³˘)♥"
  elif [ $1 -eq 4 ]; then 
    echo "ლ(｀ー 'ლ)"
  elif [ $1 -eq 5 ]; then 
    echo "~(^-^)~"
  elif [ $1 -eq 6 ]; then 
    echo "    |   | "
    echo "----|---|"
    echo "    |   |" 
  elif [ $1 -eq 7 ]; then 
    echo "ñ"
  else 
    echo "LOL"
  fi
  rep=$((rep - 1))
done 

