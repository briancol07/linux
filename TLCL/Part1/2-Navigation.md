# Navigation 


command | Output
--------|--------
pwd     | Print name of current working directory 
cd      | Change Directory 
ls      | List directory contents 

```
/home/repos/gitlab.com/briancol07/linux/TLCL/Part1

1-What-is-the-Shell.md
2-Navigation.md
```

## File system Tree 

* Hirearchical directory structure 
* Organized in a tree
* First directory is called root ("/")

``` 
/
 -- bin
 -- boot 
 -- dev
 -- etc
 -- home
        -- me  
```

## Changing: The current workin Directory 

cd + pathname 

pathname of the desire working directory. Rout we take along the branches of the ree to get to the directory we want. 

### Absolute Pathnames 

Begin with the rootdirectory and follows the tree branch by branch until the path to the desire directory or file is completed. 

### Relative Pathnames 

Start from the working directory. To do this, it uses:

* . (dot)(working directory)
* .. (dot dot) (parent directory)

### Some helpful Shortcuts 

command | Output
--------|--------
cd  | Change the working directory to your home directory 
cd - | Changes the working directory to the previous working directory 
cd ~user_name | To home directory of user_name


## Important Facts 

1. ls -a to see hidden files 
2. filenames and commands case sensitive
3. Linux has no concep of a file extension
4. Do not embed spaces in filenames


