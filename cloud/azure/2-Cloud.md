# What is cloud computing?

It's the delivery of computing services over the internet (cloud). 

* This include 
  * Servers 
  * Storage
  * Databases
  * Networking 
  * Software
  * Analytics 
  * Intelligence

Compute power and processing , you can add compute power as you need, also with storage.Also backups, up and running 24h a day.

## Why is cloud computing typically cheaper to use?

> Using pay-as-you-go pricing model 

* Lower your operating costs 
* Run your infrastructure more efficiently
* Scale as your business needs change 

Cloud computing is a way to rent compute powre and storage from someone else's datacenter. 

## Why should I move to the cloud?

The cloud help you move faster and innovate in ways that were once nearly impossible 

* Two trends 
  * Teams deliver new feature to their users at record speeds
  * Users expect an increasingly rich and immersive experience with their devices and with software 

* On-Demand 
  * Nearly limitless pool of raw compute, storage and networking components 
  * Speech recongition and other cognitive services that help make your application stand out.
  * Analytics services that deliver telemetry data fromyour software and devices.



