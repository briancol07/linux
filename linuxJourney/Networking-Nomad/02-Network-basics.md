# Network Basics 

## Index 

* [## Intro ](#intro)
* [## OSI ](#OSI)
* [## TCP/IP model ](#tcp/ip)
  * [### Application layer ](#application)
  * [### Transport layer ](#transport)
  * [### Network layer ](#network)
  * [### Link Layer ](#link)
* [## Networking addressing ](#addressing)
  * [### MAC ](#mac)
  * [### IP Addresses ](#ip)
  * [### Hostnames ](#hostanmes)
* [## Application layer ](#app2)
* [## Transport Layer ](#transport)
  * [### Ports ](#portss)
  * [### UDP ](#udpp)
  * [### TCP ](#tcp)
* [## Network Layer ](#nt-layer)
* [## Link layer ](#link-ly)
  * [### ARP ( Address resolution protocol ) ](#arp)
* [## DHCP overview ](#dhcp)

## Intro <a name="intro"></a>

* Components 
  * ISP : Internet service provider
  * Router : THe router allows each machine on you network to connect to the internet.
  * Wan : Wide area network 
  * WLAN: Wireless local area network . This is the network between your router and any wireless devices 
  * LAN : Local area network,This is the network between your router and any wired devices such as Desktop.
  * Host: Each machine on a netwrok is known as host

## OSI <a name="OSI"></a>

> Open System Interconnection model is a theorical model 

This model show us hwo a packet traverses through a netwrok in seven different layers. I won't get into specifics of this model, since most of these networking courses will be focused on the TCP/IP model .

## TCP/IP model <a name="tcp/ip"></a>

These protocols work together to specify how dat should be gathered, addressed, transmitted and routed through a network.

### Application layer <a name="application"></a>

The top layer of the model. It determines how your computer's programs ( such as your web browser) interface with the transport layes services to view data that get sent or received 

* Uses 
  * HTTP (Hypertext Transfer Protocol) - used for the webpages on the internet
  * SMPT (simple mail transfer protocol) - electronic mail transmission

### Transport layer <a name="transport"></a>

How data will be transmitted, inclueds checking the correct ports, the integrity of the data and basically delivering our packets 

* Uses 
  * TCP (Transmission Control Protocol) - Reliable data delivery
  * UDP (User datagram protocol) - Unreliable data delivery 

### Network layer <a name="network"></a>

This layer specifies how to move packets between hosts and across networks 

* Uses 
  * IP (internet protocol) - Helps route packets from one machine to another
  * ICMP (Internet Control message Protocol) - Helps tell us what is going on such as error messages and debugging information 

### Link Layer <a name="link"></a>

This layer specifies how to send data across a physical piece of hardware. SUch as data travelling through ethernet,fiber,etc.


## Networking addressing <a name="addressing"></a>

When you mail a letter, you must know who it is being sent to and where it is coming from. Packets need the same information, our hosts and other hosts are identified using MAC (media access control) addresses and IP addresses, to make it easier on us humans we use hostnames to identify a host 

### MAC <a name="mac"></a>

Is a unique identifies used as a hardware address. This address will never change.They look like `00:C4:B5:45:B2:43`. Also gave when they are manufactured. Each manufacturer has an organizationally unique identifier (OUI) to identify them as the manufacturer. Denotate by the first 3 bytes of the mac. Example dell has `00-14-22` `00-14-22-34-B2-C2`

### IP Addresses <a name="ip"></a>

Is used to identify a device on a network they are hardware independent and can vary in syntax depending on if you are using IPv4 or IPv6 . IPV4: `10.24.12.4`.

Anytime a systemp is connected to the internet it should have an IP address. They can aslo change if your network changes and are unique to the entire internet (not always the case)

### Hostnames <a name="hostanmes"></a>

Hostnames take your IP address and allow you to tie that address to a human readable name. example : `myhost.com`

## Application layer <a name="app2"></a>

* Packets consist of a header and payload. 
  * The header contains information about where the packet is and where it came from. 
  * The payload is the actual data that is being transferred.

Each layer add a bit of information to the header of the packet. 
In the transport layer we essentially encapsulate our data in a segment and in the link layer we refer to this as a frame, but just know that packet can be used in regards to the same thing.

When we send our email through our email client, the application layer will encapsulate this data, Also talks to the transport layer through a specified port and through this port sends its data. 

The dat is sent through our transport protocol which opens a connection toi this port ( 25 SMTP), then is encapsulated into segments.

## Transport Layer <a name="transport"></a>

Help us transfer our data in a way networks read it. It breaks our data into chunks that will transported and put back together in the correct order, knon as segments.

### Ports <a name="portss"></a>

The transport layer will also attach the source and destination ports to the segment, so when the receiver gets the final packet it will known what port to use 

### UDP <a name="udpp"></a>

Is not a reliable method of transporting data, in fact it doesn't really care if you get all your original data. Example media streaming, it's ok if you lose some frames in return you get your data a little faster 

### TCP <a name="tcp"></a>

Reliable connection-oriented stream of data. Uses ports to send data and from hosts. An application opens up a connection from one port on its host to another on a remote host.

* We use TCP handshake 
  * Client ( connecting process) send SYN segment to the server to request connection
  * Server send the client a SYN-ACK segment to acknowledge the client's connection request 
  * Client send an ACK to the server to acknowledge the server's connection request

Once this connection is established, data can be exchanged over a TCP connection. Data is sent over in different segmetns and are tracked with TCP sequence numbers so they can be arranged in the correct order when they are delivered.

## Network Layer <a name="nt-layer"></a>

Determines the routing of our packets from our source host to a destination host. 
Smaller networks that make up the internet are known as subnets. All subnets conenct to each other in some way, which is why we are able to get to `www.google.com` even though it's on its own network.

In the network layer , it receives the segment coming from the transport layer and encapsulates this segments in a IP packet then attaches the IP address of the source host and the IP address of the destination host to a packet header.

So at this point, our packet has information about where it is going and where it came from . 

## Link layer <a name="link-ly"></a>

At the bottom of the TCP/IP model sits the link layer ( HW specific)

Now its is encapsulated once more into soemthign called a frame. This attaches the source and destination MAC addresses of our hosts, checksums and packet separators so that the receiver can tell when the packets end.

1. Attach my source MAC address to the frame header.


### ARP ( Address resolution protocol ) <a name="arp"></a>

This find the MAC address associated with an IP address. If you are in the same network first use the ARP look-up table if not there , uses ARP to find the host.
Any machine with the request ip address will reply with an ARP packet containing the IP address and the MAC address 

When travelling through networks, it requires going through the TCP/IP model at least twice before any data is sent or received.

* Packet traversal
  1. a sent to b an emal: This dat gets sent to the trasport layer
  2. The transport layer encapsulates the data into TCP or UDP header to form a segment.
    * The segment attaches the destination and source TCP/UDP port, then the segment is sent to the network layer 
  3. The network layer encapsulates the TCP segment inside an IP packet. It attaches the source and destination IP address 
    * Then routes the packet to the link layer 
  4. The packet reaches a physical hardware and gets encapsulated in a frame.
    * The source and destination MAC address get added to the frame 
  5. B receives this data fram through her physical layer and checks each frame for data integrity.
    * Then de-encapsulates the frame contents and sends the IP packet to the network layer 
  6. The network layer reads the packet to find the source and destination IP that was previously attached.
    * It cheks if its IP is the same as the destination IP, Which it is. 
    * De-encapsulates the packet and sends the segment to the transport layer
  7. The transport layer de-encapsulates the segments, checks the TCP or UDP port numbers and makes a connection to the application layer based on those port numbers 
  8. The application layer receives the data from the transport layer on the port that was specivied
    * Present it to B in the form of the final email message.

## DHCP overview <a name="dhcp"></a>

> Dynamic Host Configuration protocol

Assigns IP addresses, subnet mask and gateways to our machine. 

Allows a network administrator to not worry about assigning IP addresses and it also prevents them from setting up duplicat IP addresses. Every physical network should have its own DHCP server so that a host can request an IP address. 

* DHCP get dynamic host information
  * DHCP DISCOVER - This message is broadcasted to search for DHCP server
  * DHCP OFFER - The DHCP server in the network replies with an offer message. The offer contains a packet with DHCP lease time, subnet mask, IP address etc.
  * DHCP REQUEST - The client sends out another broadcast to let all DHCP server know which offer it accepted 
  * DHCP  ACK -  Acknowledgement is sent by the server 
