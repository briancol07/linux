#!/bin/bash

#Crea un script que imprima los numeros del 1 al 10 usando bucle for

for num in {1..10}; do
  if [ $num -eq 5 ]; then
    continue
  fi
  echo "$num"
done 
