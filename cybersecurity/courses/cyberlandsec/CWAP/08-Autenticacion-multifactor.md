# Autenticacion Multifactor (MFA)

La autenticacion Multifactor es un metodo de seguridad que requiere que un usuario proporcione dos o mas tipos de verificaciones para acceder. 
El objetivo de MFA es reducir el riesgo de acceso no autorizado en caso de que una contrasenia se vea comprometida.

## Indice 

* [## Como Funciona? ](#funcionamiento)
* [## Importancia del mfa ](#importancia)
* [## Tipos ](#tipos)
* [## Implementacion ](#implementacion)
* [## Buenas practicas ](#buenas-practicas)

## Como Funciona? <a name="funcionamiento"></a>

* Combinacion de dos o mas : 
  * Algo que sabes 
    * Contrasenia / pin 
    * Algo que el usuario debe recordar 
  * Algo que tiene 
    * Dispositivo fisico que el usuario posee 
  * Algo que eres 
    * Factores Biometricos 

![mfa](./img/mfa.png) 

## Importancia del mfa <a name="importancia"></a>

MFA aniade capa critica de seguridad que incluso si las contrasenias se ven comprometidas, impide que los atacantes accedan 

* Ventajas 
  * Proteccion adiciona contra robo de credenciales
  * Cumplimiento normativo 
    * GDPR,PCI DSS, HIPAA
  * Menor riesgo de ataques automatizados 

## Tipos <a name="tipos"></a>

* Autenticacion mediante codigos temporales (TOTP)
  * Time-based One-Time Password 
* SMS o Correo electronico 
* Biometrico 
* Dispositivos fisico ( U2F ) 
  * Universal 2nd Factor 

## Implementacion <a name="implementacion"></a>

* Uso de servicios de terceros 
  * Auth0
  * Okta 
  * Firebase authenticacion 
* Personalizda 
  * Speakeasy (node js) 
  * PyOTP ( Python ) 

![implementacion](./img/implementacion.png)

## Buenas practicas <a name="buenas-practicas"></a>

* Evitar el uso de solo SMS 
* Facilita la recuperacion de cuentas 
* Educacion a los usuarios 
