# Bandwidth and Throughput 

## Bandwidth 

To support the applications, networks hace to be capable to transmitting and receiving bits at a very high rate 

> Bandwidth is the capacity of a medium to carry data.

Digital bandwidth measures the amount of data thta can flow from one place to another in a given amount of time (typically measured in the number of bits) 

* Thousands of bits per second (Kbps)
* Millions of bits per second (Mbps)
* Billions of bits per second (Gbps)

## Throguhput 

The measure of the transfer of bits across the media over a given period of time (not usually match the specified bandwidth) 

* Factors that influence 
  * The amount of data being sent and received over the connection 
  * The types of data being transmitted 
  * The latency created by the number of network devices encountered between source and destination 

Latency refers to the amount of time, including delays, for data to travel from one given point to another 

> Do not take into account the validity or usefulness of the bits being transmitted and received. 


In a network with multiple segments, throughput cannot be faster than the slowest link of the pathe from sending device to the receiving device. Even if all or most of the segments have high bandwidht it will only take one segmet in the path with lower badwidth to create a slowdown of the throughtput of the entire network 
