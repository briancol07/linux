# Meterpreter 4 

windows cada tanto reinicia los servicios y puede caer la conexion 

## Continuar sniffer 

vamos a usar wireshark,puede haber mas de una tarjeta de red 

```bash 
help
```

## Agrega componentes 

Como kiwi,sniffer 

```bash 
use -l
```

## Usar sniffer 

```bash 
use sniffer
```

### Ver interfaces 

lista todas las interfaces , hay que ir una por una 

```bash 
sniffer_interfaces 
```


### Select target 

El ID lo obtuvimos en interfaces , El buffer lo elegimos nosotros (puede ser 1024) que es para almacenar los datos 

```bash 
sniffer_start <ID> <Buffer>
```

### Ver cuantos paquetes fueron capturados 

```bash 
sniffer_stats <ID>
```
Si no captura nada significa que no es la indicada, y seguir buscando cual es la correcta 

### Guardar el buffer 

```bash 
sniffer_dump <ID> escaner.pcap
```
