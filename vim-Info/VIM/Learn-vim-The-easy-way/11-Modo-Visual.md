# Modo Visual 

> Se puede salir del modo visual con la misma tecla que entraste o con `ctrl-c` o `<esc>`


## Tipos 

* Seleccion de caracter 
  * `v`
  * `-- VISUAL --`
* Seleccion de lineas 
  * `V`
  * `-- VISUAL LINE --`
* Seleccion de Bloque 
  * `Ctrl-v`
  * `-- VISUAL BLOCK --`
* Otra forma de acceder al modo visual 
  * `gv`
  * Pero esto resaltando el mismo texto que resaltaste anteriormente 

## Modo visual de caracteres 

Es utilizado para seleccionar caracteres de forma individual.

* convertir a mayusculas apartir de lo seleccionado visualemnte 
  * `gU`

## Modo Visual de seleccion de linea 

Funciona con lineas como unidades de seleccion y obserca como vim selecciona la linea completa en la que este ubicado el cursor.  

## Modo Visual de seleccion de bloque 

Funciona con fila y columnas. Te da mas libertad de movimientos que los dos modos anteriores. 


## Navegacion en modo visual 

Con las letras `o` o `O` se puede mover el cursor de la parte de arriba a la parte de abajo de lo seleccionado . 

Luego los movimientos normales de jhkl. `

## Gramatica en el modo visual 

Comparte mucho con el modo normal como por ejemplo eliminar con `d` , podemos seleccionar y eliminar pulsando `d` 

> Aca es sustantivo + verbo 

* Tambien se puede utilizar
  * Borrar caracter bajo el cursor : `x`
  * Reemplazar bajo el cursor: `r` 
 
Copiar linea `yy`

## Modo visual y comandos Ex 

Si queremos sustituir en una cantidad de lineas una palabra podemos seleccionarlas y luego usar el siguiente comando 

 ```
 :s/palabraRemplazada/nuevaPalabra/g
 ```
## Aniadir texto en multiples lineas 

Si se quiere poner un punto y coma al fina de cada lineas se haria:

* Forma 1: 
  1. `ctrl-V`+`jj`
  2. Resaltar hasta el final de la lineaa `$` 
  3. Aniade `A` y despues escribe ;
  4. Sal del modo visual 
* Forma 2: 
  * `vjj`
  * `:normal! A;` 


## Incrementar numeros 

* Bajar 
`* `Ctrl-X`
* Aumentar
  * `Ctrl-A`

## Seleccionar la ultima area del modo visual 

Se puede ir a la ubicacion del comienzo y final de la anterior zona del modo visual con: 

* Ir al ultimo lugar de la anterior zona resaltada 
  * `<`
* Ir al comienzo de la anterior zona resaltada modo visual
  * `>`

Antes lo vimos en la suplantacion 

 ```
 :`<,`>s/const/let/g
 ```

 ## Entrando en el modo visual desde el modo insertar 

 > Funciona con cualquier modo de visualizar

 * `Ctrl-O v`
 
 ## Modo Seleccionar 

 > Simialr al modo visual 

* Modos 
  * Seleccion de caracter : `gh`
  * Seleccion de linea : `gH`
  * Seleccion de bloque : `gCtrl-h`

Emula al comportamiento de un editor de texto normal al selecciona un texto de una manera mas cercana a como hace vim en el modo visual 

En un editor normal, despues de selecciona un bloque de texto y escribir una letra por ejemplo la letra "y", el editor borra todo el texto resaltado e inserta la letra escrita 

> Tener cuidado con el modo visual puede ser una antipatron.






