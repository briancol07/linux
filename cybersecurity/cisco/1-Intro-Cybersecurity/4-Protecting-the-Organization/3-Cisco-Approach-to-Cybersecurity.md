# Cisco's Approach to Cybersecurity

## Cisco's CSIRT 

Many large organizations have a computer Security incident Response Team CSIRT to receive, review and respond to computer security incident resport.
Cisco CSIRT provides proactive threat assessment, minitigation, incident trend analysis and security architecture review in an effort to prevent security incidents from happening.


Type | Meaning 
-----|--------
FIRST|Forum of incident Response and security Teams  
NSIE | National Safety Information Exchange
DSIE | Defense security Information Exchange 
DNS-OARC | DNS Operations Analysus and Research Center 

## Security Playbook 

Guidance on:

* How to identify the cybersecurity risk to systems, assests data and capabilities 
* The Implementation of safeguards and personnel training 
* A flexible reponse plan that minimizes the impact and damage in the event of a security breach 
* Security measures and processes that need to be put in place in the afermath of a security breach 

A Security playbook is a collection of repetable queries or reports that outline a standardized process for incident detection and response. Ideally a security playbook should :

* highlight how to identify and automate the response to common threat such as the detection of malware-infected machines, suspicious network activity or irregular authentication attempts 
* Describe and clearly define inbound and outbound traffic 
* Provide summary information including trends,statistics and counts 
* Provide usable and quick access to key statistics and metrics 
* Correlate event across all relevant data sources.

## Tools for incident Detections and prevention

A Security information and Event management (SIEM) system collects and analyse security alerts, logs and other real-time and historical data from security devices on the network to facilitate early detection of cyber attacks 

A data Loss prevention (DLP) system is designed to stop sensitive data from being stolen from or escaping a network. It monitors and protects data in three different states: 

* Data in use 
  * Being accessed by a user
* Data in motion 
  * Data traveling through the network
* Data in rest 
  * Data Store in a computer network or device.

## Cisco's ISE and TrustSec 

Cisco identity Services Engine and TrustSec enforce user access to network resources by creating role-based access control policies. 


* IPS
  * Can block or deny traffic based on a positive rule or signature match
* IDS Scans data against a database of rule or attack signatures, looking for malicious traffic 
* DLP
  * System is desinged to stop sensitive data from being stolen from or escaping a network
* SIEM 
  * system collects and analyzes security alerts, logs and other real-time and historical data from security devices on the network 


