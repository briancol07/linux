# Certificaciones 

* [Certificaciones](https://pbs.twimg.com/media/GClc40cWMAASaxY?format=jpg&name=900x900)

## EwptV2 

## CEH

* [Guia](https://achirou.com/guia-rapida-para-el-examen-de-hacker-etico-certificado-ceh-2024/)

## OSCP

* [OSCP-guide-YT](https://www.youtube.com/watch?v=HUmtfFKKWWc)

## Burp suite Practitioner 

* [Github](https://github.com/DingyShark/BurpSuiteCertifiedPractitioner)

## Links 

* [Red team operations](https://github.com/CyberSecurityUP/Awesome-Red-Team-Operations)
* [Compitia-Pentest+-11hs](https://www.youtube.com/watch?v=WczBlBjoQeI)
