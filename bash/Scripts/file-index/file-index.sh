#!/bin/bash

while read -r line; do 

  var=$(echo "* [$line](./$line)")
  if grep -qF "$var" "$1"; then
    continue 
  fi

  echo "$var"

done < <(ls -p | grep -v /)


