# Wireless and Mobile Networks 

## What will I learn in this module?

> Configure mobile devices for wireless access 

Topic Title | Topic Objective 
------------|---------------
Wireless Networks | Describe the different types of network used by cell phones and mobile devices
Mobile Devices Connectivity | Configure mobile devices for wireless connectivity 

## Topic 

* [1-Wireless-Networks.md](./1-Wireless-Networks.md)
* [2-Mobile-Devices-Connectivity.md](./2-Mobile-Devices-Connectivity.md)
