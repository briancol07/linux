# Working With Commands 


Commands   | Description
:---------:|:------------:
type | Indicate how a command name is interpreted
which | Display which executable program will be executed
help | Get help for shell builtins
man | Display a command's manual page
apropos | Display a list of appropiate commands
info | Display a command's info entry 
whatis | Display a very brief description of a command 
alias | Create an alias for a command 

## What Exactly are commands ? 

1. An executable program 
2. A command built into the shell itself (bash) 
3. A Shell function 
4. An alias 

## Display a Command's type 

The type command is a shell builtin that display the kind of command the shell will execute 

## Display an Executable's location 

Sometimes there is more than one versin of an executable.To determine the exact location of a given executalbe we use the command which 

## Getting a command's Documentation

```bash 
help cd 

mkdir --help

man
```

## Display Appropriate Commands 

The whatis program displays the name and a one linedescription of a man page matching a  specified keyword 

```bash 
whatis ls 
```
```
output:
ls (1)               - list directory contents
```

## Display a Programs's info Entry 

The Gnu project porvides an alternative to man pages for their progrmas, called "info".Info pages are displayed with a reader program name "info". Hyperlinked much like web pages 



