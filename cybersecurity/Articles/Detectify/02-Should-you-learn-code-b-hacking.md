# Should you learn to code before you learn to hack?

> It's possible to become a great hacker without coding knowledge, but having coding experience makes it a whole lot easier.

## Writing your own tools 

By creating your own tools, you know exactly what each tool is doing.

* Find a gap in currently available tooling 
* Create you updated or new tool 
* Test your new tool agains local targets 
* Deploy you tool agains real targets 
* Open source it ( or don't ) 

> you can use fingerprint tools such as wppalyzer or httpx, also engines like google and shodan 

## Source code review 

* Reviewing open source code 
* Decompiling java 
* Reviewing containerized software in public registries

## Automation 

* Questions you should ask: 
  * Waht does the input data look like ?
  * How should your automation be ingesting this data to produce results?
  * What does the resulting output data look like, and where is it stored? 
  * How will deploy this automation in a way that can scale and handle enough targets ? 
  
## Resources to learn more 

* [Sources and sinks](https://www.youtube.com/watch?v=ZaOtY4i5w_U)
* [OWASP - Code review](https://owasp.org/www-project-code-review-guide/assets/OWASP_Code_Review_Guide_v2.pdf)
* [Writeups by security researchers](https://labs.detectify.com/category/writeups/) 
