# Operadores basicos 

## Tipos de operadores 

* Aritmeticos 
* De comparacion
* Logicos 
* Asignacion

## Aritmeticos

Permiten realizar calculos numericos ( `+ , - , * , /` )
Se utilizan usando `$(())` o `let` 

```
#!/bin/bash
# Asignar valores a las variables
numero1=10
numero2=5

# Realizar operaciones
suma=$((numero1 + numero2))
resta=$((numero1 - numero2))
multiplicacion=$((numero1 * numero2))
division=$((numero1 / numero2))
modulo=$((numero1 % numero2))

# Imprimir los resultados
echo "Suma: $suma"
echo "Resta: $resta"
echo "Multiplicación: $multiplicacion"
echo "División: $division"
echo "Módulo: $modulo"
```

## Operadores de Comparacion 

Comparar valores numericos, con condicionales `if`

Operador | Descripcion
---------|--------------
`-eq` | Compara si dos numeros son iguales 
`-ne` | Compara si dos numeros son diferentes
`-lt` | Compara si un numero es menor que otro
`-le` | Compara si un numero es menor o igual que otro 
`-gt` | Compara si un numero es mayor que otro
`-ge` | Compara si un numero es mayor o igual que otro 

```
#!/bin/bash
numero1=10
numero2=20

# Comparar si los números son iguales
if [ $numero1 -eq $numero2 ]; then
    echo "Los números son iguales"
else
    echo "Los números son diferentes"
fi
```

## Operadores logicos 

Permiten combinar varias condiciones dentro de un script. Son esenciales cuando necesitas evaluar multiples expresiones al mismo tiempo 

Operador | Descripcion
---------|--------------
AND `&&` | Ambas condiciones deben ser verdaderas para que la expresion sea verdadera 
OR `||`  | Al menos una de las condiciones debe ser verdadera para que la expresion sea verdadera 
NOT `!`  | Niega el resultado de una condicion 

```
#!/bin/bash
numero=15

# Usar operadores lógicos para evaluar múltiples condiciones
if [ $numero -gt 10 ] && [ $numero -lt 20 ]; then
    echo "El número está entre 10 y 20"
else
    echo "El número no está entre 10 y 20"
fi
```

## Operadores de asignacion 

Para asignar los valores a las variables , el mas basico es el igual 


Operador | Descripcion
---------|--------------
`+=` |  Suma el valor a la variable y asigna resultado 
`-=` |  Resta el valor a la variable y asigna el resultado
`*=` |  Multiplica la variable por el valor y asigna resultado
`/=` |  Divide la variable por el valor y asigna el resultado 

```
#!/bin/bash
numero=10

# Aumentar el valor de la variable
numero+=5
echo "Nuevo valor de numero: $numero"

# Multiplicar la variable
numero*=2
echo "Valor después de la multiplicación: $numero"
```

## Uso de parentesis en Expresiones 

Para agrupar operaciones aritmeticas y asegurarse de que se evaluen en orden correcto 

```
#!/bin/bash
numero1=10
numero2=5

resultado=$(( (numero1 + numero2) * 2 ))
echo "El resultado es: $resultado"
```

## Tarea 

1. Crea un script que pida al usuario que introduzca un numero y luego le diga si ese numero es mayor, menor o igual a 100
2. Modifica el script de calculadora para que tambien calcule el modulo entre dos numeros 
