# Introduction to Kubernetes (LSF158)

* [link](https://trainingportal.linuxfoundation.org/courses/introduction-to-kubernetes)
* Courses 

## Resources 

* [Introduction Cloud Infraestructure Technologies](https://training.linuxfoundation.org/training/introduction-to-cloud-infrastructure-technologies/)
* [Large-scale-Google-Borg](https://research.google/pubs/large-scale-cluster-management-at-google-with-borg/)
* [Courses](https://www.cncf.io/training/courses/)
