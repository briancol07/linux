# Conexion Directa 

## Netcat

Abrir puertos para ver las conexiones 

``` bash 
# 4447 puerto ejemplo 
nc -lvvp 4447
```

-lvvp pone a netcat en forma de escucha y luego te conectas a la ip de la pc 


```bash 

nc -lvvp 4446 - e cmd.exe

```

El -e me permite cargar ejecutables 
