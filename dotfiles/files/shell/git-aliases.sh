#!/bin/bash 

alias ga='git add'
alias gall='git add .'
alias gco='git commit -m'
alias gs='git status'
alias gd='git diff'
alias gl='git log'
alias gc='git checkout'
alias gp='git push'
alias gpl='git pull'
alias dotcommit='git commit -a -m "."'
