# Determining your hacking target with recon and automation

* [Determining hacking target](https://labs.detectify.com/ethical-hacking/determining-your-hacking-targets-with-recon-and-automation/)

## Why picking targets is so important 

* Focus on target you are comfortable with 
* A target using technology or framework you are an expert in 
* A website or asset that appears old or deprecated 
* Older verions of a released product that are still deployed 
* Web applicacion that accept a large amount of user input 
* Hacking targets that have had vulnerabilities in the past that you are an expert in 

## When recon and automation are an advantage for hacking targets 

> Recon is the step in which asset discovery takes place, The better you perform the better the results of your hacking are likely to be. 

* Ways of recon 
  * Finding hacking targets other ethical hackers missed 
  * Creating a database of assets that can continously be hacked or scanned 
  * Fingerprinting assts to find technoloy/ frameworks you know 
  * Creating a system to learn about new assets that get deployed 

## when recon is a disavantage 

* Balance between reconnaissance and hand-on hacking 
* Use recon to find the information you actively use in your  hands-on hacking 
* Automate more of your recon tasks
* minimize the targets you are hacking at one time 


## Dig harder 

* Hidden endpoints
  * Unlinked endpoints
  * Older versions of APIs
* Hidden parameters 
  * Admin = true 
  * debug = true 
* Comments from developers 
* Virtual hosts 
* Secrets 

## Tools 

* [FFUF](https://github.com/ffuf/ffuf)
* [Arjun](https://github.com/s0md3v/Arjun)
* [Gobuster](https://github.com/OJ/gobuster)
* [Trufflehog](https://github.com/trufflesecurity/trufflehog)
* [Shodan CLI](https://cli.shodan.io/)
* [Hakrawler](https://github.com/hakluke/hakrawler)
* httpx 
* Subfinder 
* [GF](https://github.com/tomnomnom/gf)

## Conclusion 

Recon can be one of the strongest tools in a hacker's tool belt and is a great way to discover hacking targets. 
