#!/bin/bash
# Mostrar la fecha y hora actuales
echo "Fecha y hora actuales:"
date

# Mostrar el espacio libre en disco
echo "Espacio libre en disco:"
df -h

# Mostrar los usuarios conectados
echo "Usuarios conectados actualmente:"
who
