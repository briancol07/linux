# Introduction to linux & linux + Certification 

## Index

* [## Linux Distributions ](#linux distros)
  * [### Common uses ](#uses)

## Linux Distributions <a name="linux distros"></a>

Linux distributions (Distros) are different versions of linux OS that bundle the linux kernel with other SW

* Ubuntu
  * General users
  * User-friendliness 
* CentOS
  * Red hat enterprise linux (RHEL)
  * Common in  enterprise environments 
  * Stability and support 
* Debian 
  * Servers & advanced users 
* Fedora
  * Cutting edge features & innovations
  * For Developers 
* Kali linux 
  * Cybersecurity 

### Common uses <a name="uses"></a>

* Servers 
  * Stable, secure and flexible 
* Embedded systems and IoT
  * Routers, Smart appliances, Cars, etc
* Software development & cybersecurity
  * Flexible and massive suite of tools 
* Cloud & Data centers 
  * AWS & google cloud 
  * DevOps & cloud architecture 
