# Web app technologies 2

## Data Interchange
 
* Data interchange refers to the process of exchanging data between different computer systems or applications, allowingthem to communicate and share information 
* It is fundamental aspect of modern computing, enabling interoperability and dat sharing between diverse systems, platforms and technologies 
* Data interchange involves the conversion of data from one format to another, making it compatible with the receiving system
* This enusres that data can be interpreted and utilized correctly by the recipient, regardless of the difference in their data structures, programming languages, or operating systems.

## Technologies 

* API : Application programming interfaces 
* JSON: Javascript Object notation 
* XML : Extensible markup language 
  * Configurations files 

## Protocols APIs

* REST: Representational state transfer 
  * Architectural stypes that uses Standard http methods 
    * GET POST PUT DELETE
* SOAP: Simple object access protocol 
  * Exchanging structured information in the implementation of web sevices 
    * XML

## Security technologies 

* Authentication 
  * Verifies the identity of users 
* Authorization 
  * Controls access to different parts of the web application 
* Encryption ( SSL/TSL ) ( Secure socket layer / Transport layer security ) 
  * Used to encrypt data transmitted between the client and the server 

## External technologies 

* Content delivery networks ( CDNs) 
  * Distribute static content to multiple servers 
    * Be able to fetch the information in the nearest location
    * Performance and reliability 
  * Cloudflare
* Third-party libraries and frameworks 

![web-app-architecture](./img/Web-app-Architecure.jpeg)

## Web pages are render 

![Render](./img/render.jpeg)

## Parse response 

![Parse-response](./img/Parse-response.jpeg)
