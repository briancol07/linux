# Common Threats 

## Threat Domains 

Robust security solution in place, but in order to protect themselves, organization frist need to know what vulnerabilities exist within their threat domains.
A 'threat Domain' is considered to be an area of control, authority or protection that attackers can exploit to gain access to a system. 

Attackers can exploit system within a domain through :

* Direct, physical access to asystems and networks 
* Wireless networking that extends beyond an organization's boundaries 
* Bluetooth or near-field communication (NFC) devices
* Malicious email attachments 
* Less secure elements within an organization's supply chain
* An organization's social media accounts 
* Removable media such as flash drives 
* Cloud-based applicaitons 

## Types of cyber Threats 

* SoftWare Attacks 
  * A successful denial-of-service (DoS attack)
  * A computer virus 
* Software errors 
  * Bug
  * An applicaiton going offline
  * A Cross-site scripts or illegal file server share
* Sabotage 
  * A backdoor, or worm that erases files 
  * An authorized user successfully penetrating and compromissing an organization's primary database 
  * The defacement of an organizaton's website
* Human Error 
  * Inadvertent data entry errors 
  * An employee dropping a laptop computer
* Theft 
  * Laptops or equipment being stolen from an unlocked room 
* Hardware failures 
  * Hard dreve crashes 
  * A Firewall misconfiguration 
* Utility interruption
  * Electrical power outages 
  * Water damage resulting from sprinkler failure 
* Natural disasters 
  * Severe storms such as hurricane or tornados 
  * Earthquakes 
  * Floods 
  * Fire

## Internal vs External Threats 

Threts cna originate from both within and outside of an organization, with attackers seeking access to valuable sensitive information such as personnel records, intellectual property and financial data 

### Internal threats 

Are usually carried out by current or former employees and other contract partners who accidentally or intentionally mishandle confidential data or htreaten the operations of sever or network infrastructure devices by connecting infected media or by accessing malicious emails or websites.

### External threat 

Typically stems from amateur or skilled attackers who can exploit vulnerabilities in networked devices or can use social enfineering techniques, such as trickery, to gain access to an organization's internal resources 


## User Threats and Vulnerabilities

A user domain includes anyone with access to an organization's information system, including employees, customers and contract partners. 
Users are often considered to be the weakest link in information security systems, posing a significant threat to the confidentiality integrity and availability of an organization's data .

### No awareness of security

Users must be aware of and understand an organization's sensitive data, security policies and procedures, technologies and countermeasures that are implemented in order to protect information and information systems 

### Poorly enforced security policies 

All users must be aware of and understand an organization's security policies, as well as the consequences of non-compliance.

### Data theft 

Data stolen by ysers can pose a significant ffinancial threat to organizations, both in terms of the resulting damage to their reputation and /or the legar liability associated with the disclosure of sensitive information.

### Unauthorized downloads and media 

Many network and devices infections and attacks can be traced bak to users who have downloaded aunauthorized emails, photos, music, games, apps and videos to their computers, networks or storage devices, or used unauthorized media such as external hard disks and USB drives 

### Unauthorized virtual private networkds (VPNs)

VPNs can hide the theft of uanauthorized information because the encryption normally used to protect confidentiality can stop a network administrator from tracking data transmission (unless htey have permission to do so).

### Unauthorized websites 

Accesing unauthorized websites can pose a risk to a user's data and device as well as the organization itself. Often, these website prompt users to download scripts or plugins that contain malicious code or adware. Some of these sites can even take over user devices like cameras and applications 

### Destruction of systems, applications or data

The accidental or deliberate destruction or sabotage of systems, applications and data poses a serious risk to all organizations. Activists, disgruntled employees or industry compettitors attempt to delete data and destroy or misconfigure devices to make organizational data and informaiton systems unavailable .

## Threats to devices 

* Any device left powered on an unattended pose the risk of someone gaining unauthorized access to network resources.
* Downloading files,photos,music or videos from unreliable sources could lead to the execution of malicious code on devices
* Cybercriminals often exploit security vulnerabilities within SW installed on an organization's device to launch an attack.
* An organization's information security teams must try to keep up to date with the daily discovery of new viruses, worms and other malware that pose a threat to their devices 
* Users who insert unauthorized USB drives,CDs or DVD run the risk of introducing malware, or compromising data stored on their device 
* Policies are in place to protect an organization's IT infrastructure. A user can face serious consequences for purposefully violating such policies.
* Using outdated HW or SW makes an organization's systems and data more vulnerable to attack 

## Threats to the Local Area Network 

The LAN is a collection of devices, Typically in the sam geographic area, connected by cable or airwaves 


* Examples of threats 
  * Unauthorized access to wiring closets, data centers and computer rooms 
  * Unauthorized access to systems, applications and data 
  * Network operating system or SW vulnerabilities and updates
  * Rogue users gaining unauthorized access to wireless networks 
  * Exploits of data in transit 
  * Having LAN servers with different HW or operating systems make managing and troubleshooting them more difficult. 
  * Unauthorized network probing and port scanning 
  * Misconfigured firewalls

## Threats to the private cloud

The private cloud domain includes any private servers, resources and IT infrastructure available to members of a single organization via the internet.

* Security Threats:
  * Unauthorized network probing and port scanning 
  * Unauthorized access to resources 
  * Router, firewall or network device operating system or SW vulnerabilities 
  * Router, firewall or network device configuration errors 
  * Remote users accessing an organization's infrastructure and downloading sensitive data.

## Threats to the public cloud 

The public cloud domain is the entirety of computing services hosted by a cloud, service or internet provider that are available to the public and shared across organizations.

### SW as a service (SaaS)

Thi is a subscription-based model that provides organizations with SW that is centrally hosted and accessed by users via web browser, app or other SW. In other words, this is SW not stored locally but in the cloud 

### Platform as a Service (Paas)

This subscription-based model provides a platform that allows an organization to develop, run and manage its applications on the service's hardware, using tools that the service provides. This platfomr is accessed via the public cloud.

### Infrastructure as a Service (IaaS)

This subscription-based model provides virtual computing resources such as HW, SW, servers storage and other infrastructure components over the internet. An organization will buy access to htem and use them via public cloud.


## Threats to Applications 

The application domain includes all of the critical systems, applications and data used by an organization to support operations. Increasingly, organizaitons are movin applications such as emails, security monitoring and database management to the public cloud 

* Common Threats 
  * Someone gaining unauthorized access to data centers, computer rooms, wiring closets or systems
  * Server downtime during maintenance periods
  * Network operating system SW vulnerabilities 
  * Data loss
  * Client-server on web applicaiton development vulnerabilities 

## Threat complexity 

SW vulnerabilities occur as a result of programming mistake, protocol vulnerabilities or system misconfigurations.

### Advanced persistent threat (APT) 

Is a continuous attack that uses elaborate espionage tactics involving multiple actors and/or sophisticated malware to gain access to and analyze a traget's network. 
Attackers operate under the radar and reamin undetected for a long period of time, with potentially devastating consequences. APTs typically target governments and high-level organizations and are usually well-orchestrated and well-funded.

### Algorithm attacks 

Take advantage of algorithms in a piece of legitimate SW to generate unintended behaviors. 

## Backdoors 

Such as netbus and Back Orifice, are used by cybercriminals to gain unauthorized access to a system by bypassing the normal authentication procedures.

Cybercriminals typically have authorized users unknowingly run a remote access Trojan horse program (RAT) on their machinte to install a backdoor that gives the criminal administrative control over a target computer. Backdoors grant cybercriminals continued access to a system, even if the organizaiton has fixed the original vulnerability used to attack the system.

## Rootkit 

This malware is designed to modify the operating system to create a backdoor, which attackers can then use to access the computer remotely.
Most rootkits take advantage of SW vulnerabilities to gain access to resources that normally shouldn't be accessible (privilege escalation) and modify system files .
Rootkits can also modify system forensics and monitoring tools, making them very hard to detect. In most cases, a computer infected by a rootkit has to be wiped and any required SW reinstalled.

## Threat Intelligence and Research Sources 

### The dark web 

This refers to encrypted web content that is not indexed by conventional search engines and requires specific SW, authorization or configuration to access. Expert researchers monitor the dark web for new threat intelligence.

### Indicator of compromise (IOC) 

IOCs such a malware signature or domain names provide evidence of security breaches and details about them.

### Automated Indicator Sharing (AIS)

Automated indicator Sharing (AIS), a Cybersecurity and Infrastructure Security Agency (CISA) capability enables the real-time exchange of cybersecurity threat indicators using a standardized and structured language called Structured Threat Information Expression (STIX) and trusted Automated Exchange of intelligence information (TAXII)

