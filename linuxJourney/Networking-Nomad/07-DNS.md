# DNS

* [## What is DNS? ](#dns)
* [## DNS Components ](#components)
  * [### Name server ](#name)
  * [### Zone file ](#zone)
  * [### Resource Record ](#resource)
* [## DNS Process ](#process)
  * [### Local DNS Server ](#local)
  * [### Root Servers ](#root)
  * [### Top-Level Domain ](#top)
  * [### Authoriative DNS Server ](#authoriative)
* [## /etc/hosts ](#hosts)
  * [### /etc/resolv.conf ](#etc)
* [## DNS setup ](#setup)
  * [### BIND ](#bind)
  * [### DNSmasq ](#masq)
  * [### PowerDNS ](#power)
* [## DNS Tools ](#tools)
  * [### nslookup ](#nslookup)
  * [### Dig ](#dig)
## What is DNS? <a name="dns"></a>

> Domain name system

Low level networking only understand the raw IP address to identify a host. DNS allows us humans to keep track of websites and hsots by name instead of an IP.

Is fundamentally a distributed database of hostnames to IP addresses, we manage our databe so people know how to get to our site/domain, and soemwhere else another person is managing their database so others can get to their domain.

These domains are then able to talk to each other and build a massive contact lsit of the internet

## DNS Components <a name="components"></a>

### Name server <a name="name"></a>

Setup via "name servers", the name servers load up our DNS settings and config and answers any question from clients or other servers that want to know things like is google.com?

If the name server doesn't know the answer to that query, it will redirect the request to another name servers.

* Authoritative 
* Recursive 

### Zone file <a name="zone"></a>

Inside name server. Zone files are how the name server stores information about the domain or how to get to the domain if it doesn't know.

### Resource Record <a name="resource"></a>

In the zone file full of entries of resource record. Each line is a record and contains information about hosts, nameservers, other resources, etc.

* Record name
* TTL : The time after which we discard the record and obtain a new one
* Class : Namespace of the record information, most commonly IN is used for internet
* Type: Type of information stored in the record data. 
  * A for address
  * MX or mail
  * exchanger 
* Data : Contain an IP address if it's an A record or something else depending on the record type 

## DNS Process <a name="process"></a>

### Local DNS Server <a name="local"></a>

First our host ask for the DNS, so it goes and starts from the top of the funnel to ask the Root Severs. 

### Root Servers <a name="root"></a>

There are 13 root servers for the internet, they are mirrored and distributed around the world to handle DNS request for the internet. 

Top-level domain are what you know as `.org`,`.com`,`.net`. If the page is not in these will ask to `.com`

### Top-Level Domain <a name="top"></a>

So now we send another request to the name server that knows about `.com` addresses and ask if knows the page. The TLD doesn't have the page in their zone file, but it does see a record for the name server . So it gives us the IP address of that name server and tell us to look there.

### Authoriative DNS Server <a name="authoriative"></a>

Now we send a final request to the DNS server thatactually has the record we want. The name server sees that it has a zone file for the page and there is a resource record for www for this host. It then give us the IP address of this host and we can finally see the page.

## /etc/hosts <a name="hosts"></a>

Before our machine actually hits DNS to do a query, it first looks locally on our machines

The `/etc/hosts` file contains mappings of some hostanmes to IP addresses. The fields are pretty self explanatory, there is one for the IP address, the hostname and then any alias's for the host

you can also manage access to hosts by modifying the `/etc/hosts.deny` or `/etc/hosts.allow` files or modifying your firewal rules instead .

### /etc/resolv.conf <a name="etc"></a>

To map DNS name server for more efficient lookup. However with the improvements made to DNS this file is quite often irrelevant.

## DNS setup <a name="setup"></a>

### BIND <a name="bind"></a>

> Berkeley Internet Name Domain

Most Popular DNS server on the interne, it's the standard that is used with linux distributions. 

* Full-featured power 
* Flexibility 

### DNSmasq <a name="masq"></a>

* Lightweight 
* Easier to configure 
* Simplicity 
* No all the bells and whistles of BIND
* All the tools to setup DHCP and DNS recommendend for a smaller network

### PowerDNS <a name="power"></a>

* FUll-featured and similar to BIND
* More Flexibility with options
* Read information from multiple databases 

## DNS Tools <a name="tools"></a>

### nslookup <a name="nslookup"></a>

> name server lookup

Is a tool used to query name servers to find information aboutresource records , example `nslookup www.google.com`

### Dig <a name="dig"></a>

> Domain information groper

Is a powerful tool for getting information about DNS name server, it is more flexible than nslookup and great for troubleshooting DNS issues. Example `dig www.google.com`




