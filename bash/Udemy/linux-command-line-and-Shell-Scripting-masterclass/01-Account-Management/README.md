# Account Management 

## Overview 

* Types of users 
  * Root (sudo)
  * Admin 
  * Regular User 
* What is a Root user? What are the privileges? 
* How do you log in as Root ? 
* Why should you not log in as Root?
  * What is Sudo? why use sudo instead of Root? 
* How do you lock/unlock root Access? 
* What is an Admin user in linux? What is the benfir of this role?
* What is a Regular user? What are the privileges of a regular user? 
* How do you give a regular user sudo privileges? 

### User management 

* Adding Users
* Adding Groups 
* where can you see the stored user and group information? 
* Adding user to groups 
* Adding users to sudo 
* Deleting users from groups 
* Deleting groups 
* Deleting users 
