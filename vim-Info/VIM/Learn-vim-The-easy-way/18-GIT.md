# GIT 

## Destacando diferencias 

`vimdiff` para mostrar las diferencias entre multiples archivos, otra manera es ` vim -d arch1.txt arch2.txt`.
Lo que hace es mostrar dos buffers uno al lado del otro y las diferencias aparecen resaltadas.

Para ir a la diferncia `]c` y para saltar a la previa `[c` y si ejecutas `:diffput` ambos archivos tendran lo mismo. Pero si usas `:diffget` reemplaza en el buffer actual la diferencia del otro 

## Vim como herramienta para la fusion ( merge ) 

```
git config merge.tool vimdiff
git config merge.conflictstyle diff3
git config mergetool.prompt false
```
Esto permite modificar el .gitconfig y poder usar otras herramientas de vim, usando `git mergetool`
Vim muestra 4 ventanas 

* `LOCAL` Este es el cambbio realizado en "local", en el que vas a fusionar 
* `BASE` Este es el ancestro comun entre `LOCAL` y `REMOTE` para comparar como difieren
* `REMOTE` Esto es lo que esta siendo fusionado 

* Para obtener los cambios de local `:diffget LOCAL`
* Los cambios de base `:diffget BASE`
* los cambios de remote `:diffget REMOTE`


## Git Dentro de vim 

No tiene una funcionalidad propia para trabajar con git, una manera de ejecutar es con el comando `!`

```
:!git status
:!git commit
:!git diff
:!git push origin master
```

Tambien puedes utilizar las convenciones der vim para el buffer actual o otro 

```
:!git add %         " git añade el archivo actual
:!git checkout #    " git hace un *checkout* para el archivo alternativo
```

Un truco de vim que puedes utilizar para aniadir multiples archivos en un ventana diferente 

```
:windo !git add %

```

Es parecido al comando `argdo`


## Plugins 

* vim-fugitive 
* vim-signify 
* vim gitgutter
