# Web Application Architecture 

* Refers to the structure and organization of components and technologies used to build a new application
* It defines how different parts of the application interact with each other to deliver its functionality, handle requests and manage data 
* Crucial for ensuring scalability, maintainability and security 


## Client-Server model

### Client 

The client represents the user interface and user interaction with the web application. It is the front-end of the application that users access through their web browsers .
The client is responsible for displaying the web pages, handling user input and sending request to the server for data or actiosn 

### Server

THe server represents the back-end of the web application. It processes client request, executes the applications's business logic, communicates with databases and other services and generate responses 

## Client-side processing 

* Involves executing tasks and computations on the use's device, typically within their web browser 
* Has some limitations. It is not suitable for handlingsensitive or critical operations, as it can be easily manipulated by users or malicious actors 
* XSS 

### Key characteristics 

* User interaction : Taks that require immediate user interaction and feedback, no need to send data to the server
* Responisve user experience 
* Javascript 
* Data validation: ensures that user input meets specific criteria before senidn to the server 

## Server side Processing 

* Involves executing tasks and computations on the web server. Remote computer is 
* Refers to the backend of the web application, where the business logic and data processing take place 

### Key characteristics 

* Data Processing: for tasks that involve sensitive data handling complex computations and interactions with databases or external services.
* Security: More secure than client-side 
* Languages : Php, java, Python, Ruby 
* Data Storage: Enables secure storage and management ofsensitive data in ddbb

## Communications & data flow 

* Communicates over the internet using HTTP ( Hypertext transfer protocol )
  * When user interacts send http request to the server
* The server processes the requests, interacts with the database if nexessary, performs the required actions, and generates an HTTP response
* Response sent back to the client which renders the content 
