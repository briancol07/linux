function! MyPopup()
    cal popup_clear()
    call popup_create('This is a floating window!',{'close':'button','border':[]})
endfunction

call MyPopup()

