# A penetration tester’s guide to subdomain enumeration

* [ guide ](https://medium.com/@0xbharath/a-penetration-testers-guide-to-sub-domain-enumeration-7d842d5570f6)

Things you have to find when you are doing the assestment:

* Servers 
* Web applications 
* Domains 
  * Subdomains 

* [subDomain-bugcrow](https://github.com/appsecco/bugcrowd-levelup-subdomain-enumeration)
* [0xffsec](https://0xffsec.com/handbook/information-gathering/subdomain-enumeration/)

## What is sub-domain enumaration? 

Is the process of finding sub-domains for one or more domains.

> Reconnaissance phase 

1. Reveal a lot of domains/sub-domains that are in scope. Increase chances of finding vulnerabilities 
2. Finding applications running on hidden, forgotten sub-domains may lead to unvocering critical vulnerabilities 
3. The same vulnerability tend to be present across differnte domains/applications of the same organization 

## Techniques 

### Google/bing

Use some wildcards to find them in google search 

```
site: <site-name>
site:*.wikimeida.org
```


### Third party service 

Some of them aggregate massive DNS datasets and look throught them to retrieve sub-domains for a given domain

* Virus total
* DNSdumpster 
* OWASP Amass 

## Certificate Transparency (CT)

[Blog](https://blog.appsecco.com/certificate-transparency-the-bright-side-and-the-dark-side-8aa47d9a6616)

Is a project uner which a certificate authority (CA) hasto publish every SSL/TLS certificate they issue to a public log . 

* This usually contains: 
  * Domain names 
  * Sub-domain names
  * Email addresses 


Use search engines that collect the CT logs 

* https://crt.sh/
* https://censys.io/
* https://developers.facebook.com/tools/ct/
* https://google.com/transparencyreport/https/ct/

Some names could not exist anymore. you could use [massdns](https://github.com/blechschmidt/massdns)

## Dictionary based enumaration 

To find subdomains with generic names [dnsrecon](https://github.com/darkoperator/dnsrecon)

## Permutation scanning 

In this technique, we identify new  sub-domains using permutations, alterations and mutations of already known domain/sub-domains 

* Tool [altdn](https://github.com/infosec-au/altdns)

## Autonomous System (AS) numbers 

Helpful to identify netblock belonging to an organization which in turns may have valid domains 

[nmap](https://nmap.org/nsedoc/scripts/targets-asn.html)

## Zone transfer 

Is a type of DNS Transaction where a DNS server passes a copy of full or part of it's zone file to another DNS server. If is not securely configured someone can obtain data. 

## DNSSEC

[LDNS](https://www.nlnetlabs.nl/projects/ldns/about/)

Some DNSSEC zones use NSEC3 records which uses hashed domain names to prevent attackers from gathering the plain text domain names

* Tools:
  * [nsec3walker](https://dnscurve.org/nsec3walker.html)
  * [nsec3map](https://github.com/anonion0/nsec3map)

## Data sets 

Although finding sub-domains in this massive datasets is like finding a needle in the haystack, it is worth the effort.


## Content Security Policy (CSP) 

Defines the content-security-policy HTTP header. which allows you to create a whitelist of sources of trusted content, and instructs the browser to only execute or render resources from those sources


