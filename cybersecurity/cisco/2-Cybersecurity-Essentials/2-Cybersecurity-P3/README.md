# Module 2 Cybersecurity P3 ( Principles, Practices and Processes)

Focuses on the principles, practices and processes that can maintain robust cybersecurity measures.

Every organization is a risk of a cyber attack but there are many ways to keep systems and data protected. 

## Overview 

* [1-The-Three-Dimensions.md](./1-The-Three-Dimensions.md)
* [2-States-of-Data.md](./2-States-of-Data.md)
* [3-Cybersecurity-Countermeasures.md](./3-Cybersecurity-Countermeasures.md)
* [4-Access-Controls.md](./4-Access-Controls.md)
* [5-Cryptography.md](./5-Cryptography.md)
* [6-Hashing.md](./6-Hashing.md)
* [7-Obscuring-Data.md](./7-Obscuring-Data.md)

