#!/bin/bash

#Crear una funcion que reciba tres numeros como argumentos y devuelva el mayor de los tres

mayor() {
  if [ $1 -gt $2 ]; then
    if [ $1 -gt $3 ]; then
      echo "el mayor es $1"
    else 
      echo "el mayor es $3"
    fi
  else 
    if [ $2 -gt $3 ]; then
      echo "el mayor es $2"
    else 
      echo "el mayor es $3"
    fi
  fi
}

echo $(mayor 1 2 3)
echo $(mayor 2 4 3)
echo $(mayor 7 2 3)
