# Hacker One

## Links 

* [H1](https://www.hackerone.com/)
  * [CTF](https://ctf.hacker101.com/howtoplay)
* [YT Tutorial](https://www.youtube.com/watch?v=zPYfT9azdK8&list=PLxhvVyxYRviZd1oEA9nmnilY3PhVrt4nj&index=2)
* [How to start bug bounty](https://www.youtube.com/watch?v=Ukv85Dx0GhA)
* [InfoSec-Write-UPs](https://infosecwriteups.com/how-to-get-started-into-bug-bounty-1be52b3064e0)

### Road Maps 

* [Hacking-notes](https://github.com/Hacking-Notes/Hacker-Roadmap)

### Recon 

* HTTPx (list domains)
  * [Github](https://github.com/projectdiscovery/httpx)
  * [Python](https://www.python-httpx.org/)
* CRT
  * [Certificate Search](https://crt.sh/)
    * you can put input=json to see it as a json 

### Bootcamps 

* [Zseano](https://www.bugbountyhunter.com/)

### Youtubers 

* [Zseano](https://www.youtube.com/@zseano)
* [Nahamsec](https://www.youtube.com/watch?v=9vaEwycet90)

## Proyects 

* Recon with Bash
