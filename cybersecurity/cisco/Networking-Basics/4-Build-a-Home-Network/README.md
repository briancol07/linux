# Build a Home Network 

## What will I learn in this module ? 

> Configure an integrated wireless router and wireless cliet to connect scurely to the internet 

Topic Title | Topic Objective 
------------|-----------------
Home Network Basics | Describe th component required to build a home network
Network Technologies in the home | Describe wired and wireless network technologies 
Wireless Standards | Describe Wi-Fi 
Set up a home router | Configure wireless devices for secure communications 


## Topics 


* [1-Home-Network\_basics.md](./1-Home-Network\_basics.md)
* [2-Network-Technologies-in-the-home.md](./2-Network-Technologies-in-the-home.md)
* [3-Wireless-Standards.md](./3-Wireless-Standards.md)
* [4-Set-up-Home-Router.md](./4-Set-up-Home-Router.md)
