# Escaneo de redes con bash 

## Intro a nmap 

> nmap ( Network Mapper )

Es una herramienta mas utilizadas para el escaneo de redes. Permite detectar dispositivos conectados a una red, identificar servicio que estan en ejecucion, puertos abiertos, ssitemas operativos y mas 

## Instalar 

```
sudo apt install namp 

sudo yum install nmap 
```

```
# ver version 
namp --version
```

## Escaneo basico de red 

Este comando realiza un ping scan para detectar los dispositivos activos en la red sin escanear puertos 

```
nmap -sn <Red>
```
### Escanear puertos en un dispositivo especifico 

```
nmap <red>
```

## Automatizacion del escaneo

```
#!/bin/bash

# Dirección de la red que se desea escanear
red="192.168.1.0/24"

# Archivo de salida para el informe
archivo_salida="informe_escaneo_$(date +%Y%m%d_%H%M%S).log"

# Realizar el escaneo de red
echo "Iniciando escaneo de red en $red..."
nmap -sn $red > $archivo_salida

# Informar al usuario
echo "Escaneo completado. Los resultados se han guardado en $archivo_salida"
```

## Deteccion de dispositivos en la red 

```
#!/bin/bash

red="192.168.1.0/24"
archivo_salida="informe_dispositivos_$(date +%Y%m%d_%H%M%S).log"

echo "Escaneando dispositivos activos en la red $red..."
nmap -sn $red | grep "Nmap scan report for" | awk '{print $5}' > $archivo_salida

echo "Dispositivos activos:"
cat $archivo_salida
```

## Generacion de informes 

```
#!/bin/bash

# Red a escanear
red="192.168.1.0/24"

# Archivo de salida para el informe
archivo_salida="informe_detallado_$(date +%Y%m%d_%H%M%S).log"

# Escanear dispositivos activos y guardar la salida detallada
echo "Iniciando escaneo detallado en $red..."
nmap -A $red > $archivo_salida

# Informar al usuario
echo "Escaneo completado. El informe detallado se ha guardado en $archivo_salida"
```

El parametro -A realiza un escaneo avanzado que incluye 
* Deteccion del sistema operativo
* Deteccion de servicios y versiones
* Escaneo de puertos 

## Tarea 

1. Crea un script que realice un escaneo de red semanal y genere un informe con detalles sobre puertos abierto y servicios activos en cada dispositivo
2. Programa el script para que se ejecute automaticamente cada lunes a las 5 am y almacene los informes en un directorio especifico 
