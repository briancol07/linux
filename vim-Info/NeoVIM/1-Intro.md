# Introduccion 

Editor de texto 

Personal development environment 

## Instalar neovim 

```bash 
sudo smap install nvim
```

## Modos 

> Vim/Neovim trabaja con modos 

1. Insert
  * Para escribir 
2. Normal
  * ESC 
3. Command 


## Formas de salir 

> Siempre desde el modo normal 

1. :wq! 
  * Salir sin escribir
2. :w nombre\_archivo
3. ZZ  

