# Intro to MySQL 

## Structured Query language ( SQL )

Can Differ from one RDBMS to another but is required to follow [ISO-standard](https://en.wikipedia.org/wiki/ISO/IEC_9075) 

* SQL can 
  * Retrieve data 
  * Upgrade data 
  * Delete data 
  * Create new tables and databases 
  * Add / remove users 
  * Assign permissions to these users 

## Command line 

> SQL statement are not case sensitive, but the database name is case sensitive 

```
mysql -u root -p
```

* -u flag is used to supply the username
* -p flag is for password 

> Tip: There shouldn't be any spaces between '-p' and the password.

To see privileges we can use `SHOW GRANTS` 

* With 
  * -h we can specify host 
  * -P for port 

## Creating Database 

Use the command `CREATE DATABASE`, all inside mysql or mariadb

```
CREATE DATABASE users;
```

* To see databases we can use `SHOW DATABASES`
* To swith database we use the command `USE <databaseName>` 

## Tables 

DBMS stores data in the form of tables ( Rows and columns ) The intersection is called a cell. Fixed set of columns, wher each column is of a particular data type.

* Data Types 
  * numbers
  * Strings 
  * Date 
  * Time 
  * Binary data 
  * [more](https://dev.mysql.com/doc/refman/8.0/en/data-types.html)

### Create table 

```
CREATE TABLE logins (
    id INT,
    username VARCHAR(100),
    password VARCHAR(100),
    date_of_joining DATETIME
    );
```

To show tables we can use `SHOW TABLES` or `DESCRIBE <TABLE>` that is used to list the table structure with its fields and data types 

### Tables properties 

* [SOME Properties](https://dev.mysql.com/doc/refman/8.0/en/create-table.html)
  * `AUTO_INCREMENT` Increment the value when a new item is added 
  * `NOT NULL` 
  * `UNIQUE` example: `username VARCHAR(100) UNIQUE NOT NULL,`
  * `DEFAULT` example: `date_of_joining DATETIME DEFAULT NOW()`

> PRIMARY KEY : Uniquely identify each record in the table, Referring to all data of a record within a table for relational databases

```
CREATE TABLE logins (
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(100) UNIQUE NOT NULL,
    password VARCHAR(100) NOT NULL,
    date_of_joining DATETIME DEFAULT NOW(),
    PRIMARY KEY (id)
    );
```

```
para Hackthebox mysql -h <host> -P <port> -u <username> -p<password>
```
