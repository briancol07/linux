# Modelo OSi

Nos permite entender como surgen las comunicaciones atravez del hardware y como va transitando atravez de cada capa

## Topics 

* [Capa1 - Fisica](./01-Capa-Fisica.md)
* [Capa2 - Enlace de datos ](./02-Capa-Enlace-de-Datos.md)
* [Capa3 - Red ](./03-Capa-Red.md)
* [Capa4 - Transporte](./04-Capa-Transporte.md)
* [Capa5 - Sesion](./05-Capa-Sesion.md)
* [Capa6 - Presentacion](./06-Capa-Presentacion.md)
* [Capa7 - Aplicacion](./07-Capa-Aplicacion.md)


