# Nmap scripts NSE

## Que es el Nmap scripting engine (NSE)? <a name="que"></a>

Es una de las caracteristicas mas poderosas de nmap, permite automatizar tareas complejas de enumeracion y explotacion mediante scritps.

* Categorias 
  * Auth (autentication) : Scripts que prueban mecanismos de autenticacion en diferentes servicios 
  * Vuln (Vulnerabilidad) : Scripts diseniados para detectar vulnerabilidades especificas en servicios comunes 
  * Malware : Detectar comportamientos sospechosos o malware conocido en servicios
  * Discovery: Ayudan a descubrir informacion adicional sobre servicios, configuraciones y versiones 

## NSE para enumeracion <a name="enumeracion"></a>

Comando basico `nmap -script`, ejemplo : con servidos SMB , `nmap -script smb-vuln-ms17-010` este comando ejecuta el script que detecta si el sistema es vulnerable al exploit MS17-010 utilizado en ataques como wannacry 

* Script comunes 
  * http-enum
    * Permiete enumerar posibles directorios y archivos en un servidor web 
    * `nmap -script http-enum`
  * ftp-anon
    * Comprueba si el servidor FTP permite el acceso anonimo, una configuracion insegura que puede permitir el acceso a archivos confidenciales sin necesidad de autenticacion 
    * `nmap -script ftp-anon`
  * smb-os-discovery
    * Detecta la version del sistema operativo a traves del servicio SMB.
    * `nmap -script smb-os-discovery`

