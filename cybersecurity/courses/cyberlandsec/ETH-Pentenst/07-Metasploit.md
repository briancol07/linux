# Metasploit

## Index 

* [## Que es metasploit? ](#que)
* [## Arquitectura ](#arquitectura)
* [## Interfaz ](#interfaz)
* [## Uso Basico ](#uso)
* [## Modulos Auxiliares ](#modulos)
* [## Hacking etico ](#eth)

## Que es metasploit? <a name="que"></a>

Es un framework de codigo abierto utilizado para realizar pruebas de penetracion y explotacion de vulnerabilidades. 

* Instalacion linux : `sudo apt-get install metasploit-framework`

## Arquitectura <a name="arquitectura"></a>

* Exploits : Codigo que aprovecha una vulnerabilidad en un sistema
* Payloads : Codigo que se ejecuta en el sistema objetivo despues de una explotacion exitosa 
* Encoders : Utilizados para codificar payloads y evitar la deteccion por antivirus 
* Auxiliares: Modulos que ayudan en tareas como la explotacion de redes y escaneo de peurtos 

## Interfaz <a name="interfaz"></a>

* GUI : Armitage o Consola 
  * msfconsole

## Uso Basico <a name="uso"></a>

* Buscar un exploit `search`
  * Ejemplo `search ftp`
* Seleccionar un exploit `use`
* Configurar opciones `show options` 
  * Ejemplo `set RHOST` 
* Configurar un Payload `set PAYLOAD`
* Lanzar el exploit `exploit`

## Modulos Auxiliares <a name="modulos"></a>

* Escaneo de puertos `use auxiliary/scanner/portscan/tcp`
* Captura de credenciales 

## Hacking etico <a name="eth"></a>

* Pruebas de vulnerabilidades conocidas
* Validacion de Parcheado 
* Simulacion de ataques 



