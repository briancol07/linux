# User management 

## Index

* [## root ](#root)
* [## /etc/passwd ](#etc)
* [## /etc/shadow ](#shadow)
* [## /etc/group ](#groupp)
* [## User Management Tools ](#tools)

Each user has their own home directory where their user specific files get stored . Located in /home/username (can vary in different distributions) 

The system uses user ids(UID).Usernames are the friendly way to associate users with identification, but the system identifies by their UID. 

Groups are just sets of users with permissionset by that group. They are identified by the system with their group ID(GID) 

Linux have a superuser su , that use sudo (superuser do) is used to run a command with root access 

```bash 
cat /etc/shadow
ls -la /etc/shadow
sudo cat /etc/shadow
```
ls -la shows the permissions of each files 

## root <a name="root"></a>

This command will "substitute users" and open a root shell if no username is specified 
You can use this command (su) to substitute to any user as long as you know the password 
The system doesn't let every single run commands as the superuser. There is a file called /etc/sudoers , that lists the users who can run sudo. you can edit this file with the visudo command.

## /etc/passwd <a name="etc"></a>

> To find out what users are mapped to what ID, look at /etc/passwd

this file shows you a list of users and detailed information about them (root:x:0:0:root:/root:/bin/bash) 

There are many fields separated by colons that tell you additional information about the user. 

1. Username 
2. User's password ( encrypted user password) 
  * x mean that is stored in /etc/shadow 
  * * mean that the user doesn't have login access 
  * if there is a blank , the user dosen't have password 
3. The user ID
4. The group ID 
5. GECOS field 
6. User's home directory 
7. User's shell 

if you want to add an user and modify information you should do it with the vipw tool .

## /etc/shadow <a name="shadow"></a>

> To store information about user authentication , it requires superuser read permissions (sudo cat /etc/shadow) 

Information similar to the last one 

1. Username 
2. Encrypted password
3. Date of last password change
  * if there is a 0 that mean the user should change password next time they login 
4. Minimun password age 
  * Days that a user will have to wait before being able to change their password again
5. Maximum password age 
  *  Maximum number of days before a user has to change their password
6. Password warning period 
  *  Number of days before a password is going to expire
7. Password inactivity period 
  * Number of days after a password has expired to allow login with their password
8. Account expiration date 
  *  Date that user will not be able to login
9. Reserved field for future use

> In most distributions today, user authentication doesn't rely on just the /etc/shadow file, there are other mechanisms in place such as PAM (Pluggable Authentication Modules) that replace authentication.

## /etc/group <a name="groupp"></a>

This file allows for different groups with different permissions (cat /etc/group)

You should find: 

1. Group name 
2. Group password 
  * No need to set password 
  * * will be put in place as the default value 
3. Group ID (GID) 
4. List of users 
  * You can manually specify users you want in a specific group .

## User Management Tools <a name="tools"></a>

* Adding users 
  * sudo useradd name 
* Removing Users 
  * sudo userdel name 
* Changing Passwords 
  * passwd name 
