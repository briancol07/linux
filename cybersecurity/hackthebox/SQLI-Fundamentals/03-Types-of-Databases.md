# Types of Databases 

* Relational Databases 
* Non-Relational Databases 

## Relational Databases 

Most common type of database. It uses a schema, a template to dictate the data structure stored in the database. 
Tables in a relational database are associated with keys that provide a quick database summary or access to the specific row or column when specific data needs to be reviewed ( Also called entities ) Related to each other. 

When processing an integrated database, to link one table to another we use a relational database management system (RDBMS) 

> The relationship between tables within a database is called a Schema 

* Example MySQL

## Non-Relational Databases 

Also called as NoSQL does not use tables, rows and columns or prime keys relationships or schemas. Instead stores data using various storage models, depending on the type of data stored. 

* Scalabe 
* Flexible 
* Example MongoDB

* Models
  * Key-Value
    * Data in JSON or XML
  * Document-based
  * Wide-column
  * Graph

