# Common Threats & Risks 

* Web apps are constantly exposed to various security Threats and risks due to their widespread accessibility and the valuable data they process 
* Understanding threats and organization to implement effective measures and safeguard their web application 
* The impact and severity may vary depending on the specific web app and its security measures 
* Require a a proactive an compehensive approach to mitigate these threats and protect sensitive data and user interactions effectively 

## Threat 

* Refers to any potential source of harm or adverse event that may exploit a vulnerability in a system or organization's security measures 
* Can be human-made such as cybercriminals , hacker or insiders with malicious intent, or they can be natural such as floods, earthquakes or power outages 
* Can include various types of attacks 
  * Malware infections 
  * Phishing 
  * Denial-of-service attack 
  * Data breaches 

## Risk 

* POtential for a loss or harm resulting from a threat exploiting a vulnerability in a system or organization 
* It is a combination of the likelihood or probability of a threat occurence and the impact of severity of the resulting adverse event 
* Measured in terms of the likelihood of an incident happening and the potential magnitud of its impact 

### Summary 

* Threat is a potential danger or harmful event, risk is the probability and potential impact of that event happening 
* Threats can exits but they may or may not pose a significant risk depending on hte vulnerabilities and security measures in place to mitigate them 

Threat/risk | Description 
------------|-----------
Cross-site scripting (XSS) | Inject malicious script into web pages viewed by other users, leading to anauthorized access to user data, session, hijacking and browser manipulation
SQL injection (SQLi) | Attacker manipulate user input to inject malicious SQL code  into the application's database, leading to unauthorized data access data manipulation or database compromise 
Cross-site request forgery (CSRF) | Attacker trick authenticated users into unknowingly performing action on a web application, such as changing account details by exploiting their active sessions
Security Misconfiguration | improperly configured servers databases or applications frameworks can expose sensitive data or provide entry points for attackers 
Sensitive data Exposure | Failure to adequately protect sensitive data, such as passwords or personal information can leadto data breaches and identity theft 
Brute-Force and credential stuffing attacks | Attackers use automated tools to guess usernames and passwords, attempting to gain anauthorized access to user accounts 
File upload Vulnerabilities | insecure file upload mechanisms can enable attackers to upload malicious files, leading to remote code execution or unauthorized access to the server 
Denial of service (DoS) and Distributed Dos (DDoS ) DoS and DDoS attacks aim to overwhelm web applicationserver , causing service disruption and denying legitimate user access 
Server-Side request forgery (SSRF) | Attacker use ssrf to make requests from the server to internal resources or external networks potentially leading to dat theft or unauthorized access
Inadequate access controls | weak access controls may allow unauthorized users to access restircted functionaliites or sensitive data 
Using compnonets with known vulnerabilities | integrating third-party compononents iwht nown security flaws can introduce weaknesses itno the web application 
Broken access control | inadequate access controls can allow unauthorized users to access restricted funtionalities or sensitive data
