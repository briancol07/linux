# Adding and deleting group 

## Create a new group 

> You have to be root 

```bash 
 
groupadd groupName
```

## To see groups 

```bash 

cat /etc/group
```

## Add someone to a group

```bash 
usermod -a -G groupName userName
```

## Delete user from a group 

```bash 
gpasswd -d userName groupName
``` 

## Delete group 

```bash 

groupdel groupName
```
