# System Configuration

## Index

* [## System Settings ](#settings)
* [## Gnome-tweaks ](#teaks)
* [## Display setting ](#display)
* [## Network configuration ](#network)
* [## Installing and updating SW ](#update)
* [## Debian Packaging ](#d-packaging)
* [## Red Hat package manager (RPM) ](#rpm)
* [## open SUSE's YaST SW managemente ](#yast)

## System Settings <a name="settings"></a>

Allow you to control most of the basic configurations options and desktop settings, such as specifying the screen resolution, managing network connections or changing the date and time of the system.

## Gnome-tweaks <a name="teaks"></a>

Lunch a tool called gnome-tweaks, you can select theme, configuring extensions which you can get from your distribution or download from the internet, control fonts, modify the keyboard layout and set which programs start when you login 

## Display setting <a name="display"></a>

Change desktop appearance. On systems utilizing the x window system, the server which actually provides the GUI, uses `/etc/x11/xorg.conf` as its configuration file.

## Network configuration <a name="network"></a>

All linux distribution have network configuration files. Network manager was develop to make things easir and more uniform across ditributions.

## Installing and updating SW <a name="update"></a>

Each package in a linux distribution provides one piece of the system, such as the linux kernel the ccompiler, utilities for manipulating text or configuring the network.

## Debian Packaging <a name="d-packaging"></a>

dpkg is the underlying package manager for these systems. It does not automatically download and install packages and satisfy their dependencies

The higher-level package management system is teh advanced package tool (APT) system of utilities.

## Red Hat package manager (RPM) <a name="rpm"></a>

The higher-level package manager differs between distributions.

* dnf 
  * fedora
  * RHEL
  * CentOS
* Zypper interface 
  * SUSE 
  * OpenSUSE

## open SUSE's YaST SW managemente <a name="yast"></a>

> Yet another Setup Tool 

Similar to other graphical package manager. It is RPM based application .

