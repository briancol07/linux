# What is The Shell? 


The command line is the shell, The sell is a program that takes keyboard commands and passes the mto the operating system to carry out. 

> Bash = bourn again shell 

```bash 

[me@linuxbox ~]$
``` 
This is the shell prompt and it will appear whenever the shell is ready to accept input.

* Ends by 
  * # superuser 
  * $ Normal user 

## Command History 

It remember the las 500 commands by default.Press the down-arrow key and the previous command disappear

## Cursor Movement 

* To see previous command up-arrow 
* to move anywhere in the command line left/right arrow  
* Ctrl-v or Ctrl-c , do not work here 


## Try some Simple Commands 

command | Output
--------|--------
date    | Displays the current time and date  
cal     | Displays a calendar of the current month 
dg      | To see the current amount of free space on disk drives 
free    | Display the amout of free memory  
exit    | End a terminal session  


### output: 
```
lun 10 may 2021 19:30:56 -03

     Mayo 2021        
do lu ma mi ju vi sá  
                   1  
 2  3  4  5  6  7  8  
 9 _1_0 11 12 13 14 15  
16 17 18 19 20 21 22  
23 24 25 26 27 28 29  
30 31                 
```
## The console behind the curtain 

> Several terminal sessions continue to run behind the graphical desktop.Can be accessed by pressing ctrl-alt-F1 through ctrl-alt-F6, To return to graphical desktop press alt-F7

### Extra 

> In vim (inside) you can use :r !\<coomand\> to redirect the command to vim 

