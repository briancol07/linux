#!/bin/bash

calcular_suma() {
    local suma=$(( $1 + $2 ))
    echo "La suma de $1 y $2 es: $suma"
}

multiplicar() {
    echo $(( $1 * $2 ))
}
