# Networking Topologies 

> The network topology determines the components to be used and the access methods to the transmission media 

* Physical 
* Logical 

Computers = Hosts ( Clients / servers ) they also include network components such as switches, bridges and routers. 

The transmission medium layout uses to conenct devices in the physical topology of the network. The position of the nodes, and connections between the nodes and the cabling. In contrast, the logical topology  is how the signals act on the network media or how data will be transmitted accross the network from one device to the devices. 

### Connections <a name="connections"></a>

Wired Connections | Wireless connections 
:----------------:|:-------------------:
coaxial cabling | wifi 
glass fiber cabling | cellular 
twisted-pair cabling | satelite 


### Nodes <a name="nodes"></a>

Repeater| Hubs | bridges  
:------:|:----:|:------:
Router/modem | Gateways | Firewalls 

* Nodes are the transmission medium's connection points to transmitters and receivers of electrical, optical or radio signals in the medium. 
* A node may be connected to a computer, but certain types may have only one microcontroller on a node or may have no prgrammable device at all. 

## Clasifications <a name="clasifications"></a>

> Topology as Virtual form or structure of a network

Can be physical or logical

* Point-to-Point
* Star 
* Mesh 
* Hybrid 
* Bus 
* Ring 
* Tree 
* Daisy Chain 
* ETC

### Point-to-Point <a name="ptp"></a>

The simplest with dedicated connection between two host. In this topology, a direct and straighforward physical link exists only between two host they can use it for mutual communication 

![ptp](./img/ptp.png)

### Bus <a name="bus"></a>

All host are connected via a transmission medium. Every host has access to the transmission medium and the signals that are transmitted over it. 
There is no central network component htat controls the processes on it. 

Only one host can send and all the others cna only receive and evaluate the data and see whether it is itended ofr itself 

![bus](./img/bus.png)

### star <a name="star"></a>

Network component that maintains a connection to all hosts. Each host is connected to the central network component via separate link. These handle the forwading function for the data packets.
The data traffic on the central network component can be very high since all data and connections go through it 

![star](./img/star.png)

### Ring <a name="ring"></a>

The physical ring topology is such that each host or node is connected to the ring with two cables :

* One for incoming signal
* Other for the outgoing ones 

Does not require an active network component. The control and access to the transmission medium are regulated by a protocol to which all station adhere 

A logical ring topoloty is based on a physical star topology, where a distributor at the node simulates the ring by fowarding one port to the next.

Uses a retrieval system from the central station or a token. A token is a bit pattern that continually passes through a ring network in one direction. Which works according to the claim token process 

![ring](./img/ring.png)

### Mesh <a name="mesh"></a>

This have no fixed topology, there are two basic strucutres from the basic concept: the fully meshed  and the partially messhed structure 

* Fully meshed strucutre 
  * Each host is connected to eveyr other host in the network 
  * WAN or MAN ( High reliability and bandwidth ) 
* Partially meshed structure 
  * The endpoints are connected by only one connection 
  * Specific nodes are connected to exactly on other node and some other nodes are connected to two o more other nodes wihtpoint-to-point connection 

![mesh](./img/mesh.png)

### Tree <a name="tree"></a>

Extended star topology that more extensive local networks have in this structure. This is especially useful when several topologies are combined. 

* Used in
  * Broadband networks 
  * MAN

![tree](./img/tree.png)

### Hybrid <a name="hybrid"></a>

Combines Two topologies so does not represent any standard topologies.

![hybrid](./img/hybrid/png)

### Daisy Chain <a name="daisy"></a>

Multiple hosts are connected by placing a able from one node to another, This create a chain of connections, it is also knwon as a daisy-chain configuration in which multiple hardware compononents are connected in a series. 
This type of networking is often found in automation technology ( CAN ).

* Based on the physical arrangement of the nodes

![daisy](./img/daisy.png)
