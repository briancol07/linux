# Certifies Web applications Pentester ( CWAP )

## Index 

* [01-Intro-owasp.md](./01-Intro-owasp.md)
* [02-Vulnerabilidades-inyeccion.md](./02-Vulnerabilidades-inyeccion.md)
* [03-Configuraciones-incorrectas.md](./03-Configuraciones-incorrectas.md)
* [04-Pruebas-pentest-web.md](./04-Pruebas-pentest-web.md)
* [05-Herramientras.md](./05-Herramientras.md)
* [06-Automatizacion-Pruebas.md](./06-Automatizacion-Pruebas.md)
* [07-Autenticacion-basada-roles.md](./07-Autenticacion-basada-roles.md)
* [08-Autenticacion-multifactor.md](./08-Autenticacion-multifactor.md)
* [09-Gestion-Segura-sesiones.md](./09-Gestion-Segura-sesiones.md)
* [10-Proteccion-contra-SQL.md](./10-Proteccion-contra-SQL.md)
* [11-Proteccion-contra-XSS.md](./11-Proteccion-contra-XSS.md)
* [12-Prevencion-CSRF.md](./12-Prevencion-CSRF.md)
* [cert-CWAP.pdf](./cert-CWAP.pdf)
* [Example-Pentest-Report.pdf](./Example-Pentest-Report.pdf)


