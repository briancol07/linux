# Compensating controls

* Like layers 
  * CLient-side validation 
  * Cookies flags 
  * Web application firewall (waf)
  * Content security policy (CSP)
  * Server-side validation 
  * Output encoding

Compensating controsl play a vital role in thwarting attackers attempting clien-side injection attacks and the execution of malicious js. These controls act as secondary security measures to mitigate the impact of a potential breach of vulnerability. In the context of client sanitization mechanisms. 
By thoroughly validating and cleasing user inputs on the client-side, organizations cna prevent unauthoraized code execution and manipulation of the DOM. Additionaly, employing content security policy (CSP) can restrict the sources from which resources inclundg scritps can be loaded further mitigating the risk of succesful client-side injection attacks. 
These compensating controls collectively strengthen the overall security posture, limiting the atack surface and fortifyng web applications against potentioal exploits. 
