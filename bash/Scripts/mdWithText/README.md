# Md with text 

## Index 

* [## Example ](#example)
* [## Something to add ](#something)

In this script I will create a file with text inside that will be an md 

## Example <a name="example"></a>

```
mdtext 04-fileName
```

This will create a file called `04-fileName.md` with a text inside `# fileName`

## Something to add <a name="something"></a>

One modification that I will do to the file is to be able to write something else instead of the name of the file 

Like if there is any extra flag, will write the second text instead of the name of file 

```
mdtext 04-fileName text
```

* Result in the file 
  * `# text`

