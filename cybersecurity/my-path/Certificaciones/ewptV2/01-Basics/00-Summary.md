## HTTP (Hypertext Transfer Protocol):

* Request-Response Model:
  * Client-Server Communication: It operates on a client-server model. The client (usually a web browser) sends a request to the server for a resource (e.g., a webpage).
  * Server Response: The server processes the request and sends back a response, typically containing the requested resource.
* HTTP Methods (Verbs):
  * GET: Retrieve data from the server.
  * POST: Send data to be processed to the server (e.g., form submissions).
  * PUT: Update a resource on the server.
  * DELETE: Delete a resource on the server.
  * HEAD, OPTIONS, PATCH, etc.: Additional methods with specific purposes.
* Headers:
  * Request Headers: Contain additional information about the request (e.g., user agent, accepted content types).
  * Response Headers: Provide information about the server's response (e.g., content type, server type).
* Status Codes:
  * 1xx: Informational
  * 2xx: Success
  * 3xx: Redirection
  * 4xx: Client Errors (e.g., 404 Not Found)
  * 5xx: Server Errors (e.g., 500 Internal Server Error)
  
## HTTPS (Hypertext Transfer Protocol Secure):

* Secure Version of HTTP:
* Encryption: HTTPS adds a layer of security by encrypting the data exchanged between the client and server using TLS (Transport Layer Security) or its predecessor SSL (Secure Sockets Layer).
* TLS Handshake:
  * Client Hello: The client initiates a connection by sending a hello message to the server.
  * Server Hello: The server responds with its own hello message, including the chosen encryption algorithms.
  * Key Exchange: The client and server exchange cryptographic keys to establish a secure connection.
  * Finished: Both parties confirm the completion of the handshake.
* Secure Sockets Layer (SSL) and Transport Layer Security (TLS):
* SSL: Deprecated, but you might still encounter it.
* TLS: The modern and more secure successor to SSL.
* HTTPS Port:
  * HTTP: Port 80
  * HTTPS: Port 443
* Tools for Understanding:
  * Browser Developer Tools:
    * Use the network tab in browser developer tools to inspect HTTP requests and responses.
    * Examine headers, request methods, status codes, and response data.
* Packet Sniffers:
  * Tools like Wireshark can capture and analyze network traffic, allowing you to inspect HTTP/HTTPS packets.
* Online Resources:
  * Read official documentation, tutorials, and articles about HTTP/HTTPS protocols.
* Practical Application:
  * Create a simple web server, send requests using tools like cURL, and observe the protocol in action.
