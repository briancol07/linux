# Introduction

## Topics 

* Introduction to web application security testing 
* Web App Security Testing VS web app pentesting 
* Common web application threats 
* web application architecture 
* Web application technologies 
* HTTP Protocol Fundamentals 
* HTTP Requests & Responses 
* HTTPS 
* Web app Pentesting Methodology 
* Introduction to OWASP TOP 10
* OWASP Web Security Testing Guide 
* Pre-Engagement Phase 
* Documenting & Communicating Findings 


