# Command Line 

## Index 

* [## The shell ](#shell)
* [## echo ](#echo)
* [## date ](#date)
* [## whoami ](#whoami)
* [## pwd ](#pwd)
* [## cd ](#cd)
* [## ls ](#ls)
* [## touch ](#touch)
* [## file ](#file)
* [## cat ](#cat)
* [## less ](#less)
* [## history ](#history)
* [## clear ](#clear)
* [## cp ](#cp)
* [## mv ](#mv)
* [## mkdir ](#mkdir)
* [## rm ](#rm)
* [## find ](#find)
* [## help ](#help)
* [## man ](#man)
* [## whatis ](#whatis)
* [## alias ](#alias)
* [## exit ](#exit)

## The shell <a name="shell"></a>

> The sheel is basically a program that takes your commands from the keyboard and sends them to the operating system to perform 

## echo <a name="echo"></a>

> This command just print out the text arguments to the display 

echo hello world 

## date <a name="date"></a>

> This command will print the current date 

## whoami <a name="whoami"></a>

> Will tell you wich user you are 

## pwd <a name="pwd"></a>

> print working directory 

Everything in linux is a file. 

Every file is organized in a hierarchichal directory tree . The first directory in the filesystem is apltly named the root directory .
The location of these files and directories are referred to as paths . 

## cd <a name="cd"></a>

> Change directory 

1. Absolute path 
  * this is the path from the root directory . This is commonly shown as a slash or ~ 
2. Relative path 
  * This is the path from where you are currently in filesystem . you can only do cd folder instead of writing it from begining 
<br>
---- 

<br>

1. Current directory . 
2. Parent directory .. 
3. home directory ~ 
4. Previous directory - 

## ls <a name="ls"></a>

> List Directories 

it shows the files and directory of the current directory 

command | meaning 
--------|--------
ls -a   | show all the files , including the hidden ones 
ls -l   | show all including information about it like if you can write read o execute 
ls -la  | Combination of both 

## touch <a name="touch"></a>

> To create a new empty file 

## file <a name="file"></a>

this will show the type of file that is 

```   
file banana.jpg
```

## cat <a name="cat"></a>

> Not only will display file contents also can combine multiple files and show you the output 

## less <a name="less"></a>

> less is more will show less lines of a file 

instruction | meaning 
------------|--------
q | will quit out less and go back to your shell
Page up , Page down | navigate using the arrow keys and page keys 
g | Moves to beginning of the text file 
G | Moves to the end of the text file 
/search | you can search for a specific text inside the text document 
h | if you need help  

## history <a name="history"></a>

> show the history of the commands that you used 

## clear <a name="clear"></a>

> clear all text in the shell 

## cp <a name="cp"></a>

> copy 

it will copy a file into a new directory, you can use wild cards 

wildcards | use 
----------|----- 
\*        | used to represent all sible character or any string 
?         | used to represent one character 
[]        | used to represent any character within the brackets 

also you can combine with other flags like r that will overwrite the files in the new place but if you don't want that is i .

## mv <a name="mv"></a>

> move 

it's used for moving files and also renaming them. You can move more than one file. 

```
mv oldfile newfile
mv file2 /home/brian/Documents
```
there are also flags that allow to do other stuff like prompt you before overwriting anything (i) or to make a back up (b) 

## mkdir <a name="mkdir"></a>

> make directory 

you can make more than one at the same time , also you have parent flag (p) to make subdirectories 

## rm <a name="rm"></a>

> Remove 

Is used to remove files and directories 

flags | use 
------|-----
-f | tells remove all the files they are write protected or not , without pompting the user (as long as you have permission) 
-i | give a prompt on whether you want to actually remove the file / directories 
-r | for directories that will do a recursive to remove all the files and any subdirectories 
or use rmdir

## find <a name="find"></a>

> to search for a specific one 

```
   find /home -type d -name MyFolder
``` 

## help <a name="help"></a>

> is a built in bash command that provides help for other commands 

``` bash  
  help echo 
  echo --help
```

## man <a name="man"></a>

> This name come from manuals 

It provide documentation about commands and other aspects of the system

``` bash 
man ls 

```

## whatis <a name="whatis"></a>

The whatis command provides a brief description of command line programs

``` bash
whatis cat
```

## alias <a name="alias"></a>

> To create an alias for a command you simply specify an alias name and set it to the command 

``` bash 

alias foobar='ls -la'
```

this won't  save your alias after reboot , so you'll need to add a permanent alias in ~/.bashrc 

also you can remove aliases 

``` bash 
unalias foobar 
```

## exit <a name="exit"></a>

> to exit from the shell

``` bash 

exit 
logout 
```
