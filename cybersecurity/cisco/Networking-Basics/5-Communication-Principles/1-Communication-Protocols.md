# Communication Protocols 

Agreements to govern the conversation 

* What method of communication should we use? 
* What language shoud we use ?
* Do we need to confirm that our messages are received?

These rules, or protocols, must be followed in order for the message to be successfully delivered and understood.

* An identified sender and receiver 
* Agreed upon method of communicating 
* Common language and grammar 
* Speed and timing of delivery 
* Confirmation or acknowledgment requirements 

## Why Protocols Matter

Protocol Characteristic | Description 
------------------------|---------------
Message Format | Format or Structure, depends on the type of message and the channel tha is used to deliver the message 
Message Size | Depending on the channel used. When a long message is sent from one host to another over a network, it may be necessary to break the message into smaller pieces in order to ensure that the message can be delivered reliably. 
Timnig | Timing determines the speed at which the bits are transmitted across the network. It also affect when an individual host can send data and the total amount of dat that can be sent in any one transmission 
Encapsultation | Must inclued a header that contains addressing information that identifies the source and destination hosts. In addition to addressing, there may be other information in the header that ensures that the message is delivered to the correct application on the destination host 
Message Pattern | Some messages require an acknowledgment before the next message can be sent. This type of reques/reponse patter is a common aspect of many networking protocols.
