# Bucles en bash 

## Que es un bucle? 

un bucle es una estructura de control que permite ejecutar un conjunto de instrucciones repetidamente hasta que se cumpla una condicion especifica o se procese toda una lista de elementos. 

## Bucle for 

Se utiliza para iterar sobre una lista de elementos y ejecutar un bloque de codigo para cada uno de esos elementos. 

```
for variable in lista; do
    # Código a ejecutar
done
```
### Ejemplo 
```
#!/bin/bash
for nombre in "Rocío" "Elena" "David"; do
    echo "Hola, $nombre"
done
```
### Iterar sobre rango de numeros 
```
#!/bin/bash
for numero in {1..5}; do
    echo "Número: $numero"
done
```
### Iterando sobre archivos en un directorio 

```
#!/bin/bash
for archivo in *.txt; do
    echo "Procesando $archivo"
done
```

## Bucle while 

Ejecuta un bloque de codigo repetidamente mientras una condicion sea verdadera. Es ideal cuando no sabes cuantas veces necesitaras ejecutar el bucle, pero tienes una condicion que controlara cuando debe detenerse 

```
while [ condición ]; do
    # Código a ejecutar
done
```

### Ejemplo 

```
#!/bin/bash
contador=1

while [ $contador -le 5 ]; do
    echo "Contador: $contador"
    contador=$((contador + 1))
done
```
### Con entrada de usuario 
```
#!/bin/bash
respuesta="no"

while [ "$respuesta" != "sí" ]; do
    echo "¿Quieres salir del bucle? (escribe 'sí' para salir)"
    read respuesta
done
```

## Bucle until 

Es similar al bucle while, pero en lugar de ejecutarse mientras la condicion sea verdadera, se ejecuta hasta que la condicion sea verdadera . Mientras la condicion sea falsa 

```
until [ condición ]; do
    # Código a ejecutar
done
```
### Ejemplo 

```
#!/bin/bash
contador=1

until [ $contador -gt 5 ]; do
    echo "Contador: $contador"
    contador=$((contador + 1))
done
```
## Control de bucles `break` y `continue`

A veces es necesario salir del  bucle antes de que termine o saltar una iteracion. 

### Break 

Termina el bucle inmediatamente sin importar si la condicion aun es verdadera 

```
#!/bin/bash
for numero in {1..10}; do
    if [ $numero -eq 5 ]; then
        break
    fi
    echo "Número: $numero"
done
```

### Continue 

El comando continue salta la iteracion actual y pasa directamente a la siguiente 

```
#!/bin/bash
for numero in {1..5}; do
    if [ $numero -eq 3 ]; then
        continue
    fi
    echo "Número: $numero"
done
```

## generador de archivos 

```
#!/bin/bash

echo "Introduce el prefijo de los archivos:"
read prefijo

echo "Introduce la cantidad de archivos a generar:"
read cantidad

for i in $(seq 1 $cantidad); do
    touch "${prefijo}_archivo_$i.txt"
    echo "Creado: ${prefijo}_archivo_$i.txt"
done
```

## Tarea de esta leccion 

1. Crea un script que imprima los numeros del 1 al 10 usando bucle for 
2. Modifica el script para que no imprima le numero 5 ( usa continue para saltarlo)
3. Crea un script que pida al usuario contrasenia hasta que la introduzca correctamente ( while ) 
