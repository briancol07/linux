# Packages 

## Index

* [## Package Repositories ](#package)
* [## tar and gzip ](#tar)
* [## Package Dependencies ](#dependencies)
* [## rpm and dpkg ](#rpm)
  * [### Install a package ](#install)
  * [### Remove a package ](#remove)
  * [### List installed packages ](#list)
* [## yum and apt ](#yum)
  * [### install a package ](#a)
  * [### Remove a package ](#b)
  * [### Updating packages ](#c)
  * [### Get information ](#get)
* [## Compile source code ](#compile)

You can install packages through package manager or from source code. 
Most common variet of packages are Debia(.deb) and Red Hat(.rpm) 
The people that write this SW are knoen as upstram providers, the send to package maintainers. 

## Package Repositories <a name="package"></a>

Your distribution already comes with pre-approved sources to get packages from this is how it install all the base packages you see on your system
On a Debian system, this source file is /etc/apt/source.list file. 

## tar and gzip <a name="tar"></a>

Compressing file with gzip, the end in gz. You cant add more than 1 file 

* To compress 
  * gzip mycoolfile
* To decompress 
  * gunzip mycoolfile.gz 

Creating archives with tar, the end in tar 

* to compress 
  * tar cvf mytarfile.tar mycoolfile1 mycoolfile2
  * c (create)
  * v (Tell the program to be verbose and let uss see what it's doing)
  * f (the filename of the tar file has to come after this option)
* To Unpack 
  * tar xvf mytarfile.tar 
  * x (Extract)
  * f (the file you want to extract) 

Many times you'll see a tar file that has been compressed such as: mycompressedarchive.tar.gz, all you need to do is work outside in, so first remove the compression with gunzip and then you can unpack the tar file. Or you can alternatively use the z option with tar, which just tells it to use the gzip or gunzip utility.

* Create a compressed tar file:
  * tar czf myfile.tar.gz
* Uncompres and unpack 
  * tar xzf file.tar 

> If you need help remember this: eXtract all Zee Files!

## Package Dependencies <a name="dependencies"></a>

Most often acoompanied by dependencies to help them run. These dependecies are often other packages or shared libraries(libraries of code) 

## rpm and dpkg <a name="rpm"></a>

Just like .exe is a single executable file, so is .deb and .rpm. You normally wouldn't see these if you use package repositories, but if you directly download packages, you will most likely get them in these popular format. Obviously, they are exclusive to their distributions, .deb for Debian based and .rpm for Red Hat based.

### Install a package <a name="install"></a>

* Debian
  * dpkg -i some.deb
* RPM 
  * rpm -i some.rpm 

### Remove a package <a name="remove"></a>

* Debian
  * dpkg -r some.deb
  * r for remove 
* RPM 
  * rpm -e some.rpm
  * e for erase 

### List installed packages <a name="list"></a>

* Debian
  * dpkg -l 
  * l for list
* RPM 
  * rpm -qa
  * q for query and a for all 

## yum and apt <a name="yum"></a>

Two of the most popular management systems is yum(redhat) and apt(Debian) 

### install a package <a name="a"></a>

* Debian 
  * apt install package 
* RPM
  * yum install package

### Remove a package <a name="b"></a>

* Debian 
  * apt remove package 
* RPM
  * yum erase package 

### Updating packages <a name="c"></a>

* Debian 
  * apt update; apt upgrade 
* RPM   
  * yum update 

### Get information <a name="get"></a>

* Debian 
  * Aot show package
* RPM 
  * yum info package

## Compile source code <a name="compile"></a>

Install tool that will allow you to compile source code 

```bash 
sudo apt install build-essential
tar -xzvf package.tar.gz
```
Read the README or INSTALL to see the commands.

```bash 
./configure 
make
sudo make install
```
 
To uninstall 

``` bash 

sudo make uninstall 
```

If you decide to remove this package, you may not actually remove everything because you didn't realize what was added to your system.
Checkinstall command wil make a .deb file for you that you can easily install and uninstall

```bash

sudo checkinstall

```
