#!/bin/bash 

legend="john nash"
actor="JULIA ROBERTS"

echo ${legend^^}
echo ${actor,,}

echo "-----------"
echo ${legend^}
echo ${actor,}

echo "-----------"
echo ${legend^^[jn]}
echo ${actor,,[JR]}
