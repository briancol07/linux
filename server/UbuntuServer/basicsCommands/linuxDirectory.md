# Linux Directories 


## Bin 

> Binary

the basic commands that can be perform by any user. 

## Dev 

> Related to hardware 

## Home 

> Information about user profile 

## lib 

> Support files , contains all about libraries

## media
 
> All the information about removable media example cdrom 

## opt

> About addons 

## Root 

> Home directory for root user 

## Sbin 

> Contains the binaries of the super user 

## srv 

> Services 

## tmp 

> To call tempory files

## var 

> Information about environment variables

## Boot 

> Information for the boot (kernel and others)

## etc

> Configurations files 

## lost/found

> Something wrong happend and restore are going there

## proc 

> Proccess 

## run 
## sys
