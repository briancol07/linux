# Clients & Servers 


All computers connected to a network that participate directly in network communicatio nare classified as hosts. 

* Host can send and receive messages on the network 
* Computer host cna act as a client, a server or both 


## Servers 

Are hosts that have software intalled which enable them to provide information:

* Email 
  * Email server SW 
* Web pages 
  * Web server sw 
* File 
  * Stores corporat and user files in a central location 
* Other host on the network 

## Clients 

Are computer hosts that have software intalled that enables the hosts to reuqes and display the information obtained from the server. 

* Web browser 


## Peer to peer networks (P2P)

Client and server SW usually run on separate computers but it also possible for one computer to run both client and server at the same time .

The simples P2P netwrok consist of two directly connected computers using either a wired or wireless connection.

The main disadvantage of a P2P environment is that the performance of a host can be slowed down if it is acting as both. 

* Advantages:
  * Easy to set up 
  * Less complex 
  * Lower cost because network devices and dedicated server may not be required
  * Can be used for simple tasks such as transferring files and sharing printers 
* Disadvnatages 
  * No centralized administration\
  * Not as secure 
  * Not scalable 
  * All devices may act as both clients and servers which can slow their performance 

### Applications 

Require that each end device provide a user interface and ru na backgorund service, Some of them are decentralized but the index that point to resource locations are stored in a centralized directory.

