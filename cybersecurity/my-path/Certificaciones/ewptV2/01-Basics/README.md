# Web application Basics 

## Topics 

* Understanding HTTP/HTTPS protocols
* Web application architecture and components
* Client-side vs. server-side processing


