# Exersice 

1. Find all lines containing the word "error" in a log file.
2. Count the number of lines containing the word "success" in a text file.
3. Find all lines that start with the word "ERROR" in a log file.
4. Display lines containing "user" in a file, ignoring case sensitivity.
5. Find all lines that end with a period in a text file.
6. List all files in a directory that contain the word "TODO".
7. Find lines containing either "foo" or "bar" in a text file.
8. Display lines containing "http" in a file but exclude "https".
9. Search for lines that contain digits in a file.
10. Find all lines containing "localhost" in /etc/hosts.
