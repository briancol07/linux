# Arrancar con Metasploit 

## Implementar bases de datos 

##  Postgresql

### iniciar el servicio

```bash 
service postgresql start 
``` 

### ver estado del servicio

```bash 

service postgresql status 
``` 

### Terminar el sevicion 

```bash 

service - postgresql stop
``` 

## db 

Corre metasploit con otro recurso que seria como una api
Tener bbdd como nmap, esta viene de manera automatica desactivada

```bash 
msfdb
```

### Iniciar 

```bash 
msfdb intit
# luego start 
msfdb start 
# con msfdb stop lo paramos 
``` 

## Iniciar Metasploit 

```bash 
msdfconsole
```
