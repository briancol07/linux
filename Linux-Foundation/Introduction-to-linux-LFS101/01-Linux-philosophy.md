# Introduction 

## Index 

* [## Linux History ](#history)
* [## Linux Distributions ](#distros)

## Linux History <a name="history"></a>

Open source computer operating system.

In 1992, linux was re-licensed using the General Public license ( GPL ) by GNU ( a project of the free software foundation ).

By Combining the kernel with other system components from the GNU project, other developers created complete systems called Linux Distributions. 

Linux borrows heavily from the well-established family of UNIX operating systems. It was written to be a free and open source alternative. 

Files are stored in hierarchichal filesystem, with the top node of the system being the root or simply `/` . Linux is fully multitasking ( multiple threads of execution are performed simultaneously), multiuser operating system with built-iin networking and service processes knwon as daemos in the UNIX world 

> Linux was inspired by UNIX, but it is not UNIX.

* Open Soruce examples 
  * Android 
  * Apache web server 
  * Social media 
  * Search engines 
  * Weather forecasting 
  * Personal fitness
  * Medical devices 

## Linux Distributions <a name="distros"></a>

The linux kernel is the core of the operating system. A full linux distribution consists of the kernel plus a number of other SW tools for file-related oeparations, user management and SW package management. 
The kernel is not an all-or-nothing proposition.

* Example:
  * include c/c++ and Clang compilers, the gdb debugger, the core system libraries application

![kernel-tools](./img/kernel.png)
