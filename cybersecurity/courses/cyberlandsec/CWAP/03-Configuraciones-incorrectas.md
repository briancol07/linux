# Configuraciones de seguridad incorrectas 

## index 

* [## Que son configuraciones incorrectas ? ](#configuraciones)
* [## Por que son peligrosas ? ](#peligrosas)
* [## Ejemplos ](#ejemplos)
* [## Buenas Practicas ](#buenas-practicas)

Son como dejar la puerta trasera abierta en tu aplicacion sin que te des cuenta .

## Que son configuraciones incorrectas ? <a name="configuraciones"></a>

Se refieren a cualquier mal ajuste en la configuracion del servidor, el software, o la aplicacion que puedan ser explotado por un atacante.

* Valores predeterminados inseguros 
* Permisos mal gestionados 
* Falta de restricciones apropiadas 
* Servicios Innecesarios habilitados 
* Mensajes de error detallados 
* Software sin parches 
* Configuraciones predeterminadas peligrosas 

## Por que son peligrosas ? <a name="peligrosas"></a>

* Permiten nuevos vectores de ataques como:
  * Inyecciones 
  * Escalada de privilegios 
  * Acceso no autorizado 

## Ejemplos <a name="ejemplos"></a>

* Configuracion predeterminada expuesta 
  * Admin/admin
* Servicios innecesarios habilitados 
  * Servicios o puertos abiertos 
  * SSH (Brute force para tratar de ingresar ) 
* Falta de restricciones en permisos de archivos 
  * `/etc/`

## Buenas Practicas <a name="buenas-practicas"></a>

* Deshabilita servicios innecesarios 
* Revisa los permisos de archivos y directorios 
* Manten el software actualizado 
* Configura mensajes de error seguros 
* Revision de seguridad periodicas 
