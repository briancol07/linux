# Introduction to the Motherboard 

> The motherboard is the physical foundation layer for every system

The motherboard is also know by other names:

* The system board
* The base board 
* The MB
* The MoBo

The motherboard, along with the BIOS, establishes what the system is and what components may be present. Some motherboards are very simple and some are very complex, but they all do basically the same thing lay the foundation of the system.

## Different form factors 

* ATX 
  * The basic form factor for a full size, full power PC
  * 305 mm x 244 mm
* Micro-ATX 
  * More compact, usually a little less capable than the ATX
  * Sizes may vary within a range 
  * 244 mm x 244 mm down to 171. 45 mm x 171.45 mm 
* ITX (Mini-ITX)
  * Low power consumption but even more compact 
  * Often doesn't require the use of fan for system cooling
  * 170 mm x 170 mm 
* Nano-ITX and Pico-ITX
  * 120 mm x 120 mm & 10 mm x 7,2 mm 
  * Tend to be used for specially applications, very low power consumption capabilities 

## Common factors

* Disclaimer 
  * as a general rule, these factors are present on all ATX and mos ITX motherboards, but it is up to the manufacturer to decide what is prenset and what is not 
* Common factors 
  * CPU sockets 
  * Ram slots 
  * Chipsets (North bridge, south bridge and BIOS) 
  * Power connections (main power and CPU power) 
  * Fan connections 
  * Expansion slots 
  * Back panel connections 
  * Front panel connections (USB, audio, Power button, powerlight, hdd activity light, reset button)

