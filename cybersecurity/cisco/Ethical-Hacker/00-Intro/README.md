# Introduction

## What will I learn in this course ??

This course is designed to prepare you with ETH skillset and give you a solid understanding of offensive security. 

* Red team 
* ETH
* Pentester 
* Blue team 

## ETH Statement 

In this course, you will explore and apply various tools and techniques withing a controlled sandbox. 

The vulnerabilities and weaknesses demonstratedf here must be used resdponsibly and ethically, eclusively within this designated sandbos environment .

It is imperative to comprehend that unauthorized access to data, computers systems and network is illegal, in numerous jurisdicitons regadrles of intentohn or motivations. 

## First 

Understand: 

* The art of hacking 
* Ethical hacking vs Unethical hacking 
* Penetration testing methodology 

Topic Title | Topic Objective 
------------|-----------------
Understanding ETH and Penetration Testing | Explain the importance of ethical hacking and penetration testing 
Exploring Penetration Testing Methodologies | Explain different types of penetration testing methodoogies and frameworks 
Building Your Own lab | Configura a virtual machine for your penetration testing learning Experience 
