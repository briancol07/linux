# Titles with grep 

Here I will try to make faster indexes for titles in markdown 

## topics 

* [(## Ideas )](#ideas)
* [(## Example )](#example)
* [(## Process )](#process)
* [(## Usefull links)](### Usefull links)
* [(## Errors )](#error)

## Ideas <a name="ideas"></a>

```
# Table of contents
1. [Introduction](#introduction)
2. [Some paragraph](#paragraph1)
    1. [Sub paragraph](#subparagraph1)
3. [Another paragraph](#paragraph2)

## This is the introduction <a name="introduction"></a>
Some introduction text, formatted in heading 2 style

## Some paragraph <a name="paragraph1"></a>
The first paragraph text

### Sub paragraph <a name="subparagraph1"></a>
This is a sub paragraph, formatted in heading 3 style

## Another paragraph <a name="paragraph2"></a>
The second paragraph text

```
## Example <a name="example"></a>

```
* [title](#title)
* [title 2](#title-2)
```

## Process <a name="process"></a>

1. When writting use the script `place-a.sh` 
  * This will place an placeholder anchor with the word that you use 
2. build a script that take title and the name inside the anchor so he can build the title

```
    -------- This title 
    |
## Test <a name="anchor"></a>
                  \--- This name 
```

So the result will be a list with the title and link to the name 

```
1. [Test](#anchor)
```

## Usefull links  <a name="links"></a>

* [Stack-overflow-page-table](https://stackoverflow.com/questions/11948245/markdown-to-create-pages-and-table-of-contents#27953547)
* [Stack-overflow-Extracting-Part-of-Variable](https://stackoverflow.com/questions/15897276/extracting-part-of-a-string-to-a-variable-in-bash)
* [Parameter-Expansion](https://stackoverflow.com/questions/19482123/extract-part-of-a-string-using-bash-cut-split)


## Errors <a name="error"></a>

When you try to save a result from grep, be careful because each reasult is not a new variable , so when you assign to a variable you will get a large string lol.

![error1](./img/fail-grep.png)

The issue with your script lies in how the result array is being updated and accessed inside the while loop. The problem occurs because while loops that process grep output run in a subshell in Bash.

``` 
Wrong 
grep "##" $1 | while read -r line; do
```
Changes made to variables inside a subshell do not persist in the parent shell. This means that the result array gets reset after each iteration of the loop.

```
Solution
while read -r line; do
done < <(grep "##" "$1")
```

One error handle that I can add if there is no anchor in the line skip so when you have multiple lines with ## can avoid the ones that are not titles 


