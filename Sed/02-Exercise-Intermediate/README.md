# Intermediate 

## Exersices 

1. Replace the third occurrence of "foo" with "bar" on each line.
2. Delete lines that do not contain the word "important".
3. Insert a line after every line containing "error".
4. Replace the last occurrence of "foo" with "bar" on each line.
5. Remove the first 5 lines of a text file.
6. Insert line numbers at the beginning of each line in a file.
7. Swap the first and second words on each line in a file.
8. Replace all digits with asterisks (\*) in a text file.
9. Delete all lines that start with a hash (#).
10. Replace all spaces with underscores in a file
