# Behavior Approach to Cybersecurity 

## Behavior-Based Security 

Behavior-based security is a form of threat detection that involves capturing and analyzing the flow of communication between a user on the local network and a local or remote destination. Any changes in normal patterns of behavior are regarded as anomalies and may indicate an attack 

### Honeypots

A honeypot is a behavior-based detection tool that lures the attacker in by appealing to their predicted pattern of malicious behavior. Once the attacker is inside the honeypot, the network administrator can capture, log and analyze their behavior so that they can build a better defense.

### Cisco's Cyber threat Defense solution Architecture 

This security architecture uses behavior-based detection and indicators to provide greater visibility context and control. The aim is to know who is carrying out the attack, waht type of attack they are performing and where, when and how the attack is taking place. This security architecture uses many security technologies to achieve this goal 

## NetFlow 

Is used to gather information about data flowing through a network, including who and what devices are in the network and when and how users and devices access the network.

Behavior-based detection and analysis 

Switches, routers and firewalls equipped with NetFlow can report information about data entering leaving and traveling through th network.

## Penetration Testing 

Known as pen testing, is the act of assessing a computer system, network or organization for security vulnerabilities . A pen Test seks to breach syhstem, people, processes and code to uncover vulnerabilities which could be exploited. 
This information is then used to improve the system's defenses to ensure that it is better able to withstand cyber attack in the future.

## Step 1: Planning 

The pen tester gathers as much information as possible about a target system or network, its potential vulnerabilities and exploits to use against it.
This involves conducting passive or active reconnalssance(footprinting) and vulnerability research 

## Step 2: Scanning 

The pen tester carries out active reconnaissance to probe a target system or network and identify potential weaknesses which, if exploited, could give an attacker access.

Active Reconnaissance may include:

* Port scanning to identify potential access points into a target system 
* Vulnerability scanning to identify potential exploitable vulnerabilities of a particular target.
* Establishing an active connection to a target (enumeration) to identify the user account, system account and admin account.

## Step 3: Gaining Access

The pen tester will attempt to gain access to a target system and sniff network traffic, using various methods to exploit the system including:

* Launching an exploit with a payload onto the sytem
* Breaching physical barriers to assets 
* Social Engineering 
* Exploiting website vulnerabilities
* Exploiting SW and HW vulnerabilities or misconfigurations 
* Breaching acces controls security 
* Cracking weak encrypted Wi-Fi.

## Step 4: Maintaining access 

The pen tester will maintian access to the target to find out what data and systems are vulnerable to exploitation. It is important that theyremain undetected, typically using backdoors, trojan horses, rootkits and other conver channels to hide their presence.
When this infrastructure is in place ,will proceed to gather the data that they consider  valuable .

## Step 5: Analysis and reporting 

Will Provide feedback via a report that recommends update to products, policies and training to imporve an organization's security 


## Impact Reduction 

No set of security practices is foolproof. Therfore, organization must be prepared to contain the damage if a security breach occurs.

### Communicate the issue 

Communication creates transparency, which is critical in this type of situation.
Internally, all employees should be informed and a clear call to action communicated 
Externally, All client should be informed through direct communicaiton and official announcements

### Be sincere and accountable 

Respond to the breach in an honest and genuine way. taking responsibility where the organization is at fault 

### Provide the details 

Be open and explain why the breach took place and what information was compromised. Organizations are generally expected to take care of any client costs associated with identity theft services required as a result of a security breach 


### Find the cuase 

Take steps to understand what cause and facilitated the breach. This may involve  hiring forensics experts to research and find out the details.

### Apply lessons learned 

Make sure that any lessons learned from forensic investigations are applied to prevent similar breaches from happening in the future.

### Check and check again 

Attackers will often attempt to leave a backdoor to facilitate future breaches. To prevnet this from happening, make sure that all systems are clean, no bakcdoors are installed and nothing else has been compromised. 

### Educate 

Raise awareness, train and educate employees, partners and clients on how to prevent future breaches.

## What is Risk Management?

Risk management is the formal process of continuously identifying and assessing risk in an effort to reduce the impact of htreats and vulnerabilities. You canno eliminate risk completely, but you can determine acceptable levels by weighing up the impact of a threat with the cost of implementing controls to mitigate it. The cost of a control should never be more than the value of the asset you are protecting.

### Frame The risk 

Identify the threats that increase risk, Threats my include processes, products, attacks, potential failure or disruption of services, negative perception of an organization's reputation, potential legal liability or loss of intellectual property. 

### Assess the risk 

Determine the severity that each threat poses. For example some threats may have the potential to bring an entire organization to a standstill, while other threats may be only minor inconveniences. risk can be prioritized by assessing financial impact (a quantative analysis) or scaled impact on an organization's operation (a qualitative analysis).

### Respond to the risk 

Develop an action plan to reduce overall organization risk exposure, detailing where risk can be eliminated, mitigated,tranferred or accepted 

### Monitor the risk 

Continuously review any risk reduce through elimination, mitigation or tranfer actions. Remember, not all risk can be eliminated, so you will need to closely monitor any threats that have been accepted.
