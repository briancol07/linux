# Explotacion de servicios HTTP y HTTPs 

## Index 

* [## Introduccion a vulnerabilidades web comunes ](#vuln-comun)
  * [### Cross-site Scripting (XSS) ](#xss)
  * [### SQL inyection (SQLi) ](#sqli)
  * [### Local file inclusion (LFI) ](#lfi)
  * [### Remote file inclusion (RFI) ](#rfi)
* [## Explotacion de aplicaciones web y configuraciones inseguras ](#expl)
  * [### Enumeracion de Directorios y archivos ocultos ](#oculto)
  * [### Explotacion de paneles de administracion expuestos ](#expuesto)
  * [### Explotacion de subdominios vulnerables ](#subdomains)

## Introduccion a vulnerabilidades web comunes <a name="vuln-comun"></a>

### Cross-site Scripting (XSS) <a name="xss"></a>

XSS es una vulnerabilidad que permite a un atacante inyectar scripts maliciosos en sitios web confiables.

* Tipos 
  * Almacenado
  * Reflejado 
  * DOM-based 

### SQL inyection (SQLi) <a name="sqli"></a>

Permite a un atacante manipular las consultas SQL que una aplicacion envia a la base de datos.
Se puede utilizar SQLMap para automatizar SQL injection

* Ejemplo `admin' OR '1' = 1`

### Local file inclusion (LFI) <a name="lfi"></a>

LFI permite a los atacantes incluir archivos locales en un servidor remoto, lo que podria revelar informacion confidencial

Ejemplo: si el sitio tiene un parametro vulnerable como ?file=about.php, y intenta acceder a archivos sensible del sistema `?file=../../../../../etc/passwd`, mostrando el archivo 

### Remote file inclusion (RFI) <a name="rfi"></a>

Permite a los atacantes incluir archivos remotos en la aplicacion, que podria ejecutar codigo malicioso en el servidor 

## Explotacion de aplicaciones web y configuraciones inseguras <a name="expl"></a>

### Enumeracion de Directorios y archivos ocultos <a name="oculto"></a>

Sirve para revelar informacion valiosa como archivos de configuracion o scripts de administracion

Comando con dirbuster `dirbuster -u http://targer.com -W /usr/share/wordlist/dirbuster/directory-lsit-2.3-medium.txt`
Utiliza un diccionario de palabras para detectar directorios ocultos 

### Explotacion de paneles de administracion expuestos <a name="expuesto"></a>

Paneles de administracion mal protegidos pueden permitir que los atacantes accedan y controlen el sitio web. Los atacantes pueden usar diccionarios de contrasenias para realizar ataques de fuerza bruta.

### Explotacion de subdominios vulnerables <a name="subdomains"></a>

Los subdominios mal configurados pueden ser utilizados para ataques de subdoamin takeover o para obtener acceso no autorizado a recursos internos. Se puede usar sublist3r `sublist3r -d target.com`
