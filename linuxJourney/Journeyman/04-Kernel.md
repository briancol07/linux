# Kernel 

## Index

* [## Privilege Levels ](#privilege)
* [## System Calls ](#syscalls)
  * [### Wrapper ](#wrapper)
* [## Kernel Installation ](#kernel)
  * [### Installing ](#installing)
* [## Kernel Location ](#location)
* [## Kernel Modules ](#modules)

The linux operating system can be organized into three different levels of abstraction 

* Basic level 
  * Hardware 
    * CPU
    * Memory 
    * Hard disk 
    * Networking ports 
  * Kernel 
    * Handles process and memory management
    * Device communication 
    * System calls 
    * Set up our filesystem 
  * User space 
    * Includes the shell 
    * programs that you run
    * The graphics 

## Privilege Levels <a name="privilege"></a>

> Why do we have different abstraction layers for user space and kernel ?

They both operate in different modes, the kernel operates in kernel mode an the user space operates in user mode.

* In Kernel mode:
  * Has complete access to the HW, it controls eveyrthing. 
* User mode:
  * Small amount of safe memory and CPU that you are allowed to access.

These are called privilege levels ( aptly named for the levels of privilege you get ) and often described as protection rings. 

* Ring3 : User mode 
* Ring0 : Kernel 
  * Full trust 

System calls allow us to perform a privileged instruction in kernel mode and then switch back to user mode.

## System Calls <a name="syscalls"></a>

> Like VIP pass

Provide user space processes a way to request the kernel to do something for us. The kernel makes certain services available through the system call API. 
The system has a table of what system calls exist and each system call has a unique ID.

### Wrapper <a name="wrapper"></a>

Inside this wrapper it invokes the system call which will execute a trap. This trap then gets caught by the system call handler and then references the system call in the system call table.

It goes over kernel mode and user mode switching. 

you can actually view the system calls that a process make with the strace command `strace ls` 

## Kernel Installation <a name="kernel"></a>

You can install multiple kernels on your system

To see what kernel version you have on your system use 

```
uname -r 
```

Prints system information, the -r command will print out all of the kernel release version 

### Installing <a name="installing"></a>

* Download the source package and compile from source 
* you can install it using package management tool
  * `sudo apt install linux-generic-lts-vivid`
* If you wan to update use:
  * `sudo apt dist-upgrade`
  * Some are used as LTS ( Long Term support )

## Kernel Location <a name="location"></a>

When you install an ew kernel, some file are added to the `/boot` directory 

* Like this ones 
  * vmlinuz ( Actual kernel ) 
  * initrd ( temporary file system )
  * System.map ( symbolic lookup table ) 
  * Config ( kernel configuration settings ) 

If your `/boot` directory runs out of space, you can always delet old version of these files. or just use a package manager.

## Kernel Modules <a name="modules"></a>

Kernel is a monoliuthic piece of software.

Modules are pieces of code that can be loaded and unloaded into the kernel on demand. They allow us to extend the functionality of the kernel without actually addign to the core kernel code.

* View a list of currently loaded modules `lsmod`
* Load module `modprobe`
  * `sudo modprobe bluetooth`
  * Loads our module dependencies if they are not already loaded 
* remove a module `modprobe -r `
* Load on bootup 
  * Modify `modprobe.d` and add a configuration file in it 
  * `/etc/modprobe.d/peanutbutter.conf`
  * you can also make sure a module does not load on bootup 
    * adding `blacklist peanut_butter`
