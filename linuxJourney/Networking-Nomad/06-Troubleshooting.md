# Trouble shooting 

## Index 

* [## ICMP ](#icmp)
* [## Ping ](#ping )
  * [### icmp\_seq ](#seq)
  * [### ttl ](#ttl)
  * [### time ](#time)
* [## Traceroute ](#traceroute)
* [## Netstat ](#netstat)
* [## Packet Analysis ](#analysis)
  * [### Understanding output ](#output)

## ICMP <a name="icmp"></a>

> Internet control message protocol 

Part of the TCP/IP protocol suite, it used to send updates and error messages and is an extremely useful protocol used for debugging network issues such as a failed packet delivery.

* Contains 
  * Type
    * Type 0 : echo reply
    * Type 3 : destination unreachable
    * Type 8 : Echo Request
    * Type 11 : Time exceeded 
  * code 
    * There are 16 code values for type 3 
    * Code 0 : Network unreachable 
    * Code 1 : Host unreachable 
  * Checksum field

## Ping <a name="ping "></a>

It's used to test wheteher or not a packet can reach a host. It works by sending ICMP echo request type 8 packets to the destination host and waits for an ICMP echo reply (type 0) .

Example `ping -c 3 www.google.com`

The -c flag (count) is used to stop sending echo request packets after the count has been reached 

### icmp\_seq <a name="seq"></a>

Is used to shwo the sequence number of packets sent, so in this case, I sent out 3 packets and we can see that 3 packets made it back. if you do a ping and you get some sequence numbers missing that means that some connectivity issue is happening and not all your packets are getting through. 

If the number is out of order, your connetion is probably very slow as your packets are exceedingthe one second default

### ttl <a name="ttl"></a>

> The time to live

Used as a hop counter, as you make hops, it decrements the counter by one and once the hop counter reaches 0 our packet dies.

This is meant to give the packet a lifespan, we don't want our packts traveling around forever 

### time <a name="time"></a>

The roundtrip time it took from you sending the echo request packet to getting an echo reply

## Traceroute <a name="traceroute"></a>

To see how packets are getting routed. It works by sending packets with increasing TTL values, starting with 1. So the first router gets the packet, and it decrements the TTL value by one, thus dropping the packet. The router sends back an ICMP time exceeded message back to us. 
Then next packet with 2. so it makes it past the first but when it get the second router the TTL is 0 an returns with another ICMP time exceed message. 

Traceroute work this way because as it send and drops packets it is build a list of routers that the packets traverse, until it finally get to its destination and gets an ICMP echo Reply message 

## Netstat <a name="netstat"></a>

You can get a list of well-known ports by looking at the file `/etc/services`

Netstat displays various network related information such network connections, routing tables, information about network interfaces and more.

A socket is an interface that allows programs to send and receive data while a port is used to identify which application should send or receive data. The socket address is the combination of the IP address and port . 

Every connection between a host and destination requires a unique socket. Example HTTP is a service that runs on port 80, however we can have many HTTP connections and to maintain each connection a socket gets created per connection.

The `netstat -a` shows the listening and non-listening sockets for network connections, the `-t` flag` shows only tcp connections.

* Columns 
  * Proto: Protocol used TCP or UDP
  * Recv-Q: Data that is queued to be received 
  * Send-Q: Data that is queued to be sent 
  * Local Address: locally connected host
  * Foreign address: Remotely connected host 
  * State the state of the socket 
* List of states 
  * LISTENING : The socket is listening for incoming connections
  * SYN\_SENT : The socket is actively attempting to establish a connection
  * ESTABLISHED: The socket has an established connection
  * CLOSE_WAIT: The remote host has shutdown and we're waiting for the socket to close
  * TIME_WAIT: The socket is waiting after close to handle packets still in the network 

## Packet Analysis <a name="analysis"></a>

* Packet analyzers 
  * Wireshark
  * tcpdump

These tools scan your network interfaces, capture the packet activity parse the packages and output the information for us to see.

* Install tcpdump `sudo apt install tcpdump`
* Capture packet data on an interface `sudo tcpdump -i wlan0`
* Writing tcpdump output to a file `sudo tcpdump -w /some/file`

### Understanding output <a name="output"></a>

* The first field is a timestamp of the network activity 
* IP, this contains the protocol information
* Next, you'll see the source and destination address: icebox.lan > nuq04s29-in-f4.1e100.net
* seq, this is the TCP packets's starting and ending sequence number 
* Length, length in bytes 
