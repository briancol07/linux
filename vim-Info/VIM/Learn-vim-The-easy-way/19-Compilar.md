# Compilar 

## Compilar desde la linea de comandos <a name="compilar"></a>

Puedes utilizar el comando bang(!) para compilar 

```
:!g++ hola.cpp -o hola
```

Sin embargo tener que escribir manualmente el nombre del archivo fuente y del archivo de salida todas las veces provoca que podamos cometer un error y ademas es tedioso .

## Comando Make <a name="make"></a>

Vim tiene el comando :make para ejecutar un archivo makefile. Cuando lo ejecutar vim busca un archivo makefile en el directorio actual para ejecutarlo.

El comando utiliza la ventana quickfix de vim para almacenar cualquier error si has ejecutado mal un comando, Con `:copen` se pude ver la ventana de quickfix

```
:make noexistente

:copen
```

## Compilando con make <a name="make2"></a>

makefile
```
all:
  echo "compilar, ejecutar"
build:
  g++ hola.cpp -o hola
run: 
  ./hola
```
Ejecutas `:make build`

## Programa diferente para make <a name="diferente"></a>

Vim ejecuta cualqueir comando que este configurado bajo la opcion `makeprg`, si ejecutas `:set makeprg?` se vera `makeprg=make`

Para cambiar el comando :make para que ejecute g++ {nombrearchivo} cada vez que lo ejecutas `:set makeprg=g++\ %`

```
set makeprg=g++\ %\ -o\ %<
```
* `g++\ % `es lo mismo que hemos visto anteriormente. Es equivalente a ejecutar g++ `<tu_archivo>.`
* -o es la opción de salida.
* `%<` en Vim representa el nombre del archivo actual sin la extensión (hola.cpp se convierte en hola).

## Compilado automatico al guardar el archivo <a name="automatico"></a>

Se puede automatizar el proceso de compilacio. Utilizar autocmd para lanzar una accion automatica basada en ciertos eventos 

```
autocmd BufWritePost *.cpp make
```

## Cambiando Compilador <a name="compilador"></a>

Existe el comando `:compiler` para cambiar rapidamente entre compiladores.
Para comprobar que compiladores tenes  ` :e $VIMRUNTIME/compiler/<Tab>`

Si se ejecuta `:compiler ruby` cambiara el makeprg para utilizar el comando ruby. Ahora si ejecutas `set makeprg?` deberia aparecer ruby 

## Crear un compilador personalizado <a name="personalizado"></a>

Para crear un compilador simpede Typescript, en tu directorio `~/.vim/` aniade un directorio llamdo compiler y despues crea dentro un directorio llamado typescrip.vim y escribe 

```
CompilerSet makeprg=tsc
CompilerSet errorformat=%f:\ %m
```
LA primera linea establece que el makeprg para ejecutar el comando tsc. La segunda linea establece el formato de error para mostrar el archivo (%f), segudo por un simbolo de dos puntos y un espacio en blanco, por lo que necesitamos una barra invertida para escapar ese espacio seguido por el mensaje de error (`%m`) para aprender mas `:h errorformat`

Luego comprobar que existe con `set makeprg?` y solo queda seleccionarlo con compiler 

## Compilador Asincrono <a name="asicrono"></a>

* Complementos
  * vim-dispatch
  * asyncrun.vim
