# Deception 

## Social Engineering 

Social engineering is a non-technical strategy that attempts to manipulate individuals into performing certain action or divulging confidential information.
Exploits human nature, taking advantage of people's willingness to help or preying on their weaknesses, such as greed or vanity.

### Pretexting 

This type of attack occurs when an individual lies to gain access to privileged data. 

### Something for something (quid pro quo) 

Quid pro quo attack involve a request for personal information in exchange for something, like a gift.

### Identity fraud 

This is the use of a person's stolen identity to obtain goods or services by deception

## Social Engineering Tactics 

### Authority 

Attackers prey on the fact that people are more likely to comply when instructed by someone they perceive as an authority figure 

### Intimidation 

Cyberciminals will often bully a victim into takin an action that compromises security.

For example a secretary receives a call that their boss is about to give an important presentation but the files are corrupt.

### Consensus 

Often called "social proof", consensus attacks work because people tend to act in the same way as other people around them thinking that something must be right if other are doing it 

### Scarcity 

A well known marketing tactic, scarcity attacks work because attackers know that people tned to act when they thing there is a limited quantity of somethin available.

### Urgency 

Similarly, people also tend to act when they thing there is a limited time to do so. 

### Familiarity 

People are more likely to do what another person asks if they like this person . Therefore, attackers will often try to build a rapport with their victim in order to establish a relationship. In other cases, they may clone the social media profile of a friend of yours, in order to get you to htink you are speaking to them 

### Trust 

Building trust in a relationship with a victim may require more time to establish.

## Shoulder surfing 

Shoulder surfing is a simple attack that involves observing or literally looking over a target's shoulder to gain valuable information such as PINs, Access codes or credit card details. Criminals do not always have to be near their victim to shoulder surf the can use binoculars or security cameras to obatin this information.

## Dumpster Diving 
The process of going thorugh a target's trash to see what information has ben thrown out. This is why documents containing sensitive information should be shredded or stored in burn bags until they are destroyed by fire after a certain period of time 

## Impersonation 

Is the act of tricking someone into doing something they would not ordinarily do by pretending to be someone else.Criminals can also use impersonation to attack others.

## Hoaxes 

A hoax is an act intended to deceive or trick someone, and can cuase just as much disruption as an actual security breach. 

## Piggybacking and Tailgating 

Occurs when a criminal follows an authorized person to gain physical entry into a secure location or a restricted area.

* Giving the appearance of being escorted into the facility by an authorized person
* JOining and pretneding to be part of a large crowd that enter the facility
* Targeting an authorized person who is careless about the rules of the facility. 

One way of preventing this is to use two sets of doors. This is sometimes referred to as a mantrap and means individuals enter through an outer door, which must close before they can gain acces thorugh an inner door.

## Other methods of Deception 

### Invoice scam 

Fake invoices are sent with the goal of receiving money from a victim by prompting them to put their credentials into a fake login screen. The fake invoice may also include urgent or threatening language

### Watering hole attack 

A watering hole attack describes an exploit in which an attacker observes or guesses what websites an organization uses most often, and infects one or more of them with malware 

### Typosquatting 

This type of attack relies on common mistakes such as typos made by individuals when inputting a website  address into thrit browser. The incorrect URL will bring the individuals to a legitimate-looking website owned by the attacker, whose goal is to gather their personal or financial information. 

### Prepending 

Attackers can remove the 'external' email tag used by organizations to warn the recipient that an email has originated from an external source. This tricks individuals into believing that a malicious email was sent from inside their organization.

### Influence campaigns 

Often used in cyberwarfare, influence campaigns are usually very well coordinated and blend various methods such as fake news, disinformation campaigns and social media posts.

## Defending Against Deception 

Organizations need to promote awareness of social engineering tactics and properly educate employees on prevention measures.

* Never disclose confidential inforamtion or credentials via email, chat, text messages. In person or over the phone to unknown parties
* Resist the urge to click on enticing emails and web links 
* Be wary of uninitiated or automatic downloads 
* Establish and educate employees on key security policies 
* Encourage employees to take ownership of security issues.
* Do not give in to pressure by unknown individuals 
