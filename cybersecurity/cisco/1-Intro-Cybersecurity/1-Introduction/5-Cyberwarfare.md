# Cyberwarfare

Is the use of technology to penetrate and attack another nation's computer systems and networks in an effort to cause damage or disrupt services, such as shutting down a power grid.

## Sign of the times 

Example : Stuxnet malware that was designed not just to hijack targeted computers but to actually cause physical damage to equipment controlled by computers

## The Purpose

The main reason for resorting to cyberwarfare is to gain advantage over adversaries, whether they are nations or competitors

### To gather compromised information and/or defense secrets 

A nation or international organization can engage in cyberwarfare in order to steal defense secrets and gather information about technology that will help narrow the gaps in its industries and military capabilities.
furthermore, compromised sensitive data can give attackers leverage to blackmail personnel within a foreign government

### To impact another nation's infrastructure 

Besides industrial and military espionage, a nation can continuously invade another nation's infrastructure in order to cause disruption and chaos.

----
<br>

> Cyberwarfare can destabilize a nation, disrupt its commerce and cause its citizens to lose faith and confidence in their government.
