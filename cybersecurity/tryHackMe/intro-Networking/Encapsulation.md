# Encapsulation 

As the data passed down each layer of the model, more information containning details specific to the layer in question is added on to the start of the transmission.

stage | Data |Diagram
------|------|-------
\1. Application | Header is added | L7 header\|Data
\2. Presntation | Header is added | L6 header\|Data 
\3. Session     | Header is added | L5 header\|Data
\4. Transport   | Header is added | L4 header\|Data(segments/datagrams)
\5. Network     | header is added | L3 header\|Data(Packets)
\6. Data link   | Header and Trailes added| L2 Header \|Data(Frames)
\7. Encapsulated | Sent accross the network | Data Stream (bits)

When the message is received by the second computer, it reverses the process -- starting at the physical layer and working up until it reaches the application layer, stripping off the added information as it goes. This referred to as  **de-encapsulation** 




