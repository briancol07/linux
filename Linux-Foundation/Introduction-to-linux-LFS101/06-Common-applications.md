# Common applications 

## Index 

* [## Internet applications ](#internet)
* [## Email Apllications ](#email)
* [## Other internet applications ](#other)
* [## Productivity and development applications ](#prod)
* [## Multimedia applicatiosn ](#sound)
* [## Graphics editors and utilities ](#utilities)
* [### GIMP (GNU Image manipulation program) ](#gimp)
* [### Other utilities ](#other2)

## Internet applications <a name="internet"></a>

Internet allows users around the world to perform multiple tasks, such as searching for data, communicating through emails and online shoping.

* Web Browsers 
* Productivity applications 
  * Email Clients
  * Internet relay chats
  * Conferencing software 
* Online media applications 
  * Streaming media applications 

## Email Apllications <a name="email"></a>

Email applciations allow for sending , receiving and reading mesages ofer the internet. Most email clients use the Internet message access protocol (IMAP) or the older Post office protocol (POP). They display HTML (HyperText markup language) formatted emails that display objects. 
The features of advanced email applications include the ability of importin address  books /contact, list, ocnfigurin information and enmails from other email applicatoins 

* Linux supports 
  * Graphical email clients
    * Thunderbird 
    * Evolution 
    * Claws mail
  * Text mode email clients 
    * Mutt and mail
  * All web browser-based clients
    * gmail 
    * Yahoo 

## Other internet applications <a name="other"></a>

* FileZilla 
  * Intiuitive graphical FTP clietn atha supports FTP, SFTP and FTPS 
* Pidgin 
  * To access Gtalk, AIM, ICQ, MSN, IRC and other messaging networks 
* Hexchat 
  * To access internet realy chat (IRC) networks.


## Productivity and development applications <a name="prod"></a>

* Office applications 
  * Text: Writer 
  * Spreadsheets : Calc
  * Presentations : Impress
  * Graphical objects : Draw
* Development applications 
  * Advanced editors ( VI , emacs )
  * Compilers 
  * Debuggers 
  * performance measuring and monitoring programs 
  * GIT 
  * Complete IDE's 

## Multimedia applicatiosn <a name="sound"></a>

* Sound Players 
  * Amarok :graphical interface 
  * Audacity : record and edit sounds 
  * Audacious 
  * Rhythmbox
* Movie players 
  * VLC
  * MPlayer
  * Xine 
  * Totem
* Movie editors 
  * Blender 
  * Cinelerra
  * FFmpeg

## Graphics editors and utilities <a name="utilities"></a>

### GIMP (GNU Image manipulation program) <a name="gimp"></a>

Graphic editors aloow you to create, edit, view an organize images of various format(JPEG,JPG,PNG,GIF,TIFF).

* It can handle any image file format 
* It has many special purpose plugins and filters 
* It provides extenive information about the image, such layers, channels and historgrams 

### Other utilities <a name="other2"></a>

* Eye of Gnome (eog) 
  * image viewer
* Inkscape
  * Image editor with lots of editing features .
* Convert
  * CLI 
  * Can modify images files in many ways 
* Scribus 
  * Creating documents 

