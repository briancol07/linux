# Module 5 Arithmetic Operations

## Overview

1. Addition and Subtraction
2. Multiplication and Division
3. Powers and Remainders 
4. Celsius to Fahrenheit Calculator 

## 1 

To sum two variables you need to do :

```bash 
fs1=1
fs2=3

total=$(($fs1 + $fs2))
echo $total
```

## 2 

```bash
giga=$1
mega=$(($giga * 1024))
```
To get decimals you have to use the basic calculator, or you will get the int 

standard math lib

```bash 
echo "5/2" | bc -l
```

## 3 

The power is with 2 * and for the remainder you will use %

```bash 

echo $((2**3))
echo $((17%5))
```

## 4

scale 2 will show up to 2 decimals

```bash
F=$(echo "scale=2; $c*(9/5) +32" | bc -l)
```




