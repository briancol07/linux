# Wireless Standards 

## Wi-Fi Networks 

Institute of Electrical and Electronics Engineers (IEEE) is in charge of creating wireless technical standards 

* IEEE 802.11 
  * Governs the Wlan environmen 
  * Describe characteristics for different standars of wireless communications

The Wi-Fi Alliance is responsible for testing wireless LAN devices from differnet manufacturers.


## Wireless Settings 

* Network mode 
  * Determines the type of technology that must be supported
* Network Name (SSID)
  * Used to identify the WLAN. all devices that wish to participate in the WLAN must have the same SSID 
* Standard Channel 
  * Specifies the channel over which commmunication will occur.
  * By default this is set to Auto to allow the accespoint (AP) to determine the optimum channel to use
* SSID Broadcast 
  * Determines if the SSID will be broadcast to all devices within range 
  * Default = Enabled 


