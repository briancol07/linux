function! OpenPopupTerminal()
    " Open a hidden terminal buffer (assumes termopen is available)
    let buf = termopen('bash')
    
    " Use the buffer number to create a popup window for it
    call popup_create(buf, {})
endfunction
