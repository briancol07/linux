# Our Vim Tutor 

## we move with keys 

 UP -----> k
<br/> 
 Down ---> j
<br/>
 Left ---> h
<br/>
 Right --> l 
<br/> 

move to the end of words -----> e 
to move to the start of line ---> 0
To end of line ----------> $  
## Different types of mode 

1. esc is to enter normal mode 
2. " i "  to insert text

## Ways of exit 
first char |combination| meaning 
----------|------------|-------
 ESC | :q!| Quit without saving 
 ESC | :wq | Save and exit 

## Text editing 

meaning|letters
-------|-------
Undo/Undo all | u / U
Undo the undo's | CTRL + R 
Appending | A/a 
Delete char | x 
To put previos deleted txt | p 
To replace char | r 
to change until end of word | ce 




------- 
 d   |letter |meaning 
-----|----|--------
   -| w | delete until next word excluding its first char 
   -| e | delete to the end of current word , including last char 
   -| $ | delete to the end of line including last char 
   -| d | delete the hole line 

## Count For a motion 
> Typing a number before a motion repeats it that many times 
