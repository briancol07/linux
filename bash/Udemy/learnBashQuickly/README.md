# Learn Bash Quickly 

## Overview 

* [module-01-Overview](./module-01-Overview)
* [module-02-Variables](./module-02-Variables)
* [module-03-Arguments](./module-03-Arguments)
* [module-04-Arrays](./module-04-Arrays)
* [module-05-Arithmetic-Operations](./module-05-Arithmetic-Operations)
* [module-06-Strings](./module-06-Strings)
* [module-07-Decision-Making](./module-07-Decision-Making)
* [module-08-Loops](./module-08-Loops)
* [module-09-Functions](./module-09-Functions)
* [module-10-Automation](./module-10-Automation)
* [Debugging](./Debugging)
