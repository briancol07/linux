# TCP-IP

## Index

* [## Layers ](#layers)
* [## Tasks ](#tasks)

> Often referred to as Internet protocol Suite 

* Stands for the two protocols 
  * TransmissionControl Protocol
    * Located within the transport layer (4) 
  * Internet Protocol 
    * Located within the network layer (3) 

## Layers <a name="layers"></a>

Layer | Function 
:----:|:-------:
4 - Application | Allows applications to access the other layer's services and defines the protocols applications use to exchange data 
3 - Transport | Responsible for providing (TCP) session and (UDP) datagram services for the application layer 
2 - Internet | Responsible for host addressing, packaging and routing functions 
1 - Link Responsible for palcing the TCP/IP packets on the network medium and receiving corresponding packets from the network medium. TCP/IP is designed to work independently of the network access method, frame forma and medium .

Every application can transfer and exchange data over any network, and it does not matter where the receiver is located. IP ensures that the data packet reaches its destination, and TCP controls the data transfer and ensures the connection between data stream and application. 
 
![osi-tcp](./img/osi-tcp.png)

## Tasks <a name="tasks"></a>

Task |Protocol | Description
:----:|:-------:|:-------:
Logical Addressing | IP | Due to many hosts in different netwroks, there is a need to structure the network topology and logical addressing. IP takes over the logical addressing of networks and nodes. Data packets only reach the network where they are supposed to be. Method: Network classes subnetting and CIDR
Routing | IP | For each data packet, the next node is determiend in each node on the way from the sender to the receiver. This way, a data packet is routed to its receiver, even if its location is unknown to the sender 
Error & control flow | TCP | The sender and receiver are frequently in touch with each other via a virtual connection. Therefore cohntrol message are sent continuously to check if the connection is still stablished
Application Support | TCP | TCP and UDP ports form a software abstraction to distinguish specific applications and their communication links 
Name Resolution | DNS | Provides name resolution through fully qualified domain names (FQDN) in IP addresses,enabling us to reach the desired hsot with the specified name on the internet 
