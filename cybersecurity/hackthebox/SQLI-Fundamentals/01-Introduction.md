# Introduction 

Web application use database on the backend. To store an retrieve data related to the web application . o

To make the web application dynamic , the web application has to interact with the database in real-time. As HTTP(S) request 

![Intro](./img/01-Intro.png)

When a user-supplied information is used to construct the query to the database, malicious users can trick the query into being used ofr something other than what the original programmer intended, providing the userr access to query the database using an attack know as SQL injection ( SQLi ).

## SQLi

SQL injection occurs when a malicious user attempts to pass input that chnages the final SQL query sent by the web applications to the database enabling the user to perform other unintended SQL queries directly against the database. 

First the attacker has to inject code outside the expected user input limits, so it does not get executed as simple user input. In the most basic case this is done by injecting a single quote or a double quote to escape the limits of hte user input and inject data directly into the sql query 

* Can use [stacked queries](https://www.sqlinjection.net/stacked-queries/) or using [unions](https://www.mysqltutorial.org/mysql-basics/mysql-union/) queries .

## Use Cases and impact 

First, we may retrieve secret/sensitive information that should not be visible to us, like user logings and passwords or credit card information, which can then be used for other malicious purposes. SQL injectiosn cause many password and data breaches against websites ,which are then re-used to steal user accounts acces other services, or perform other nefarious actions 

Attacker may also be able to read and write files directly on the back-end server, which may in turn lead to placing backdoors.

## Prevention 

* Poorly coded web applications 
* Poorly secured back-end server and database privileges. 
* Prevention 
  * User input sanitization 
  * Validation
  * Proper back-end user privileges and control

