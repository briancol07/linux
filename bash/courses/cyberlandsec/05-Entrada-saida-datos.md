# Entrada de datos 

## Comando read 

El comando `read` se utiliza para recibir entrada de datos del usuario. Permite que el script haga preguntas o espere atos que el usuario debe introducir 

```
#!/bin/bash
echo "Introduce tu nombre:"
read nombre
echo "Hola, $nombre"
```

### Leer multiples entradas 

```
#!/bin/bash
echo "Introduce tu nombre y tu edad:"
read nombre edad
echo "Hola, $nombre. Tienes $edad años."
```

### Leer entradas silenciosas ( Contrasenias )

> sin mostrar lo que el usuario escribe 

Utilizar la flag -s con el comando read 

## Salida de datos 

El comando `echo` se utiliza para mostrar mensajes en la terminal.`echo "Mensaje a mostrar"`

### Escapando Caracteres especiales 

Si necesitas utilizar caracteres especiales como comillas dobles dentro de la cadena de texto puedes utilizar la barra invertida `\` para escaparlos 

## Redireccion de salida 

Permite redirigir la salida de un comando o script a un archivo lo que es util para guardar resultados 

El operador `>` se utiliza para redirigir la salida de un comando a un archivo. Si el archivo ya existe se sobreescribe.Para estos casos que se desea aniadir se usa `>>` para agregar al final.
Tambien se puede redirigir la salida de errores utilizando `2>`

## Redireccion de entrada 

En lugar de pedirla usa un archivo con el operador `<`

## Uso de pipes 

Los Pipes `|` permiten encadenar varios comandos, de manera que la salida de un comando sea la entrada de otro 

```
#!/bin/bash
ls -l | grep ".sh"
```
### Ejemplo avanzado

Se quiere encontrar todos los archivos `.log` y contarlos, guardar ese valor en un archivo 

```
#!/bin/bash
find /var/log -name "*.log" | wc -l > cantidad_logs.txt
```

## Tarea 

1. Crear un script que pida al usuario su nombre y comida favorita, guarde esta informacion en un archivo de texto 
2. Modifica el script para que, si el archivo ya existe, la nueva informacion se aniada sin sobreescribir contenido existente


