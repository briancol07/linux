# MAC addresses

## MAC <a name="mac"></a>

Each host in a network has it own 48bit (6 octets) Media access control address, represented in hexadecimal format.

* Standard 
  * Ethernet  (IEEE 802.3)
  * Bluetooth (IEEE 802.15)
  * WLAN      (IEE 802.11)

These addresses the physical conenction (network card, bluetooth, wlan adapter)
of a host. Is configured once on hte manufacturer's hardware side but can always be changed, at least temporarily

* Formats 
  * DE:AD:BE:EF:13:37
  * DE-AD-BE-EF-13-37
  * DEAD.BEEF.1337

![example](./img/example-mac.png)

When an IP pakcet is delivered, it must be addressed on layer 2 to the destination host's physical address to the router. Each packet has a sender address and a destination address.

The first half (3bytes/24 bit) is the SO-called `organization unique identifies (OUI)` defined by the IEEE ( Institute of electtrical and electronics engineers )

![oui](./img/oui.png)

The last half of the MAC address is called the individual address part or Network interface controller (NIC), which the manufacturers assign. THe manufacturer sets this bit sequence only once and thus ensures that the complete address is unique.

If the hsot belongs to a different subnet, the ethernet frame is addressed to the mac address of the responsible router( default gateway). If the ethernet frame's destination address matches its own layer 2 address the router will forward the fram to the higher layers. 

Address Resolution Protocol (ARP) is used in IPv4 to determine the MAC address associated with the IP addresses o

The local range is the second number/letter : 02:00:00:00:00:00

## Unicast <a name="unicast"></a>

THe last two bits in the first octet can play another essential role. The last bit identifies the MAC address as Unicast(0) or multicast(1). With unicast it means that the packet sent will reach only one specific host .


## Multicast <a name="multicast"></a>

With multicast the packet is sent only once to all hosts on the local network, which then decides whether ot not to accept the packet based on their configuration. 

The multicast address is a unique address, just like broadcast address 
