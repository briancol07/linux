# Module 7 Decision making 

## Overview 

1. Using if statement 
2. Using if-else statement
3. Using elif statement
4. Using nested if statements 
5. Using case statement 
6. Test conditions in bash

## 1-Using if statement 

You need to leave spaces in the brakets like this [ a = 'p' ]

```
if [condition]; then
  your code
fi
```
## 2-Using if-else statement

```
if [condition]; then
  your code
else 
  more code
fi
```

## 3-Using elif statement

```
if [condition]; then
  your code
elif [condition]; then
  your code
elif [condition]; then
  your code 
else 
  more code
fi
```
## 4-Using nested if statements 

> Something inside of something

```
if [condition]; then
  if [condition]; then
    your code
  fi
elif [condition]; then
  your code 
else 
  more code
fi
```

## 5-Using case statement 

``` 

case expression in 
  pattern1 )
    Commands;;
  pattern2 )
    Commands;;
  pattern3 )
    Commands;;
  * )
    Commands;;
esac

```

## 6-Test conditions in bash

Condition   | Meaning 
:----------:|:---------:
$a -lt $b   | $a < $b
$a -gt $b   | $a > $b
$a -le $b   | $a <= $b
$a -ge $b   | $a >= $b
$a -eq $b   | $a == $b
$a -ne $b   | $a != $b
\-e $FILE   | $FILE exists
\-d $FILE   | $FILE exits and is a directory
\-f $FILE   | $FILE exits and is a regular file
\-L $FILE   | $FILE exists and is a soft link (symbolic)
$strA = $strB | $strA is equal to $strB
$strA != $strB| $strA is not equal to $strB
\-z $str1   | $str1 is empty

You can check all this you can do **man test**





