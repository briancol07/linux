# Union CLause 

* [Union](https://dev.mysql.com/doc/refman/8.0/en/union.html)

Used to combine result from multiple select statements. This means that through a union injection, we will be able to select and dump data from all across the DBMS.

> Data types of te selected columns on all positions should be the same 

```
SELECT * FROM ports UNION SELECT * FROM ships;
```

## Un-even columns 

Sometimes we know that there are un-even the queries so we should fill with junk 

```
SELECT "junk" from passwords
```

When fillign other colums with junk data, we must ensure that the data type matches the columns data type, otherwise the query will return an error . Fot hte sake of simplicity, we will use number as our junk data, which will also become handy for tracking our payloads positions,as we will discuss later 

```
SELECT * from products where product_id = '1' UNION SELECT username, 2 from passwords

