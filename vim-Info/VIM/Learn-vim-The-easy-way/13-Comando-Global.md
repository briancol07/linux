# El comando global 

El comando global de vim es utilizado para ejecutar un comand de la linea de comandos en multiples lineas de manera simultanea . Llamado `comandos Ex` por que provenian del editor de texto externo , para mas ayuda `:h ex-cmd-index`

Tiene la siguiente sintaxis 

```
:g/patron/comando
``` 

El patron busca todas las lineas que contengan ese patron, de manera similar al patron en el comando de sustitucion.

Ejemplo : Para eliminar todas las lineas que contengan la palabra "console"

``` 
:g/console/d
```
## Invertir las coincidencias 

Para las que no cumplan con la condicion

```
:g!/patron/comando
:v/patron/comando 
```

### Ejemplos 

```
:g/one\|two/d
:g/[0-9]/d
:g/\d/d
```

## Pasando un rango

* Encuentra la cadena "console" entre la linea 1,5 ylas elimina `:1,5g/console/d`
* Si no se especifica un numero delante de la coma entonces desde la linea actual `:,5g/console/d`
* Si no se especifica el numero despues de la coma , el comando termina en la linea actual `:1,g/console/d` ( entre linea 3 y actual) 
* Si solo se aclara un numero busca en esa linea y si encuentra elimina `:5g/console/d`
* Simbolos como rango 
  * `.` signfica linea actual `.,3` entre la linea actual y 3 
  * `$` Significa la ultima linea del archivo  `3,$` linea 3 y la ultima 
  * `+n` n lineas despues de la actual `3,+1`


## El comando normal 

Puedes ejecutar un comando normal dentro del comando global con el comando `:normal`

Ejemplo: ` :g/./normal A;` El punto significa para todas las que no esten vacias ( cualquier caracter )

## Ejecutar una macro 

Tambien se puede ejecutar una macro con el comando global. Una macro es simplemente un modo normal de operacion.

Macro para agregar ; al final `qaA;<Esc>q`
Ejecutarla `:g/./normal @a`

## Comando recursivo global 

Es un comando de la linea de comandos asi que se puede ejecutar dentro de un comando global 

Ejemplo `:g/console/g/two/d` Primero buscara console y luego dentro de las que encontro buscara two y las eliminara 

## El comando predeterminado 

El comando seria `:p` por default y es print , mostrara en pantalla las lineas 

## Mas ejemplos 

Invertir archivo `:g/^/m 0` , el aptron `^` siginifica comienzo de una linea 
ver ejemplos 

