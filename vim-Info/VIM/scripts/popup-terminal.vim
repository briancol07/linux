function! OpenPopupTerminal()
    let popup_opts = {
    \ 'line': 'center',
    \ 'col': 'center',
    \ 'minwidth': 80,
    \ 'minheight': 20,
    \ 'border': [],
    \ }
    let buf = termopen(&shell)
    call popup_create(buf, popup_opts)
endfunction

call OpenPopupTerminal()
