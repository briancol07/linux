# Conceptos Importantes

1. [Que es una vulnerabilidad?](./1-Vulnerabilidad.md)
2. [Conexion Directa](./2-Conexion-Directa.md)
3. [Conexion Inversa](./3-Conexion-Inversa.md)
4. Conexion del lado del cliente
