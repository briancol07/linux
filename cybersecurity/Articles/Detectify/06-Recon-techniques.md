# Recon techniques 

> From hacker akhil george

## What is recon? 

COnsists of the method followed to widen the attack surface of a target or to fingerprint the acquired targets and much more. 

* Steps 
  * Subdomain recon 
  * Passive endpoint gathering 
  * Subdomain takeover 
  * Directory brute-forcing 
  * Visual recon
  * Javascript recon
  * Sensitive data Exposure: Firebase and slack webhook tokens 
  * Managing data and failed attempts 

### Subdomain recon 

This is one of the main steps as it gives us an idea of 3rd party used by the company 

* Tools 
  * [subfinder](https://github.com/projectdiscovery/subfinder)
  * [Amass](https://github.com/owasp-amass/amass)
  * [Massdns](https://github.com/blechschmidt/massdns)
  * [Dnsgen](https://github.com/AlephNullSK/dnsgen)

* Run subfinder with api 
* Gather subdomains form data sources not covered in subfinder
* Run amss for active and passive gathering of data 
* Run massdns with wordlist from commonspeak
* gather all the doamins from above , run dnsgen
* Resolve them and continue with endpoint gathering 

## Gathering endpoint passively 

Use data sources as the have endpoints which sometimes cannot be found by brute-forcing 

* [Commoncrawl](https://commoncrawl.org/)
* [virustotal](https://www.virustotal.com/)
* [Urlscan](https://urlscan.io/)
* [Archive.is](http://archive.is/)

## Javascript recon 

One way is to gather javascript files for this you can use [subjs](https://github.com/lc/subjs)
then run a tool called [meg](https://github.com/tomnomnom/meg) by tomnomnom . It gathers the content of the javascript files and saves it onto a directory which can then be used for further processing.

* [linkfinder](https://github.com/GerbenJavado/LinkFinder)
* gf to grep for sensitive informaiton 
* grep for comments in the js files 

## Subdomain takeover 

The bug class which allows an attacker to takeover a subdomain due to an unused DNS record 


## Sensitive data exposure 

* Code commits 
* Password dumps 
* Slack webhook tokens 
* Firebase API tokens 

## meg + fg

Meg is a tool used for gathering responses for a path or a set of path for a list of domains, gf is a tool used to grep for keywords in a huge dataset quickly 

## Directory and endpoint brute-forcing 

Find directories and endpoints by brute-forcing it agains a wordlist 

`Parallel + gobuster`

## Visual recon aka screenshots 

* [tool](https://github.com/sensepost/gowitness)
* [eyewitness](https://github.com/FortyNorthSecurity/EyeWitness)
* [httpscreenshot](https://github.com/breenmachine/httpscreenshot)

* Managing Data
* Grabbing titles of each web page 
* Grabbing domains which returns servfail response tocheck for ns record based takeovers 
* Grabbing all the dns recors to llok for partiuclar cname or such 

## Failed attempts 

Gathering the ASNs an organization is under to get the ip Ranges 

* Gather the org name from the ssl cert 
* Use the ip output from aquatone and greap each ip with the organization name using `whois.cymru.com` to get ASN.

Using shodan cli for details on each host, limited credits on shodan 



