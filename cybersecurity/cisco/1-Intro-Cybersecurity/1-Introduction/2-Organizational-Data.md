# Organizational Data 

Traditional data is typically generated and maintained by all organizations,big and small.

## Transactional data 

> Such as details relating to buying and selling, production activities and basic organizational operations such as any information used to make employment decisions 

### Intellectual property 

> such as patents, trademarks and new produc plans, which allows an organization to gain economic advantage over its competitors. Is know as trade secet and losing it could prove disastrous for the future of a company 

### Financial data

> Such as income statement,balance sheet and cash flow statement which provide insight into the health of a company.
  
## Iot and Big Data 

> Iot is a large networkof physical objects. Connected to the internet, with the ability to collect and share data. Expanding through the cloud and virtualization. Create a new are of interest in technology and business called **Big Data**

* Sensors
* SW 
* Othe equipment.

<br>
----
<br>

## The cube 

> Is a model framework to help organizations estabish and evaluate information security initiatives by considering all of the related factors that impact them.

* Three Dimensions 
  1. The foundational principles for protecting information systems
  2. The protection of information in each of its possible states
  3. The security measures used to protect data 

### 1. The foundational principles for protecting information systems

####  Confidentiallity 

> A set of rules that prevent sensitive information from being disclosed to unauthorized people, resources and processes . 

* Method to ensure confidentiallity include:
  * Data Encryption
  * Identity Proofing 
  * Two factor authentication

#### Integrity 

> Ensures that system information or processes are protected from intentional or accidental modification

* To ensure 
  * Hash function
  * Checksum

### Availability 

> Means that uthorized users are able to access system and data when and where needed and those that do no meet established conditions are not.

* Achieved by:
  * Maintaining equipment
  * Performing HW reapirs 
  * Keep operating systems and SW up to date 
  * Creating backups 

## 2. The protection of information in each of its possible states

### Processing 

> Refers to data that is being used to perform an operationsuch as updating a database record (data in process)

### Storage 

> Refers to data stored in memory or on a permanent storage device such as a hard drive or USB drive (data at rest)

### Transmission 

> Refers to data traveling between information systems (data in transit)

## 3. The security measures used to protect data 

### Awareness, Training and education 

> Are the measure put in place by an organization to ensure that users are knowledgeable about potential security threats and the actions they can take to protect information systems.

### Tehcnology 

> Refer to the SW and HW based solutions designed to protect information systems such as firewalls, which continuously monitor your network in search of possible malicious incidents 

### Policy and Procedure 

> Refers to the administrative controls that provide a foundation for how an organization implements information assurance such as incident response plans and best practice guidelines.


