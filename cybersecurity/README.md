# Cybersecurity 

## Roadmap 

* [Roadmap-sh](https://roadmap.sh/cyber-security)
* [Hacker-Roadmap](https://github.com/Hacking-Notes/Hacker-Roadmap?tab=readme-ov-file)
* [TCM-Security](https://tcm-sec.com/how-to-be-an-ethical-hacker-in-2025/)

## Links 

* [CTFs](https://github.com/Crypto-Cat/CTF?tab=readme-ov-file)
* [Hacking-Wifi](https://medium.com/@shunxianou/tryhackme-wifi-hacking-101-detailed-step-by-step-walk-through-9ee4da7f86c2)
* Bounty with Bash 
  * [Bounty-tomnomnom](https://www.youtube.com/watch?v=s9w0KutMorE)
  * [Hacking with vim](https://www.youtube.com/watch?v=l8iXMgk2nnY&t=541s)
* General info 
  * [Brevity-In-Motion](https://www.brevityinmotion.com/learning)
* Programming 
  * [Python-Ryan-john](https://www.youtube.com/watch?v=XWuP5Yf5ILI)
* Web explotation
  * [hexdump-9hs](https://www.youtube.com/watch?v=ik2p4Rz4QzM)
  * [list-omar-palomino](https://www.youtube.com/watch?v=Gc_Dm5z9p08&list=PLWDGPX-YlS2mGnOkCKMLY7kp5MCWuHaDZ)
  * [Red-teaming-eth-7hs](https://www.youtube.com/watch?v=OtcP8c4wZys)
  * [whitesec-11hs](https://www.youtube.com/watch?v=wNA4CLG-OSM)
* Cert
  * [Compitia-Pentest+-11hs](https://www.youtube.com/watch?v=WczBlBjoQeI)
* Curiosity 
  * [RedGuild](https://blog.theredguild.org/the-state-of-the-red-guild-6/)
* Trainnings 
  * [RanaKhalil101](https://www.youtube.com/@RanaKhalil101)
  * Cursos
    * [Cyberland](https://cyberlandsec.com/)
* Firewall
  * [pfSense](https://www.pfsense.org/our-services/#pfsense-training)
* [Curso-Hefin.net](https://www.youtube.com/watch?v=7JcyW9AKiZA)

## Blueteam 

* [Cyberdefenders](https://cyberdefenders.org/) ( este es mejor )
* [Level-Effect](https://www.youtube.com/@leveleffect)
* [LetsDefend](https://letsdefend.io/) 
