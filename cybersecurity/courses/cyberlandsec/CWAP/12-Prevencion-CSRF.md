# Prevencion de CSRF 

## Indice 

* [## Que es el Cross-site Request forgery ? ](#csrf)
* [## Como funciona el ataque? ](#funcionamiento)
  * [### Ejemplo ](#ejemplo)
* [## Impacto ](#impacto)
* [## Como Prevenir? ](#prevenir)

## Que es el Cross-site Request forgery ? <a name="csrf"></a>

Es un ataque en el que un atacante engania a un usuario autenticado para que realice acciones no deseadas en una aplicacion web sin su consentimiento. Estos ataques aprovechan la confianza que el servidor tiene en el usuario autenticado, haciendo que el servidor acepte solicitudes maliciosas como si provinieran del propio usuario.

![csrf](./img/csrf.png)

## Como funciona el ataque? <a name="funcionamiento"></a>

Cuando una victima autenticada en una aplicacion web es enganiada para que realice una solicitud no deseada a esa aplicaicon. 

1. El usuario inicia sesion en una aplicacion web legitima 
2. Mientras el usuario sigue autenticado, visita un sitio malicioso que contiene un enlace o formulario oculto 
3. El sitio malicioso envia una solicitud a la aplicacion legitima utilizando las credenciales de sesion del usuario 
4. La aplicacion web legitima, sin saber que la solicitud fue inicia por un atacante, procesa la solicitud como si fuera legitima.

### Ejemplo <a name="ejemplo"></a>

```
http://banco.com/transfer?account=123&amount=1000
```
Ataque : Hace click en un enlace malicioso como este: 

```
<img src="http://banco.com/transfer?account=999&amount=1000" />
```

## Impacto <a name="impacto"></a>

* Transferencia no autorizada de fondos
* Cambio en la configuracion de la cuenta ( como contrasenias o correos electronicos )
* Envio de mensajes o publicacion de contenido en nombre del usuario 
* Eliminacion de cuentas o datos 

## Como Prevenir? <a name="prevenir"></a>

* Utilizar toekns CSRF
  * Valor unico y aleatorio que se genera por el servidor y se incluye en cada formulario o solicitud autenticada 
  * Funcionamiento
    * Cuando el servidor carga una pagina que contiene un formulario genera un token CSRF unico y lo incluye en el formulario 
    * Cuando el usuario envia el formulario el token se envia junto a la solicitud 
    * El servidor verifica que el token recibido coincida con el token generado antes de procesar la solicitud, si no coincide rechaza 
* Metodos de solicitud seguros 
  * Las solicitudes `GET` son mas faciles de explotar 
* Uso de samesite en las cookies 
  * Configurar las cookies con `SameSite=Strict`, evita que las cookies de sesion se envien en solicitudes provenientes de otros sitios 
* Verificacion de referer y Origin 
  * verificar las cabeceras `Referer` y `Origin`.
  * Indican desde que pagina se envio la solicitud 
* Limitar duracion de la sesion 


