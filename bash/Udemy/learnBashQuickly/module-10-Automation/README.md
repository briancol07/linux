# Module 10 Automation with bash 

## Overview

1. Automating User Management 
2. Automating Backups
3. Monitoring Disk space 

## Automatically 

if you want your code to run automatically at any time you should add it in crontab

``` bash 

crontab -e 
crontab -l
``` 
