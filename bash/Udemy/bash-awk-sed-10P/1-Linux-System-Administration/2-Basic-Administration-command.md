# Basic linux administration commands 

## hostname 

> Print the name of the computer 

```bash  
hostname

# Output ->  brian-H55M-D2H
``` 

## uname

> Information about the machine 

```bash 
uname 

# Output -> Linux

uname -a
# Output -> Linux brian-H55M-D2H 5.15.0-56-generic #62-Ubuntu SMP Tue Nov 22 19:54:14 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux
```

## who 

> Informaiton about the user 

```bash 

who
# Output -> 
# brian    tty7         2022-12-13 15:55 (:0)
# brian    pts/0        2022-12-13 18:58 (192.168.0.209)
``` 

## whoami 

> Tell the current user 

```bash 
whoami
# Output -> brian
``` 

## id 

> Give information about the user, group user 

```bash 
id 
# to see information about that user 
id root 
``` 
