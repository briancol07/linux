# From Monolith to Microservices 

## Index 

* [## The modern Microservice ](#microservice)
* [## Refactoring ](#refactoring )
* [## Challenges ](#challenges)

A monolith has a rather expensive taste in hardware. Being a large, single piece of software which continuously grows, It has to run on a single system which has to satisfy its compute, memory, storage and networking requirements. 

## The modern Microservice <a name="microservice"></a>

Microservices can be deployed individually on separate server provisioned with fewer resources. Only what is requires by each service and the host system itself, helping to lower compute resource expenses 

Microservices-based architercture is aligned with event-driven architecture and service oriented architecture (SOA) principles, where complex applications are composed of small programming interfaces (APIs) over a network.

Each microservice is developed and written in a modern programming language, selected to be the best suitable for the type of service and its business function.

* Adds complexity 
* Scalability 
  * Individually
* Modular 
* Demand-based autoscaling 

There is virtually no downtime and no service distuption to client because upgrades are rolled out seamslessly ( one service at a time )

## Refactoring <a name="refactoring "></a>

Monolith to microservices ->> Refactoring 

A so-called "big-bang" approach focuses all effort with the refactoring of the monolith postponing the development and implementation of any new features.

An incremental refactoring approach guarantees that new features are developed and implemented as modern microservices which are able to communicate with the monolith through APIs, without appending to the monolith's code.

The refactoring phase slowly tansforms the monolith into a cloud-native application which takes full advantage of cloud features, by coding in new programming languages and applying modern architectural patterns.

## Challenges <a name="challenges"></a>

when considering a legacy mainframe based system, written in older programming languages - cobol or assembler, it may be more economical to just re-build it from the ground up as a cloud-native application.

Once the monolith survived the refactoring phase, the next challenge is to design mechanisms or find suitable tools to keep alive all the decoupled modules to ensure application resiliency as a whole.

Eventually a solution emerged to tackle refactoring challenges. Application containers came along providing encapsulated lightweight runtime environments for application modules. 

Containers promised consistent SW environments for developers, tester, all the way from development to production.

* Individual module scalability
* Flexibility 
* Interoperability 
* Easy integration with automation tools 
