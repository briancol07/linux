# Network Components 

## Network Infrastructure 

This network infrastructure is the platform that supports the network. It provides the stable and reliable channel over which our communications can occur 

* End devices
* Intermediate devices
* Network media 

Devices and media are the physical elements or HW of the network.

## End Devices 

> Hosts 

These devices from the interface betwee users and the underlying communication network.

* Computers 
* Network printers
* Telephones and teleconferencing equipment 
* Security cameras 
* Mobile devices 

Source or destination of a message transmitted over the network 
