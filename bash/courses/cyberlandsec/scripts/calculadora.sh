#!/bin/bash
echo "Introduce el primer número:"
read numero1

echo "Introduce el segundo número:"
read numero2

echo "Selecciona una operación (+, -, *, /):"
read operacion

case $operacion in
    +)
        resultado=$((numero1 + numero2))
        echo "El resultado de la suma es: $resultado"
        ;;
    -)
        resultado=$((numero1 - numero2))
        echo "El resultado de la resta es: $resultado"
        ;;
    \*)
        resultado=$((numero1 * numero2))
        echo "El resultado de la multiplicación es: $resultado"
        ;;
    /)
        resultado=$((numero1 / numero2))
        echo "El resultado de la división es: $resultado"
        ;;
    *)
        echo "Operación no válida"
        ;;
esac
