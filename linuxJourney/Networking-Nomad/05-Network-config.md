# Network Config

## Index

* [## Network interfaces ](#interfaces)
  * [### ifconfig command ](#ifconfig)
  * [### Create an interface and bring it up ](#create)
  * [### ip Command ](#ip)
* [## Route ](#route)
* [## dhclient ](#client)
* [## Network manager ](#manager)
* [## arp ](#arp)

## Network interfaces <a name="interfaces"></a>

Is how the kernel links up the software side of networking to the hardware side.

### ifconfig command <a name="ifconfig"></a>

`ifconfig -a`

Allow us to configure our network interfaces, if we don't have any network interfaces set up, the kernel's device drivers and the network won't know how to talk to each other. IT runs on bootup and configures our interfaces through config file, but we can manually modify them.

The output of ifconfig shows the interface name on the lef side and the right side shows detailed information.

you'll most commonly see interfaces named: 

* eth0 First ethernet card in the machine
* wlan0 wireless interface 
* lo loopback interface
  * Used to represent your computer 
  * Good for debugging local server
* Things you will search 
  * HWaddr ( MAC address ) 
  * inet address (IPv4 address 
  * inet6 (IPv6)
  * Subnets masks and broadcast address

### Create an interface and bring it up <a name="create"></a>

This assigns an IP address and netwmask to the eth0 interface and also turns it up

```
ifconfig eth0 192.168.2.1 netmask 255.255.255.0 up
```

### ip Command <a name="ip"></a>

Also allow us to manipulate the networking stack of a system. Depending on the distribution you are using it may be the prefereed method of manipulating your network setting.

* To show interface information for all interfaces `ip link show`
* To show the statistics of an interface `ip -s link show eth0`
* To show ip addresses allocated to interfaces `ip address show`
* To bring interfaces up and down
  * `ip link set eth0 up`
  * `ip link set eth0 down`
* To add an IP address to an interface `ip address add 192.168.1.1/24 dev eth0`


## Route <a name="route"></a>

Viewing our routing tables with the route command 

* Add a new route `sudo route add -net 192.168.2.1/23 gw 10.11.12.3`
* Delete a route `sudo route del -net 192.168.2.1/23`
* To add a route `ip route add 192.168.2.1/23 via 10.11.12.3`
* To delete a route 
  * `ip route delete 192.168.2.1/23 via 10.11.12.3`
  * `ip route delete 192.168.2.1/23`

## dhclient <a name="client"></a>

The dhclient starts up on boot and gets a list of network interfaces from the dhclient.conf file. For each interface listed it tries to configure the interface using the DHCP protocol.

In the dhclient.leases file, keeps track of a list of leases across system reboots, after reading dhclient.conf, the dhclient.leases file is read to let it know what leases it's already assigned 

* Obtain a fresh IP `sudo dhclient`

## Network manager <a name="manager"></a>

Most distributions utilize the networkManager daemon to configure  their networks automatically

For instance on startup NetworkManager will gather network hardware information, search for connections to wireless, wired, etc. 

* There is a tool called `nm-tool`
* `nmcli` command allows you to control and modify networkManager

## arp <a name="arp"></a>

It first checks the locally stored arp cache on our system, you can actually view this cache.

The arp cache is actually empty when a machine boots up, it gets populated as packets are being sent to other hosts. If we send a packet to a destination that isn't in the arp cache this happends 

1. The source host creates the ethernet frame with ARP request packet 
2. The source host broadcast this frame to the entire network
3. If one of the host on the network knows the correct MAC, it will send a reply packet and frame containeing the MAC
4. The source host adds the IP to MAC address mapping to the ARP cache and then proceeds with sending the packet

You can also view your arp cache via the ip command `ip neighbour show`
