# Hacking Android apps 

Bypass certificate pinning 
limits app to trust predefined certificates only
SSLpinning / cert pinning -> against man in the middle 

> use Frida 

## Anatomy of an app 

* Android package 

> All the things ned to operate (zip)

* Android manifest (xml): 
  * Meta data 
  * Insight of the app
  * Permission to share data
* Components 
  * Activities : 
    * Interact with user -> windows
  * Services: 
    * Long running operations 
  * Broadcast receivers 
    * Alolow app to respond broadcast messages 
  * Content providers 
    * Way to share data
* Clases.dex
  * App source code compiled (the dex file format .nte) 
* Resources.arsc 
  * Precompiled resources ( Strings , colors , styles ) 
* Lib folder 
  * Compiles code that is platform dependents 
  * Compiled kernel 
* Assets folder 
  * Video 
  * Audio 
  * Documents templates 
* Meta inf folder 
  * manifest ( inf ) 
    * Store data of app 
  * Certificate 
  * Signature apk 
* Android debug bridge ( adb ) 
  * Command line tool 
    * Communicate with android devices connected 
* Android studio
* Apktool 
  * Reverse engineering 
    * Apk into readable soruce code file 
* Frida 
  * Toolkit that let you inject scripts into runnign processes
* Mobile security framework 
  * Mobile app testing 

