#!/bin/bash 

function ask(){
  read -p "$1 (y/n): " response 
  [ -z "$response" ] || [ "$response" = "y" ] 
}


for file in ".vimrc" ".tmux.conf"; do 
  if ask "Do you want to copy this ${file} to HOME ?"; then 
    cp -f ${file} ./.. 
  fi
done 

