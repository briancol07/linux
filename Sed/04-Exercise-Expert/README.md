# Expert 

## Exercises 

1. Reverse the order of words on each line.
2. Delete all lines before the first line that contains "BEGIN".
3. Replace all instances of "foo" with "bar" on lines that contain "baz".
4. Insert a line between every two lines in a text file.
5. Replace text within parentheses with "REDACTED".
6. Extract and print only the third word from each line in a file.
7. Replace the first occurrence of "foo" after line 10 with "bar".
8. Convert a comma-separated list into a tab-separated list.
9. Delete all lines that contain more than one occurrence of a word.
10. Replace every word "foo" with "bar", but only on lines that contain exactly 3 words.

# Expert + 

## Exercises

1. Replace only the words that are exactly 4 letters long with "XXXX".
2. Insert a line "NOTE:" before every line that contains "warning".
3. Replace the contents of a line with "EMPTY" if the line is blank.
4. Extract and print the domain names from email addresses in a file.
5. Delete the first and last word on each line.
6. Replace a sequence of digits with "[NUMBER]" in a file.
7. Insert the current date at the beginning of each line in a file.
8. Replace all occurrences of "foo" with "bar" on odd-numbered lines.
9. Reverse the order of lines in a file.
10. Replace all occurrences of the word "foo" with "bar", but only if "baz" appears earlier on the same line.
