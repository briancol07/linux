# File index 

* [## More features ](#feature)

This will list the file in the folders an make an index 

 With this command that show only the files without directory : `ls -p | grep -v / `

```
* [file](./file)
* [file2](./file2)
* [file3](./file3)
* [file4](./file4)
```

## More features <a name="feature"></a>

- [ ] Check if the string is not in the file if there is continue 

## Files 

* [README.md](./README.md)
* [file-index.sh](./file-index.sh)
