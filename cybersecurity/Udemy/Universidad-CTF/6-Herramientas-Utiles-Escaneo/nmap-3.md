# NMAP 3  

* Se guardan 2 ips en un archivo de texto 

## Escanear a partir de un archivo de texto 

```bash 
nmap -Pn -il <UbicacionArchivo>
```

##  Sacar version de servicio

```bash 
nmap -sV <IP>
```

## Para sacar la version del sistema Operativo

```bash 
nmap -O <IP>
```

### Sacar mas info 

Son scripts extras que sacan mas informacion 

```bash 
nmap -sC <IP>
```

## Combinacion de Varios 

> Este caso la -A == -sV -O -sC

```bash 
nmap -A <IP>
```

## Mostrar avance cada x tiempo 

```bash 
nmap -stats-every 10s <IP>
```
En este caso nos mostraria avance cada 10 segundos

