# Hack with 'goodfaith'

> A tool to automate and scale good faith hacking 

> The tough part about hacking is to stay in scope 

## Importance of good faith hacking 

* [Press-release](https://www.justice.gov/opa/pr/department-justice-announces-new-policy-charging-cases-under-computer-fraud-and-abuse-act)

This policy of `united states department of justice (DOJ)` Does not directly protect a researcher from a corporation pursuing charges.

## Scope challenges vs good faith 

* Requires manual intervention along the way to ensure that programming processes and recursion are not going out of scope of the program 

There are many variations of scope that are defined between programs as well as schema variations across platforms. All of the variations need to be processed and actioned.

## Solution to overcome the challenges 

* Feed them one by one into the automation workflows 
  * Downside : the program is updating the scope 

> if it’s not running, you’re not being 100% efficient 

Best way is to develop custom software to process and continuously monitor the scope 

## Intro to goodfaith 

* [goodfaith](https://github.com/brevityinmotion/goodfaith)

Intended to help researchers avoid generating traffic agains out-of-scope targets.

* The tool can : 
  * Compare a list of URLs to a program scope file and output the explicitly in-scope targets 
  * Be utilized within bug bounty one-liners to process standards input and deliver it to downstream tool via standard output 
  * Be impoorted as a module inot a larger project or automation ecosystem 
  * Generate statistics for processed url , scope status and unique domains 
  * Output a graph database visualization displayin relationshipt between the URLs 
  * Generate scope files for the entire public bug bounty space across platforms ( h1,bugcrowd,etc)

## Moving Forward

> Does not by default mean that you are acting in good faith 

Scope is just one component as other areas such as researcher intent, permission, disclosure confidentiality and compliance with anything else 
