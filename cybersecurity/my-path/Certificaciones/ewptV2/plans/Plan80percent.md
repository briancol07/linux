# Bug bounty with Pareto Rule 

> Recuerda que el principio de Pareto es solo una guía y puede que necesites ajustar tu plan de aprendizaje según tu progreso y áreas de interés específicas. 

1. Fundamentos de Seguridad Web (20%):
    * Aprender los conceptos básicos de seguridad web, como inyecciones SQL, XSS, CSRF, etc.
    * Recursos: Sitios web de tutoriales de seguridad web, como OWASP (Open Web Application Security Project) y PortSwigger Web Security Academy.
2. Herramientas de Hacking Ético (20%):
    * Familiarizarte con herramientas como Burp Suite, Nmap, Metasploit, etc.
    * Recursos: Tutoriales en línea, documentación oficial de las herramientas y cursos de plataformas como Udemy o Coursera.
3. Reconocimiento y Enumeración (20%):
    * Aprender a identificar y recopilar información sobre el objetivo, como subdominios, tecnologías utilizadas, etc.
    * Recursos: Utiliza herramientas como recon-ng, sublist3r, amass, etc., y practica en entornos controlados como TryHackMe o Hack The Box.
4. Explotación (20%):
    * Practicar la explotación de vulnerabilidades comunes, como XSS, SQLi, RCE, etc.
    * Recursos: Laboratorios de práctica en plataformas como PortSwigger Web Security Academy, Hack The Box y PentesterLab.
5. Informes de Bug (20%):
    * Aprender a redactar informes claros y detallados sobre las vulnerabilidades encontradas.
    * Recursos: Guías de informes de errores en plataformas de bug bounty, ejemplos de informes de errores bien redactados y retroalimentación de la comunidad de bug bounty.

