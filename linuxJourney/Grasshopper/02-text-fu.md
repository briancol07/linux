# Text - Fu 

## Index

* [## Standard out (stdout) ](#stdout)
* [## Standard In (stdin) ](#stdin)
* [## Standard Error (stderr) ](#stderr)
* [## Pipe and Tee ](#pipe)
* [## Environment (env) ](#env)
* [## cut ](#cut)
* [## paste ](#paste)
* [## Head ](#head)
* [## tail ](#taill)
* [## expand and unexpand ](#expand)
* [## join and split ](#join)
* [## sort ](#sort)
* [## tr (Translate) ](#tr)
* [## uniq (Unique) ](#uniq)
* [## wc and nl ](#wc)
* [## grep ](#grep)

## Standard out (stdout) <a name="stdout"></a>

``` bash 

echo Hello world > peanut.txt 

```

This will print the hello world inside a file called peanut.txt. The > tell where to redirection (redirecction operator). To change where standard output goes . If the file does not exist it will create it for us . However if it does exist it will overwrite it . So you can use >> to add at the end of file . 

## Standard In (stdin) <a name="stdin"></a>

``` bash 
cat < peanut.txt > banana.txt 
```

The redirection for stdin is < . In this case we redirected  peanut.txt to be our stdin . Then the output of cat peanut.txt gets redirected to another file called banana.txt 

## Standard Error (stderr) <a name="stderr"></a>

If you try to redirect something that does not exist it will show an error.
By default stderr sends its output to the screen as well. To redirect stderr we have to use a file descriptors ( stdin = 0, stdout = 1 and stderr = 2) 

``` bash 
ls /fake/directory 2> peanut.txt 
ls /fake/directory > peanuts.txt 2>&1
ls /fake/directory &> peanuts.txt
```
the second one goes stderr and stdout, the third one is a shorter way 

if I don't want any of that cruft and want to get rid of stderr messages completely? Well you can also redirect output to a special file call /dev/null and it will discard any input.

``` bash 
ls /fake/directory 2> /dev/null
```
## Pipe and Tee <a name="pipe"></a>

The pipe operator (|), allow us to get stdout of a command and make that the stdin to another process 
``` bash 
ls -la /etc | less 
```
If i wanted to write the output of my command to two different streams , i will use the tee command 

``` bash 
ls | tee peanuts.txt
```
## Environment (env) <a name="env"></a>

There are some variables that came from our environment 
* env 
  * $HOME
  * $USER 
  * PATH 
That path is particularly important. You can access these variables by sticking a $ infront 

``` bash 
echo $PATH 
```
This returns a list of paths separated by a colon that your system searches when it runs a command. Let's say you manually download and install a package from the internet and put it in to a non standard directory and want to run that command, you type $ coolcommand and the prompt says command not found. Well that's silly you are looking at the binary in a folder and know it exists. What is happening is that $PATH variable doesn't check that directory for this binary so it's throwing an error. 

## cut <a name="cut"></a>

To extract contents by a list of characters : 

``` bash 
cut -c 5 sample.txt
```

flags | use 
------|-----
-f | Extract content by field (separated by TAB is a field 
-d | delimiter , To extract the content by a custom delimiter 


``` bash 
cut -f 1 -d ";" sample.txt
```
## paste <a name="paste"></a>

The paste command is similar to the cat command, it merges lines together in a file.

``` bash 
paste -s sample2.txt
paste -d ' ' -s sample2.txt
```
## Head <a name="head"></a>

By default the head command will show you the first 10 lines in a file.

This will modify the line count to 15 ( -n flags stands for number of lines) 
``` bash 
head -n 15 /var/log/syslog
```
## tail <a name="taill"></a>

> Similar to head , the tail command lets you see the last 10 lines of a file by default 


``` bash 
tail -n 15 /var/log/syslog
tail -f /var/log/syslog
```
## expand and unexpand <a name="expand"></a>

> To change your tabs to spaces , use the expand command 

``` bash 
expand sample.txt 
expand sample.txt > result.txt 
unexpand -a result.txt 
```

## join and split <a name="join"></a>

> The join command allows you to join multiples file together by a common field

>  This will split it into different files, by default it will split them once they reach a 1000 line limit. The files are named x\* by default.  

``` bash 
join file1.txt file2.txt
join -1 2 -2 1 file1.txt file2.txt
split somefile
```
## sort <a name="sort"></a>

> The sort command is useful for sorting lines.

``` bash 
sort file1.txt
sort -r file1.txt
sort -n file1.txt
```
The n is to sort via numerical value 

## tr (Translate) <a name="tr"></a>

> The command allows you to translate a set of characters into another set . 

``` bash 
tr a-z A-Z 
```
## uniq (Unique) <a name="uniq"></a>

> Remove the duplicates . uniq does not detect duplicate lines unless they are adjacent 

``` bash 
uniq reading.txt 
uniq -c reading.txt 

```
The c flag counts how many occurrences of a line 

Flag | use 
-----|-----
 c | count of how many occurrences of a line 
 u | get unique values 
 d | get duplicate values  

 ``` bash 
sort reading.txt | uniq 
``` 

## wc and nl <a name="wc"></a>

> wc(word count), shows the total count of words in a file. nl (number lines) to count the number of lines. 

Flag | use 
-----|-----
l | number of lines 
w| number of words 
c| number of bytes 

## grep <a name="grep"></a>

> It allows you to search for characters that mach a certain pattern . 

```bash 
grep fox sample.txt
grep -i somepattern somefile
env | grep -i User
ls /somedir | grep '.txt$'
```

the i flag is for case insensitive .
the last command should return all files ending wwith .txt in somedir  

