# PortSwigger 

* [Academy](https://portswigger.net/web-security/all-labs)
* [Rana Khalil](https://www.youtube.com/@RanaKhalil101)

## Lab Links 

### SQLi

- [ ] [Retrieve hidden data](https://portswigger.net/web-security/sql-injection/lab-retrieve-hidden-data)
- [ ] [Allowing login bypass](https://portswigger.net/web-security/sql-injection/lab-login-bypass)
- [ ] [Querying the database type](https://portswigger.net/web-security/sql-injection/examining-the-database/lab-querying-database-version-oracle)
- [ ] [Querying the database type 2](https://portswigger.net/web-security/sql-injection/examining-the-database/lab-querying-database-version-mysql-microsoft)
- [ ] [Listing the database contents](https://portswigger.net/web-security/sql-injection/examining-the-database/lab-listing-database-contents-non-oracle)
- [ ] [Listing the database contents 2 ](https://portswigger.net/web-security/sql-injection/examining-the-database/lab-listing-database-contents-oracle)
- [ ] [Determine number of columns](https://portswigger.net/web-security/sql-injection/union-attacks/lab-determine-number-of-columns)
- [ ] [Retrieving data from other tables](https://portswigger.net/web-security/sql-injection/union-attacks/lab-retrieve-data-from-other-tables)
- [ ] [Retrieving multiple values in a single column](https://portswigger.net/web-security/sql-injection/union-attacks/lab-retrieve-multiple-values-in-single-column)
- [ ] [Injection with conditional responses](https://portswigger.net/web-security/sql-injection/blind/lab-conditional-responses)
- [ ] [With conditional errors](https://portswigger.net/web-security/sql-injection/blind/lab-conditional-errors)
- [ ] [Error Based](https://portswigger.net/web-security/sql-injection/blind/lab-sql-injection-visible-error-based)
- [ ] [With Time delays](https://portswigger.net/web-security/sql-injection/blind/lab-time-delays)
- [ ] [Time delay 2](https://portswigger.net/web-security/sql-injection/blind/lab-time-delays-info-retrieval)
- [ ] [Out of band interaction](https://portswigger.net/web-security/sql-injection/blind/lab-out-of-band)
- [ ] [Out of band 2](https://portswigger.net/web-security/sql-injection/blind/lab-out-of-band-data-exfiltration)
- [ ] [Bypass via XML encoding](https://portswigger.net/web-security/sql-injection/lab-sql-injection-with-filter-bypass-via-xml-encoding)

### Cross-site scripting 
### Cross-site Request forgery ( CSRF ) 
### Cross-origin resource sharing ( CORS )
### Server-side request forgery ( SSRF )
### Path traversal 

* [Path-traversal](https://portswigger.net/web-security/file-path-traversal#what-is-path-traversal)

### Information disclosure 
### Business logic vulnerabilities
### CLickjacking 
### DOM-Based Vulnerabilities 
### XML external entity (XXE) injection 
### HTTP request smugglging 
### OS command Injection 
### Server-side template injection 
### Access control vulnerabilities 
### Authentication 
### Websockets 
### Web cache Poisoning 
### Insecure deserialization 
### HTTP host header attacks 
### Oauth authentication
### File upload vulnerabilities 
### JWT 
### Essential skills 
### Prototype pollution 
### GraphQL API Vulnerabilities 
### Race conditions 
### NoSQL injection
### API Testing 
### Web LLM attack 
### Web cache deseption
