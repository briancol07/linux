# Remote code execution 

Execute arbitrary code on a target machine

* Compromise 
  * WebApp
  * Underlying Server

* Code injection Vulnerability
* File inclusion 

## Code injection 

When application allows user input to be confused with executable code.

Reverse shell makes the target server communicaite with the attacker machie and establish a remotly accessble connection allowign attacker tot execute system commands.

## File Inclusion 

* Remote file inclusion
  * From a reomote server be included
* Local file inclusion
  * Includes files in a unsafe way , first uploaded 

## Prevention 

* Avoid inserting user input into code that get exhausted 
* Treat user uploaded file like untrusted 
* Avoid calling system commands directly -> Use api 
* Stay up to date 
  * Software supply chain attakc 
    * WAF ( web application firewall )
* Pinciple least privilege 

## Hunting

* Clasic 
* BLind

1. Gather information about target
2. Identify suspicious user input locations 
3. Submit test payloads 
4. Confirm vulnerability 

Bypassing rec protection quotes -> unisx 

* Search 
  * RCE Filter bypass 
  * WADF bypass


