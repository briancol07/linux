# What is a root user ?
  
Default user with full privileges, when you install the sw , that user will be root. 

Only use root when it's necesary, you can break things (can do anything and everything) 

Id for the root is 0 

```bash 
#To login as root (switch user) 
su 
su - 
# You can exit with exit 
```

> The prompt will show a pound sign insted of a dolar to distinguish root 


```bash 
# to set a password for root 
sudo passwd root 
``` 

## To see UID

This is the user id  

```bash 
echo $UID
```

## Why should you not log in as Root ?

Running command as root potentially cause irreversible damage to the system 

you could accidentally delete or modify system files, a malicious program could log in as root and xause sever damage 

## What is sudo?

Sudo is short form for super user do. It gives you the ability to act as Root for one command at a time 


```bash 
# To expire the password for root 
sudo passwd -l root
```

min 13:20
