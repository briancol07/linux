# Introduccion a bash 

> Bourn Again Shell

Interprete de comandos que permite a los usuarios interactuar con el sistema operativo a traves de la linea de comandos.
Tambien es un lenguajes de scripting que te permite escribir scripts para realizar tareas repetitivas de manera automatizada.

## Porque aprender bash ?

1. Automatizacion de tareas repetitivas 
2. Administracion eficiente del sistema 
3. Optimizacion del tiempo y recursos 
4. Flexibilidad y potencia 
5. Ciberseguridad 

## Como se diferencia bash de otros shells ? 

* Otros 
  * Zsh
  * Fish 
  * Ksh 
  * Otros 
* Diferencias 
  * Compatibilidad 
  * Flexibilidad 
  * Ecosistema de scripts 

## Primeros pasos 

El clasico hello world , en una termina ejecutaremos el siguiente comando 

```
echo "Hello world"
```
Este comando utiliza echo para imprimir el texto en la terminal.

Probar los siguientes comandos `pwd` , ` echo`, `ls`
