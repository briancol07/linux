# Que es una vulnerabilidad?

Vulnerabilidad y exploit 

Sin vulnerabilidad no hay explotacion 

## Vulnerabilidad 

> Es un punto debil en un sistema, que puede ser explotada por un ataque

Todo dispositivo que tenga acceso a una red.

ej: eternal blue es un fallo en windows 7. 

## Exploit 

Es un fragmento de codigo, el cual aprovecha una vulnerabilidad en un SW o HW o dispositivo
Es lo que se utiliza para aprovechar esa vulnerabilidad

## Explotacion Remota 

Ocurre a traves de la red. Sin necesidad de estar adentro de la red de sistemas a atacar
 
## Explotacion Local 

Ocurre a traves de la red. Pero esta vez es necesario estar dentro de la red de sistemas a atacar.

## Lado del cliente 

Son lanzados al usuario. No necesariamente a un sistema. Sino que obtener la vulnerabilidad de la capa 8.
