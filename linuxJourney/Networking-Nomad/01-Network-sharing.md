# Netwrok Sharing 

## File Sharing Overview <a name="sharing"></a>

One file sharing tool is the scp command. The scp command stands for secure copy. It works exactly the way the cp command does, but allows you to copy from one host over to another host on the same network. 
It work via SSH so all your actions are using the same authentication and security as ssh 

* Copy a file over from local host to a remote host
  * `scp myfile.txt username@remotehost.com:/remote/directory`
* Copy a file from a remote host to your local host 
  * `scp username@remotehost.com:/remote/directory/myfile.txt /local/directory`
* Copy over a directory from your local host to a remote host 
  * `scp -r mydir username@remotehost.com:/remote/directory`

## rsync <a name="rsync"></a>

Another tool used to copy data from different host is rsync ( remote synchronization ) . Uses a special algorithm that checks in advanced if there is already data you are copying to and will only copy over the differneces. If network get interrupted will copy only parts that didn't get copied 

* Verify integrity of a file you are copying over with checksums.
* Some options 
  * v : verbose output 
  * r : recursive into directories 
  * h : Human readable output 
  * z : Compressed for easier transfer, great for slow connections 
* Copy/sync files on the same host
  * `rsync -zvr /my/local/directory/one /my/local/directory/two`
* Copy/sync files to local host from a remote host
  * `rsync /local/directory username@remotehost.com:/remote/directory`
* copy/sync file to a remote host from local host 
  * `rsync username@remotehost.com:/remote/directory /local/directory`

## Simple HTTP Server <a name="http-server"></a>

Python has a super useful tool for serving files over HTTP. This is great if you just want to create a quick network share than other machines on your network can access 

```
python -m SimpleHTTPServer
```

This set up a basic webserver that you can access via the localhost address. So grab the IP address of the machine you ran this on and then on another machine access it in the browser `http://IP_ADDRESS:8000`

## NFS <a name="nfs"></a>

Network file system. Allows a server to share directories and files with one or more clients over the network. 

setting up NFS client 
```
sudo service nfsclient start
sudo mount server:/directory /mount_directory
```
### Automounting <a name="automounting"></a>

You want to keep it permanently mounted, normally you think you'd edit the `/etc/fstab` file ( not always work ). You want to do is setup automounting so that you can connect to the NFS server when you need to. This is done with the auto mount tool or in recent version of linux amd. 

When a file is accessed in a specified directory, automount will look up the remote server and automatically mount it. 

## Samba <a name="samba"></a>

> SMB ( server message block ) 

This protocol was used for sharing files between OS. Then was cleaned up and optimized in the form of the Common internet file system (CIFS) protocol.

Samba is what we call the linux utilities to work with CIFS on linux. In addition to file sharing you can also share reources like printers.


### Create a netwrok share with samba <a name="netowork"></a>

* Install samba 
  * `sudo apt install samba`
* Setup smb.conf 
  * Configuration file for samba in `/etc/samba/smb.conf`
* Setup up a password for samba 
  * `sudo smbpasswd -a [username]`
* Create a shared directory 
  * `mkdir /my/directory/to/share`
* Restart the samba servive
  * `sudo service smbd restart`
* Accessing a samba share via windows 
  * In Windows, just type in the network connection in the run prompt: \\HOST\sharename.
* Accessing a samba/windows share via linux 
  * `smbclient //HOST/directory -U user`

The samba package includes a command line tool called smbclient that you can use to access any Windoes or Samba server

* Attach a samba share to your system 
  * Instead of transferring files one by one you can jsut mount the network share on your system
  * `sudo mount -t cifs servername:directory mountpount -o user=username,pass=password`
