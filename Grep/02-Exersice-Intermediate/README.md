# Intermediate 

1. Find lines that contain exactly three characters.
2. Search for all lines that do not contain the word "failure" in a file.
3. Display the line number along with the matching line for the word "password".
4. Find lines that contain the word "root" followed by exactly one space and another word.
5. Display lines containing "error" in all .log files in a directory.
6. Search for lines that match an email address pattern in a text file.
7. Find lines that contain a phone number format like 123-456-7890.
8. Search for lines containing IP addresses in a log file.
9. Display all lines that contain the word "test" within double quotes.
10. Find lines with "ERROR" but exclude lines that contain "DEBUG".
