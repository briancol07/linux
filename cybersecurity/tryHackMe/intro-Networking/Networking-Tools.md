# Networking Tool 

## Ping

The ping command is used when we want to test whether a connection to a remote resource is possible. It one of the slightly less well-known TCP/IP

The ping command actually return the Ip address.

## Traceroute 

Can be used to map the path your request takes as it heads to the target machine.
The internet is made up of many, many different servers and end-points, all networked up to each toher. This means that, in order to get to the content you actyally want,you first need to go through a bunch of other servers.Allows you to see each of these connections

``` 
traceroute <destination>
```
By default, the Windows tracerroute utility (tracert) operates using the same icmp protocol that ping utilises.

## WHOIS 

Domains translates into an IP Address, so that we don't need to remember it 

Whois essentially allows you to query who a domain name is registered to .

```
whois url
```

* The output you got is:
  * Domain Name
  * The company that registered the domain
  * Last renewal 
  * Next due 
  * Bunch of information about nameservers 

## DIG

Convert URL into IP address -> TCP/IP protocol called DNS (Domain Name System).
At the most basic level, DNS allows us to ask a special server to give us the IP address of the website. 

You make a request to a website.The first thing that your computer doe is check its local cache to if it's already got an IP address stored for the website. If it not , send a request to what's know as a recursive DNS server. These will automatically be know to the router on your network. Many Internet Service Providers ( ISPs) maintain their own recursive server. Detals for a recursive DNS server are stored in your router. 
Tje root name server essentially keep track of the DNS server in the next level down, choosing an appropiate one to redirect your requesto. These lower servers are called Top-Level Domain servers.
Top-Level Domain(TLD) server are split up into extensions.So, for example, if you were searching for tryhackme.com your reques would be redirected to a TLDserver that handled **.com** domains. There are different, other can handle **.co.uk**. When a TLD server receives your request for information, the server passes it down to an appropiate Authoritative name server .

When you visit a website in your web this all happends automatically, but we can also do it manually with a tool called dig.

```
dig <domain> @<dns-server-ip>
```

Another interesting piece of information that dig gives us is the TTL (Time To Live) of the queried DNS record.
