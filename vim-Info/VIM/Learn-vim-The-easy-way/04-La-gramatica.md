# La gramatica de vim 

Una vez que entiendas la estructura similar a la gramatica de los comandos de vim, seras capaz de "hablarle" a vim. 

## Como aprender un lenguaje

1. Aprender las reglas gramaticales
2. Incrementar mi vocabulario 
3. Practicar, practicar y practicar

### Reglas gramaticales 

```
verbo + sustantivo
```

### Vocabulario (Movimientos)

Los movimientos son utilizados para movernos por el texto en vim. 

```vim
h    "Izquierda
j    "Abajo
k    "Arriba
l    "Derecha
w    "Mover el cursor hacia adelante al principio de la palabra siguiente
}    "Saltar al siguiente párrafo
$    "Ir al final de la línea
```

### Verbos 

see help 

```vim 
:h operator
```

Tiene 16 operadores pero con 3 podes hacer el 80%

```vim 
y    "Copiar un texto (*yank* en Vim sería la acción de copiar, de ahí la letra `y`)
d    "Eliminar un texto y guardarlo en el registro (*delete* en Vim sería la acción de eliminar, de ahí la letra `d`)
c    "Eliminar un texto, guardarlo en el registro y comenzar en el modo de insertar
```

Pata pegar un texto copiado se puede hacer con p (despues de la posicion del cursor) o P (antes de la posicion del cursor)

### Verbo + Sustantivo

* Para copiar desde la ubicacion actual al final hay ques usar y$
* Para eliminar desde la ubicacion actual hasta principio de la siguiente palabra dw 
* Para camibar el texto desde la posicion actual del cursor hasta final de parrafo actual c)

### Agregar conteos 

```vim 
y2h " Para corias dos caracters de la izquierda
d2w " Para eliminar las siguientes dos palabras
c2j " Para cambiar las siguientes dos lineas
```

### Afectar linea 

* dd
  * Elimina toda la linea
* yy 
  * Copia toda la linea
* cc 
  * Cambiar toda la linea 

### Mas y mas 

```vim 
i + objeto   " Dentro del objeto de texto
a + objeto   " Fuera del objeto de texto
```

* di( 
  * Borra dentro del parentesis sin borrar este ulitmo
* da(
  * Borra lo de adentro mas el parentesis
* diw 
  * Para eliminar la palabra 
* dit 

Los objetos de texto son muy potentes por que puedes seleccionar differentes ofjetos desde una misma ubicacion 

```vim 
w        " Una palabra
p        " Un párrafo
s        " Una frase (*sentence* en inglés)
( o )    " Un par de ( )
{ o }    " Un par de { }
[ o ]    " Un par de [ ]
< o >    " Un par de < >
t        " Etiquetas XML (*tags* en inglés)
"        " Un par de " "
'        " Un par de ' '
`        " Un par de ` `
```

Para mas informacion :h text-objects 

## Como componer frases y gramaticas 

La capacidad o habilidad de contruir frases significa disponer de un conjuto de comandos generales que pueden ser combinados para desarollar comandos mas complicados. 

Vim tiene un operador de filtro (!) para utilizar programas externos como filtros para nuestros textos 

```
Id|Name|Cuteness
01|Puppy|Very
02|Kitten|Ok
03|Bunny|Ok
```

```vim 
!}column -t -s "|"
```
toca primero signo esclamacion luego llave cerrada y escribir lo otro y haria eso 

```
Id  Name    Cuteness
01  Puppy   Very
02  Kitten  Ok
03  Bunny   Ok
```

El verbo es ! sustantivo es } (para ir al siguiente parrafo) y acepto todo el argumento column -t -s "|" . (column ordena el texto mediante tabualciones) 

```vim 
" Cuatro movimientos w , $ , } , G 

dw 
d$
d} 
dG
```

Commando | accion 
---------|--------
gU | Hace los siguiente mayuscula 
gu | Hace lo siguiente minuscula 

```vim 
gUw 
gu$
gU}
guG
```

## Desplazarse n lineas 

5G nos dirigia a la linea 5 
5j nos deplaza 5 lineas abajo 
El movimiento de busqueda numero ilimitado de movimientos, nos permite buscar cualquier cosa 
! herramienta de filtrado 

Se pueden crear tus propios movimientos y operadores personalizados para aniadirlos a vim 

vim-textobj-user tiene una lista de objetos de texto personalizados 
