# Network types (Types and topologies )

## Index 

* [## Common Terminology ](#ct)
* [## WAN ](#wan)
* [## LAN / WLAN ](#LAN)
* [## VPN ](#VPN)
* [### Site-to-site VPN ](#StS)
* [### Remote Access VPN ](#RA)
* [### SSL VPN ](#SSL)
* [## Book Terms ](#bk)
* [## GAN ](#gan)
* [## MAN ](#man)
* [## PAN / WPAN ](#PAN)

## Common Terminology <a name="ct"></a>

Network type | Definition 
:-------------:|:----------:
Wide Area Network (WAN) | Internet
Localarea Network (LAN) | Internal Networks ( Home or office ) 
Wireless local Arean Network (WLAN) | Internal networks accesible over Wifi 
Virtual Private Network (VPN) | Connects multiple network sites to one LAN 

## WAN <a name="wan"></a>

> Wide area network 

Commonly referred to `The Internet`. Is one address that is generally accessed by the internet . 
Large number of LANs joined together. 
Some companies have a `Internal WAN` that is also called intranet, airgap network , etc. 
TTo determine if the network is a WAN is to use a WAN Specific routing protocol such as BGP and if hte IP schema in use is not withint RFC 1918 ( 10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16)

## LAN / WLAN <a name="LAN"></a>

> Local Area Network / Wireless local area Network 

## VPN <a name="VPN"></a>

> Virtual Private Networks 

Make the user feel as if they were plugged into a different network 

### Site-to-site VPN <a name="StS"></a>

Both the client and server are network devices, typically either routers of firewalls and share entire network ranges. This is most commonly used to join company networks together over the internet, allowing multiple locations to communicate over the internet as if they were local 

### Remote Access VPN <a name="RA"></a>

This involves the client's computer creating a virtual interface that behaves as if it is on a client's network. 
HTB utilizes `OpenVPN` which makes a TUN adapter letting us access the labs.

`Routing table` 

If the VPN only creates routes for specific networks this is called a `Split-Tunnel VPN`, meaning the internet connection is not going out of the VPN. For Companies is not ideal because if the machine is infected with malware, network-based detection methods will most likely not owrk as that traffic oges out the internet.

### SSL VPN <a name="SSL"></a>

Is done withint our web browser and is becoming increasingly common as web browsers are becoming capable of doing anything. Typically these will stream applications or entire desktop sessions to your web browser. Example : HTB Pwnbox 

## Book Terms <a name="bk"></a>


Network type | Definition 
:-------------:|:----------:
Global Area Network (GAN) | Global netwrok ( the internet) 
Metropolitan Area Network (MAN) | Regional network ( Multiple LANs) 
Wireless personal Area Network ( WPAN ) | Personal Network ( Bluetooth )

## GAN <a name="gan"></a>

> Global Area Network 

A worldwide network such as the Internet. However, the internet is no the only computer network of this kind. Active companies also maintain isolated networks thatspan several WANs and connect company computers worldwide.
GANs use the glass fibers infrastructure of wide-area networks and interconnect them by international udersea cables or satellite transmission. 

## MAN <a name="man"></a>

> Metropolitan Area Network 

Broadband telecommunications networks that connects several LANs in geographical proximity. As a rule, these are individuals branches of a company connected to a MAN via leased lines. The transmission speed between two remote nodes is comparable to communication within a LAN. 

Cities wired A MAN can be integrated supra-regionally in WAN and internationally in GAN 

## PAN / WPAN <a name="PAN"></a>

> Personal Area Network / Wireless personal area network 

The wireless varian is WPAN is based on bluetooth or wireless USB techonlogies. A WPAN stablished via bluetooth is called `piconet`. 

WPAN and PAN extend only a few meters and are therefore  not suitable for connecting devices in separate rooms or even buildings.

* Some protocols in IoT 
  * Z-wave 
  * ZigBee 
