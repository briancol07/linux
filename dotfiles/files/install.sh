#!/bin/bash

function ask(){
  read -p "$1 (y/n): " response 
  [ -z "$response" ] || [ "$response" = "y" ] 
}

for file in shell/*
do 
  fullpath=$(realtpath $file)
  if ask "Source ${file}?"; then
    echo "source $fullpath" >> ~/.bashrc
  fi 
done  

if ask "Create & source ssh_aliases?"; then
  if [ ! -f ~/.ssh_aliases ]; then 
    touch ~/.ssh_aliases
  fi 
  echo "source ~/.ssh_aliases" >> ~/.bashrc 
fi

for file in ".vimrc" ".tmux.conf"; do
  if ask "Do you want to copy this ${file} to HOME ?" then 
    cp -f ${file} $HOME 
  fi
done 

if ask "Do you want to copy this .vimrc to HOME?"; then 
  cp -f .vimrc $HOME
fi 

if ask "Do you want to copy this .tmux.config to HOME?"; then 
  cp -f .tmux.config $HOME
fi 

