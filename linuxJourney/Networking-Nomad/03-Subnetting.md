# Subnetting 

## Index 

* [## IPv4 ](#ipv4)
* [## Subnets ](#subnets )
  * [### Subnet mask ](#mask)
* [### Why? ](#why)
* [## Subnet math ](#math)
* [## CIDR ](#CIDR)
* [## NAT ](#nat)
  * [### How does it work? ](#work)
* [## IPv6](### IPv6)
## IPv4 <a name="ipv4"></a>

So we know that network host have a unique address they can be found at, example `204.23.124.23`.
This address actually contains two parts, the network portion that tells us know network it's on and the host portion that tell us which host on that network is.

It's separated into octets by the periods. So there are 4 octets in a IPv4 address. There are 4 octets that are 8bits that is 1byte because of that IPv4 are 4 bytes 

You can vire you IP address with `ifconfig -a` command 

## Subnets <a name="subnets "></a>

A subnet is a group of hosts with IP addresses that are similar in a certain way. These hosts usually are in a proximate location from each other and you can easily send dat to and from hosts on the same subnet. 

Example IP address that starts with 123.45.67 would be on the same subnet, like 123.45.67.8 .A subnet is divided into a network prefix, such us 123.45.67.0 and a subnet mask.

### Subnet mask <a name="mask"></a>

Determine what part of you IP address is the network portion and what part is the host portion `255.255.255.0`

The 255 portion is acuatually our mask. 

Also when we talk about our subnet, we commonly denote it by the network prefix follow by the subnet mask `123.234.0.0/255.255.0.0`

### Why? <a name="why"></a>

Subnetting is used to segment networks and control the flow of traffic within that network. SO a host on one subnet can't interact with another host on a different subnet.

To connect subnets you just need to find the host that are connected to more than one subnet example 192.168.1.129 is connected to a local network of 129.168.1.129/24 it can reach any host on that network. To reach host on the rest of the internet, it needs to communicate through the router. Traditionally, on most neworks with a subnet mask of 255.255.255.0 the router usually at address 1 of the subnet , so 192.168.1.1. Now that router will have a port that connects it to another subnet.

Private netwroks are not visible to the internet.

## Subnet math <a name="math"></a>

How to know how many host we can have on our subnet 

Example IP address of 192.168.1.0 and a subnet mask of 255.255.255.0


```
192.168.1.165  = 11000000.10101000.00000001.10100101
255.255.255.0  = 11111111.11111111.11111111.00000000
```

The IP address is masked by our subnet mask, when you see a 1, it is masked and we pretend like we don't see it . So the only possible hsot we can have are from the 0000000 region. So there are 256 possible options. However, it may look like we have 256 possible options, but we actually subtract 2 host because we have to account for the broadcast address and the subnet address, leaving us with 254 possible host on our subnet.

So the range will be 192.168.1.1 to 192.168.1.254 

## CIDR <a name="CIDR"></a>

> Classless inter-domain routing

Is used to represent a subnet mask in a more compact way. Where a subnet such as the 10.42.3.0/255.255.255.0 is written as 10.42.3.0/24 which just means that includes both the subnet prefix and the subnet mask. The `/24` means that the first 24 bits are used 

A simple trick is to subtract the total of bits and IP address can have 32 from the CIDR address so that leave with 8 bits , `2^8` but we have to remove 2 addresses , so we have 254 usable hosts 

## NAT <a name="nat"></a>

> Network address translation

NAT makes a device like our router act as an intermediary between the itnernet and private network. So only a single unique IP address is required to represent an etnire group of computers.

### How does it work? <a name="work"></a>

1. The computer make a request through the router 
2. The Router takes that request and opens its own connection to the page, then it send the request once it makes a connection 
3. The router is the intermediary between the computer and the page . So the page doesn't know about the computer instead all it can see is the router 

## IPv6

Was created to allow us to connect more host to the internet, it comes with more IP improvements however, it's adoption is quite slow. Meant no replace for IPv4, they are meant to complement each other 

```
2dde:1235:1256:3:200:f8ed:fe23:59cf
```
