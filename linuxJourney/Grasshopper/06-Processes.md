# Processes 

## Index

* [## ps (processes) ](#ps)
* [## Controlling Terminal ](#terminal)
* [## Process Details ](#process)
  * [### Example ](#example)
* [## Process Creation ](#creation)
* [## Process Termination ](#end)
  * [### Orphan Processes ](#orphan)
  * [### Zombie Processes ](#zombie)
* [## Signals ](#signals)
  * [### Signal process ](#sig)
  * [### Common signals ](#common)
* [## kill (Terminate) ](#kill)
  * [### Differences ](#difference)
* [## niceness ](#nice)
* [## Process States ](#states)
* [## /proc filesystem ](#fs)
* [## Job Control ](#job)

## ps (processes) <a name="ps"></a>

The process has an ID associated with it called the process ID (PID). The command ps show a snapshot of the current processes 

1. PID 
  * Process ID 
2. TTY 
  * Controlling terminal associated with the process
3. STAT 
  * Process status code
4. Time 
  * Total CPU usage time 
5. CMD 
  * Name of executable/command 

* ps aux 

letter | meaning
-------|--------
 a | Display all processes running 
 u | Shows more detail about the processes 
 x | List all processes that don't have TTY

* USER 
  * The effective user 
* PID 
  * Process ID
* %CPU 
  * CPU time used divided by the time the process has been running 
* %MEM 
  * Ratio of the process's resident set size to the physical memory on the machine 
* VSZ 
  * Virtual memory usage of the entire process
* RSS 
  * Resident set size , the non-swapped physical memory that a task has udes 
* TTY 
  * Controlling terminal associated with the process 
* STAT 
  * Process status code 
* START 
  * Start time of the process 
* TIME 
  * Total CPU usage time 
* COMMAND 
  * Name of executable/command 

> top gives you real time information about the processes running on your system instead of a snapshot.  

## Controlling Terminal <a name="terminal"></a>

TTY is the terminal that executed the command 

1. Terminal Devices 
  * Native Terminal 
  * Type into and send output 
2. Pseudoterminal Devices 
  * Emulat terminals with the shellterminal window andare denoted by PTS 
> Ctrl + alt + F1 to get into TTY1 (the first vitual console) , to exit Ctrl + alt + F7  

There are processes such as daemon processes wich are special processes that are essentially keeping the system running.They often start at system boot and usually get terminated when the system is shutdown. In th ps output, the TTY is listed as a **?** meaning it does not have a controlling terminal.

## Process Details <a name="process"></a>

A process like we sid before is a runnning program on the system, more precisely it's the system allocating memory, CPU,I/O to make the program run . 

The kernel is in charge of processes, when we run a program the kernel loads up the code of the program in memory, determines and allocates resources and then keep labs on each process 

* The status of the process
* The resources the process is using and receives 
* The process owner 
* Signal handling 
* Basically everything else 

### Example <a name="example"></a>

Go ahead and open 3 terminal windows, in two windows, run the cat command without passing any options (the cat process will stay open as a process because it expects stdin). Now in the third window run: ps aux | grep cat. You'll see that there are two processes for cat, even though they are calling the same program.

## Process Creation <a name="creation"></a>

When a new process is created, an existing process basically clones itself using something called the fork system call. The fork system call creates a mostrly identical child process . This child takes on anew PID and the original process becomes its parent and has PPID (Parent process ID).This system call destroys the memory management that the kernel put into place for that process and sets up new ones for the new program , to see in action ps -l (l for long format)
The Pid of the bash shell is the PPOD of the ps l command.

## Process Termination <a name="end"></a>

A process can exit using the \_exit system call. This will free up the resources that process was using for reallocation . It lets the kernel know why it's terminating something called a termination status (0 means that the process succeeded). The parent process has to acknowledge ther termination of the child process by using the wait system call and what this does is it checks the termination status of the child process. 

### Orphan Processes <a name="orphan"></a>

When a parent process dies before a child process, the kenel knows that it's not going to get a wait call, so insted it makes these processes "orphans" and puts them under the care of init(remember mother of all processes). Init will eventually perform the wait system call for these orphans so they can die. 

### Zombie Processes <a name="zombie"></a>

What happens when a child terminates and the parent process hasn't called wait yet? We still want to be able to see how a child process terminated, so even though the child process finished, the kernel turns the child process into a zombie process. The resources the child process used are still freed up for other processes, however there is still an entry in the process table for this zombie. Zombie processes also cannot be killed, since they are technically "dead", so you can't use signals to kill them. Eventually if the parent process calls the wait system call, the zombie will disappear, this is known as "reaping".

## Signals <a name="signals"></a>

> A signal is a notification to a process that something has happened 

Why we have signals 

1. A usercan type one of the special terminal characters
  * Ctrl + C 
  * Ctrl + Z 
  * To kill, interrupt or suspend processes
2. HW issues can occur and the kernel wants to notify the process
3. SW issues can occur and the kernel wants to notify 
4. They arebasically ways processes can communicate 

### Signal process <a name="sig"></a>

A event generate a signal , then its delivered to a process (considered in a pending state until it's delivered). When the process is ran, the signal will be delivered. The process have signal masks and they can set original delivery to be blicked. 

* Things the process can do 
  * Ignore the signal
  * "Catch" the signal and perform a specific handler routine
  * Process can be terminated
  * Block the signal, depending on the signal mask

### Common signals <a name="common"></a>

> Each signal is defined by integers with symbolica names 

* SIGHUP or HUP or 1 (Hangup) 
* SIGNIT or INIT or 2 (Interrupt) 
* SIGKILL or kill or 9 (Kill) (unblockable) 
* SIGSEGV or SEGV or 11 (Segmentation fault) 
* SIGTERM or TERM or 15 (SW termination) 
* SIGSTOP or stop (stop)

## kill (Terminate) <a name="kill"></a>

```bash
kill PID
kill -9 PID
```

By default it sends a TERM signal. Also in the second one you can send SIGKILL. 

### Differences <a name="difference"></a>

signal | Difference 
-------|-----------
SIGHUP | Hangup sent to a process when the controlling terminal is closed. For example, if you closed a terminal window that had a process running in it, you would get a SIGHUP signal. So basically you've been hung up on.
SIGINT | Is an interrupt signal(Ctrl + c) and the system will try to gracefully kill the process
SIGTERM| Kill the process but allow it to do some cleanup first 
SIGKILL| Kill the process, kill it with fire 
SIGSTOP| Stop / suspend a process 

## niceness <a name="nice"></a>

Processes aren't able to decide when and how long they get CPU time.Niceness mean that processes have a number to determine their priority for the CPU. High number is nice Low numbers is bad.

```bash 
nice -n 5 apt upgrade
renice 10 -p 3245
``` 

The nice command is used to set priority for a new process. The renice command is us to set priority on an existing process

## Process States <a name="states"></a>

STAT | Meaning 
-----|--------
 R | Running o runnable , it is just waiting for the CPU to process it
 S | Interruptible sleep , waiting for an event to complete, such as input from the terminal
 D | Uninterruptible sleep, processes that cannot be killed or interrupted with a signal 
 Z | Zombie
 T | Stopped or Suspended 

## /proc filesystem <a name="fs"></a>

```bash
ls /proc 
cat /proc/12345/status 
``` 
In the second one you should see process state informtion and more tailed information. The /proc directory is how the kernel is views the system, so there is a lot more information.

## Job Control <a name="job"></a>

* Sending a job to the background 
 * Appending an ampersand & to the command will run it in backgroud 
* View all backgroud 
  * The command: jobs 
  * + is most recent 
  * - is 2do 
* Existing job 
  1. Ctrl + Z 
  2. bg 
* Background to the foreground 
  * fg (without any options,bring back most recent) 
  * example: fg %1 
* Kill background jobs
  * kill %1    
