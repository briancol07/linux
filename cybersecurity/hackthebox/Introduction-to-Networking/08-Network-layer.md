# Network Layer

## Index 

* [## Protocols ](#protocols)

The network layer (3) of OSI controls the exchange of data packets, as these cannot be directly routed to the receiver and therefore have to be provided with routing nodes.
The data packets are then transferred fro mnode to node until they reach their target. To make this the network layer identifies the individual network nodes, set up and clears connection channels, and take care of routing and data flow control. 
When sending the packet, addresses are evaluated and the data is routed through the network from node to node.
There is no processing of the data in the layer above the L3 in the nodes.

* Responsible for 
  * Logial Addressing 
  * Routing 

## Protocols <a name="protocols"></a>

They are transparent to the protocols of the layers above or below. Some protocols fulfill tasks of several layers and extend over two or more . 

* IPv4 / IPv6
* IPsec
* ICMP
* IGMP
* RIP
* OSPF

It sensures the routing of packets from source to destination within or outside a subnet. Subnets may have different addressing schemes or incompatible addressing types. In both cases , the data transmission in each case goes through the entire communication network and includes routing between the network nodes.

Direct communicaiton is not always possible due to the different subnets, packets must be forwarded from nodes (routesrs) that are on the way. Forwarded packets do not reach the higher layers but are assigned a new intermediate destination and sent to the next node.
