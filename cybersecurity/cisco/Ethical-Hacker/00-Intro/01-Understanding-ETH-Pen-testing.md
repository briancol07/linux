# Understanding Ethical Hacking and Penetration Testing 

> Ethical hacker describes a person who acts as an attacker and evaluates the security posture of a computer network for purpose of minimizing risk

## NIST Computer security resource center (CSRC)

Defines a hacker as an "unauthorized user who attempts to or gains access to an information system". 

## Ethical vs Nonethical hacking 

Is that the latter involves malicious intent. 

> Permission to attack or permission to test is crucia land what will keep you out of trouble. 

This permission to attack is often referred to as "the scope" of the test 

ethical hacker will report the findings to the vendor 
Both use the same technologies/tools 

## Security Penetration testing or ETH

An Ethical hackers's goal is to analyze the security posture of a network's or system's infraestructure in an effor to identify and possibly exploit any security weaknesses found and then determine if a compromise is possible 

[Hacking-NOT-a-Crime](https://www.hackingisnotacrime.org/)

## Why do We need to do Penetration testing ? 

* You want to find any possible paths of compromise before bad guys do 
  * Know if the defenses really work?
  * How valuable is the data that wer are protecting? are we protecting the right things? 
* We need to determine what it is we are protecting and whether our defenses can hold up to the threats that are omposed on them.

## Threat actors 

### Organized Crime 

Consists of very well-funded and motivated groups that will typically use any and all of the latest attack tehcniques. 

### Hacktivists 

This type of threat actor is not motivated by money. They are looking to make a point or to futher their beliefs.
They usually carried out by strealing sensitive data and the nrevealing it to the public for the purpose of embarranssing or financially affecting a target. 

### State-Sponsored Attackers 

* Cyber war 
* Cyber espionage 

Many goverments aroudn the world today use cyber attacks to steal information from their opponents and cause disruption. 

### Insider Threats 

Comes from inside an organization. The motivatiosn of these types of actors are normally different from those of many of the other ommon threat actors. Often normal Employees who are ticked into divulging sensitive information or mistakenly clicking on linkst that allow attacker to gain access to their computers. However they could also be malicious insiders who wants revenge or money. 
