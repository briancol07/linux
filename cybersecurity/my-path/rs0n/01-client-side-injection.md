# Client side Injections 

How to inject malicious into the dom (document object model) 

## Document Object Model ( DOM )

Represnt the current running state, with attributes 

Can do this with :

* User control input 
* Source, to manipulate another attribute 


* Items:
  * Window object (browser)
  * Window.document (application)
  * Documen.location.href (URL)
  * Document.body (HTML)

http request going to the server and the application 

js can change part of the dom 


## Hunting strategies 

1. Reflected input in unauthenticated routes 
2. Reflected input (authenticated)
3. Dom injection & npm packages 

## Reflected input in unauthenticated routes 

First try to find subdomains

Reconnaissance involves gathering information about a target and enumeration involves systematically listing and probing for resources associated with the target.
Once they have identified subdomains, they turn their attention to finding potential attack vectors, and one commmon fous is on client-side injection attack. 
In this context, the objective is to identify areas in the web appication where user input might be improperly handled leading to security vulnerabilities. 

*Fuzzing* is a technique widely employed during this stage. Researchers systematically inject various payloads into input fields and parameters of HTTP request to observe how the application handles them. THis includes using payloads designed to explot client-side vulnerabilites such as cross-site scripting (XSS)

## Reflected input in authenticated routes 

Find enpoints wihtin authenticated routes of a web application, a process critical for identifying potential security vulnerabilities .
Systematically probing afor and mapping out the various endpoint and functionalities of a web application. 
THis targeted approach is driven by the understanding that such hidden functionalities may harbor unique vulnerabilities that have not been throughly tested or discovered by other bug bounty researchers 

THe goal of this enumeration process is to identify endpoints or functionalities that are not easily accessible through standard user, interface, making them potentially challenging for other researchers to find by meticulously exploring authenticated routes and employing tools and techniques desinged for endpoint discovery. This approach contributes to a more comprehensive assessment of the web application's security posture and enhances the bug bounty program's effectiveness in identifying and mitigating potentials risks 

## DOM injection (custom js) 

Rigorously testing custom js files and checking for known vulnerabilities (CVEs) in client-side npm packages. this multifaceted approach aims to identify flaws in the processign of user-controlled input within the browser without necessarily sending an HTTP request to the server 

Where data from a source is used without proper validation or sanitization. by carfully analyzing and manipulating these sources and sinks , researcher cna inject malicious js into the DOM of the web page. 
The injection can exploit vulnerabilities arising from insufficient validation or sanitization of user-controlled input, potentially leading to client-side injection vulnerabilities. Identifying and responsibly disclosing such issues are essential steps in fortifying the security of web applications and safeguading users from potential browser-based attacks. 
