# Plegado de texto ( FOLD ) 

Cuando lees un archivo a menudo hay mucho texto irrelevante que te impide entneder lo que realiza ese archivo. Para ocutar ese ruido innecesario, utiliza el plegado de texto que ofrece vim ( vim fold ) 

## Plegado manual 

Este pliega una parte del texto, evitando mostrarlo pero sin eliminarlo del archivo, El operador es la z 

ejemplo `zfj` en las primera linea del archio lo que hara es ocultar esas lineas 

* `zf` es el operador del operador de plegado 
* `j` es el indicador de movimiento para el operador de plegado
* `zo` para volver a abrilo 
* `zc` para cerrarlo

El comando para plegar texto en vim es un operador, por lo que cumple las reglas gramaticales de vim ( Verbo + sustantivo ) . puedes pasarle al operador de plegado un movimiento o un objeto de texto. 

* Plegar parafo usar `zfip`
* Plega a fin de archivo `zfG`
* Plegar entre `{}` usar `zfa{`

Tambien se puede desde el modo visual `zf`
Plegar lineas `:,+1fold`

Hay muchos otros comando para gestionar el plegado de texto 

* `zR` para abrir todos los plegados 
* `zM` Cerrar todos los plegados 
* `za` para alterar el estado en el que se encuentra el plegaod

## Diferentes metodos de plegado de texto

1. Manual     ( Manual )
2. Sangria    ( Indent ) 
3. Expresion  ( Expression ) 
4. Sintaxis   ( Syntax ) 
5. Diferencia ( Diff ) 
6. Marcador   ( Marker )

Para ver el metodo de plegado de texto `:set foldmethod?`
