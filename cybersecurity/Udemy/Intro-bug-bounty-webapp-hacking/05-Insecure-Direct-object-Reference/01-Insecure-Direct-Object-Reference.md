# Insecure Direct Object Reference 

IDOR occur when an appication provides direct access to objects based on user-supplied input. This will allow a malicious attacker to access data belongign to other users by manipulating the request.

* Retrieve data 
  * Will allow you to retrieve data such as personally identifiable information (PII), phone number adress, receipts/invoices,etc.
* Modify or Delete data
  * Allows you to modifies someone else's information such as email address, password, account details, etc. 

## Understand 

* How the application works 
  * How does it fetch data 
  * How does it create new data 
  * How does it delete or modify data 
* Look for any numerical IDs in the request
* Create two user in order to have accurate date ( userId, objectID )
* Sometimes application use "encryption" to obfuscate information such as the userID (base64)
* Automate using burp 
