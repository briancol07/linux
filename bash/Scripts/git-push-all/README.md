# Git Push all

Create an script to upload in gitlab, using flags and receiving 1 or 2 parameters 

* I have to check if there is one or two to make a title and description for the commit 

```
git add .
git commit -m $1
git push 
```
```
git add .
git commit -m $1 -m $2
git push 
```

## Links 

* [GIT-Docs-Status](https://git-scm.com/docs/git-status)
* [arguments](https://linuxsimply.com/bash-scripting-tutorial/functions/script-argument/number-of-args/)
