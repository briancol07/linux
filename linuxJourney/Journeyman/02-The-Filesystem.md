# The Filesystem 

## Index 

* [## Hierarchy ](#hierarchy)
* [## Types ](#types)
  * [### journaling ](#journaling)
  * [### Common Desktop filesystem types ](#fs)
* [## Anatomy of a Disk ](#anatomy)
  * [### Partition Table ](#partition)
  * [### Partition ](#a)
  * [### Filesystem structure ](#structure)
* [## Disk Partitioning ](#disk)
  * [### Launch parted ](#launch)
    * [#### select device ](#select)
    * [#### Parition The device ](#the)
    * [#### Resize a partition ](#resize)
* [## Creating Filesystems ](#cfs)
* [## Mount and Unmount ](#mount)
  * [### 1. Create the mount point ](#point)
  * [### To unmount a device ](#unmount)
  * [### To see UUIDS on system ](#uuids)
* [## /etc/fstab ](#fstab)
  * [### Fields ](#fsss)
* [## Swap ](#swap)
  * [### Using a partition for swap space ](#swp-space)
* [## Disk Usage ](#z)
* [## Filessystem Repair ](#repair)
* [## Inodes ](#inode)
  * [### What is an inode? ](#what)
  * [### When are inodes created? ](#when)
  * [### Inode information ](#info)
  * [### How do inodes locate files? ](#locate)
* [## Symlinks ](#symlinks)
  * [### Symlinks ](#s)

## Hierarchy <a name="hierarchy"></a>

directory | Name | Description 
--------- | -----| ----------
/-        | root | Everithing is nestled under this 
/bin      | binaries| Essential ready-to-run programs,also include most basic commands such as ls and cp 
/boot     | boot loader | Contains that files 
/dev      | Devices files | Contains all the devices information 
/etc      | Configuration Directory | should hold only configuration files and not any binaries 
/home     | Personal Directory | Holds your documents, files, settings ,etc.
/lib      | library | HOlds library files that binaries can use 
/media    | media | Used as an attachment point for removable media like usb drives 
/mnt      | mount | Temporrarily mounted filesystems 
/opt      | optional | Optional application software packages 
/proc     | process | Information about currently running processes 
/root     | root | The root user's home directory 
/run      | run | Information about the running system since the last boot
/sbin     | essential system binaries | Usually can only be ran by root
/srv      | Site-specific data | Data served by the system 
/tmp      | Temporal  | Sotrage for temporary files 
/usr      | unfortunately named | It does not contain user files inthe sense of a home folder. This is meant for user installed software and utilities. You can also add personal directories like /usr/bin or /usr/local 
/var      | Variable Directory | It's used for system logging, user tracking, caches. Basically anything that is subject to change all the time 

## Types <a name="types"></a>

There is something called The Virtual File System (VFS) abstraction layer. It is a layer between applications and the different filesystem types. You can have many filesystem on your disk, depending on how they are partitioned.

### journaling <a name="journaling"></a>

if you are on a non-journaled filesystem, and you're copying a large file and all of a sudden you lose power. The file would end up corrupted and your filesystem would be inconsistent. Instead the journaled system will wire a log file (journal). 

### Common Desktop filesystem types <a name="fs"></a>

* ext4 
  * current version of the native linux filesystem (standard) --> 16 tera
* Btrfs 
  * Betteror butter FS
  * Incremental backups 
  * Performance increase 
* XFS
  * High performance journaling file system
  * great for a system with large files such as a media server 
* NTFS and FAT
  * Windows filesystems
* HFS + 
  * Macintosh filesystem 

To check out 

```bash 
df -T
```

## Anatomy of a Disk <a name="anatomy"></a>

Partitions are extremely useful for separating data and if you need a certain filesystem, you can easily create a partition instead of making the entire disk one filesystem type.

### Partition Table <a name="partition"></a>

Every disk will have a partition table (telling how the system is partitioned). This table tells you where partitions begin and end, which are bootable and what sectors of the disk are allocated to what partition.

* 2 main partition 
  * Master boot Record (MBR)
  * GUID partition table (GPT)

### Partition <a name="a"></a>

* MBR 
  * Traditional partition table (standard)
  * Can have primary, extended and logical partitions 
  * MBR has a limit of 4 primary partitinos 
  * Additional partitions can be made by making a primary partition into an extended partition (there can be one extended partition on a disk) 
  * Supports disks up to 2 terabytes 
* GPT
  * New standard 
  * Has only one type of partition and you can make many of them
  * Each partition has a blobally unique ID (GUID)
  * Used mostly in conjunction with UEFI based booting 

### Filesystem structure <a name="structure"></a>

* Boot block 
  * This is located in the first few sectors of the filesystem, and it's not really used by the filesystem. Rather, it contains information used to boot .
  * Only one boot block is needed, if you have multiple partitions, they will have boot blocks, but many of them are unused 
* Super block 
  * This is a single block that comes after the boot block, and it contains informatiion about the filesystem
  * size of the inode table
  * Size of the logical blocks and the size of the filesystem
* Inode table 
  * Think of this as the database that manages our files.
  * Each file or directory has a unique entry in the inode table and it has various information
* Data Block 
  * This is the actual data for the files and directories. 

```bash 
sudo parted -l
```

## Disk Partitioning <a name="disk"></a>

* Patition Disk options: 
  * fdisk --> Basic command line partitioning tool, it does not support GPT
  * parted --> This is a command line tool that support both MBR and GPT 
  * gparted --> This is the GUI version of parted 
  * gdisk --> Fdisk , but it does not support MBR only GPT 

### Launch parted <a name="launch"></a>

> Let's use parted to do our partitioning. Let's say i connect the USB device and we see the divice name is /dev/sdb2

```bash 
sudo parted
```
#### select device <a name="select"></a>

```bash 
select /dev/sdb2
```
#### Parition The device <a name="the"></a>

```bash 
mkpart primary 123 4567
```
#### Resize a partition <a name="resize"></a>

```bash 
resize 2 12345 3456
```
> You should be careful when partitioning your disk 

## Creating Filesystems <a name="cfs"></a>

```bash 
sudo mkfs -t ext4 /dev/sdb2
```
> The mkfs (make filesystem) tool allows us to specify the type of filesystem we want and where we want it. 

You'll most likely leave your filesystem in a corrupted state if you try to create one on top of an existing one.

## Mount and Unmount <a name="mount"></a>

Before you can view the contents of your filesystem, you will have to mount it.

* Need:
  * Device location 
  * The filesystem type 
  * Mount point 
    * Directory on the system where the filesystem is goint to be attached.

### 1. Create the mount point <a name="point"></a>

```bash 
sudo mount -t ext4 /dev/sdb2 /mydrive
```
When we go to **/mydrive** we can see our filesystem contents, the **-t** specifies the type of filesystem.

### To unmount a device <a name="unmount"></a>

```bash 
sudo unmount /mydrive 
# or 
sudo unmount /dev/sdb2 
```
> Remember that the kernel names devices in the order it finds them. If the name changes, you can use a device's universally unique ID (UUID)

### To see UUIDS on system <a name="uuids"></a>

```bash 
sudo blkid
```
We can see our device name their corresponding filesystem type and their UUIDs .

``` bash 
sudo mount UUID=130b882f-7d79-436d-a096-1e594c92bb76 /mydrive
```

## /etc/fstab <a name="fstab"></a>

When we want to automatically mount filesystem at statup we can add them to a file called **/etc/fstab** (short for filesystem table).

Each line represent one filesystem
  
### Fields <a name="fsss"></a>

Fields   | Description
:-------:|------------
UUID     | Device Identifier
Mount Point | Directory the filesystem is mounted to 
Filesystem type | - 
Options  | Other mount options
Dump     | Used byh the dump utility to decide when to make a bakcup, you should just default to 0 
Pass     | Used by fsck to decide what order filesystems should be checked, if the value is 0, it will not be checked.

To add an entry, just directly modify the /etc/fstab file using the entry sintax above. Be careful when modifying this file.

## Swap <a name="swap"></a>

We used to allocate virtual memory to our system. If you are lowon memory, the system uses this partition to "swap" pieces of memory of ifle processes to the disk.

### Using a partition for swap space <a name="swp-space"></a>

wanted to set our partition of /dev/sdb2 to be used foe swap space 

1. First make sure we don't have anything on the partition 
2. Run: **mkswap /dev/sdb2** to initialize swap areas 
3. Run" **swapon /dev/sdb2** this will enable the swap device 
4. If you want the swap partition to persist on bootup, you need to add an entry to the /etc/fstab file.sw is the filesystem type that you'll use.
5. To remove swap: **swapoff /dev/sdb2** 

> Generally you should allocate about twice as much swap space as you have memory.

## Disk Usage <a name="z"></a>

To se the utilization of your disks.

```bash 
df -h
```
The df command shows you the utilization of your currently mounted filesystems. The -h flag gives you a human readable format.

```bash 
du -h
``` 
This shows you the disk usage of the current directory you are in.

* Disk -- Free
  * df
* Disk -- Usage
  * du 

## Filessystem Repair <a name="repair"></a>

The **fsck** (filesystem check) command is used to check the consistency of a filesystem and even try to repair it for us. Usually when you boot up a disk,fsck will run before your disk is mounted to make sure everything is ok.

```bash
sudo fsck /dev/sda
```
## Inodes <a name="inode"></a>

### What is an inode? <a name="what"></a>

An inode (index node) is an entry in ths table and there is one for every file. It describes everything about the file such as:

> Stores everything about the file, except the filename and the file itself.

* File Type
* Owner
* Group 
* Access permissions 
* Timestamps 
  * mtime (time of last file modification)
  * ctime (time of last attibute change)
  * atime (time of last access)
* Number of blocks allocated to the file 
* Size of the file 
* Number of blocks allocated to the file 
* Pointers to the data blocks of the file

### When are inodes created? <a name="when"></a>

when a filesytem is created, space for inodes is allocated as well. 
To see how many inodes are left on your system use:

```bash 
df -i
```
### Inode information <a name="info"></a>

Inodes are identified by numbers, when a file gets crated it is assigned an inode number (sequential). When some is deleted, it take the free numbers. 

To view inode numbers run:

```bash 
ls -li
```
The first field in this command list the inode number.

You can also detailed information about a file with stat, it tells you information about the inode as well.

```bash 
stat ~/desktop/
```
### How do inodes locate files? <a name="locate"></a>

Points to the actual data blocks of your files. Each inode contains 15 pointers, the first 12 pointers point directly to the data block. the 13th , points to a block containing pointers to movre blocks.The 14th points to another nested block of pointers and the 15th pointer points yet again to another block of pointer.Smaller files with the 12 first nodes larger files can be foun with the nests of pointers.

## Symlinks <a name="symlinks"></a>

The link count is the total number of hard links a file has.

### Symlinks <a name="s"></a>

Windows = shortcuts. In linux, the equivalent of shortcuts are symbolic links(or soft link or symlinks). Allow us to link to another file by its filename. Another type of links found in linux are hardlinks, these are actually another file with a link to an inode.
