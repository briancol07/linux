# Dev-Ops Roadmap

* [Mischa-van-denburg-Roadmap](https://www.youtube.com/watch?v=8s0DWeHuEaw)

> Put Things in practice , kubernetes homelab 

> Goal: Install arch linux and kwno what are you doing 

## Linux 

* Topics 
  * Boot process 
  * File system 
  * Permissions 
  * Systemd 
  * Package managers
  * Debugging skills 
  * Finding/reading logs 
  * Networking fundamentals 
  * Bash scripting 

1. Learning into practice 
2. Documetn all online
3. Self Hosting hobby 


* Cert 
  * RHCSA
  * LPIC-1

## Containers 

* Topics
  * Docker 
  * Docker file 
  * Container security 
  * Running multiple containers 
  * Docker composoe 
  * Docker Networking 
  * Dev Container (microsoft)

> Home lab 2gb RAM 

## Programming 

* Topics 
  * Variables 
  * Functions 
  * OOP
  * Libraries 
* Git 
* YAML 
  * Cloud native world 
  * Ansible 
  * Kubernetes 
  * Github actions 
* Bash Scripting 
* Python 
  * Course:
    * 100 day of code python
* GO 
* RUST 
  * Software 

### Pages

* Excercism 
* Freecodecamp


### Cloud 

Especialize in one cloud provider ?

* Providers 
  * Azure 
    * Lot of learning 
    * John Savill (yt)
  * AWS
    * Skybuilder
  * GCP 

### Infraestructure as code 

* Bicep 
* Terraform 
* Pulumi 

### Cloud element 

* Databses 
* Store & backups 
* Networking -> AZ-104
* IAM
  * RBAC 

Jenkins?

## Kubernetes 

* Learn first these:
  * Linux fundamental 
  * Containers and docker 

* Run kubernetes in homelab
* Use git-ops as early as possible 
* Make your home lab code public 

### Cert 

* CKAD
* CKA
* CKS
* Kuberstranout 

## Skills (soft)

* Join a community 
* Ask for help 
* Start building network 
* Sorround with other students 
* Presentations skills 
* Public speacking 
* Building teams and bringing people 

## Personal brand 

* Start blog 
* Share Journey
* Show your work 
* Good github / Personal page 
* Post linkeid 2x week 

## Finding jobs & interviewing 

> Practices 

To get first job be able to accept low pay 

## AI 

* Knowledge 
* Process that create model 
* Basic understanding, memory 
* Book: 
  * Artificial intelligence as guide for thinking humanas ( melanie mitchell ) 

### MLOPS

> Future 

* Kuberflow 
* Kaito 
