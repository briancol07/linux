# Directiva de Seguridad Local 

Nos permite definir muchisimos aspectos del sistema relacionados con la seguridad. Es muy similar al concepto de directivas de grupo que se utiliza en Ative Directory con la diferencia de que esta es unicamente local y no se aplica a traves de la red. 

```
secpol.msc
```
## Account Policies 

* Password Policy 
  * Enforce Password History 
    * Permite definir un historial de contraseñas para que no puedan repetir las N ultimas 
  * Maximum Password Age 
    * Permite definir cual es el tiempo maximo de vida de una contraseña 
  * Minimum Password Length 
    * Permite definir cual es la cantidad minima de caracteres con los que debe contar.
  * Password must meet complexity requirements 
    * No contener nombre de usuario o parte del nombre completo 
    * Tener como minimo 6 caracteres 
    * Tener, por lo menos 3:
      * Mayusculas 
      * Minusculas 
      * NUmeros 
      * Caracteres especiales 

## Lockout policy 

* Account Lockout Threshold 
  * Despues de que cantidad de intentos de acceso fallidos se va a bloquear la cuenta de usuario
* Accout Lockout Duration 
  * Cuanto tiempo debe transcurrir para que una cuenta se desbloquee automaticamente

## Local Policies 

* User Rights Assignment 
  * Act as part of the operating system 
    * Podran ejecutar procesos en nombre de cualquier usuario 
  * Allow logon through Remote Desktop Services 
    * RDP ( Remote Desktop Protocol)
  * Load and unload device drivers
    * Cargar y descargar controladores de dispositivos 
  * Take ownership of files or other objects 
    * Podran modificar quie es el dueño de archivos, directorios y otros.
* Security Options 
  * Accounts:
    *  Administrator account status 
    * Guest Account status 
  * Interactive Logon 
    * Display user information when the session is locked 
    * Do not display last user name 
    * Do not require  CTRL + ALT + DEL 
      * Define si los usuarios se ven obligados a usar esas teclas antes de iniciar sesion
  * Network Access
    * Do not allow anonymous enumeration of Sam acocounts 
    * Do not store LAN manager hash value on next password change  


