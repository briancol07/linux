# Optimizacion de scripts

## Optimizacion en bash 

* Objetivos clave 
  * Reduccion del consumo de recursos del sistema 
  * Mejora de la velocidad de procesamiento 
  * Mayor claridad y legibilidad en el codigo para un facil mantenimiento 
* Evitar bucles innecesarios 
  * Utilizar grep, sed, awk 
* Condicionales 
  * Utilizar operadores condicionales inteligentes ( `&&`, `||`) permite evitar bloques if largos y repetitivos.
* Minimizacion de procesos subsidiarios 
  * Usar funciones internas de bash reduce la cantidad de subprocesos necesario y aumenta la eficiencia 
* Reducir variables temporales
* Funciones reutilizables 
* Redireccion de entrada y salida 
  * Utiliza `>>` en vez de `>` cuando no se necesita 
* Liberacion de recursos 
* Validacion de entradas y comprobacion de seguridad 
  * Comandos criticos sin validacion ( evitar ) 
* Proteccion contra inyeccion de comandos 


```
# Uso ineficiente
for i in $(seq 1 100); do
    echo "$i"
done

# Optimización con brace expansion
echo {1..100}
```
## Practicas de debugging y monitorio en scripts <a name="practicas"></a>

* Uso de opciones de debugging 
  * agregar las flags `-x` `-v` ayuda a ver la ejecucion detallada del script para encontrar rapidamente cuellos de botella o errores logicos 
* Registro de errores y monitoreo 
  * Utiliza `echo` o `logger`


