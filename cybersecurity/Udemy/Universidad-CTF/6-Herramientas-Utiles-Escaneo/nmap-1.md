# Nmap parte 1 

> Es un scanner de red, para analizar redes 

command:

```bash
nmap
```

Cuales son los dispositivos, la cantidad de dispositivos. Nos da un boquejo de la red.

## Barrido de red con ping 

```bash 
nmap -sP ip/24 
```

barra espaciadora para ver el porcentaje 

## Barrido sin hacer el ping 

``` bash 
nmap -sn ip/24
```
El mismo recorrido sin hacer el ping y verificar es la conexion, solo nos daria la ip y mac.

## Cantidad de puertos abiertos 

```bash 
nmap -Pn ip
```

Lo que hara es analizar los puertos mas comunes aver si estan abiertos
