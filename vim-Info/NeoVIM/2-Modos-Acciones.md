# Modos , Comandos , Acciones 

## Si nos perdemos 

Comando help por si necesitamos algo 

```bash 
:help 
:h
```

## Modos 

### Normal 

Es el modo que normalmente se encuentra vim

Nos permite hacer el movimiento entre las lineas con las teclas 

Tecla   | Accion
:------:|:-------:
  k     | Arriba 
  j     | Abajo 
  l     | Derecha 
  h     | Izquierda 

### Insertar 

Tecla   | Accion
:------:|:-------:
 i      | Insert , pone para escribir donde esta el cursor (izquierda)
  I     | Inserta al incio de la linea y nos permite escribir 
  a     | Nos posiciona adelante del caracter que esta el cursor y nos permite escribir
  A     | Nos posiciona al final de la linea y nos permite escribir 
  o     | Crea una lina por Debajo y nos permite escribir 
  O     | Crea una linea por Encima y nos permite escribir 

### Visual 

Tecla   | Accion
:------:|:-------:
  v     | Nos permite entrar en modo visual 
  V     | Seleccionar toda la linea 

#### Bloque visual 

ctrl + v para seleccionar varios bloques 
luego con shift + I puedo hacer cambios en mas de una linea 
 
### Copiar y Pegar 

> Los registros de neoVim no son los mismo que el operativo 

Tecla   | Accion
:------:|:-------:
  y     | yank copia lo seleccionado 
  yy    | selecciona toda la linea y la copia 

### Terminal

Hay varias maneras de acceder

Para salir de la terminal es ctrl + \ + n  o escribir exit 
```bash
:term
```
### Modo Explorar 

Es un file manager para ver los archivos 
ctrl+6 me manda al archivo anterios para tener varios archivos abiertos 

```bash
:ex
```

```bash 
:buffer 
```
Con esto puedo ver los archivos que estan en los buffer al mismo tiempo
```bash 
:b1 
```
Con eso entrariamos a lo que esta en el buffer 1 

Esto me abre un archivo para modificar 

```bash 
:e nombre_archivo.txt
# :w me guarda el archivo
```

Esto es para copiar un archivo en otro  tenemos que estar posicionado en el archivo donde se copiaria 
```bash 
:r
# :r! ls 
```
Es para leer un comando y que lo peque en el archivo 



