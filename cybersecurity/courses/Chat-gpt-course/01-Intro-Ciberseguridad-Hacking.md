# Intro Ciberseguridad Hacking

## Index 

* [## Que es la ciberseguridad? ](#que)
* [## Tipo De hackers ](#tipos)
* [## Metodologias de hacking etico ](#metodologias)
* [## Normativas y aspectos legales ](#normativas )
* [## Configuracion del entorno de practicas ](#config)
* [## Repaso ](#repaso)

## Que es la ciberseguridad? <a name="que"></a>

La ciberseguridad es la practica de proteger sistemas, redes y datos contra ataques, danios o accesos no autorizados. 

* Objetivo (CIA)
  * Confidencialidad 
    * Solo las personas autorizadas pueden acceder a la informacion
  * Integridad 
    * La informacion debe mantenerse exacta y sin alteracioens no autorizadas
  * Disponibilidad 
    * Los sitemas deben estar accesibles cuando sean necesarios 

## Tipo De hackers <a name="tipos"></a>

* White Hat 
  * Trabajan legalmente para proteger sistemas y empresas
* Black hat 
  * Realizan ataques ilegales con fines maliciosos o lucrativos 
* Grey hat 
  * Se encuentran en un punto intermedio 
* Script kiddies 
  * Usan herramientas sin conocimiento profundo, copiando tecnicas de otros 
* Hacktivistas 
  * Persiguen causas politicas o sociales
* State-sponsored hacker 
  * Trabajan para los gobiernos en ciberespionaje o guerra cibernetica

## Metodologias de hacking etico <a name="metodologias"></a>

> Ciclo de pentesting 

1. Reconocimiento
  * Recolectar informacion sobre el objetivo (OSINT, footprinting)
  * Herramientas : whois,dig, theHarvester, shodan
2. Escaneo y enumeracion 
  * Identificar puertos abiertos, servicios y vulnerabilidades 
  * Herramientas: nmap, nikto, dirb,wpscan
3. Explotacion 
  * Escalar privilegios y manterner acceso
  * Herramientas: metasploit, sqlmap, burpsuite
4. Post explotacion
  * Escalar privilaegios y mantener acceso
  * Herramientas: mimikatz, empire, bloodhound
5. Reporte
  * Documentar hallazgos y recomendaciones para mitigarlos 

## Normativas y aspectos legales <a name="normativas "></a>

> hacking etico 

* GDPR ( Reglamento General de proteccion de datos - UE )
* ISO 27001 : Estandar para gestio nde seguridad de informacion
* Ley de cibercrimen 
* Computer Fraud and abuse act (EE.UU)

## Configuracion del entorno de practicas <a name="config"></a>

* Parot OS / Kali linux 
* Maquinas virtuales
* Plataforma de entrenamiento 
  * HTB
  * TryHackMe 
  * VulnHub

## Repaso <a name="repaso"></a>

1. Que significa la triada CIA en la ciberseguridad? 
2. Cuales es la diferencia entre hacker etico y un hacker malicioso?
3. Cuales son los pasos principales de un pentest? 
4. Por que es importante el aspecto legal en el hacking etico?
5. Que herramientas peudes usar para pracitcar hacking etico de manera segura? 
