# Como Detectar un firewall y evadirlo con NMAP 

Cuando un host no responde no nos tenemos que quedar con solo eso 

## nping 

con nping podemos hacer un pin a una ip y determinar cuantos paquetes enviaremos 

```bash 
# -c 3 (seran 3 paquetes)
nping -c 3 <IP>
```

## Diferencias entre nping y nmap

```bash 
nping -c 3 192.168.0.13

nmap -sn -v 192.168.0.13
```

Nmap usa ARP solo para redes locales , encambio nping usa ISMP

## Packet trace 

```bash 
# En una pagina web 
nmap -sn --packet-trace --send-ip -v <Pagina>
```

No nos devuelven los Servicios, entonces significa que esta el firewall

## Evadir firewall 

```bash 
nmap -n -sS -v -f nmap.scanme.org
```

Adaptarse a cada contexto y entender la herramienta 


