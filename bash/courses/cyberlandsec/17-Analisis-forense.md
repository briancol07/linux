# Analisis Forense 

## Recoleccion Automatica de evidencias Digitales <a name="raed"></a>

La recoleccion de evidencias es uno de los primeros pasos en el analisis forense. Es criucial capturar datos clave como procesos en ejecucion, conexiones de red, archivos de log importantes y otros detalles del sistema antes de que se pierdan.

## Script recoleccion <a name="recoleccion"></a>

```
#!/bin/bash

# Directorio donde se almacenarán las evidencias
evidencia_dir="/var/log/evidencias_forense_$(date +%Y%m%d_%H%M%S)"
mkdir -p $evidencia_dir

# Capturar procesos en ejecución
echo "Recolectando información de procesos en ejecución..."
ps aux > $evidencia_dir/procesos.txt

# Capturar conexiones de red activas
echo "Recolectando conexiones de red activas..."
netstat -tuln > $evidencia_dir/conexiones_red.txt

# Capturar usuarios conectados
echo "Recolectando información de usuarios conectados..."
who > $evidencia_dir/usuarios_conectados.txt

# Capturar lista de servicios activos
echo "Recolectando estado de servicios del sistema..."
systemctl list-units --type=service --state=running > $evidencia_dir/servicios.txt

# Capturar logs de autenticación
echo "Recolectando logs de autenticación..."
cp /var/log/auth.log $evidencia_dir/auth.log

# Informar al usuario
echo "Recolección de evidencias completada. Los datos se han guardado en $evidencia_dir"
```

* Crea un directorio con la fecha y hora actuales para almacenar las evidencias 
* Captura la informacion de los procesos en ejecucion ( `ps aux` ), las conexiones de red ( `netstat -tuln `) , los usuarios conectados ( `who` ), los servicios activos ( `systemctl` ) y la copia el archivo de log de autenticacion 

## Extraccion de metadatos y analisis de archivos <a name="ex-metadatos"></a>

Los metadatos de archivos son extremadamente importantes por que contienen informacion sobre la creacion, modificacion y otros detalles del archivo que pueden ser utilizados para rastrear actividades sospechosas 

* Herramietnas 
  * exiftool 
  * stat 

### Ejemplo <a name="example1"></a>

```
#!/bin/bash

# Directorio de salida para los metadatos
salida_dir="metadatos_$(date +%Y%m%d_%H%M%S)"
mkdir -p $salida_dir

# Verificar si el argumento es un archivo o directorio
if [ -f "$1" ]; then
    echo "Extrayendo metadatos del archivo $1..."
    exiftool $1 > $salida_dir/metadatos.txt
elif [ -d "$1" ]; then
    echo "Extrayendo metadatos de todos los archivos en el directorio $1..."
    find "$1" -type f -exec exiftool {} \; > $salida_dir/metadatos_directorio.txt
else
    echo "Error: $1 no es un archivo ni un directorio válido."
    exit 1
fi

echo "Extracción de metadatos completada. Los resultados están en $salida_dir"
```

## Creacion de scripts para busquedas rapidas de archivos maliciosos <a name="bus-mal"></a>

Los archivos maliciosos a menudo pueden ser identificados mediante firmas especificas, patrones en los nombres de archivos, extensiones inusuales o caracteristicas que se repiten en los archivos comprometidos. Puedes crear un script para buscar archivos con caracteristicas sospechos en un sistema 

* Este script 
  * Busca archivos ejecutables que podrian estar ocultos o colocados en directorios inesperados 
  * Encuentra archivos modificados recientemente 
  * Identifica archivos con extensiones inusuales ( `exe , bat , ps1`) que no suelen encontrarse en un sistemas linux y pueden ser indicativos de malware 

```
#!/bin/bash

# Directorio a analizar (por defecto, el directorio actual)
directorio=${1:-.}

# Archivo de salida para los resultados
archivo_salida="archivos_sospechosos_$(date +%Y%m%d_%H%M%S).log"

echo "Buscando archivos sospechosos en $directorio..."

# Buscar archivos ejecutables en lugares inesperados
echo "Archivos ejecutables sospechosos:" > $archivo_salida
find $directorio -type f -perm /111 >> $archivo_salida

# Buscar archivos modificados en los últimos 7 días
echo -e "\nArchivos modificados recientemente (últimos 7 días):" >> $archivo_salida
find $directorio -type f -mtime -7 >> $archivo_salida

# Buscar archivos con extensiones inusuales
echo -e "\nArchivos con extensiones inusuales (.exe, .bat, .ps1):" >> $archivo_salida
find $directorio -type f \( -iname "*.exe" -o -iname "*.bat" -o -iname "*.ps1" \) >> $archivo_salida

# Informar al usuario
echo "Búsqueda de archivos sospechosos completada. Los resultados se han guardado en $archivo_salida"
```
## Resumen <a name="resumen"></a>

* La recoleccion automatica de evidencias es clave para preservar informacion critica antes de que se pierda. Puedes automatzar la caputra de informacion del sistema como procesos, conexiones de re y logs del sistema 
* La extraccion de metadatos te permite analizar archivos para obtener informacion sobre su creacion, modificacion y otros detalles relevantes que pueden ayudar a construir una linea temporal 
* Los scripts para busquedas rapdisas de archivos maliciosos peuden detectar archivos con extensiones sospechosas, modificacion recientes o caraacteristicas que sugieran actividad maliciosa. 

## Tarea <a name="tarea"></a>

1. Crea un script que realice un analissi forense de un sistema completo, recolectando procesos, conexiones de red metadatos de archivos y buscando arhvios ejecutables sospechosos 
2. Modifia el script para generar un informe detallado con todos los resultados organizados en un archivo de salida
