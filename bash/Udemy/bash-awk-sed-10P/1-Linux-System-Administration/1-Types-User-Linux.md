# Types of users in linux 

## Types 

1. The root user 
2. The regular user 
3. The service user 

## Root user 

* This is the main user account in linux system 
* It is automatically created during the installation 
* It has the highest privilege in system 
* It can do any administrative work and can access any service 
* This account is intended for system administration and should because only for this purpose 
* It should notbe for routine activities. It can't ve deleted. But if require, it can be disabled 

## Regular user 

* This is the norma uer account.
* Created automatically
* After the installation, we can create as many regular user as we need.
* Moderate privilege
* Routine work 
* It can be disabled or deleted 

## Service user 

* Created by installtion packages 
* Used by services to run processes and execute functions
* These account are neither intended nor should be used for routine work 

