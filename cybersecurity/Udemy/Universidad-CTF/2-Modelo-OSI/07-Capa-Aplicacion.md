# Capa 7 Aplicacion 

Servicios de red a aplicaciones

Responsable de gestionar la informacion de las aplicaciones del cliente 

Es importante no confundir el nombre de esta capa con aplicaciones que podamos tener instaladas en nuestras pc .
Esta capa se centra netamente en las comunicaciones

## Protocolos 

* Correo
  * SMTP 
  * POP
  * IMAP
* Navegacion web
  * HTTP
  * HTTPS
* Transferencia de archivos
  * FTP
  * TFTP
  * SFTP
* Conexion Remota
  * SSH 
  * RDP 
  * Telnet
  * DNS 
  * DHCP

