# Network Components, types and connections 

## What will i learn in this module ? 

> Objective : Explain network types, components and connections 

Topic Title | Topic Objective 
------------|----------------
Clients and Servers | Explain the roles of clients and servers in a network 
Network Components | Explain the role of network infraestructure devices
ISP Connectivity options | Describe ISP connectivity options 

## Topics 

* [1-Clients-Servers.md](./1-Clients-Servers.md)
* [2-Network-Components.md](./2-Network-Components.md)
* [3-ISP-Connectivity-Options.md](./3-ISP-Connectivity-Options.md)
