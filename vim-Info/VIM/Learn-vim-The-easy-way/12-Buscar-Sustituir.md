# Buscar y sustituir 

Aveces necesitas buscar multiples textos basando esas busquedas en un patrion que sea su minimo denominador comun. 

> Aprender a hacer expresiones regulares 

* Se utilizaran dos teclas 
  * Busqueda de patron hacia adelante 
    * `/`
  * Busqueda de patron hacia atras 
    * `?` 

## Sensibilidad inteligente a mayusculas y minusculas 

Lo hace case insensitive 
```
set ignorecase
set ignorecase smartcase 
```
Tambien existe la posibilidad de usar smartcase

Lo hace case sensitive 
```
set noignorecase
```

## El primer y ultima caracter en una linea 

* Debe ser el primer caracter de la linea 
  * `^`
* Debera terminar con 
  * `$`

## Repetir la busqueda 

* Si queres repetir la busqueda anterior con `//`. 
* Tambien se puede usar `n` y `N` para repetira la ultima busqueda. 
* Se puede navegar al historial con `/` y usando `arirba/abajo` o `Ctrl-N` o `Ctrol-P`
* Para ver todo el historial de la busqueda `:histroy /`

Si se llega al final y no encontro salta un cartel, se puede evitar con:

```
set wrapscan
set nowrapscan 
```

## Buscando palabras alternativas 

Se pueden usar los pipes para buscar multiples palabras (`|`) se tendra que agregar una barra invertida antes del pipe 

```
/hello\|hola
```
Se puede reemplazar con la sintaxis magic ( `\v` ) al comienzo de la vusqueda 

```
:h \v
```

## Estableciendo el inicio y el final de una busqueda 

* Coincidencia de comienzo 
  * `\zs`
* Coincidencia de final 
  * `\ze`

## Buscando un rango de caracteres 

* Patron basico de rango de caracteres 
  * `[ ]` 
    * [0-9]
    * [a-z]






