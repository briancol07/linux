# Data transmission 

> Computer and networks only work with binary digits (0,1)


A bit is stores and transmitted as one of two possible dicrete states

* 0,1
* Off,on
* false,true

A computer use binary codes to represent and interpret letters, number and special characters with bits. A commonly used code is the American Standard Code for information interchange (ASCII), each character is represented by eight bits.

## Common method of data Transmission 

After is transformed into a series of bits, it must be converted into signals that can be sent across the network media to its destination (physical medium) 

* Three common mehtods of signal transmission
  * Electrical Signals 
    * Copper wire 
  * Optical signals 
    * Fiber-optic cable
  * Wireless signals 
    * electromagnetic waves 
