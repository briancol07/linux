# Automatizacion de pruebas 

Permite una vigilancia continua para identificar y mitigar vulnerabilidades antes de que los atacantes puedan explotarlas. 

> No reemplazan las pruebas manuales, pero puede aumentar la velocidad y consistencia de los analisis 
## Index 

* [## Por que automatizarlas ](#porque)
* [## Tipos de pruebas que se pueden automatizar ](#tipos)
* [## Herramientas ](#herramientas)
* [## Como integrarlo en tu flujo de trabajo ](#flujo)

## Por que automatizarlas <a name="porque"></a>

* Velocidad y eficiencia 
* Cobertura completa 
* Deteccion continua de vulnerabilidades
* Reduccion de errores humanos 

## Tipos de pruebas que se pueden automatizar <a name="tipos"></a>

* Escaneo de Vulnerabilidades 
  * SQL 
  * XSS 
  * Configuraciones incorrectas 
* Pruebas de carga y estres
* Validacion de permisos y accesos 
* Escaneo de dependencias 

## Herramientas <a name="herramientas"></a>
  
* OWASP ZAP + jenkins/gitlab CI 
  * Escaneo automatico 
  * Integracion CI/CD
  * Scripting en ZAP
* Burp Suite PRO
  * Escaneo Automatizado
  * Generacion de informes Automatizados 
  * Integracion con sistemas CI/CD
* Nikto 
  * Programacion de escaneo periodicos 
  * Integracion con cron jobs 
* Dependabot o SNYK 
  * Analisis de dependencias externas 

## Como integrarlo en tu flujo de trabajo <a name="flujo"></a>

* Integracion CI/CD
  * Configuracion de pipeline 
  * Aniade las herramientas de escaneo 
  * Generacion de reportes 
* Escaneos periodicos 
  * Programa escaneos nocturnos o semanales 
  * Revisa los reportes 




