# Proteccion contra SQL 

## Indice 

* [## Que es SQL injetion ? ](#SQLi)
* [## Como Funciona? ](#funcionamiento)
* [## Tipos Comunes de SQLi ](#tipos)
* [## Consecuencias de una injeccion ](#consecuencias)
* [## Como Proteger? ](#proteger)
* [## DVWA ](#dvwa)
* [## Modo de seguridad](### Modo de seguridad)

## Que es SQL injetion ? <a name="SQLi"></a>

Es uno de los tipos de ataques y ocurre cunado un atacante inserta codigo SQL malicioso en las entradasde una aplicacion, uqe luego es ejecutado directamente por el motor de bbdd. 
Esto permite al atacante manipular consutlas SQL ( Accediendo, modificando y eliminando ).

![SQLI](./img/SQLi.png)

## Como Funciona? <a name="funcionamiento"></a>


Codigo Vulnerable 

```
SELECT * FROM usuarios WHERE nombre_usuario = 'usuario' AND contraseña = 'contraseña';
```

Si el atacante introduce en el campo nombre `' OR '1'='1` la consulta se convierte en 

```
SELECT * FROM usuarios WHERE nombre_usuario = '' OR '1'='1' AND contraseña = 'contraseña';
```

Como 1=1 siempre es verdadero la consulta devuelve todos los usuarios de la base datos y permite el acceso sin una contrasenia valida

## Tipos Comunes de SQLi <a name="tipos"></a>

* Classic SQL injection 
* Time-based blind SQL injection 
  * Utiliza retrasos en el tiempo de respuesta para deducisr informacion sensible, incluso cuando la aplicacion no muestra resultados directamente 
* Error-based SQL injection 
  * El atacante provoca errores en las consultas SQL para que la aplicacion muestre infromacion util sobre la estructura de la base de datos 

## Consecuencias de una injeccion <a name="consecuencias"></a>

* Robo de datos sensibles 
* Modificacion de datos 
* Control total del sistema 

## Como Proteger? <a name="proteger"></a>

* Uso de consultas preparadas 
  * Separan la logica de la consutla 
* Uso de ORM ( Object-Relational mapping ) 
* Validacion y escape de entradas 
  * Asegurate de que los datos que el usuario introduce solo contenga los caracteres esperados 
* Uso de privilegios minimos 
  * Cuentas con acceso limitado 
  * Separacion de cuentas 
* Registro de auditoria y monitoreo de consultas 
  * Registrar las consultas SQL ejecutadas 
  * Configurar alertas cuando se detectan patrones de consultas inusuales e inesperadas 

## DVWA <a name="dvwa"></a>

> Es una plataforma educativa y segura, cuyo objetivo es permitir a los usuarios practicar con diversas vulnerabilidades web.[link](https://github.com/digininja/DVWA)

## Modo de seguridad 

Simular diferentes niveles de proteccion en una applicacion web 

* Low 
  * No hay protecciones activas, ideal aprender conceptos basicos y realizar inyecciones SQL sin restricciones 
* Medium 
  * Se implementan algunas medidas que requieren mayor comprension de las vulnerabilidades 
* High 
  * Mas avanzadas, util para practicar tecnicas mas sofisticadas 
* Impossible 
  * Todas las protecicones estan completament activadas 

