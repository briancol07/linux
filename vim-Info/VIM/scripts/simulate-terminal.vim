function! OpenPopupTerminal()
    let cmd_output = system('pwd')  " Replace 'ls' with any command you want to run
    call popup_create(cmd_output, {})
endfunction

call OpenPopupTerminal()
