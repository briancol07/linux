#!/bin/bash 

# alias 

alias a='alias'
alias ll='ls -la'
alias vi='vim'
alias ls='ls --color=always'

# to create files 

alias mk='mkdir'
alias t='touch'

# Tmux

alias tm='tmux'
alias tks='tmux kill-server'

alias tmh='tmux new-session \; split-window -h'
alias tmv='tmux new-session \; split-window -v'

# Inside tmux 

alias th='tmux split-window -h'
alias tv='tmux split-window -v'

# Movement 

alias ..='cd ..'
alias ...='cd ../../'
alias ....='cd ../../../'

# Create README.md

alias readme='touch README.md'

# Create folder img 

alias img='mk img'
