# TCP-IP

Very similar to OSI Model. Consists of four layers:

* Application 
* Transport 
* Intenet 
* Network interface 

> Encapsulation and de-encapsulation work exactly

Transmission Control Protocol (which we touched upon earlier in the OSI model) that controls the flow of data between two endpoints, and the internet Protocol, which controls how packets are addressed and sent. 

TCP is a connection-based protocol.befor you send any data via TCP, you must first form a stable connection between the two computers. The process of forming this connection is called the **Three-Way Handshake**.

1. Send Especial requesto to the remote server.
  * Indicating that it wants to initialise a  connection
2. The server will then respond with a packet
  * Containing SYN bit
  * Another acknowledgement bit call ACK.
3. The Computer send a packet 
  * Contain ACK bit (confirming the connection) 
4. Now data can be reliably transmitted 

