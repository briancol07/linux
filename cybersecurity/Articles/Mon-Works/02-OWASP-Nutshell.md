# OWASP in a nutshell

* [Article](https://medium.com/@mon.cybersec/owasp-in-a-nutshell-81df9b6106d0)

> Open Web Application security project 

Es una comunidad global sin fines de lucro dedicada a mejorar la seguridad de SW. 

* Tienen 
  * Guia detallada de como buscar y resolver vulnerabilidades 
  * Una web para testear y buscar vulnerabilidades ([juice-Shop](https://owasp.org/www-project-juice-shop/)
  * Top 10 vulns web mas criticas 
  * Guias, estandares, mejores practicas y proyectos para todos los gustos 
    * OWASP mobile security project 
    * OWASP cloud security project
    * OWASP API security project
    * OWASP IoT prject 
    * OWASP cheat sheets

## TOP 10 <a name="top-10"></a>

Cada anio cambian pero este de 2021

* A01: Broken Access control
  * Bypass de autorizacion permitiendo que obtenga datos confidenciales o realizar actividades
* A02: Cryptographic failures
  * Mal uso o flata de uso de algoritmos criptograficos
* A03: Injection
* A04: Insecure Design
* A05: Security Misconfiguration
* A06: Vulnerable and outdated components
* A07: Identification and Authentication failures
* A08: Software and data integrity failures
* A09: Security logging and monitoring failures
* A010: Server-side request forgery
