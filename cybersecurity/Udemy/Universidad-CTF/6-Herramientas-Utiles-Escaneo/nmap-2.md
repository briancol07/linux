# Nmap 2 

## Para ver ayuda en linux 

```bash 

namp -h 
```

## Sacar mas informacion 

Puertos pueden ser tcp y udp 

### TCP 

Solo analizara los puertos TCP con el siguiente comando 

```bash 
nmap -sT <ip>
```

Se hace la diferencia por que se sabe a quien va dirigido encambio UDP no se sabe

### UDP 

```bash 
namp -sU <IP>
```

> namp -Pn \<IP\> revisa los 1024 puertos mas conocidos 

## Puertos Abiertos 

> se tiene que saber cuales estan abiertos 

```bash 
nmap -p<puertos> <IP>
```
Los puertos tienen que estar separados por comas

## Para ver sin el DNS 

```bash

nmap -p<Puertos> -n <IP>
# nmap -p139,445 192.168.1.7
```

## Para analizar todos los puertos 

Va a la maquina objetivo y pregunta tenes algun puerto abierto y ella le responde, a mas puertos tiene mas preguntas tendra que hacerele y sera mas propenso a que se detecte 

```bash 
nmap -p- <IP>
```

## Agregar velocidad 

> T5 mas rapido T1 mas rapido (hay que tener cuidado algunas veces es mejor lento y contento) 

```bash 
nmap -p- -T1 <IP>
```




