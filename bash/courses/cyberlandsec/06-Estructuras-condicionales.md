# Estructuras condicionales 

## Que es una estructura condicional ? 

Permite ejecutar un bloque de codigo solo si se cumple una determinada condicion, Controlar que acciones se ejecutan en funcion de los datos o el estado del sistema 

## Sintaxis basica 

> if else elif 

```
if [ condición ]; then
    # Código a ejecutar si la condición es verdadera
elif [ otra_condición ]; then
    # Código a ejecutar si la otra condición es verdadera
else
    # Código a ejecutar si ninguna condición es verdadera
fi
```

`elif` opcional, el `else` tambien, `fi` marca el fin de la estructura condicional 

## Uso de test y [] 

En bash, las condiciones se escriben entre corchetes , que son equivalentes al comando `test`

* Comparadores de cadenas 
  * `=` compara si dos cadenas son iguales
  * `!=` compara si dos cadenas son diferentes
  * `-n` Eevalua si uan cadena no esta vacia 
  * `-z` Evalua si una cadena esta vacia 
 
```
cadena="hola"

if [ "$cadena" = "hola" ]; then
    echo "La cadena es igual a 'hola'"
fi
```

## Comparaciones avanzadas 

Una forma mas avanzada de realizar comparaciones con `[[]]` que permite usar expresiones mas complejas y evita algunos problemas con operadores especiales, ejemplo expresiones regulares 

```
cadena="123abc"

if [[ $cadena =~ ^[0-9]+$ ]]; then
    echo "La cadena solo contiene números"
else
    echo "La cadena contiene otros caracteres además de números"
fi
```

## Tarea de esta leccion 

1. Crear un script que pida al usuario un numero y le diga si es positivo o negativo o cero 
2. Modifica el script anterior para que tambien determine si el numero es par o impar 
