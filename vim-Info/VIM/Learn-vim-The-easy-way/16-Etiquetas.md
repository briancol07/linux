# Etiquetas 

Una caracteristica muy util a la hora de editar texto es poder de ir a cualquier definicion rapidamente 

## Descripcion de etiquetas 

Para buscar se puede usar fzf o grep o vimgrep 

Las librerias son como una libreta de direcciones de contactos, para buscar una podes hacer `ctrl+J`.
Si no lo encuentra se debera generar el archivo de etiquetas 

## Generador de etiquetas 

Como vim no trae incorporado uno propio tendremos que descargar alguno 

* ctags = c
* exuberant ctags = muchos lenguajes [exuberant](https://ctags.sourceforge.net)
* etags = emacs 
* jtags = java 
* ptags.py = python
* ptags = perl 
* gnatxref = ada 

> Falta terminar 
