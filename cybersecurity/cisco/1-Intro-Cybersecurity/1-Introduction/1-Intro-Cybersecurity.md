# Introduction to cybersecurity 

all around us, 

Help to stop being attack

@apollo

## What is Cybersecurity ?

Ongoing effort to protect individuals, organizations and governments from digital attacks by protecting networked systems and data from unauthorized use or harm 

* Individuals 
  * safeguard your identity 
  * data 
  * Computing devices 
* Organizational 
  * Responsibility to protect the organization's reputation
  * Data 
  * Customers 
* Government 
  * National security 
  * Economic stability 
  * safety and wellbeing of citizens

## Protecting Your personal Data

> Personal data is any information that can be used to identify you, and it can exist both offline and online 

### Offline identity 

Is the real-life persona that you present on a daily basis at home,school or at work.Including your full name , age , address.

### Online identity

* Username
* Alias 
* Social identity

## Your data 

Personal data describes any information about you, including your name, social security number , driver's license number, data and place of bith.
Cyberciminals can use this sensitive information to identify and impersonate you,infinging on your privacy and potentially causing serious damage to your reputation

* Medical records 
* Education records 
* Employment and financial records 


