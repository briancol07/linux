# Protecting your Data and Privacy 

Looks at how to protect Yourself online and maintain your privacy.
Remember, your online data is valueable to cybercriminals. So what can you do to stay safe?

* 1-Protecting-Your-Devices-and-Network.md
* 2-Data-Maintenance.md
* 3-Who-Owns-Your-Data?.md
* 4-Safeguarding-Your-online-Privacy.md
* 5-Discover-Your-own-risky-online-behavior.md
