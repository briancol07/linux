# Windows & buffers

buffer es un espacio de memoria que esta en ram, donde se puede editar y no esta vinculado hasta que lo guardas 

```bash
:buffers
```

Los temporales desaparecen cuando abris otro 

```bash 
:e archivo
```

## Cambiar de archivo/buffer

b1 viene del id que se le da cuando se ven los buffers

```bash 
:b1 
:buffer1
:b2
:buffer2
```

## split 

Para tener varias ventanas

```bash 
# split horizontal
:split
# ctrl + w + s 
# v de vertical
:vsplit 
#ctrl + w + v
```
Para moverte entre ellas es ctrl + w + j 
Para el vertical es ctrl + w + l 

Si estas en el mismo archivo ambas pestanias ven el mismo cambio 
si aparece el [+] nos dice que tengo un buffer sin guardar

## Aumentar tamanio de ventanas 

ctrl + w + 20 + este ultimo mas tambine incluido que nos agrega 20 

igualar todas es ctrl + w + = y pone todas al mismo tamanio 


## Otro tipo de buffer

```bash 
:term
```
Para salir de la terminal
ctrl + \   ctrl + n 

## Para terminar buffer 

Buffer delete ! yes 

```bash 
:bd! 
```

Si se quedo si buffer muestra un no name , para crear uno nuevo 

```bash
:enew
```
