# Escaneo puertos

## Index 

* [## Introduccion al escaneo de Puertos ](#escaneo)
* [## Escaneo Basico de puertos con Nmap ](#nmap)
* [## Escaneo Avanzados ](#avanzado)
* [## Escaneo Completo de puertos ](#full)
* [## Escaneo deteccion de OS ](#os)
* [## Intermpretar Resultados ](#resultados)

## Introduccion al escaneo de Puertos <a name="escaneo"></a>

Es uno de los pasos para el reconocimiento y analisis de una red.

* Permite 
  * Identificar que servicio estan activos 
  * Identificar puertos abiertos y en que estado se encuentran
* Puertos y Servicios 
  * Putos de entrada para diferentes servicios 
  * Solo se exponen un subconjunto de puertos que son necesarios para los servicios que ofrecen
  * Puerto 21 (FTP) : Transferencia archivos 
  * Puerto 22 (SSH) : Acceso remoto seguro
  * Puerto 80 (HTTP): Navegacion web
  * Puerto 443 (HTTPS) : Web segura
  * Puerto 445 (SMB): Comparticion archivos en red locales 
* Tipos de puertos 
  * Abiertos : Servicio activo y esperando conexiones
  * Cerrados : El puerto esta accesible pero no tiene ningun servicio corriendo en ese momento
  * Filtrados: Generalmente bloqueados por un firewall que impida la deteccion del estado real del puerto 

## Escaneo Basico de puertos con Nmap <a name="nmap"></a>

Comando de escaneo basico `namp` este comando escanea los peurtos predeterminados mas comunes en el objetivo, permitiendo al pentester detectar los servicios que podrian estar activos en esos puertos 

`nmap -p 80` Este comando se utiliza para escanear un puerto en particular, en este caso el puerto 80. Esto puede ser util cuando ya se tiene informacion sobre el sistema y se desea enfocarse en un servicio especifico 

## Escaneo Avanzados <a name="avanzado"></a>

Permite obtener informacion adicional evadiendo medidas de seguridad, como firewalls o sistema de detecicon de intrusos (IDS) 

* Escaneo SYN (Half-Open Scan) `nmap -sS`
  * Eficiencia y sigilo 
  * En vez de completar conexion TCP envia un paquete SYN y espera una respuesta SYN/ACK del objetivo, lo que indica que el puerto esta abierto.Luego cierra conexion 
  * Evadir sistemas de deteccion que solo registran conexiones complejas
  * Permite identificar puertos abiertos de manera rapida sin comprometer seguridad atacante
* Escaneo TCP connect  `nmap -sT`
  * Conexion coimpleta 
  * Mas facil de detectar , pero util cuando no se tiene permisos de root 
  * Ideal para obtener una conexion completa (Realizar ataques mas especificos)
* Escaneo UDP `nmap -sU`
  * UDP no requiere una conexion establecida como TCP, lo que los hace mas dificil de escanear
  * Mas lento 
  
## Escaneo Completo de puertos <a name="full"></a>

* Comando de escaneo completo `nmap -p-`
  * Analisis exhaustivo

## Escaneo deteccion de OS <a name="os"></a>

Utiliza una tecnica de huella digital del SO , en la que analiza las repsuestas de los puertos abiertos a diferentes tipos de paquetes y compara esos patrones con una bbdd , el comando es `nmap -O`

## Intermpretar Resultados <a name="resultados"></a>

* Puertos Abiertos (OPEN) 
  * Indica que un servicio esta activo y puede estar escuchand conexiones en ese puerto. Un puerto abierto es una oportunidad para identificar vulnerabilidades en el servicio que se ejecuta 
* Puertos Filtrados (Filtered) 
  * Estan protegidos por un firewall o algun otro mecanismo de seguridad que evita que Nmap determine si estan abiertos o cerrados 
* Puertos Cerrados (Closed)
  * Accesible pero no tiene ningun servicio ejecutandose en el. Aunque no es vulnerable podria abrirse en el futuro.


