# Advanced Text Fu 

## Index 

* [## regex (Regular Expressions) ](#regex)
* [## Text Editors ](#text)

## regex (Regular Expressions) <a name="regex"></a>

> Powerful tool to do pattern based selection , uses special notations similar to those we've encountered already such as the \* wildcard 

Notation | use 
---------|------
 ^  | Beginning of a line 
 $ | End of a line 
 [] | allow us to specify characters found within the bracket
 . | Any single character 

* d[iou]g this would match: dig dog dug
* d[\^i]g this would match: dog and dug but not dig 
* d[a-c]g this would match: dag dbg dcg

## Text Editors <a name="text"></a>

* Vim (vi improved)
  * Use vim to learn vim 
* Emacs 
