# wireshark

## Index

* [## Que es wireshark? ](#que )
* [## Interfaz ](#interfaz)
* [## Analisis de trafico ](#analisis)
* [## Identificacion de vulnerabilidades y amenazas ](#identificacion)

## Que es wireshark? <a name="que "></a>

Es una herramienta utilizada para la captura y analisis de trafico de red. 

* Permite
  * Capturar paquetes de datos que se transmiten
  * Proporciona una interfaz grafica para examinar el contenido de estos paquetes
* Instalacion linux `sudo apt-get install wireshark`

## Interfaz <a name="interfaz"></a>

* Barra de menu 
  * Filtrado de trafico 
  * captura y analisis
* Lista de paquetes capturados 
  * Muestra todos los paquetes que han sido capturados en tiempo real con detalle de los mismos
* Detalles del paquete 
  * Analisis detallado de su contenido en diferentes niveles del modelo OSI
* Bytes del Paquete 
  * Muestra los datos brutos del paquete seleccionado en formato hexadecimal 

## Analisis de trafico <a name="analisis"></a>

* Filtrado de trafico 
  * Permite buscar y filtrar paquetes especificos
* Inspeccion de protocolo
  * Ver los protocolos en el detalle de cada paquete 
* Analisis de Conversaciones 
  * Puede agrupar paquetes en flujos o coversaciones, lo que te permite ver como se estan intercambiando los datos entre dos puntos 

## Identificacion de vulnerabilidades y amenazas <a name="identificacion"></a>

* Captura de credenciales en texto claro 
  * Telnet o FTP se esta utilizando wireshark puede capturar y mostrar credenciales en texto claro
* Analisis de trafico malicioso
  * Los atacantes pueden utilizar herramientas como netcat para exfiltrar datos de una red . 
  * Se puede detectar trafico no autorizado y alertar a los administradores de red 
* Deteccion de ataques de red 
  * Detecta varios tipos de ataques 
    * DDoS
    * Escaneo de puertos 
    * Inyeccion de paquetes maliciosos
    * Analizando el patron y el contenido del trafico 
