# manipulating Files And Directories 

command | Output
--------|--------
cp      | Copy files and directories 
mv      | Move/rename files and directories 
mkdir   | Create Directories
rm      | Remove files and directories 
ln      | Create hard and symbolic links 


how could we copy all the HTML files from one directory to another, but only copy files that do not exist int the destination directory. 

```bash 

cp -u *.html destination 

```

## Wildcards 

Example   | Description 
:--------:|:------------:
Data???   | Any file with "Data" followed by exactly three characters 
BACKUP.[0-9][0-9] | Any file beginning with "BACKUP" followed by exactly 2 numerals 
[[:upper:]]\*  | Any file beginning with an uppercase letter
[![:digit:]]\* | Any file not beginning with a numeral
\*[[:lower"]123] | Any file ending with a lowercase letter or the numerals "1","2" or "3"

## Useful Options and Examples 

Option | Long Way | Meaning 
:-------:|:----------:|:----------:
-a       | --archive | Copy with all attributes (ownership,permissions)
-i       |--interactive | prompt the user for confimation before overwriting an existing file 
-r | --recursive | recursively copy directories and content.
-u | --update | Copy the files that either don't exist or are newer than the existing 
-v | --verbose | Display informative message as the copy is performed 


## Create links 

### Hard link 

```bash
ln file link
```

> When we create a hard link, we create an additional directory entry for a file. 

1. A hard link cannot reference a file outside its own file system. This means a link cannot reference a file that is not on the same disk as the link itself 
2. May not refrence a directory 

when a hard link is deleted , the link is removed but the contents of the file itself continue to exist.

Its like a shortcut for the files that make reference to the original.

### Symbolic link 

```bash 
ln -s item link
```
This create a special type of file that contains a text pointer to the referenced file or cp command 

## Moving and Renaming Files 

```bash 
mv passwd fun
ln fun fun-hard
ln fun dir1/fun-hard
ln fun dir2/fun-hard
ls -l
```
The last number is a 4 in fun and fun-hard , which is the number of hard linkss that now exist for the file. The system assigns a chain of disk blocks to what is called an inode. Each hard link therefore refers to a specific inode containing the file's contents 


## Removing Files and Directories 

```bash 
rm filename
rm -i filename 
```
