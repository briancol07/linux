# Relevamiento 

## Acercamiento 

En esta primer estapa vamos a relizar el primer acercameinto a la empresa donde vamos a poder observar, detectar y Analizar la informacion obtenida atravez de los diversos metodos como :

Es un colegio que tiene : 

* Maternal 
  * 0 - 5 meses 
* jardin 
  * 1 - 4 anios 
* Primaria
* Secundaria 
  * Tecnica 
    * Quimica 
    * Electromecanica
    * Electronica 
  * Comun
    * Sociales 
    * Artes visuales
    * Naturales 
    * Economia 
* Terciario
  * Educacion Fisica 

## Obtencion de informacion 

1. Paginas web ( del institito)
2. Informacion que aparece en buscadores 
  * Imagenes Obtenidas 
3. Experiencia previa 
4. Encuestas a personas que hayan concurrido 

### 1. Pagina Web 

[La Salle](http://www.ilasalle.com.ar/)

Primero lo que podemos observar del link es que no esta con el protocolo https por ende no seria seguro y no estaria encriptado.

Tiene varios apartados en los cuales se pueden llegar a explotar con SQL inyection, tienen varios links que no redirigen a publicaciones de ellos en otras redes sociales lo cual nos permitiria obtener, personas que los siguen o participan de los eventos 

A simple vista no tiene un index of , probe buscando y haciendo pruebas como poniendo index.php o main.css o otros.

* La direccion obtenida es: 
  * Hipólito Yrigoyen 2599 - Florida - Provincia de Buenos Aires

Dentro de la pagina se puede observar que tienen un apartado llamado radar en el cual se pueden ver a los egresados y que camada (anio en el que se egresaron) y tambien otro blog de bodas en los cuales podemos obtener mas infomacion de la gente que se caso en la iglesia del mismo 


### 2. Informacion que aparece en Buscardores 

![Colegio1](./img/colegio1.jpeg)
![Colegio2](./img/colegio2.jpeg)

En estas imagenes se puede observar el volumen de la institucion, teniendo dos gimnasios de deporte 2 canchas de futbol once , una de hockey sintetico, la iglesia y parte de las aulas que contiene una biblioteca, sala de audio, laboratorios, etc .

### 3. Experiencia Previa 

El instituto tiene todo en la nube, cuenta con un sistema para carga de notas llamado edu cloud en el cual cada docente carga las notas de los alumnos .

El colegio apesar de no dejar entrar a la gente con una cita previa (gente externa al colegio), se puede realizar algo de social Engineering que es que decis que iras a comprar ropa para tus hijos o necesitas retirar algo o ir a la caja a pagar unas boletas. Esto te perminte entrar y obtener acceso a la red o instalar algun dispositivo, si la biblioteca esta libre pudes entrar y observar, tambien utilizar las computardoras que estan. 
Cuenta con un firewall llamado fortinet, si neceistas conectarte hay wifi publico con esa red la cual nos permitira ver cual es la ip y otros datos del mismo para clonarla, realizar un ataque 


## 4. Encuestas a personas que hayan concurrido 

Serian mas preguntas , por que hay gente que no forma parte del instituto como ex alumnos, clubes que utilizan el campo de deporte para entrenar, gente invitada a los recorridos, eventos especiales y otros. 
Lo cual permite que varias personas puedan acceder al establecimiento.



