# The Cybersecurity landscape 

## Cryptocurrency

Isdigital money that can be used to buy goods and services, using strong encryption techniques to secure online transactions.

Owners keep their money in encrypted virtual 'wallets'.
Approximately every ten minutes, special computers collect data about the latest cryptocurrency transactions, turn them into mathematical puzzles to amintain confidentiality. These transactions are then verified through a technical and highly complex process known as mining.

Once verified, the ledger is updted and electronically copied and disseminated worldwide to anyone belongoing to the blockchain network, effectively completing a transaction.

## Cryptojacking 

Is an emerging threat that hides on a user's computer mobile phone, tablet, laptop or server using that machine's resources to 'mine' cryptocurrencies without the user's consent or knowlege.
