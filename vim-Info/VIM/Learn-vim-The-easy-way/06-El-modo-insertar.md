# El modo insertar 

El modo insertar es el modo predeterminado de la mayoria de los editores de texto. En este modo, lo que escribes es lo que aparece en pantalla

Command | Accion
--------|--------
i       | Inserta texto antes del cursor 
I       | Inserta texto antes del primer caracter que no sea un espacio 
a       | Aniadir texto despues del cursor 
A       | Aniadir texto al final de la linea
o       | Crea una nueva linea debajo del cursor y cambia de modo 
O       | Crea una nueva linea encima del cursor y cambia de modo 
s       | Elimina el caracter debajo del cursor e inserta texto  (sustituye)
S       | Eleimina la linea acutal e intersta texto (sustituye) 
gi      | Inserta texto en la misma posicion donde el modo insertar fue detenido por ultima vez en el buffer actual
gI      | Inserta texto al comienzo de una linea (columna1)


## Formas diferentes de salir del modo insertar 

Command | Accion
--------|--------
<esc>   | Salir del modo insertar y volver al modo normal
ctrl-[  | Salir del modo insertar y volver al modo normal
ctrl-c  | Similar a ctrl-[ y <esc> perno no controla las abreviaciones

Otra convencion : 

```
inoremap jj <esc>
inoremap jk <esc>
```

## Repetir el modo insertar 

Puedes pasar un parametro de conteo antes de netrar al modo insertar 

```
10i
```
Esto insertara 10 veces lo que escribas 

## Borrar Segmentos en el modo insertar 

Command | Accion
--------|--------
ctrl-h  | Borra un caracter 
ctrl-w  | Borrar una palabra 
ctrl-u  | Borrar una linea entera

## Insertar desde un registro 

Los registros de vim pueden almacenar textos para un uso futuro. Para insertar un texto en cualquier registro nominal minetras estas en el modo insertar 
ctrl-R + [a-z]

```
"ayiw
```

* "a le dice a vim que resultado de tu siguient accion ira al registro "a"
* yiw copia la palabra completa sobre la que esta el cursor

```
ctrl-r a 
```

## Desplazamiento de la pantalla (Scroll)


Command | Accion
--------|--------
ctrl-x ctrl-y | Desplaza el contenido de la pantalla hacia arriba
ctrl-x ctrl-e | Desplaza el contenido de la pantalla hacia abajo


## Autocompletado 

Command | Accion
--------|--------
ctrl-x ctrl-l | Inserta una linea completa 
ctrl-x ctrl-n | Inserta un texto desde el archivo actual 
ctrl-x ctrl-I | Inserta un texto desde los archivos incluidos 
ctrl-x ctrl-f | Inserta un nombre de archivo 

Para navegar por el autocompletado 

Command | Accion
--------|--------
ctrl-n  | Encuentra la siguiente palabra que coincida
ctrl-p  | Encuntra la palabra anterior que coincide

## Ejecutar un comando del modo normal 

con ctrl-o puedes entrar en un submodo insert-normal

### Centrado y salto


Command | Accion
--------|--------
ctrl-o zz | lleva la linea donde esta el cursor al centro de la ventana 
ctrl-o H/M/L | mueve el cursor a la parte superior/media/baja de la ventana 

### Repitiendo Texto 

```
ctrl-o 100ihola 
```

### Ejecutratr comandos de la terminal 

```
ctrl-o !! curl https://google.com 
ctrl-o !! pwd
```

### Borrado mas rapido 

``` 
ctrl-o dtz  " Elimina desde la ubicaicon actual justo antes de la primera letra "z" que encuentre
ctrl-o D    " Elimina desde la posicion acutal del cursor hasta el final de la linea
```
