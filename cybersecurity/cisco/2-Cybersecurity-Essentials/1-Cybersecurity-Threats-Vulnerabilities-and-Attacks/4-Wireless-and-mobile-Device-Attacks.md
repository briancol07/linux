# Wireless and Mobile Device Attacks 

## Grayware adn SMiShing 

Graywave is any unwanted application that behaves in an annoying or undesirable manner. And while grayware may not carry any recognizable malware, it may still pose a risk to the user by, for example, tracking your location or delivering unwanted advertising. 
Author of grayware typically maintain legitimacy by including these 'gray' capabilities in the small print of the SW lincense agreement. This factor poses a growing threat to mobile security in particular, as many smartphones users intall mobile apps without really considering this small print.

## Rogue Access Points 

A rogue access point is a wireless access point installed on a secure network without explicit authorization. although it could potentially be set up by a well-intentioned employee looking for a better wireless connection, it also presnet an opportunity for attackers looking to gian access to an organization's network. 

* Social engineering tactics to gain physical access to an organization's network infrastructure and install the rogue access point 
* Also known as a criminal's access point , the access point can be set up as a MitM device to capture your login information. 
* an Evil twin attack describes a situation where the attacker's access point is set up to look like a better connection option. Once you connect to the evil access point, the attacker can analyze your network traffic and execute MitM attacks .

## Radio Frequency jamming 

Wireless signals aresusceptible to electromagnetic interference (EMI), radio frequency interference (RFI) and lightning strikes or noise from fluorescent lights. 
Attackers can take advantage of this fact by deliberately jamming the transmission of the radio or satelite station to prevent a wireless signal from reaching the receiving station. 
In order to successfully jam the signal, the frequency, modulation and power of the RF jammer needs to be equal to that of the device that the attacker is seeking to disrupt.

## Bluejacking and Bluesnarfing 

Due to the limited range of bluetooth, an attacker must be within range of their target. Here are some ways that they can exploit a target's device without their knowledge 

* Bluejacking uses wireless bluetooth technology to send unauthorized messages or shocking images to another bluetooth device. 
* Bluesnarfing occurs when an attacker copies information, such as emails and contact list, from a target's device using a bluetooth connection. 

## Attacks Agains WI-FI Protocols 

Wired equivalent privacy (WEP) and wi-fi protected access (WPA) are security protocols that were designed to secure wireless networks that are vulnerable to attacks. 
WEP was developed to provide data transmitted over a wireless local area network (WLAN) with a level of protection comparable to what is usually expected of a traditional wired network.It added security to wireless networks by encrypting the data. 
WEP used a key for encryption. The problem, however, was that WEP had no provision for key management and so the number of people sharing the same key continually grew, giving criminals access to a large amount of traffic data. Furthermore, WEP's initialization vector (IV), one of the key components of its encryption key, was too smal, readable and static .
To address this and replace WEP,WPA, and then WPA2 were developed as improved security protocols. Unlike with WEP, an attacker cannot recover WPA2's encryption key by observing network traffic. However, they can still use a packet sniffer to analyze the packets going between an access point and a legitimate user.

## Wi-fi and Mobile Defense 

* Take advantage of basic wireless security features such as authentication and encryption by changing the default configuration settings
* Restrinct access point placement by placing these devices outside the firewall or within a demilitarized zone 
* Use WLAN tools such as NetStumbler to detect rogue access point or unauthorized workstations
* Develop a policy for guest access to an organization's wi-fi network
* Employees in an organization should use a remote access VPN for WLAN access .
