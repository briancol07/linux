# States of Data 

## Data at Rest 

> Refers to data that is in storage 

It is the state dat is in when no user or process is accessing, requesting or amending it. 

* Store in:
  * Hard disk
  * Centralized network 

Any data that is not in transit nor in process is cosidered data at rest. 

* Direct-Attached storage (DAS)
  * Connected to a computer 
  * Hard disk 
  * Pendrive
* Redundant array of independt disk (RAID) 
  * Multiple hard drives in array 
  * Combining multiple disk to see them as a single disk (RAID)
* Network attached storage (NAS) 
  * Connected to a network 
  * Allow storage and retrieval of data from a centralized location 
  * Flexible and scalable 
* Storage area network (SAN)
  * Network-based storage system 
  * Use high speed interfaces 
  * Improved performance and the ability to connect multiple servers to a centralized disk storage repository 
* Cloud storage 
  * Remote Storage 
  * In data centers 
  * accessible from any computer with internet access. 

## Challenges of Protecting Stored Data 

* Direct-attached storage can be one of the most difficult types of data storage to manage and control. Is vulnerble to malicious attacks on the local host. 
* Data at rest also includes backup data. Backups can be manual or automatic. To boost security and decrease data loss, organization should limit the types of data stored on direct-attached storages devices
* Network storage systems offer a more scrue option. Greater performance and redundancy. However ar more  complicated to configure and manage. They also handle more date, posing a greater risk to the organization.
  * Challenges are configuring, testing, monitoring.

## Method of trasmitting Data

Data in transit is the second state of data we are going to look at, referring simply to data which is being transmitted- it is not at rest nor in use.
Dat transmission involves sending information from one device to another, and protecting data in transit poses challenges.

* Sneaker net
  * Uses removable media to physicalle move data from one computer to another 
  * Organizations will never be able to fully eliminate the use of a sneaker net as awat to capture data
* Wired networks 
  * Use cables to transmit data 
  * LAN or span great distances (WAN)
* Wireless network
  * Use radio waves to transmit data and are replacing wired network
  * SOHO ( Small office home Office) 

Both networks uses protocols (IP internet protocol & HTTP Hypertext transfer protocol) 

## Challenges of Data in Transit 


