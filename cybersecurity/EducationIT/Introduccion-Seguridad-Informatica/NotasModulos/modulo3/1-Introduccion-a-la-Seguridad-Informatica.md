# Introduccion a la seguridad informatica 

## Dato vs Informacion 

Informacion y dato son conceptos basicos dentro de la informatica y si bien en el lenguaje cotidiano podrian enterderse como lo mismo, ambos cunetan con diferentes significados

**Dato**: Un dato es un simbolo que describe un hecho, una condicion, un valor o una situacion. Un dato puede ser una letra un numero o un signo ortografico o cualquier simbolo que representa una cantidad, una medida, una palabra o una descripcion.

**Informacion**: Se refiere al conjunto de datos que estan procesados y analizados de modo que podemos predecir o enteder la realidad por medio de ellos.

## Que es la seguridad informatica?

La seguridad informatica es la disciplina que se encarga de proteger la confidencialidad, integridad y disponibilidad de la informacion dentro de un sistema informatico.

## Por que proteger la informacion ?

La informacion es apreciada por muchos aspectos relevantes, por ejemplo, en el ambito organizacional su importancia radica en la utilidad para la toma de decisiones o por su calidad de secretos industrial, por lo que en muchos casos es considerada el activo mas importante.

## Defensa en profundidad 

La defensa en profundidad apunta a implementar varias medidas de seguridad con el objetivo de proteger un mismo activo. Es una tactica que utiliza varias capas, en las que cada una provee un nivel de proteccion adicional a las demas 

```
  ------------------------------
 | ---------------------------- |  
 ||   -------------------      ||
 ||  |                   |     ||
 ||  |   -------------   |     ||
 ||  |  |Datos        |  |     ||
 ||  |  |Aplicaciones |  |     ||
 ||  |  |Host         |  |     ||
 ||  |  |Red          |  |     ||
 ||  |  |Perimetro    |  |     ||
 ||  |   -------------   |     ||
 ||  | Seguridad Fisica  |     ||
 ||   -------------------      ||
 || Politicas y procedimientos ||
 | ---------------------------- |
 |    Capacitacion              |
   -------------------------------
```

## Defensa en profundidad

* Paradigma 
  * Proteger 
  * Detectar
  * Reaccionar

Ademas de incorporar mecanismos de proteccion, debemos estar preparados para recibir ataques, e implementar metodos de deteccion y procedimientos que nos permitan reaccionar y recuperarnos de dichos ataques 

Es importante balancear el foco de las contramedidas en los tres elementos primarios de una organizacion 

* Personas 
* Tecnologia 
* Operaciones 

### Personas 

Alcanzar un nivel de seguridad optimo empieza con el compromiso de la alta gerencia, basado en un claro entendimineto de las amenazas, el cual debe ser seguido por la creacion de politicas y procedimientos, la asignacion de roles y responsabilidades asignacion de recursos y capacitacion a los empleados.
Ademas es necesaria la implementacion de medidas de seguridad fisica y control de personal para poder monitorizar las instalaciones criticas para la organizacion

### Tecnologia 

Para asegurar que las tecnologias implementadas son las correctas, deben ser establecidas politicas y procedimientos para la adquisicion de la tecnologia.
Debemos implementar varios mecanismos de seguridad entre las amenazas y sus objetivos. Cada una debe incluir mecanismos de proteccion y deteccion.

### Operaciones 

Se enfoca en las actividades necesarias para sostener la seguridad de la organizacion en las tareas del dia a dia.

* Mantener politica de seguridad 
* Documentar todos los cambios realizado en la infrastructura 
* Realizar analisis de seguridad (periodicos)
* Implementar metodos de recuperacion 

## La triada CID 

### Confidencialidad 

La confidencialidad es la propiedad que impide la divulgacion de informacion a personas o sistemas no autorizados. A grandes rasgos, asegura el acceso a la informacion unicamente a aquellas personas que cuenten con la debida autorizacion.

### Integridad 

Es la propiedad que busca mantener los datos libres de modificaciones no autorizadas. ( no es igual a la integridad referencial en base de datos). A groso modo, la integridad es el mantener con exactitud la informacion tal cual fue generada, sin ser manipulada o alterada por personas o procesos no autorizados. 

### Disponibilidad 

La disponibilidad es la condicion de la informacion de encontrarse a disposicion de quines deben acceder a ella ya sean personas, procesos o aplicaciones. A groso modo, la disponibilidad es el acceso a la informaicion y a los sistemas por personas autorizadas en el momento que asi lo requieran
