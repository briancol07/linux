# Autenticacion basada en roles (RBAC)

* Minimo acceso posible 
  * Least privilege 

## Indice 

* [## Que es RBAC ? ](#rbac)
* [## Por que es importante ? ](#importante)
* [## Elementos ](#elementos)
* [## Ventajas ](#ventajas)
* [## Buenas Practicas ](#BuenasPracticas)
* [## Desafios Comunes ](#desafios)

## Que es RBAC ? <a name="rbac"></a>

Modelo de control de acceso que permite a los administradores gestionar los permisos asignando derechos y privilegios a grupos de usuarios basados en sus roles dentro de una organizacion o sistema. 

## Por que es importante ? <a name="importante"></a>

Garantizar que los usuarios de una aplicacion solo tengan acceso a la informacion y funciones que son necesarias para su trabajo 

* Mejor seguridad 
* Facil de gestionar
* Cumplimiento de normativas 
  * GDPR
  * HIPAA 

## Elementos <a name="elementos"></a>

* usuarios 
  * Cada usuario puede estar asociado a uno o mas roles 
* Roles 
  * Permisos predefinidos 
* Permisos 
  * Las acciones que un rol puede realizar dentro de la aplicacion 
    * Crear 
    * Leer 
    * Actualizar 
    * Eliminar datos 
* Ejemplos  
  * Administrador
  * Gerente 
  * Empleado

![rbac](./img/rbac.png)

## Ventajas <a name="ventajas"></a>

* Menor Riesgo de errores humanos 
* Facilidad de mantenimiento
* Cumplimiento Normativo
  * ISO 27001
  * PCI-DSS

## Buenas Practicas <a name="BuenasPracticas"></a>

* Definir roles claros 
* Principio del minimo privilegio 
* Revisar periodicamente los permisos 

## Desafios Comunes <a name="desafios"></a>

* Sobrecarga Administrativa 
  * Limites claros y evitar la creacion de roles redundantes
* Escalabilidad 
  * Estructurar adecuadamente las consultas de roles y permisos 
* Auditoria y registros 
  * Seguimiento de los cambios en los permisos y roles 
