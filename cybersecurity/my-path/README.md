# My own way 


## Index 

* [## Books ](#books)
* [## Some Skills ](#skills)
* [## Topics](### Topics)
* [## Task ](#tasks)
* [## Links ](#links)


In this folder/repo will write summary of the books that I read and details how I enter the cybersecurity field. 

Not only will have cybersecurity topics, also I add some linux/other topics.

It's my way, that dosen't mean that you have to do it exactly or replicate, use this as a resource and create your own path. 

I hope that this information is usefull for you 


## Books <a name="books"></a>

- [ ] Bug Bounty Bootcamp 
- [ ] 


## Some Skills <a name="skills"></a>

* Soft Skills 
  * Thinking 
    * Crititical 
    * Analitical 
    * Differnt (out of the box)
* Hard Skills
  * Networking knowledge 
  * Programming 
  * OS knowledge
  * Security 
    * Web
    * Mobile
    * Wireless
  * Cryptography 
  * Inverse engineering 


## Topics 

## Task <a name="tasks"></a>

- [ ] (CIA Triad)
- [ ] (Principle of least privilege)
- [ ] (Security models)
- [ ] (Threat Modeling and inciden response)

## Links <a name="links"></a>

* [Cisco](https://skillsforall.com/dashboard#)
* [Crear lab hacking](https://achirou.com/como-crear-un-laboratorio-y-herramientas-de-hacking-en-2024/)
* [Iniciarce Hacking](https://achirou.com/como-iniciarse-en-hacking-y-programacion-en-2024-cursos-recursos-y-certificaciones/)
* [Linux](https://twitter.com/BowTiedCyber/status/1741920491403931676)
* [Freecodecamp](https://www.freecodecamp.org/news/tag/penetration-testing/)
* [CertTree](https://twitter.com/SecurityTrybe/status/1741024885743452463)
* [Awsome-reversing](https://github.com/HACKE-RC/awesome-reversing)
* [SQLI](https://medium.com/@ucihamadara/example-of-a-error-based-sql-injection-dce72530271c)
* [Inyeccion HTML](https://achirou.com/pentesting-web-introduccion-a-inyeccion-html/)
* [Wireshark](https://achirou.com/guia-rapida-de-wireshark-todos-los-comandos-filtros-y-sintaxis/)
* CTF
  * [CTF-Resources](https://medium.com/technology-hits/capture-the-flag-ctf-resources-for-beginners-9394ee2ea07a)
* [Hacker-Roadmap](https://github.com/Hacking-Notes/Hacker-Roadmap?tab=readme-ov-file)


