# Query Results 

## Sorting Results

> ORDER BY and specifying the colum to sort 

```
SELECT * FROM logins ORDER BY password;
```

* Can be `ASC` or `DESC`
* Limit by two columns 

## Limit Results

In case our query returns a large number of records, we can `LIMIT` the result to what we want only 

```
SELECT * FROM logins LIMIT 2;
```

* We can usea an offset 

## WHERE Clause 

to filter or search for specific data, we can use conditions with the `SELECT` statement using the `WHERE` clause to finetune the resutls 

```
SELECT * FROM table_name WHERE <condition>;
```

## LIKE Clause

Enabling selecting records by matching a certain pattern 

```
 SELECT * FROM logins WHERE username LIKE 'admin%';
```

* The `%` symbol acts as a wildcard and matches all characters after admin ( Zero or more ) 
* The `_` is used to match only one character 


