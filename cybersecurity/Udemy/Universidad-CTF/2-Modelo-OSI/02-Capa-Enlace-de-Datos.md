# Capa 2 Nivel de enlace de datos

>  Se encarga de transferir (de forma confiable) en el circuito de datos, entre dos dispositivos que esten conectados de manera directa.

Evitar colapsos y saturacion 

## Funciones 

* Establecimiento , control, Finalizacion de los enlaces
* Controla el volumen de informacion 
* Control de errores y gestion 
* Evita el flujo que sature el equipo siguiente


Comunica la capa de red con la capa fisica. Brinda el direccionamiento a nivel de frames. Los switches son ejemplo de un dispositivo importante en esta capa 

La inutilizacion logica de los puertos sin utilizar es clave en esta capa, evita la conexiones fraudelentas o Ataques de envenenamieto de ARP (man in the middle) 

## Protocolos 

* WPA (medios inalambricos)
* WPA2



