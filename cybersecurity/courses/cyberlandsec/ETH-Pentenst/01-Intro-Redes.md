# Conceptos Basicos de Redes : Modelo OSI y TCP / IP 

Las redes permiten intercambiar datos e informacio. Para facilitar esta comunicacion, las redes siguen protocolos y modelos estructurados 

## Modelo OSI <a name="osi"></a>

> OSI : Open Systems interconnection

Se dvide la comunicacion de red en siete capas distintas cada una con funciones especificas y bien definidas 

* Capa 1 : Fisica
  * Mas baja , se ocupa de la transmision de bits a traves de medios fisicos como cables, seniales electircas o seniales opticas 
  * Hubs , cables y conectores 
* Capa 2 : Enlace de datos 
  * Proporciona medios para la transferencia de datos entre dispositivos en la misma red. Maneja errores de capa fisica, direccion MAC y controla el flujo de datos 
  * Switches y bridges 
* Capa 3 : Red 
  * Gestiona la direcicon logica y el encaminamiento de datos entre diferentes redes. Proporciona funciones como direccionamiento IP y routing
  * Routers 
* Capa 4 : Transporte 
  * Garantiza la transferencia de datos de extremo a extremo entre dispositivos
  * Proporciona servicios como control de flujo, reenvio de datos y correccion de errores.
  * TCP y UDP son protocolos de esta etapa
* Capa 5 : Sesion 
  * Establece, gestiona y finaliza las sesiones entre dispositivos en una red.
  * Responsable de la sincronizacion y el control del dialogo entre las aplicaciones en los sistemas finales
* Capa 6 : Presentacion 
  * Se ocupa de la traduccion, cifrado y compresion de datos. Actua como un traductor de datos entre la aplicacion y la red.
  * Maneja la codificacion de datos y la conversion de formatos
* Capa 7 : Aplicacion
  * Es la capa mas alta del modelo OSI y la mas cercana al usuario final
  * Proporciona servicios de red a las aplicaciones, como correo electronico, transferencia de archivo y servicios web. 
  * Protocolos involucrados : HTTP, FTP, SMTP 

## Modelo TCP / IP <a name="tcp-ip"></a>

> TCP : transmission control Protocol -- IP : Internet Protocol 

Es un modelo mas practico y ampliamente utilizado en redes modernas, especialemnte  en internet. Se basa en una pila de cuatro capas .

* Capa de acceso a la red 
  * Combina las funciones de las capas fisica y de enlace de datos del modelo OSI. Se encarga de la transmision y la gestion dle trafico entre redes 
  * Utiliza protocolo IP
* Capa de internet 
  * Equivale a la capa de red del modelo OSI. 
  * Es la responsable del direccionamiento IP, el encaminamieto de paquees y la gestion del trafico entre redes. 
  * IP 
* Capa de transporte 
  * Corresponde a la capa de transporte del modelo OSI 
  * Gestiona la comunicacion de datos entre los extremos 
  * Proporciona control de flujo, retransmission y reensamble de paquetes.
  * TCP Y UDP
* Capa de aplicacion
  * Combina las funciones de las capas de sesion, presentacion y aplicacion del modelo OSI. 
  * Responsable de la interaccion con el usuario y la entrega de datos a aplicaciones 
  * HTTP, FTP, SMTP 

## Como interacutan las capas <a name="interaccion"></a>

Dependen de las capas adyacentes para realizar su tarea.EJ: la capa de red necesita la capa de enlace de datos para transmitir los paquetes a traves de una red local y la capa de transporte asegura que los datos se entreguen correctamente entre dos dispositivos en diferentes redes .

### Ejemplo <a name="ejemplo"></a>

* Capa de aplicacion : El navegador genera una solicitud HTTP para el sitio web
* Capa de transporte : La solicictud se divide en segmentos TCP, se le aniade un numero de puerto y se asegura de quelos datos lleguen de manera ordenada 
* Capa de internet : Los segmentos TCP se encapsulan en paquetes IP, se les aniade una direccion IP y se encaminana a traves de la red
* Capa de acceso a la Red : Los paquetes IP se transmiten como tramas ethernet a traves del cable de re o la red Wi-Fi al router y hacia el destino final 

## Puertos y protocolos <a name="puertos-protocolos"></a>

### Puertos de red 
