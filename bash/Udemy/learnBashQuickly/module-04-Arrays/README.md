# Module 4 Arrays 


## Overview

1. Creating your first array
2. Accessing array elements 
3. Addign array elements
4. Deleting array elements
5. Creating hybrid arrays 

## Arrays

> List of items , order o not 

## 1 

```bash
file=("f1.txt" "f2.txt)
```

## 2 

To access a element in the array 

```bash 
# ${file[n]}
# n is the position of the element 
```
to display all the elements in the array you will use * instead of a numer (n)  

If you want to display the position of the file instad of n you will write an @


```bash 
# to revalue
file[0]="a.txt"
# to show length of array
echo ${#files[n]}
```

##3 append 

```
files+=("file.txt")
```

## 4 delete 

```bash 
# To delete an element in a specific position
unset num[2]
# To remove all the array or any variable 
unset nameofVariable
```
## 5 

```bash
user=("john" 122 "sudo.developers" "bash")
```


