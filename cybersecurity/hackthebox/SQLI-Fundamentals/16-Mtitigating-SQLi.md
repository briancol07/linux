# Mitigating SQLi 

## Example 

```
<SNIP>
  $username = $_POST['username'];
  $password = $_POST['password'];

  $query = "SELECT * FROM logins WHERE username='". $username. "' AND password = '" . $password . "';" ;
  echo "Executing query: " . $query . "<br /><br />";

  if (!mysqli_query($conn ,$query))
  {
          die('Error: ' . mysqli_error($conn));
  }

  $result = mysqli_query($conn, $query);
  $row = mysqli_fetch_array($result);
<SNIP>

```

The script takes in the username and password from the post request and passes it to the query directly . This will let an atttacker inject anything they wish and exploit the application 
[escape-string](https://www.php.net/manual/en/mysqli.real-escape-string.php) this help to espaes character such as `'` and `"` so they don't hold any special meaning 

## Input validation 

User input can also be validated based on the data used to query to ensure that it matches the expected input.
We can validate an input with a pattern ( Regex ) 

## User privileges 

We should ensure that the user querying the database only has minimum permissions 

> Superusers and users with administrative privileges should never be used with web application 

## Web Application firewall (WAF)

Are used to detect malicious input and reject any HTTP request containing them. 

* Some open source 
   * ModSecurity 
   * Cloudflare 

> Example Information\_schema would be rejected 

## Parameterized queries 

Ensure that the input is safely sanitized is by using parameterized queries. Parameterized queries contain placeholders for the input data, which is then escaped and passed on by the drivers 

```
<SNIP>
  $username = $_POST['username'];
  $password = $_POST['password'];

  $query = "SELECT * FROM logins WHERE username=? AND password = ?" ;
  $stmt = mysqli_prepare($conn, $query);
  mysqli_stmt_bind_param($stmt, 'ss', $username, $password);
  mysqli_stmt_execute($stmt);
  $result = mysqli_stmt_get_result($stmt);

  $row = mysqli_fetch_array($result);
  mysqli_stmt_close($stmt);
<SNIP>
```

The query is modified to contain two placeholders, marked with ? where the username and password will be placed 




