# Analyzing-a-Cyber-Attack

## Types of malware 

malicious SW or malware.

Malware is any code that can be used to steal data, bypass access controls, or cause harm to or compromise a system.

### Spyware 

Designed to track and spy on you. Monitors your online activity and can log every key you press on your keyboard,as well as capture almost any of your data, including sensitive personal information such as your onle banking detials.

### Adware 

Is often installed with some versions of SW and is designed to automatically deliver advertisments to a user, most often on a web browser. It is common for adware to come with spyware .

### Backdoor 

This type of malware is used to gain aunauthorized access by bypassing the norma l authentication procedures to access a system. As a resutl, hackers can gain remote access to resources within an applicatin and issue remote system commands. Works in the background and is difficult to detect. 

### Ransomware 

This is designed to hold a computer system or the dat it contains captive until a payment is made. Usually works by encrypting your data, so that you can't access it. Often spread through phishing emails.

### Scareware 

This is a type of malware that uses 'scare' tactics to trick you into taking a specific action. Consists of operating system style windows that pop up to warn you that your system is at risk and needs to run a specific program for it to rerutn to normal operation. If you agree to execute the specific program, your system will become infected with malware.

### Rootkit

This malware is designed to modify the operating system to create a backdoor, which attackers can then use to access your computer remotely. Most rootkits take advantage of software vulnerabilities to gain access to resources that normally shouldn't be accessible (privilege escalation and modify system files.
They also modify system forensics and monitoring tools, making them very hard to detect.

### Virus 

When executed, replicates and attaches itself to other executable files, sich as a document, by inserting its own code. Most viruses require end-user interaction. 
Can be relative harmless such as those that displey a funny image , or they can be destructive, such as those that modify or delete data. 
Viruses can also be programmed to mutate in order to avoid detection. Most viruses are spread by USB, optical disks, network shares or email.

### Trojan Horse 

This malware carries out malicious operations by masking its true intent.It might appear legitimate but is, in fact very dangerous. Exploit your user privileges. Unlike viruses, trojans do not self-replicate but act as a decoy to sneak milicious software past unsuspecting users.

### Worms 

Replicates itself in order to spread from one computer to another. Unlike virus which requires a host program to run, worms can run by themselves.Other than the initial infection of the host. They do not require user participation and can spread very quickly over the network.

* Exploit system vulnerabilities 
* Way to propagete 
* Contains malicious code 

## Symptoms of Malware 

* An increase in cpu usage, which slows down your device
* Freezing or crashing often 
* Decrease in your web browsing speed
* Unexplainable problems with your network connections
* Modified or deleted files
* The presnece of unknown files, programs or desktop icons
* Unknown processes running
* Program turning off or reconfiguring themselves
* Emails being sent without your knowledge or consent

