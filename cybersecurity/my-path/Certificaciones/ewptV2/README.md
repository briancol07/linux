# ewptV2

## Preparacion 

* [Preparacion joas](https://github.com/CyberSecurityUP/eWPT-Preparation)
* [Wannabe Hacker](https://grumpz.net/pass-the-ewpt-exam-in-2023-using-free-resources-on-your-first-attempt)
* [Deep Hacking](https://deephacking.tech/ewpt-review/)

## Entornos de practica 

* [Practica Hacking](https://achirou.com/practica-hacking-de-forma-segura-100-sitios-web-y-entornos-parte-2/)

## Files 

* [EWP-80](./EWPT-80-percent-plan.md)
* [Plan80Percent](./Plan80percent.md)
* [Study-plan-ewpt](./Study-plan-ewpt.md)
* [Checkpoints](./checkpoints.md)

## Articles 

* [Bastijn ouwendijk](https://bastijnouwendijk.com/my-journey-to-becoming-an-ewpt)
* [Review of eWPT](https://medium.com/@pr0tag0nist/review-of-ewpt-b9ce7808538a)
* [5 keys takeaways](https://medium.com/@cydtseng/i-passed-the-ewpt-these-are-my-5-key-takeaways-eb13c86184a3)


## Topics 

* Web Application Basics:
  * Understanding HTTP/HTTPS protocols
  * Web application architecture and components
  * Client-side vs. server-side processing
* Web Application Attacks:
  * Cross-Site Scripting (XSS)
  * SQL Injection (SQLi)
  * Cross-Site Request Forgery (CSRF)
  * Clickjacking
  * File Inclusion vulnerabilities
  * Insecure Direct Object References (IDOR)
  * Server-Side Request Forgery (SSRF)
  * XML External Entity (XXE) attacks
* Web Application Security Mechanisms:
  * Authentication mechanisms (e.g., session management, cookies, tokens)
  * Authorization models (e.g., role-based access control)
  * Input validation and data sanitization
  * Security headers (e.g., Content Security Policy, HTTP Strict Transport Security)
  * Encryption and hashing
* Web Application Penetration Testing Methodologies:
  * Information gathering and enumeration
  * Vulnerability assessment and scanning
  * Exploitation techniques
  * Post-exploitation activities
  * Reporting and documentation
* Tools and Techniques:
  * Burp Suite or similar web application security testing tools
  * OWASP ZAP (Zed Attack Proxy)
  * SQLMap for SQL injection testing
  * Web proxies for intercepting and modifying web traffic
  * Scripting languages (e.g., Python, Ruby) for automation
* Secure Coding Practices:
  * Understanding common programming vulnerabilities (e.g., OWASP Top 10)
  * Best practices for secure coding and development
* Additional Topics:
  * API security
  * Mobile application security (if applicable)
  * Cloud security considerations related to web applications

## Study Plan 

1 to 2 hs per day 

### Week 1-2: Foundation and Familiarization

* Days 1-4: Start with an overview of web application basics and security concepts. Focus on HTTP/HTTPS protocols, client-server architecture, and common vulnerabilities.
* Days 5-7: Begin exploring the official eLearnSecurity eWPT course material. Cover the introductory modules.

### Week 3-4: Deep Dive into Core Topics

* Days 8-18: Dive deeper into specific topics. Spend dedicated days on XSS, SQLi, CSRF, and other major vulnerabilities. Utilize resources like books, online courses, and practical labs to reinforce learning.
* Days 19-21: Review and solidify your understanding of these vulnerabilities.

### Week 5-6: Methodologies and Hands-On Practice

* Days 22-28: Focus on penetration testing methodologies, tools, and techniques. Learn about information gathering, scanning, exploitation, and post-exploitation activities.
* Days 29-35: Start practicing hands-on exercises in eLearnSecurity labs and other platforms. Apply methodologies to practical scenarios.

### Week 7-8: Review and Practice Exams

* Days 36-45: Review and revisit weak areas. Take practice exams or quizzes to assess your knowledge and identify areas needing improvement.
* Days 46-56: Dedicate time to solving challenges, participating in CTFs, and reinforcing concepts through practical exercises.

### Final Days:

* Days 57-60: Focus on refining your understanding of any remaining challenging topics. Ensure you're comfortable with all aspects of web application security and penetration testing.


## Resources 

### Websites and Platforms:

* OWASP (Open Web Application Security Project): Offers free guides, documentation, and resources on web application security.
* PortSwigger Web Security Academy: Provides free web security training, including tutorials, labs, and exercises focused on web application vulnerabilities.
* Hack The Box: Offers various web application security challenges. Some challenges are free to access and can help you practice your skills.
* OverTheWire: Provides web security challenges for beginners and more advanced users. Their challenges are free and cover various security aspects.

### YouTube Channels and Tutorials:

* The Cyber Mentor: Provides free tutorials on ethical hacking and penetration testing, including web application security topics.
* LiveOverflow: Offers videos on cybersecurity topics, including web application security, CTF challenges, and practical demonstrations.
* John Hammond: Provides tutorials and walkthroughs of Capture The Flag challenges and various hacking topics.

### Documentation and Guides:

* OWASP Testing Guide: The Open Web Application Security Project provides a comprehensive guide to testing web applications for security vulnerabilities.
 * Bash Scripting Guide: There are various free guides and tutorials available online for learning Bash scripting. Websites like Linuxize offer free Bash scripting guides.

### Practice Environments:

* OWASP Juice Shop: An intentionally insecure web application designed for practicing web security concepts. It's an open-source project and free to use.
* Vulnerable Web Applications for Practice: Platforms like DVWA (Damn Vulnerable Web Application) or WebGoat provide intentionally vulnerable applications for practice.

## Extra

* [HackerSploit](https://x.com/HackerSploit)
  * [Academy](https://hackersploit-academy.thinkific.com/collections)
