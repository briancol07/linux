# Modulo 4 

## Objetivos 

* Directiva de Seguridad local
* Politicas de contraseñas y Bloqueo de Cuentas 
* SW Restriction Policy 
* Politicas de Applocker
* Fortalecer configuracion de seguridad desde la politica local 
* Definir los requerimientos de seguridad de las contraseñas de usuario
* Configurar la politica de bloqueo de cuentas de usuario
* Establecer exactamente que programas pueden ser ejecutados por los usuarios 
