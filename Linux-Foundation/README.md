# Linux foundation 

## Resources 

* [link](https://trainingportal.linuxfoundation.org/learn/dashboard)
* [Courses](https://www.cncf.io/training/courses/)
* [Introduction Cloud Infraestructure Technologies](https://training.linuxfoundation.org/training/introduction-to-cloud-infrastructure-technologies/)
