# Notes of Chapter 1

##  Where to start

* Quick reference handbook that summarizes what you need to know to perform common task on a variety of common unix and linux systems 

## Essential duties of a system Admin 

* Controlling access
  * Access related issues 
* Adding hardware 
* Automating tasks 
  * [expect](https://core.tcl-lang.org/expect/index) 
* Overseeing backups 
* Installing and upgrading software 
* Monitoring 
* Troubleshooting 
* Maintaining local documentation
* Vigilantly monitoring security
* Tuning performance 
* Developing site policies 
* Woking with vendors 
* Fire Fighting 

## Man Pages 

> Manual pages, usually called man pages ( Read with the command man ) 

Sections | Contents 
:-------:|:--------:
1 | user-level commands and applications 
2 | System calls and kernel error codes 
3 | Library calls 
4 | Device drivers and network protocols 
5 | Standard file formats 
6 | Games and demonstrations 
7 | Miscellaneous files and documents 
8 | System administration commands 
9 | Obscure kernel specs and interfaces 

* Books 
  * Unix in a nutshell O'reilly
p18
```bash
# -k ( Keyword or apropos print a list of man pages that have keyword 
man -k translate
# Other example 
man 2 sync 
```

### Storage of man pages 

nroff input for man pages is stored in directories under `/usr/share/man` and compressed with gzip to save space, the man command knows how to decompress them on the fly.

## Add-ons 

Is often provided in the form of precompiled packages as well, although the degree ot whcih this is a mainstream approach varies widely among systems. 

The fact that two system use the same package format doesn't necessarily mean that packager for the two system are interchangeable. Example Red Hat and SUSE both use RPM .

## Determining if software is already installed 

Use command, to find out if a relevant binary is already in your search path  

 ```
 # which command 
 # example 
 which gcc 
 ```

Another alternative is the incredibly useful `locate`command which consults a pre compiled index of the filesystem to locate filenames that match a particular pattern. Can find any type of file 

## Adding new software 

Command | OS | Type 
--------|----|------
sudo apt-get install tcpdump | Debian - Ubuntu | APT
sudo yum install tcpdump | RedHat - CentOS | YUM 
sudo pkg install -y tcpdump | FreeBSD | PKG 

## Building software from source code 

* Obtain source code 
* Clone repository in `/tmp` directory 
* Sequence configure/make/make install

The last one is written in c and work on all unix and linux systems , to tweak the build configuration use `./configure --help` 

## Installing from a web script 

Cross-platform software offer expedited installation process driven by a shell script. 
* Some commands to download 
  * Curl 
  * Fetch 
  * Wget 

 ```
 # Commands 
 curl -o /tmp/saltboot -sL https://bootstrap.saltstack.com
 ```
The boostrap script investigates dthe local environment, then downloads, installs and configures an appropriate version of the software.

P25
