# Same origin Policy vulnerability 

* Bypass SOP ( standard operating procedure ) 
  * Pirvate Info leaks
  * Account Take Over 
  * Data brach 

* A script from page "A" can access "B" if they are of the same origin 
* Share
  * Protocol
  * Hostname 
  * Port Number 

> Subdomains can't share info 

For this they have a work around in js calle `documen.domanin`

* only works with one subdomain 
* Share with siblins or superdomain 

## Exploiting: Cross-Origin resource sharing ( CORS ) 

Mechanism that protect data from server 

Specify list of origins that are allowed to access 
Header -> Access-control-allow-origin

you can use wildcard `*` -> for weak regex 

* Protection
  * Allowlist
  * Robust URL validation 
 
Exploiting postmessages() --> WEB API

Event handler -> pop up

Exploting json with padidng -> Arround SOP 

Bypassing SOP by using XSS

## Hunting 

1. Dermine if SOP relaxation techniques are used ( Event listener -> Chrome inspect ) 
2. Find misconfiguration 
  * Access-control -allow-origin -> misconfiguration 
3. Finst post message bugs 
4. Find json issues
5. Consider mitigation factors 
