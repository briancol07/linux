# Introduction to linux ( LFS101 )

## Links <a name="links"></a>

* [Link](https://trainingportal.linuxfoundation.org/courses/introduction-to-linux-lfs101)
* [Distribution-List](https://lwn.net/Distributions/)

## Distros 

Here are some distros 

![distro](./img/distros.png)

## Recommendations 

* Have a linux installed on a machine or vm 
* Read man pages 
