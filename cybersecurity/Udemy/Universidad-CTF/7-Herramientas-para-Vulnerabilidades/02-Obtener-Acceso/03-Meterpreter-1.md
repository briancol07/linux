# MeterPreter - 1 

Es un payload de metasploit, nos permite hacer extra de post explotacion 


```bash 
help 
```
## Ejecutar siempre 

A la hora de hacer post-exploit , no son necesarios pero recomendables 

```bash
sysinfo 
```

sysinfo como dice nos da la informacion de la computadora

```bash 
ps
``` 
ps me muestra todos los servicios que se estan corriendo en la maquina victima

La maquina reinicia proceso para evitar conexiones y por eso hay que migrar de procesos y evitar que se caiga 

```bash 
migrate <PID>
```
Para migrar de proceso se necesita el proccess id 

## Saber el Usuario 

```bash 
getuid
```
Nos devolveria el usuario en el que estamos de la maquina atacada 

## Verificar si es una VM

```bash 
run checkvm
```

Para evitar que sea un honeypot .
