# Checkpoints 

## Mayo
- [ ] Semana 3-4: Fundamentos de Pentesting Web
    - [ ] Checkpoint 1:
        *  Comprender las primeras 3 vulnerabilidades del OWASP Top 10.
    - [ ] Checkpoint 2:
        * Comprender las siguientes 4 vulnerabilidades del OWASP Top 10.
    - [ ] Checkpoint 3:
        * Comprender las últimas 3 vulnerabilidades del OWASP Top 10.
## Junio

- [ ] Semana 1-2: Herramientas de Pentesting Web (Burp Suite)
    - [ ]Checkpoint 4:
        * Completar el módulo introductorio de Burp Suite.
    - [ ] Checkpoint 5:
        * Realizar los laboratorios de "Proxy" y "Target" en PortSwigger Academy.
    - [ ] Checkpoint 6:
        * Completar los laboratorios de "Repeater" y "Intruder".
- [ ]Semana 3-4: Herramientas de Pentesting Web (SQLMap y OWASP ZAP)
    - [ ]Checkpoint 7:
        * Realizar tutorial básico de SQLMap.
    - [ ]Checkpoint 8:
        * Completar tutorial introductorio de OWASP ZAP.
    - [ ]Checkpoint 9:
        * Completar laboratorios avanzados de SQLMap y OWASP ZAP.

## Julio

- [ ]Semana 1-2: Metodologías de Pentesting (OWASP Testing Guide)
    - [ ]Checkpoint 10:
        * Leer y comprender capítulos 1 y 2 de OWASP Testing Guide.
   - [ ] Checkpoint 11:
        * Leer y comprender capítulos 3 y 4.
  - [ ] Checkpoint 12:
       * Leer y comprender capítulos 5 y 6.
- [ ]Semana 3-4: Metodologías de Pentesting (OSSTMM)
    - [ ]Checkpoint 13:
       * Leer el capítulo introductorio de OSSTMM.
    - [ ]Checkpoint 14:
       * Leer y resumir las secciones sobre controles y auditorías.
    - [ ]Checkpoint 15:
       * Realizar ejercicios prácticos basados en OSSTMM.

## Agosto

- [ ]Semana 1-2: Explotación Avanzada (PentesterLab)
    - [ ]Checkpoint 16:
        * Completar los primeros laboratorios de PentesterLab.
    - [ ]Checkpoint 17:
        * Completar laboratorios de inyección SQL y XSS avanzados.
    - [ ]Checkpoint 18:
        * Completar laboratorios sobre deserialización y RCE.
- [ ]Semana 3-4: Explotación Avanzada (Hack The Box y TryHackMe)
    - [ ]Checkpoint 19:
        * Resolver una máquina fácil a intermedia en Hack The Box.
    - [ ]Checkpoint 20:
        * Completar un laboratorio avanzado en TryHackMe.
    - [ ]Checkpoint 21:
        * Resolver otra máquina o laboratorio avanzado.

## Septiembre

- [ ]Semana 1-2: Preparación para el Examen
    - [ ]Checkpoint 22:
        * Realizar un examen de práctica completo.
    - [ ]Checkpoint 23:
        * Analizar y corregir errores de un examen de práctica.
    - [ ]Checkpoint 24:
        * Realizar otro examen de práctica.
- [ ]Semana 3-4: Revisión y Práctica Intensiva
    - [ ]Checkpoint 25:
        * Repasar OWASP Top 10 y metodologías de pentesting.
    - [ ]Checkpoint 26:
        * Resolver laboratorios pendientes o revisitar antiguos.
    - [ ]Checkpoint 27:
        * Realizar una simulación completa del examen.

## Octubre

- [ ]Semana 1-2: Simulación de Examen
    - [ ]Checkpoint 28:
        * Realizar una simulación de examen bajo condiciones similares.
    - [ ]Checkpoint 29:
        * Analizar y corregir errores de la simulación.
    - [ ]Checkpoint 30:
        * Realizar una segunda simulación completa.
- [ ]Semana 3-4: Ajustes Finales y Revisión
    - [ ]Checkpoint 31:
        * Revisión de temas específicos y áreas débiles.
    - [ ]Checkpoint 32:
        * Practicar técnicas de manejo del estrés pre-examen.
