# Vulnerabilidades de Inyeccion

## Topics 

* [## Que son las vulnerabilidades de inyeccion? ](#vuln-inyeccion)
* [## Como funciona una inyeccion SQL ? ](#funcionamiento)
* [## Por que es peligrosa? ](#peligrosa)
* [## Como prevenirlas ? ](#prevenir)
* [## Otros tipos de inyeccio ](#otrostipos)

## Que son las vulnerabilidades de inyeccion? <a name="vuln-inyeccion"></a>

Un punto de acceso que los atacantes pueden aprovechar para ejecutar comandos maliciosos o manipular la base de datos de la aplicacion? 


Ocurren cuando una aplicacion permite que datos externos ( entradas del usuario ) se interpreten o se ejecuten como comandos. 

* Ejemplos 
  * SQL injection 
  * Inyeccion LDAP 
  * Inyeccion XML 

## Como funciona una inyeccion SQL ? <a name="funcionamiento"></a>

Ejemplo 
```
SELECT * FROM usuarios WHERE nombre_usuario = 'usuario' AND contraseña = 'contraseña';
```

Cuando el usuario introduce su nombre y contrasenia, la consulta sql se ejecuta para validar la autenticidad pero que pasa si ....

```
usuario' OR '1'='1
SELECT * FROM usuarios WHERE nombre_usuario = 'usuario' OR '1'='1' AND contraseña = 'contraseña';
```

El usuario podria acceder a la aplicacion sin una contrasenia valida 

## Por que es peligrosa? <a name="peligrosa"></a>

* Leer, modificar o borrar datos en una bbdd
* Acceder a informacion sensible 
* Tomar el control completo 
* Escalar privilegios 

## Como prevenirlas ? <a name="prevenir"></a>

* Consultas preparadas 
```
query = "SELECT * FROM usuarios WHERE nombre_usuario = ? AND contraseña = ?"
cursor.execute(query, (usuario, contraseña))
```
* Uso de ORM ( Object-relational mapping ) 
  * ejemplos 
    * DJango
    * Hibernate 
    * SQLalchemy 
* Escapar y valirdar entradas 
  * Asegurarse que no contengan caracteres peligrosos como `'` , `;` o `--`

## Otros tipos de inyeccio <a name="otrostipos"></a>

* Inyeccion de comandos 
* Inyeccion LADP
* Inyeccion XML 

> Evitar concatenaciones 
