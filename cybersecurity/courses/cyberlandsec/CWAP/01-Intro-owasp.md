# Introduccion al OWASP TOP 10 

## Topics 

* [## Que es OWASP top 10? ](#owasp10)
* [## Por que es importante? ](#importante)
* [## Como impacta ? ](#impacto)
* [## Desglozando ](#desglozando)

> OWASP : Open Web Application security project 

## Que es OWASP top 10? <a name="owasp10"></a>

Son las 10 principales vulnerabilidades de seguridad en aplicaciones web.

1. A01 Broken access control
2. A02 Cryptographic failures 
3. A03 Injection 
4. A04 Insecure design 
5. A05 Security misconfiguration 
6. A06 Vulnerable and outdated components 
7. A07 Identification and authentication failures
8. A08 Software and data integrity failure
9. A09 Security logging and monitoring failures 
10. A010 Server side request forgery 

## Por que es importante? <a name="importante"></a>

1. Conciencia y informacion 
2. Cumplimiento y estandares 
   * Como otros ( pci dss , iso 27001 ) 
3. Costos evitados 

## Como impacta ? <a name="impacto"></a>

* Desarrolladores: 
  * Se les ofrece una lista clara y priorizad de riesgos 
* Empresas 
  * Evitar la perdida de dinero, confianza de usuarios y socios 

## Desglozando <a name="desglozando"></a>

* Broken access control : OCurre cuando las aplicaciones permiten a los usuarios acceder a areas o datos que no deberian estar autorizados para ver o modificar 
* Cryptographic failures: Se refiere a la mala implementacion de criptografia que expone datos sensibles o permite ataques de suplantacion 
* Injecion: Permite a los atacantes inyectar codigo malicioso a traves de entradas inseguras 
