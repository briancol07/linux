# Learn Vim The smart way 

* [vim](https://victorhck.gitbook.io/aprende-vim/)


## De que trata ??

Siempre se necesita algo mas que :

* vimtutor
* :help 

Esta guia trata de cerrar esa brecha destacando solo las funcionalidades mas importantes para aprender las partes mas utiles de vim en el menor tiempo posible 


## Como leer y seguir esta guia? 

Para aprender a andar en bicileto no sirve solo con leer como andar, hay que montarse en una y practicar. 

* [guia-original](https://github.com/iggredible/Learn-Vim)
* [info-extra](https://victorhckinthefreeworld.com/tag/vim/)
* [blog](https://victorhckinthefreeworld.com/tag/vim/)

## Datos extra 

A la hora de buscar como utilizar :h <ctrl> + P podemosos hacer :h i\_ctrl + p , el prefijo i reprensenta inssertar 

## Overview

* [01-Ejecutando-Vim.md](./01-Ejecutando-Vim.md)
* [02-Buffers-Ventanas-Pestanias.md](./02-Buffers-Ventanas-Pestanias.md)
* [03-Buscar-Archivo.md](./03-Buscar-Archivo.md)
* [04-La-https://www.preethikasireddy.com/post/how-does-the-new-ethereum-workgramatica.md](./04-La-gramatica.md)
* [05-Moviendote-por-un-archivo.md](./05-Moviendote-por-un-archivo.md)
* [06-El-modo-insertar.md](./06-El-modo-insertar.md)
* [07-El-comando-del-punto.md](./07-El-comando-del-punto.md)
* [08-Registros.md](./08-Registros.md)
* [09-Macros.md](./09-Macros.md)
* [10-Deshacer.md](./10-Deshacer.md)
* [11-Modo-Visual.md](./11-Modo-Visual.md)
* [12-Buscar-Sustituir.md](./12-Buscar-Sustituir.md)
