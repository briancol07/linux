# Mecanismos de autenticacion

La autenticacion es el proceso de verificar que algo o  alguien es quien dice ser. En la actualidad existen varios metodos para llevar a cabo esta tarea, Entre ellos:

* Algo que el usuario sabe:
  * Contraseña
  * PIN
  * Patron 
* Algo que el usuario tiene:
  * Tarjetas de coordenadas 
  * Codigo de Token 
  * USB keys
* Algo que el usuario es:
  * Reconocimiento 
    * Facial
    * Dactilar
    * Iris
    * Voz

## Contraseñas 

Uno de los principales problemas de las personas que utlizan tecnoligia es el hecho de la cantidad de cuentas de usuario que deben administrar.

* Termina generando
  * Se termina repitiendo la misma clave en todos lados 
  * Se almacenan de forma erronea en lugar de facil acceso
  * Dificultad para recordarlas   

Se puede optar tambien por un gestor de Contraseñas

* Bitwarden
* keepassxc 

### complejidad de las Contraseñas

* Tener mas de 10 caracteres 
* Uilizar letras, numeros y caracteres especiales
* Utilizar mayusculas y minusculas 
* No utilizar datos personales como nombres,fechas etc 

## Filtraciones de datos 

Chequear si algunos de nuestros datos fue filtrado alguna vez [have I been pwned](https://haveibeenpwned.com/)

## Autenticacion de dos factores (2FA)

Si bine le uso de contraseña robusta y de los gertores de contraseñas mejoran notablemente la forma de perservar nuestros datos personales. Aun asi podemos seguir siendo victimas, por eso se creo la autenticacion de dos factores. Que agrega una capa mas de proteccion solicitando ademas del usuario y contraseña, un codigo de token que se envia al dueño de la cuenta para que lo ingrese cuando vaya a iniciar sesion.

* Opciones
  * Authy
  * Google authenticator
  * Latch 
  * Microsoft Authenticator 

