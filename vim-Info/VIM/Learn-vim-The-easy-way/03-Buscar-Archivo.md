# Buscar Archivos 

El objectivo de este capitulo es aprender como buscar rapidamente en Vim. 

## Abrir y editar archivos 

```vim 
:edit archivo.txt
```

Tambien se pueden usar las wildcard/comodines * significa all 

tambien se puede usar :edit para generar un nuevo netrw, 

```vim 

:edit . 
:edit test/unit/
```

## Buscando archivos con find 

Se pueden buscar archivos con el comando find. La sintaxis es muy parecida al comando edit. 

```vim 

:find package.json 
:find app/controllers/user_controller.rb
```

## Find y Path 

La diferencia entre find y edit es que find busca el archivo en la ruta path mientras que edit no 

Para comprobar cuales son las rutas :

```vim 
:set path? 
```
se puede aniadir un path para hacerlo mas facil, PWD hace referencia al directorio actual en el que se esta trabajando 

```vim 
:set path+=app/controllers/
:set path+=$PWD/**
```

## Buscando en archivos con grep 

Vim tiene 2 maneras de hacerlo de forma interna o externa

### Interno


```vim 
" Estructura 
:vim /patron archivo 
" Diminutivo de vimgrep
:vim 
:vimgrep
```

El patron es la estructura regex 
Despues de ejecutar este comando seras redirigido al primer resultado. El comando de busqueda de vim , usa la ventan quickfix, esto es una ventana anexa en la que se muestra todas las ocurrencias que vim ha encontrado 

Para ver todos los resultados ejecutarmos 

```vim 
:copen        " Abrir la ventana quickfix
:cclose       " Cerrar la ventana quickfix
:cnext        " Ir al siguiente error
:cprevious    " Ir al error anterior
:colder       " Ir a la lista de errores mas antigua
:cnewer       " Ir a la lista de errores mas nueva
```
### Externo 

Por defecto usa el comando grep de la terminal de comandos 

```vim
:grep 
" ejemplo
:grep -R "almuerzo" app/controllers/
``` 

## Navegando los archivos con Netrw 

netrw es el explorador de archivos propio de vim. Se necesitan 2 configuraciones en .vimrc

```vim
set nocp 
filetype plugin on 
```

```vim 
vim . 
vim src/client
vim app/controllers
```
Para iniciar netrw dentro de vim, podemos utilizar :edit, y darle un directorio como parametro directametne en lugar del nobre de un archivo 

```vim 
:edit . 
:edit src/client/
:edit app/controllers/
```
Existen otras maneras:

```vim
:Explore     "Inicia netrw en el archivo actual
:Sexplore    "No es broma. Inicia netrw en una pantalla dividida en la mitad superior
:Vexplore    "Inicia netrw en una pantalla dividida en la mitad izquierda
```
Comandos utiles 

```vim
%    "Crear un nuevo archivo
d    "Crear un nuevo directorio
R    "Renombra un archivo o directorio
D    "Elimina un archivo o directorio
```

falta ver vimplugin fzf


