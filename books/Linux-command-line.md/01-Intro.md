# Linux 

> *The shell*  : Is a program that take your commands from the keyboard and send them to the operating system to perfom ("terminal")
## Commands 

### Navigation

> Hierarchical directory structure
>
> File system tree 
>
> Case sensitive 
>
*  pwd  
   * Print working directory  : Is a Tree directory , it start from the root and continue to tthe leaves (the base is _root_ )


* cd
  * Change directory : you can move around , from root to the bottom
  * without atributes goes to root 
  * to reffer to home(ROOT) == $HOME or ~/ 
  * cd ..  jump to the node before 
> cd ./bin (./ in the mayority you can omit)

* ls
  * List directories : List the directories and files in the current directory, you can specify
  * ls -a  (List all) 
  * ls -ll (this show all the privilege of the seen files ( you can use ls -la to show all)
> ~  mean home or you can write $HOME 

----
### Exploring the System 

* file 
  * Determine <file> type 

* less
  * View file content 
  * now it seems useless but with the configuration files we can do a better job.

> Table of ls 

Option | Long Option | Description
-------|-------------|--------------
-a | --all | List all even hidden ones
-A | --almost-all | like -a but doesn`t list current dir 
-d | --directory | use this with -l to see detail about dir 
-F | --classify | this will append an indicator character to the end of each name 
-h | --human-readable | Display file size in human readable format
-l | | Display result in long format
-r | --reverse | Display the result in reverse order 
-S | | Sort result by file size 
-t |   | Sort by modification time 

> /bin contains the binaries (programs) , to boot and run 
 /boot contains the linux kernel , initial ram , disk image and the bootloader 
>
> /dev here contains devices nodes , are the list for the kernel of devices he understand 
>
> /etc  system wide configuration , shell scripts that start each system services at boot time 
> 
> /lib  contains library files use by the core system , dynamic link libraries 
> 
> /lost+found is for recovery when there is a problem 
 /media this contains the mount point for removale media such as usb then you hace /mnt that is older linux mount point also but manually
 there are alot more see page 47 linux command line. 
> 
* touch 
  * Create a new file ! 


* cat 
  * show in the terminal the content of the file , can`t show long files
  * Can read one o more files and copies them to standar output
  * Also you can join files together cat movie.mpeg.0* > movie.mpeg 
  * cat alone this waits until you enter something and end , to end this is crtl + d (EOF)
 

----

### Manipulating Files and Directories 

* cp 
  * Copy  (cp <namefile> <destination>) could be more than 1 
  * cp * <destination> (take all ) cp *.jpg (take only the files that has that format)
  * cp -r <directory> <destination> copy all directory 
  * cp -i No replacement of files 


* mv
  * Move : Can move or Rename , you can move more than one file 
  * mv <files> <destination> 
  * mv -b <files> 	to make a backup

* mkdir 
  * Make a directory : you can do more than one at the same time
  * mkdir <directoryNames>


* rm 
	* Remove : the file disapear from your computer
	* rm -f <file>       Remove all files without restriction 
	* rm -r <directory>  Remove all files and sub directories 

* ln 
	* Create hard and symbolic links 
---- 
	
### Working with commands 

1. An Executable program 
2. A command build into the shell itself 
3. A shell function 
4. An alias 

* type 
  * Indicate how a comand name is interpreted (could be one of the four) 
  * type <command> 

* which 
  * Diplay an Executable `s location 
  * Only works with Executable programs not builtins or aliases 

----

### Getting a Command`s Documentation 
 

* man 
  * Manual : It show all the information of the commands 
  * man <command>
  * you could use less to show less pages 
  * you can search in the manual  man <partnumber> <command/file>

Section|Contents
---|---
1|User commands
2|Programming interfaces for kernel system calls 
3|Programming interfaces to the C library 
4|Special files such as device nodes and drivers 
5|File formats 
6|Games and amusements such as screen savers 
7|Miscellaneous
8|System Administration commands 

* help 
  * Tell a summary of the command
  * <command> --help  option that display a description of the command (optional)

> Note on Notation  when square brakets appear in the description oa a command indicates optional items , a vertical bar indicates mutually exclusive items   

* apropos 
  * Displlay appropiate Commands 

* whatis
  * Display one line manual 

* info 
  * Display a program`s info entry
----

### Creating comman`s with alias 

> You can put more than one command on a line by separating each command with semicolon 

* alias
  * alias <name> = \' <string> \'
  * unalias <name> to get rid of 
  * alias alone show all the aliases that we have 

> for more read " The Bash Reference Manual" 

----

### Redirection 

> I/O Redirection (input / output) ,  
> pipelines  **important** 
>
> There ara 3 types of Standar  ( INPUT , OUTPUT , ERROR ) 
> 
> Standard Out (Stdout)---
Standard in (Stdin)---
Standard Error (Stderr)
>


Operator| Number (designator)  | Type  |  Example | Description 
-------|----------------------|-------|---------|----------------
\> |0 |Stdout|  ls -l > output.txt | create or overwrite with the data of the command
\>>| |Stdout|ls -l >> output.txt | append the data to the file 
\2>|2 |Stderr| ls -l /bin/usr 2> ls-error.txt | Redirrect the "error" flow to the file 
\2>&1| |Stderr+Stdout| ls -l /bin/usr > ls-output.txt 2>&1 | Both go to the same file
-| |Stderr + Stdout | ls -l /bin/usr &> ls-output.txt | do the same 
-| | Stderr + Stdout | 2>&1 > ls-output.txt | this redirect stderr to the screen 
-| | Disposing Output | ls -l /bin/usr 2> /dev/null | this throw to the bit bucket , it do nothing , supress error message  
\<| |stdin| cat < lazy.txt | this change the input , instead of board to the file 

> \> block.txt   create  a new empty file (truncate an existing file or create a new one) 
>
> bit bucket is an ancient Unix concept and because of its universality  **you know when someone send his comment to /dev/null** 
> 

> Pipelines : The capability of commands to read data from stdin  and send to stdout  
>
> Filters : Put severals commands together into a pipeline , take input, change it somehow and the output it 
>
> example ls /bin /usr/bin | sort | less this command combined list of all theexutable programs in /bin and /usr/bin put them in sorted order and viww the resulting list 
> 
* uniq 
  * Report or omit repeated lines 
  * We use with sort , by default removes any duplicates from the list 
  * if we want to see the duplicates uniq -d 

* wc
  * Print line,word and byte Counts 
  * Display the number of lines, bytes and words contain in the file 

* grep 
  * Print lines Matching a Pattern 
  * grep pattern [file..] 
  * -v print only those who don`t match and -i ignore case 

* head/tail 
  * Print first / last part of the files 
  * By default both print ten lines you can adjust with -n [number]
  * tail can monitor the file with -f  , to exit ctrl+c (Real Time)

* tee 
  * Read from stdin and output to stdout and files 
  * this allow the data to continue down the pipeline 
  * ls /usr/bin | tee ls.txt | grep zip 

---- 

### Seeing the world as the shell Sees it 

#### Expansion

> Each time we type a command and press enter, bash performs several substitutions . For example * . 
>
> echo is a built in command , that print the text arguments on stdout 
> 
> echo * --> shows all the dir 
> 

#### Pathname Expansion 

* echo D*
* echo *s
* echo [[:upper:]]*
* echo /usr/*/share  ---> this show all the path in between 
* echo .*  show hidden files and expansion for better performance echo .[!.]* 

#### Tilde Expansion 
* the tilde character ~ . This expand into the home directory  ex: echo ~ 

#### Aritmetic Expansion 

* echo $((2+2))
 
Operator | Description
---------|-----------
\+       | Addition 
\-       | Subtraction
\*       | Multiplication 
/        | Division ( Result are integer )
%        | Modulo (Remainder)
**       | Exponentiation 

#### Brace Expansion 

> we can create multiple text string from pattern containing braces 
> 
> ex : echo Front-{A,B,C}-Back 
>
> This has a preambre (leading portion) and a postscript (trailing portion) 

* echo {1..15} 
* echo {Z ..A} 
* mkdir {1990..2009}-{01..12} 

#### Parameter Expansion 

> to invoke and reveal the content of USER  --> echo $USER 
>
> To see the list of available variables ---> printenv | less 

#### Command Subtitution 
> to use the output of a command as an expansion 
>
>  echo $(ls) another ls -l $(wich cp) other form is with bakquotes "` ` "
>

#### Quoting 
> Quoting to selectively supress unwnated expansion 
>
> inside doble quotes , all special character lost their special meaning and are treated as ordinary character 



#### Escaping Characters 
> To eliminate the special character we use a backslash 
>
Escape Secuence| Meaning 
---------------|--------
\a| Bell
\b| Backspace
\n| New line 
\r| Carriage return 
\t| Tab 


> sleep 10 ; echo -e "time`s up \a" 
> 

### Advance Keyboard Tricks 
> Depend on the distro , please use vim !! 

* history 
  * store the last 500/1000 commands we have entered 
  * history | grep /usr/bin 
  * ! [number]  this will excute de number of the command

* clear 
  * clear screen 



###  Permissions 

* id 
  * Information about your entity 
  * uid  = user id  
  * gid  primary group id  , belong to aditional groups 

> Readin writing and Executing  ls -l file 
> Ten first characters of the list are the file attributes 

Attribute | File Type 
---------|-------------
\- | Regular file 
d | directory 
l | symbolic link 

> The remaining 9 are file mode , 3 Owner 3 Group 3 World  in this order 

Attribute | File | Directories 
---------|------|----------
r| Allow to open and read | can be listed if the execute attribute is also set 
w| Allow to write or truncate , eliminate files or rename  | Allow files to be created ,deleted, rename if execute is also set 
x| Allos to be treated as a program and execute  | Allos a directory to be entered 


* chmod 
  * change the file mode 
  * is in octal 
  * chmod 777 allow all 
  * can accept characters 


