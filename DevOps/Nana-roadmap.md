# Nana DevOps Roadmap 

* [Nana-Roadmap](https://www.youtube.com/watch?v=1J2YOV6LcwY)

> X as Code 

## Pre-Requisites 

1. Os & Linux fundamentals 
  * Shell commands 
  * Shell scripting 
  * File Permissions 
  * SSH key management 
  * Networking 
  * Virtualization 
2. Git 
  * HUB/LAB
  * Common Commands 
  * Branch Strategies 
  * Merging 
  * Resolving conflicts 
3. Build and package tools 
  * Install dependencies 
  * Execute tests 
  * Package application 

## Fundamentals 

1. Containarization 
  * Docker 
2. Artifact repository 
  * Registry 
3. Cloud Basics 

## Core 

1. Container orchestration 
  * Kubernetes 
  * cluster network 
  * Auto-scheduling 
2. Advanced cloud platform 
  * 1 cloud provider 
  * Networking services 
  * Compute services 
  * Storage services 
  * Access management 
3. Managed Kubernetes service 
  * EKS -> AWS
4. CI/CD
  * Continuous integration
  * Continuous Delivery

## Advanced 

1. Infraestructure as code 
2. Configuration Management 
  * Ansible 
  * Automation
3. Monitoring & Observability 
  * Prometheus 
4. Security
  * In Each step 

