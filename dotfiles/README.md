# Dotfiles 

To make it easy to go from one computer to another, to create new ones and have the same configurations in all of them 

```
shell/
  |- aliases.sh
  |- git-brach.sh
  |- ssh-agent.sh
  |- case-insensitive.sh
.vimrc
.tmux.conf 
install.sh
```
  
## Notes 

In bash a 0 is that the command execute correctly and different of zero no. 

When run scripts or any command, creating a copy of terminal and runing that program 

This will make the process run in the current terminal 

```
source aliases.sh
```

this means commands substitution 
```
$( ) 
```

## Links 

* [github](https://github.com/bartekspitza/dotfiles/tree/masterhttps://github.com/bartekspitza/dotfiles/tree/master)
* [YT](https://www.youtube.com/watch?v=mSXOYhfDFYo)
