# Boot Process Overview 

## Index 

* [## Process ](#process)
  * [### BIOS ](#BIOS)
    * [#### UEFI](##### UEFI)
  * [### Bootloader ](#bootloader)
  * [### Kernel ](#kernel)
    * [#### Initrd vs initramfs ](#initrd)
    * [#### Mounting the root filessystem ](#mounting)
  * [### Init ](#init)

## Process <a name="process"></a>

### BIOS <a name="BIOS"></a>

> Basic Input/ Output System

* Initializes the hardware and make sure with a Power-on Self test ( POST ) that all the hardware is good to go. 
* The main job of the BIOS is to load up the bootloader 

Is a firmware that comes most common in IBM PC compatible computers.

When the BIOS boot up the hard drive, it searches for the boot block to figure otu how to boot up the system. Depending on how you partiotion your disk. It will look to the MBR ( Master boot record ) or GPT. The MBR is located in the first sector of the hard drive, the first 512 bytes. Containes the code to load another program somewhere on the disk, this program in turn actually loads up our bootloader.


#### UEFI 

> Unified extensible firmware interface 
  
Instead of using the BIOS, is the successor. 

The GPT format was intended for use with EFI. The first sector of a GTP disk is reserved for a protective MBR, to make it possible to boot a BIOS based machine.

UEFI stores all the information about startup in an `.efi` file. This file is stored on a special partition called EFI system partitiosn on the hardware. Inside this partition it will contain the bootloader. 


### Bootloader <a name="bootloader"></a>

The Bootloader loads the kernel into memory and then starts the kernel with a set of kernel parameters. One of the most common bootloader is GRUB ( Universal linux standard ).

* Main Responsibilities are: 
  * Booting into an operating system, it can also be used to boot a non-linux operating system
  * Select a kernel to use 
  * Specify kernel parameters 

The most common bootloader for linux is GRUB, you are most likely using it on your system.

* Other 
  * LILO
  * efilinux 
  * Coreboot 
  * SYSLINUX 

To find the kernel we will need to look at our kernel parameters. THe parameters can be found by going into the GRUB menu on startup using the `e` key

* `initrd`
  * Specifies the location of initial RAM 
* `BOOT_IMAGE` 
  * This is where the kernel image is located 
* `root` 
  * The location of the root filesystem, the kernel searches inside this location to find init. It is often represented by it's UUID or the device name sucha as `/dev/sda1`
* `ro`
  * This parameter is pretty standard, it mounts the filesystem as read-only mode 
* `quiet` 
  * This is added so that you don't see displaymessages that are going on in the background during boot 
* `splash`
  * This lets the splash screen be shown 

### Kernel <a name="kernel"></a>

When the kernel is loaded, it immediately initializes devices and memory. The main job of the kernel is to load up the init process 

#### Initrd vs initramfs <a name="initrd"></a>

The kernel manages our systems hardware, howeber not all drivers are available to the kernel during bootup. So we depend on a temporary root filesystem that contains just essential modules that the kernel need to get the rest of the hardware. 
In older versions was initrd ( initial ram disk ). The kernel would mount it, get the necessary bootup drivers, then when it was done loading everything it needed, it would replace the initrd with the actual root filesystem.
These days we have initramfs, this is a temporary root filessytem that is built into the kernel itself to load all the necessary drivers for the real root filessytem.

#### Mounting the root filessystem <a name="mounting"></a>

Now the kernel has all the modules it needs to create a root device and mount the root partition. Before you go any further though, the root partition is actually mounted in read-only mode. First so that fsck can run safely and check for system integrity. 
Afterwards it remounts the root filesystem in read-write mode. Then the kernel locates the init program and executes it 

### Init <a name="init"></a>

The first process that gets started. Starts and stops essential service process on the system. 

* Three major implementations of init in linux 
  * system V init (sysv)
    * This is the traditional init system 
    * Starts and stops processes,based on startup scritps 
    * State of machine is denoted by runlevels 
  * Upstart 
    * In older ubuntu installations 
    * Jobs and events that works by starting jobs that perfmors certain actions in response of events 
  * Systemd
    * New standard for init 
    * You have a goal you want to achieve and stystemd tries to stisfy the goal's dependencies to complete the goal 
