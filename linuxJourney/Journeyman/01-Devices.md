# Devices 

## Index 

* [## /dev directory ](#dev)
* [## Device Types ](#types)
* [## Device Names ](#names)
  * [### SCSI ](#scsi)
* [## Sysfs ](#sysfs)
* [## Udev ](#udev)
* [## Isusb, Ispci, Issci ](#isusb)
* [## dd ](#dd)

## /dev directory <a name="dev"></a>

You can interact with device drivers through device file or device nodes, these are special files that look like regular files.

``` bash 

ls /dev
ls -l /dev
```

Some of these devices you've already used and interacted with such as /dev/null. Here nothing gets returned 

## Device Types <a name="types"></a>

With the second command you will get:

1. Permissions
2. Owner 
3. Group 
4. Major Device Number
5. Minor Device Number
6. Timestamp 
7. Device Name 

Remember in the ls command you can see the type of  file with the first bit on each line. Device files are denoted as the following

* c --> Character
* B --> Block
* p --> Pipe
* s --> Socket

## Device Names <a name="names"></a>

### SCSI <a name="scsi"></a>

> pronounced "scuzzy" protocol and stands for small computer system Interface. 

Allows communications between disks, printers, scanners and aother peripherals to your system. 

* Common devices :
  * /dev/sda --> First hard disk
  * /dev/sdb --> Second hard disk
  * /dev/sda3 --> Third partition on the hard disk 
* Pseudo Devices 
  * Aren't really physically connected to your system
  * /dev/zero 
    * Accepts and discards all input, produces a continuous stream of NULL (zero value) bytes
  * /dev/null
    * Accepts and discards all input, produces no output
  * /dev/random 
    * Produces random numbers 
* PATA Devices
  * Older system , you may see hard drives being referred with hd prefix
  * /dev/hda --> First hard disk
  * /dev/hdd2 --> Second partition on 4th hard disk
  

## Sysfs <a name="sysfs"></a>

> Virtual filesystem 

most often mounted to the /sys directory, is used to view information and manage devices.
Contains all the information for all devices on your system.

* Information
  * Manufacturer 
  * Model 
  * Where the device is plugged in 
  * State 
  * Hierarchy of devices 
  * More 

``` bash 

ls /sys/block/sda

``` 

## Udev <a name="udev"></a>

This command will make a device node /dev/sdb1 and it will make it a block device (b) with a major number of 8 and a mino number of 3

```bash 
mknod /dev/sdb1 b 8 3 
```
To remove a device you would simply rm the device file in the /dev directory.
The udev system dynamically creates and removes device files for us depending on whther or not they are connnected.
You can also view the udev database and sysfs using:

```bash 
udevad info --query=all --name=/dev/sda 
```
## Isusb, Ispci, Issci <a name="isusb"></a>

> List infromation 

* USB Devices --> lsusb
* PCI Devices --> lspci 
* SCSI Devices --> lsscsi 

## dd <a name="dd"></a>

The dd tool is super useful for converting and copying data. it reads input from a file or data stream and writes it to a file or data stream 

```bash 
dd if=/home/brian/backup.img of=/dev/sdb bs=1024
```

This command is copying the contents of bakcip.img to /dev/sdb. it will copy data in blocks of 1024 bytes until there is no more data to be copied 

* if=file
  * Input file, read from a file instead of standard input
* of=file 
  * Output file, write to a file instead of standard output
* bs=bytes 
  * Block size --> for 1m is 1 megabyte , 1k is 1 kilobyte 

```bash 

dd if=/home/pete/backup.img of=/dev/sdb bs=1M count=2
```
this will copy over 1M 2 times.

dd is extremely powerful, you can use it to make backup of anything including whole disk drives, restoring disks images, and more.

