# Dehacer 

## Deshacer, Rehacer y Deshacer 

* `u`
* `:undo`
* Deshacer todos los cambios 
  * `U` 

> Vim deshace un solo "cambio" cada vez, similar al cambio del comando del punto ( Pero a diferencia del comando del punto ) 

* Rehacer 
  * `Ctrl + R`
  * `:redo`

Vim estables un maximo de cuantas veces puede dehacer en la variable `undoLevels`

```
:echo &undolevels
:set undolevels=1000
```

## Dividiendo bloques 

He mencionado anteriormente que u deshace unico cambio, de manera similaral cambio del comando del punto . Cualquier texto introducido entre entera en el modo insert ya salir de lmodo es tomado encuenta como un cambio. 

Afortunadamente puede dividir los bloques de deshacer cuando estas escribiendo en el modo insertar presionas `ctrl-G u` para crear un punto de ruptura del comando deshacer.

Tambien es util el crear puntos de interrupcion de deshacer cuando se borran fragmentos en el modo insertar con las combinaciones de teclas `ctrl-W` ( Elimina la palabra anterior al cursor o `ctrl-U` ( elimina todo el texto anterior al cursor. 

``` 
inoremap <c-u> <c-g>u<c-u>
inoremap <c-w> <c-g>u<c-w>
```

## Arbol de cambios del comando deshacer 

Vim almacena cada cambio que se ha escrito en un arbol del comando deshacer.
Con la teclas `g+` podes recuperar del arbol 


Vim cada vez que deshacemos un cambio almacena el estado previo del camoi creando una rama de deshacer. 

Para recorrer el arbol se puede hacer usando `g+` y `g-` para ir a una estado anterior 

* u , Ctrl-R 
  * Recorren los nodos principales en el arbol de deshacer 
* g+ , g- recorren todos los nodos del arbol de cambiuo dle comando deshacer 

## Modo Persistente en deshacer 

Vim puede preservar un historial de acciones de deshacer en un archivo mediante el comando `:wundo`

```
set undodir=~/.vim/undo_dir
set undofile
```

## Viajar en el tiempo

Vim puede viajar al estado de un texto en el pasado mediante `:earlier`

```
:earlier 10s
```
* Remplazando la S
  * m ( minutos )
  * h ( Horas ) 
  * d ( Dias ) 
  * sin nada volvera a 10 cambios anteriores 

```
:later 
```

Lo mismo pero con later , a un estado posterior 



