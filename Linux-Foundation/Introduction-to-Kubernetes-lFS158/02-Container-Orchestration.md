# Container Orchestration

## Index 

* [## What are containers? ](#containers)
* [## What is container orchestration ? ](#whatis)
* [## Container Orchestrators ](#orchestrators)
* [## Why use Container Orchestrators? ](#why)
* [## Where to deploy Containers Orchestrators? ](#wheredeploy)

## What are containers? <a name="containers"></a>

Containers are an application-centric method to deliver high performing, scalable applications on any infraestructure of your choice. 
Best suited to deliver microservices by providing portable, isolated virtual environments for applications to ru nwithout interference from other running applications 

![container](./img/container.png)

## What is container orchestration ? <a name="whatis"></a>

Running containers on a single host for dev and testing of applications may be a suitebale option. However, when migrating to QA and Prod environments, that is no longer a viable option because the applications and services need to meet specific requirements 

* Fault-tolerance 
* On-demand scalability 
* Optimal resource usage 
* Auto-discovery to automatically discover and communicate with each other 
* Accessiblity from the outside world 
* Seamless updates / rollbacks without any downtime 

Containser orchestrators are tools which group systems together to form clusters where container's deployment and management is automated at scale while meeting the requirements mentioned above. 

The clustered systems confer the advantages of distributed systems such as increased performance, cost efficiency reliability, workloads distribution and reduced latency.

## Container Orchestrators <a name="orchestrators"></a>

* Some cloud orchestration tools 
  * Amazon elastic container service ( ECS )
  * Azure Container instances 
  * Azure Service fabric
  * Kubernets 
    * Part of cloud native computing foundation (CNCF)
  * Nomad 
  * Docker Swarm 


## Why use Container Orchestrators? <a name="why"></a>

When it comes to managing hundreds or thousands of containers running on a global infrastrucure 

* Most orchestrators can:
  * Group host together while creating a cluster, in order to leverage the benefits of distributed systems 
  * Schedule containers to ru on host in the cluster based on resources availability 
  * Enable containers in cluster to communicate with each other regardless of the host they are deployed to in the cluster 
  * Bind containers and storage resources 
  * Group sets of similar containers and bind them to load-balancing constructs to simplify access to containerized applications by creating an interface, a level of abstraction between the containers and the client.
  * Manage and optimize resource usage 
  * Allow for implementation of policies to secure access to applications running inside containers 

## Where to deploy Containers Orchestrators? <a name="wheredeploy"></a>

Most container orchestrators can be deployed on the infrastructure of our choice - on bare metal, VM, on-premises, on public and hybrid clouds. 
Kubernetes for example, can ve deployed on a workstation with or without an isolation layer such as local hypervisor or container runtime.

There are turnkey cloud solutions which allow production kubernetes clusters to be installed, with only a few commands, on top of cloud IaaS known as KaaS ( Kubernetes as a service ) 

* Amazon elastic kubernetes service (EKS)
* Azure kubernetes Service (AKS)
* DigitalOcean Kubernetes 
* Google kubernetes engine (GKE)
* IBM Cloud Kubernetes service 
* Oracle Container engine for Kubernetes 
