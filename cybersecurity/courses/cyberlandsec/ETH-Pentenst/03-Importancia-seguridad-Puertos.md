# Importancia de la seguridad en puertos y protocolo 

## Index

* [## Rol de los puertos y protocolos en la seguridad de redes ](#rol)
* [## Amenazas comunes Relacionadas con Puertos Y protocolos ](#amenazas)
* [## Impacto de la falla de seguridad en Puertos y Protocolos ](#impacto)
* [## Mejores practicas para la seguridad de puertos y protocolos ](#best-practices)
* [## Ejemplos ](#ejemplos)

## Rol de los puertos y protocolos en la seguridad de redes <a name="rol"></a>

Los puertos y protocolos juegan un papel fundamental en la comunicacion dentro de una red. Tambien representan puntos de ataque.

## Amenazas comunes Relacionadas con Puertos Y protocolos <a name="amenazas"></a>

* Puertos Abiertos y no utilizados
* Protocolos inseguros 
  * Telnet o FTP 
* Ataques de Fuerza bruta 
  * SSH o RDP
* Explotacion de vulnerabilidades conocidas 
* Escaneo de puertos 

## Impacto de la falla de seguridad en Puertos y Protocolos <a name="impacto"></a>

* Acceso no autorizado
* Intercepcion de datos
* Compromiso completo del sistema 

## Mejores practicas para la seguridad de puertos y protocolos <a name="best-practices"></a>

* Cierre de Puertos Innecesarios 
* Uso de protocolos Seguros 
* Implementacion de firewalls 
* Aplicacion de parches y actualizaciones 
* Monitoreo y auditoria 

## Ejemplos <a name="ejemplos"></a>

* Configuracion de SSH, mover el puerto ssh a uno no estandar para reducir exposicion
* Seguridad HTTP/HTTPS 
  * Configuraciones seguras para certificados TLS/SSL y asegurar que sean validos y actualizados 
* Proteccion SMB 
  * Restrinja el acceso a puertos SMB 445 solo a segmentos de red internos donde sea necesario, deshabilite SMBv1 y utilize la ultima version (SMBv3)


