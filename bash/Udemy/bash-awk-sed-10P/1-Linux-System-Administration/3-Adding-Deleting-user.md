# Adding and deleting a user 

## Commands 

1. Add user
  * adduser userName
2. Change password 
  * passwd userName
3. Delete user
  * userdel userName

## Extra

Only root user can do this change but with **su**(switch user) you can switch to root

> normal user have $ root # 
