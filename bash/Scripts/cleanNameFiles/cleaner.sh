#!/bin/bash 

delete_spaces(){
  #echo $1 
  echo $1 | sed 's/ /-/g'
}

lista_archivos=( $(ls -xm --width=10000) )
echo ${lista_archivos[*]}

for i in ${lista_archivos[*]}; do 
  echo $i
  delete_spaces $i
done
