# EWPT but with Pareto rule 

1. Fundamentos de Pentesting Web (20%):
    * Profundizar en conceptos básicos como inyecciones SQL, XSS, CSRF, etc., pero desde una perspectiva de pentesting.
    * Recursos: Cursos en línea sobre pentesting web en plataformas como Udemy, TryHackMe, y la documentación de OWASP.
2. Herramientas de Pentesting Web (20%):
    * Dominar el uso de herramientas como Burp Suite, SQLMap, OWASP ZAP, etc., específicamente para pentesting web.
    * Recursos: Tutoriales en línea, documentación oficial de las herramientas y práctica en laboratorios virtuales.
3. Metodologías de Pentesting (20%):
    * Familiarizarse con metodologías de pentesting web, como la metodología OSSTMM o la metodología OWASP Testing Guide.
    * Recursos: Documentación oficial de las metodologías, cursos en línea y libros especializados en pentesting web.
4. Explotación Avanzada (20%):
    * Practicar la explotación de vulnerabilidades más avanzadas como deserialización no segura, inyecciones LDAP, etc.
    * Recursos: Plataformas de laboratorios virtuales especializados en pentesting web avanzado, como PentesterLab y Hack The Box.
5. Preparación para el Examen (20%):
    * Realizar exámenes de práctica similares al eWPT para familiarizarse con el formato y el tipo de preguntas.
    * Recursos: Exámenes de práctica disponibles en línea, foros de discusión para compartir experiencias y consejos sobre el examen.

