# Gestion de vulnerabilidades 

Una vulnerabilidad es una debilidad o fallo en un sistema de informacion que pone en riesgo la seguridad de la informacion que permite que un atacante pueda comprometer la integridad, disponibilidad o confidencialidad de esta. 

* Sistemas o aplicaciones actualizados 

## Escaneres de vulnerabilidades 

Los sistemas de escaneo de vulnerabilidades ayudan a una organizacion a identificar y remediar las vulnerabilidades dentro de su ambiente de tecnologia antes de que los ciberdelicuentes/ atacantes obtengan acceso a fodificar o destruir informacion confidencial 

* Nessus 
* GFI
* Languard
* Rapid7
* OpenVas
* Etc 

## Ambiente de pruebas 

* Ambiente de Desarollo 
  * Donde se crea
* Ambiente de Prueba
  * donde se verifica lo que se creo 
* Ambiente de produccion
  * Donde funciona lo que se creo 

1. Los equipos que incluiremos dentro del ambiente deben ser representativos del total
2. Los equipos que incluiremos deben tener las mismas caracteristicas: mismo SO, aplicaciones instaladas y configuraciones
3. No deberian utilizarse dispositivos fuera de servicios o desactualizados ya que no son representativos de los que estan en produccion
4. Todos los usuarios cuyos dispositivos formen parte de este ambiente de pruebas, seran informados para poder recibir feedback.

## Actualizaciones 

* Consideraciones 
  1. Instalar actualizaciones gradualmente en los equipos, todos juntos ni el mismo dia.
  2. Realizar la tarea fuera del horario laboral 
  3. No permitir que los equipos se actualicen por si solos 
  4. Centralizar las actualizaciones desde un equipo al resto (WSUS)
  5. Descargar actualizaciones desde repositorios oficiales 



