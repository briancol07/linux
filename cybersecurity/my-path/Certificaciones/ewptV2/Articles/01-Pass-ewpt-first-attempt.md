# Pass EWPT first attempt 

* [Article](https://grumpz.net/pass-the-ewpt-exam-in-2023-using-free-resources-on-your-first-attempt)

* Show you can access 
  * Restricted Admin area of the web application 
  * Audit every asset owned by the cleint and report every vulnerability that you found, including informational findings that may be valuable to the client 

> You have to submit a good report , focus on the report quality 

## Resources 

### Cross Scripting Resources 

* Reflected XSS  
  * https://portswigger.net/web-security/cross-site-scripting/reflected
  * https://brightsec.com/blog/cross-site-scripting-xss/
  * https://www.youtube.com/watch?v=k4lUX55uNM0
  * https://public-firing-range.appspot.com/reflected/index.html
* Stored/Persistent XSS
  * https://portswigger.net/web-security/cross-site-scripting/stored
  * https://www.thesslstore.com/blog/the-ultimate-guide-to-stored-xss-attacks/
  * https://www.exploit-db.com/docs/english/18895-complete-cross-site-scripting-walkthrough.pdf
  * https://www.cobalt.io/blog/a-pentesters-guide-to-cross-site-scripting-xss
  * https://portswigger.net/web-security/cross-site-scripting/reflected/lab-html-context-nothing-encoded (lab!)
  * https://portswigger.net/web-security/cross-site-scripting/stored/lab-html-context-nothing-encoded

### SQL Injection Resources

> Remember.. SQLmap is your best friend when exploiting these vulnerabilities. The -r switch goes a long way! ;-)
* SQLi
  * https://github.com/sqlmapproject/sqlmap
  * https://dl.packetstormsecurity.net/papers/cheatsheets/sqlmap-cheatsheet-1.0-SDB.pdf
  * https://portswigger.net/web-security/sql-injection
  * https://hippidikki.wordpress.com/2018/07/12/using-sqlmap-on-a-soap-request/
* Boolean Blind SQL Injection
  * https://www.hackingarticles.in/beginner-guide-sql-injection-boolean-based-part-2/
  * https://www.hackingloops.com/boolean-exploitation-technique-to/
  * https://www.youtube.com/watch?v=MfDo_ssS4PY
  * https://null-byte.wonderhowto.com/forum/explotation-blind-boolean-based-sql-injection-by-mohamed-ahmed-0179938/
  * https://portswigger.net/web-security/sql-injection/blind/lab-conditional-responses (lab!)
  * https://portswigger.net/web-security/sql-injection/blind/lab-conditional-errors (lab!)
  * https://portswigger.net/web-security/sql-injection/blind/lab-time-delays (lab!)
  * https://portswigger.net/web-security/sql-injection/blind/lab-time-delays-info-retrieval (lab!)
* Error-Based SQL Injection
  * https://medium.com/@hninja049/example-of-a-error-based-sql-injection-dce72530271c
  * https://gbhackers.com/manual-sql-injection/
  * https://rstudio-pubs-static.s3.amazonaws.com/117265_97cc9bec3f4a4952b37369ade413e435.html
  * https://akimbocore.com/article/sql-injection-exploitation-error-based/
* Time-Based SQL Injection
  * https://beaglesecurity.com/blog/vulnerability/time-based-blind-sql-injection.html
  * https://www.youtube.com/watch?v=xHzH00vyVHA
  * http://www.securityidiots.com/Web-Pentest/SQL-Injection/time-based-blind-injection.html

### File Upload Exploitation

> I highly suggest you become a guru here. 

* https://portswigger.net/web-security/file-upload
* https://www.prplbx.com/resources/blog/exploiting-file-upload-vulnerabilities/
* https://www.youtube.com/watch?v=rPdn88pO7x0
* https://www.youtube.com/watch?v=b6R_DRT5CqQ
* https://portswigger.net/web-security/file-upload/lab-file-upload-remote-code-execution-via-web-shell-upload (lab!)
* https://portswigger.net/web-security/file-upload/lab-file-upload-web-shell-upload-via-content-type-restriction-bypass (lab!)

### Session Hijacking

    https://www.thesslstore.com/blog/the-ultimate-guide-to-session-hijacking-aka-cookie-hijacking/
    https://www.youtube.com/watch?v=z6nUbsY5B-w
    https://www.youtube.com/watch?v=T1QEs3mdJoc
    https://resources.infosecinstitute.com/topic/session-hijacking-cheat-sheet/

### Enumeration

* Subdomain Brute Forcing
  * https://infinitelogins.com/2020/09/02/bruteforcing-subdomains-wfuzz/
  * https://sidxparab.gitbook.io/subdomain-enumeration-guide/active-enumeration/dns-bruteforcing
  * https://pentester.land/blog/subdomains-enumeration-cheatsheet/
  * https://0xffsec.com/handbook/information-gathering/subdomain-enumeration/#google-dorking

### Directory Busting

* https://medium.com/@nynan/bug-bounty-recon-content-discovery-efficiency-pays-2ec2462532b1
* https://www.hackerone.com/ethical-hacker/how-recon-and-content-discovery
* https://www.youtube.com/watch?v=p4JgIu1mceI
* https://www.hackingarticles.in/5-ways-directory-bruteforcing-web-server/
* https://sushant747.gitbooks.io/total-oscp-guide/content/web-scanning.html

### Low/Informational Vulnerabilities

* https://www.imperva.com/learn/application-security/clickjacking/
* https://www.geeksforgeeks.org/session-fixation-attack/

Missing Cookie Attributes (use Nikto! https://cirt.net/Nikto2)

No Rate Limiting? https://gaya3-r.medium.com/no-rate-limiting-on-form-registration-login-email-triggering-sms-triggering-5961b64a91cb

## Report Template

* https://github.com/hmaverickadams/TCM-Security-Sample-Pentest-Report

