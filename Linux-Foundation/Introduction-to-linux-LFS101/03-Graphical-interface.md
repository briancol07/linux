# Graphical Interface 

## Graphical Desktop <a name="desktop"></a>

You can use Command line interface (CLI) or Graphical user interface (GUI) when using linux. CLI often more efficient, while the GUI is easier to navigate.

## X window system <a name="xwindows"></a>

Loading the graphical desktop is one of the final steps in the boot process of a linux desktop. Historically this was known as X windows system .

A service called the display manager keeps track of the displays being provided and loads the X server. The display manager also handles graphical logins and starts the appropriate desktop environment after a user logs in.

A newer system, known as Wayland is gradually supersending it and is the default display system for fedora, RHEL and others 

## More about the Graphical desktop <a name="more"></a>

Desktop environment consists of a session manager, which starts and maintains the components of the graphical session, and the window manager which controls the placement and movement of windows, windows title-bars and controls. 

Although these can be mixed, generally a set of utilities, session manager and window manager are used together as a unit, and together provide a seamless desktop environment.

If the display manager is not started by default in the default runlevel, you can start the graphical desktop different way, after logging on to a text-mode consoleby `startx` or start the display manager ( gdm, kdm xdm ) 

## GUI startup <a name="guia"></a>

When you install a desktop environment, the display manager starts at the end of the boot process. It is responsible for starting the graphics system, logging in the user and starting the user's desktop environment. You can often select from a choice of desktop enviromnets when logging in to the system

* GNOME : gdm
* KDE : kdm

## GNOME desktop enviroment <a name="gnome"></a>

Is a popular desktop environment with an easy-to-use graphical user interface. 
However, the look and feel can be quite different across distributions.

## Graphical desktop Background <a name="background"></a>

you can change the default by choosing a new wallpaper or selecting a custom picture to be set as the desktop background. Also you can change the desktop theme which changes the look and feel of the linux system. 

## Gnome-tweaks <a name="tweaks"></a>

Standard utility gnome-tweaks, which exposes many more setting options. It also permits you to easily install extensions by external parties. Not all linux distributions install this tool by default .

* [GNOME-wiki](https://wiki.gnome.org/Personalization)

----

## Session Management <a name="session"></a>

## Locking the screen <a name="lock-screen"></a>

* Using GUI 
  * Clicking in the upper-right corner of the desktop and then clicking on the lock icon
* Using Keyboard 
  * SUPER - L ( or super - escape )
  * SUPER is the windows key 

## Switching User <a name="switch"></a>

Allows more than one user to be simultaneously logged in. For each person must have their own user account and password. This allow for individualized settings, home directories, and other files and protects agains both accidental and malicious corruption.

## Suspending <a name="suspending"></a>

All modern computer support ( Suspend or sleep) mode, when you want to stop using your computer for a while, Suspend mode saves the current system state and allows you to resume your sesison more quickly while remaining on. 

---- 

## File manager <a name="file"></a>

Each distribution implements the Nautilus ( File manager ) utility which is used to navigate the file system. It can locate files and when a file is clicked upon, either it will run if it is a program, or an associated application will be launched using the file as data. 

You can switch between the icons and lsit formats, either by clicking the familiar icon or you can press `CTRL-1 or CTRL-2`. For showing hiddens you can press `CTRL-H` 

* Searching files: `CTRL-f`
* Access a specific directory `CTRL-L`
* Delete file : `CTRL-DELETE`
  * Nautilus send to `.local/share/Tras/files`

Default GNOME text editor is gedit.



