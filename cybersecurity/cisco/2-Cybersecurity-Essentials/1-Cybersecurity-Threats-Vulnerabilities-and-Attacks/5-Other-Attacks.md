# Other attacks 

Attacks carried out through web applications are becoming increasingly common .

## Cross-Site Scripting 

Cross-Site scripting (XSS) is a common vulnerability found in many web applications:

1. Cybercriminals exploit the XSS vulnerability by injecting scripts containing malicious code into a web page 
2. The web page is accessed by the victim, and the malicious scripts unknowingly pass to their broweser
3. The malicious script can access any cookie, session tokens or other sensitive information about the user, which is sent back to the cybercriminal.
4. Armed with this information, the cybercriminal can impersonate the user.

``` 
┌────────┐ 1             2 ┌──────┐
│Attacker│ --------------->│Victim│
└────────┘                 └──────┘
      \                      /
       \4                 3 /
        \     ┌────────┐   /
         \--->│Web Site│<-/
              └────────┘
```

## Code Injection 

MOst modern websites uses a database, such as SQL or XML database, to store and manage data. Injection attacks seek to exploit weknesses in these databases 

### XML Injection attack 

An XML injection attack can corrupt the data on the XML database and threaten the security of the website. 
It works by interfering with an application's processing of XML data or query entered by a user. 
A cybercriminal can manipulate this query by programming it to suit their needs. This will grant them access to all of the sensitive information stored on the database and allows them to amke any number of changes to the website. 

### SQL Injection Attack

By inserting a malicious SQL Stamtement in an entry field. this attack takes advantage of a vulnerability in which the application does not correctly filter the data entered by a user for characters in an SQL statement. 
As a result, the cybercriminal can gain unauthorized access to information stored on the database, from which they can spoof an identity, modify existing data, destroy data or even become an adnministrator of the database server itself.

### DLL Injection Attack

A dynamic link library file is a library that contains a set of code and data for carrying out a particular activity in windows. Applications use this type of file to add functionality that is not built-in, when they need to carry out this activity. 
DLL injection allows a cybercriminal to trick an application into calling a malicious DLL file, which executes as part of the targe process.

### LDAP Injection Attack

The lightweight Directory Access Protocol (LDAP) is an open protocol for authenticating user access to directory services.
An LDAP injection attack exploits input validation vulnerabilities by injecting and executing queries to LDAP servers, giving cybercriminal an opportunity to extract sensitive information from an organization's LDAP directory. 

## Buffer Overflow 

Buffer are memory areas allocated to an application. A buffer overflow occurs when data is written beyond the limit of a buffer. By changing data beyond the boundaries of a buffer, the application can access memory allocated to other processes. This can lead to a system crash or data compromise, or provide escalation of privileges.
These memory flaws can also give attackers complete control over a target's device. For example can change the instructions of a vulnerable application while the program is loading in memory and , as a result, can install malware and access the internal network from the infected device.

## Remote Code Executions 

Remote code execution allows a cybercriminal to take advantage of application vulnerabilities to execute any command with the privileges of the user running the applicaiton on the target device. 
Privilege escalation exploit a bug, design flaw or misconfiguration in an operating system or SW application to gain access to resource that are normally restricted. 

### Metasploit 

The metasploit Project is a computer security project that provides information about security vulnerabilities and aids in pentreation testing. Among the tools they have developed is the metasplit framework, which can be used for developing and executing exploit code against a remote target. 
Meterpreter, in particular, is a payload within Metasploit that allows user to take control of a target's device by writing their own extensions and uploading these files into a running process on the device. These files are loaded and executed from memor, so they never involve the hard drive. this means that such files fly under the radar of antivirus detection.
Meterpreter also has a module for controlling a remote system's webcam. Once Meterpreter is installed on a target device, the Metasploit user can view and capture images from the target's webcam.

## Other Application Attacks 

### Cross-site request forgery (CSRF)

CSRF decribes the malicious exploit of a website where unauthorized commands are submitted from a user's browser to a trusted web application. 
A malicious website can transmit such commands through specially-crafted image tags, hiden forms or JS requests- all of which cna work whithout the user's knowledge 

### Race condition attack 

Also known as a time of check (TOC) or a time of use (TOU) attack, a race condition attack happens when a computing system that is desinged to handle tasks in a specific sequence is forced to perform two or more operations simultaneously . 

### Improper input handling attack 

Data inputted by a user that is not properly validated can affect the data flow of a program and cause critical vulnerabilities in systems and applications that result in buffer overflow or SQL injectins attacks. 

### Error handling attack 

Attackers can use error messages to extract specific information such as the hostnames of internal systems and directories or files that exist on a given web server as well as databases, table and fiel names that can be used to craft SQL Injections attacks .

### Application programming interface (API) attack 

An API delivers a user response to a system and sends the systems's response back to the user. An API attack occurs when a cybercriminal abuses API endpoint.

### Reply attack 

This describes a situation where a valid data transmission is maliciously or fraudulently repeated or delayed by an attacker, who intercepts, amends and resubmits the data to get the receiver to do whatever they want. 

### Directory traversal attack 

Directory traversal occurs when a attacker is able to read files on the webserver outside of the directory of the website. An attacker can then use this information to download server configuration files containing sensitive informaiton, potentially expose more server vulnerabilities or even take control of the server.

### Resource exhaustation attacks 

These attacks are computer security explotis that crash hang or otherwise interfere with a targeted program or system. Rather than overwhelming network bandwidth like a DoS attack, resource exhausting attacks overwhelm the HW resources available on the target's server instead.

## Defending Against Application Attacks 

These are several actions that you can take to defend against an application attack :

* The first line of defense against an application attack is to write solid code 
* Prudent programming practice involves treating and validating all input from outside of a function as if it is hostile 
* Keep all SW, including operating systems and applications, up to date and do not ignore update prompts. Remember that not all programs update automatically. 

## Spam 

Also known as junk mail, is simply unsolicited email. In most cases, it is a method of advertising. However, a lot of spam is sent in bulk by computers infected by viruses or worms and often contains malicious links, malware or deceptive content that aims to trick recipient into disclosing sensitive information, such as social security number or bank account information. 

* The email has no subject line 
* The email asks you to update your account details 
* The email text contains misspelled words or strange punctuation
* Links within the email are long and/or cryptic
* The email looks like correspondence from a legitimate business, but there are tiny differences or it contains inforamtion that does not seem relevan to you. 
* The email asks you to open an attachment, often ugently. 

## Phishing 

Phising is aform of fraudulent activity often used to steal personal information 

Phishing occurs when a user is contacted by email or instant message or in any other way by someone masquerading as a legitimate person or organization. The intent is to trick the recipient into installing malware on their device or into sharing personal information, such as login credential or financial information.

### Spear phishing 

A Highly targeted attack, spear phising send customized emails to a speciific person based on information the attacker knows about them, which could be their interests, preferences, activities and work projects. 

## Vishing , Pharming and Whaling 

Criminals make use of a wide range of techniques to try to gain access to your personal information 

### Vishing 

Often referred to as voice phishing, this type of attack sees criminals use voice communication technology to encourage users todivulge information, such as their credit card details. 
Criminals can spoof phone calls using voice over internet protocol (VoIP), or leave recorded messages to give the impression that they are legitimate callers. 

### Pharming 

This type of attack deliberately misdirects users to a fake version of an official website. Tricked into believing that they are connected to a legitimate site, users enter their credentials into the faudulent website.

### Whaling 

Whaling is a phishing attack that target high profile individuals, such as senior executives within an organization, politicians and celebrities. 

## Defending Agains Email and Browser Attacks 

* It is difficult to stop spam, but there are ways to reduce its effects 
  * Most internet service providers (ISPs) filter spam before it reaches the user's inbox
  * Many antivirus and email SW programs automatically detect and remove dangerous spam from an email inbox 
  * Organizations should educate employees about the dangers of unsolicited emails and make them aware of the dangers of openingattachments
  * Never assume that email attachments are safe, even when they come from a trusted contact 
* Become a member of the Anti-Phishing working Group (APWG). It is an international association of companies focused on eliminating ifentity theft and fraud resulting from phishing and email spoofing 
* All SW should be kept up-to-date, with the latest security patches applied to protect against any known security vulnerabilities. 

## There's More .... 

### Physical Attacks 

Physical attacks are intentional, offensive actions used to destroy, expose, alter, disable, steal or gain unauthorized access to an organization's infratructure or HW.

* Loading malware onto a usb Flash drive that infects a device when plugged in. 
* Fitting cables and plugs such as generic USB cables, mobile deviece charging cables and wall or power adapters with advanced technologies, such as wireless chip to allow an attacker to control or provide instructions to a device. 
* Copying or skimming data from a credit or debit card using a specialized terminal to create a cloned card, which can be used to gain unauthorized access to the victim's accounts.

### Adversarial artificial intelligence attacks 

Machine learning is a method of automation that allows device to carry out analysis and perform tasks without specifically being programmed to do so. It powers many of hte applications we use today, such as web searching, photo tagging, spam detection, video survillance, fraud detection and security automation. 

### Supply chain attacks 

Many organizations interface with a third party for their systems management or to purchase components and SW. Organizations may even rely on parts or components from foreign source 

Attacker often find ways to intercerpt these supply chains.

### Cloud-Based attacks 

Rather than developing systems on their own premises, more and more organizations are making the move towards cloud-based computing, as we discussed earlier in this module. 

The advantage is that the cloud provider will maintain the equipment but this also opens up an organization to a host of potential threats. Attacker are constantly leveraging ways to exploit sensitive data stored on the cloud, as well as applications, platforms and infrastructure that is cloud-based, as we saw with SaaS , PaaS and IaaS .
