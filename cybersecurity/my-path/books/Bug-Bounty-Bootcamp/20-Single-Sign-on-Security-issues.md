# Single sign on security issues

Allow user to access multiple services bleonging to the same organization 

* Mechanism ( Implement SSO ) :
  * Cookie Sharing 
  * SAML
  * Oauth

## Cookie sharing 

If service that need to share authentication is under same parent domain can share cookies across subdomains

* Steal 1 cookie, all subdomains go with it
* Subdomains take over -> control over company unused subdomains

`cname`

## Security assertion markup language ( SAML ) 

* Larger Applications 
* XML- based markup language 
* Identity provider:
  * Server in cahrge of authenticating 
* Vulnerability 
  * Can authenticate as someone else 
  * App need to protect intergrity -> Signature in message 

```

Encryption --> Protects -> Confindentiality 
                  \- NO -> Integrity
```

## Oauth 

> Grant scope-specific access token to secure provider through na identity provider

* How it's works ? 
  * Service provider 
    * ask information 
    * email 
    * Date of birth
    * etc 
  * Permission + data = Scope
* Vulnerabilities 
  * Manipulate redirect uri 
    * Determines where the identity prover sends critical info
  * Open redirect chain 

## Hunting 

1. List the target's subdomains 
2. Find unregistered subdomains 
3. Register the page 

Monitoring subdomain takeover 

### SMAL 
1. Locate SMAL reponse
2. Analyze the reponse field 
3. Bypass signature 
4. Re-encode message 
