# Notes

> Here i will write some useful commands that I find while I am working on linux
 

## Make more than one directory 

> With the same start and changing a number 


 ``` bash 
mkdir sa{1..50}
```
The output will create 50 files with names: sa1,sa2,sa3,....,sa50 

```bash
mkdir -p Music/{Jazz/Blues,Folk,Disco,Rock/{Gothic,Punk,Progressive},Classical/Baroque/Early}
```

```
Music/
|-- Classical
|  |-- Baroque
|     |-- Early
|-- Disco
|-- Folk
|-- Jazz
|   |-- Blues
|-- Rock
    |-- Gothic
        |-- Progressive
            |-- Punk
```

## Open a file from Terminal 

```bash 
xdg-open file2open.xxx
```

