# Gestion de vulnerabilidades 

## Index 

* [## Index](### Index)
* [## Automatizacion de escaneres de vulnerabilidades ](#a-escan)
  * [### Ejemplo nmap ](#nmap)
  * [### Escaneo openVas ](#openvas)
* [## Validacion de Parches y actualizaciones de seguridad ](#parches-actualizaciones)
* [## Integracion con herramientas de evaluacion de riesgos ](#integracion)
  * [### Ejemplo ](#ejemplo-cvss)
* [## Tarea ](#tarea)

## Automatizacion de escaneres de vulnerabilidades <a name="a-escan"></a>

* Herramientas 
  * OpenVas 
  * Nessus 
  * Nmap 

### Ejemplo nmap <a name="nmap"></a>

> Tiene integrado nmap-vulners

```
#!/bin/bash

# Definir la IP o red a escanear
objetivo="192.168.1.0/24"

# Definir archivo de salida para el informe de vulnerabilidades
archivo_salida="informe_vulnerabilidades_$(date +%Y%m%d_%H%M%S).txt"

echo "Iniciando escaneo de vulnerabilidades en $objetivo..."
nmap --script nmap-vulners -sV $objetivo > $archivo_salida

echo "Escaneo completado. El informe de vulnerabilidades se ha guardado en $archivo_salida"
```

### Escaneo openVas <a name="openvas"></a>

1. Configura un escaneo 
2. Usar la API de OpenVas o scripts que interactuen con su interfaz de linea de comandos para programar escaneos automaticos
3. Guarda los resultados en archivos o bases de datos para su analisis posterior 

## Validacion de Parches y actualizaciones de seguridad <a name="parches-actualizaciones"></a>

* Con el siguiente script 
  * Se verifica si hay actualizaciones de seguridad (`apt` ) 
  * Si se encuentran actualizaciones de segurdad muestra mensaje de advertencia 
* Para fedora/redhat
  * `sudo dnf check-update --security`

```
#!/bin/bash

# Verificar si hay actualizaciones de seguridad pendientes
echo "Verificando actualizaciones de seguridad pendientes..."
sudo apt-get update > /dev/null
seguridad=$(apt list --upgradable 2>/dev/null | grep -i "security")

if [ -z "$seguridad" ]; then
    echo "No hay actualizaciones de seguridad pendientes."
else
    echo "¡Atención! Hay actualizaciones de seguridad pendientes:"
    echo "$seguridad"
fi

# Validar si se han aplicado las actualizaciones recientemente
echo "Comprobando el historial de actualizaciones..."
cat /var/log/dpkg.log | grep "upgrade"
```

## Integracion con herramientas de evaluacion de riesgos <a name="integracion"></a>

Se puede utilizar el sistema de puntuacion de vulnerabilidades comunes ( [CVSS](https://www.first.org/cvss/v3.1/use-design) ). Tambien puedes integrar con las herramientas antes mencionadas. 

### Ejemplo <a name="ejemplo-cvss"></a>

```
#!/bin/bash

# Archivo de entrada con los resultados del escaneo de vulnerabilidades
archivo_escaneo="informe_vulnerabilidades.txt"

# Archivo de salida con la evaluación de riesgos
archivo_riesgos="evaluacion_riesgos_$(date +%Y%m%d_%H%M%S).txt"

echo "Evaluando vulnerabilidades según CVSS..." > $archivo_riesgos

# Procesar cada vulnerabilidad y asignar una puntuación CVSS
while read -r linea; do
    # Aquí puedes integrar una base de datos o API que asigne puntuaciones CVSS
    echo "Vulnerabilidad detectada: $linea" >> $archivo_riesgos
    echo "Puntuación CVSS: 7.5 (Ejemplo)" >> $archivo_riesgos
done < $archivo_escaneo

echo "Evaluación de riesgos completada. Los resultados se han guardado en $archivo_riesgos"
```

## Tarea <a name="tarea"></a>

1. Crea un script que realice un escaneo de vulnerabilidades en tu red, guarde los resultados y luego los evalue basandose en una base de datos de cvss 
2. Modifica el script para verificar si el sistema tien parches de seguridad pendientes y para generar alertas si es necesario.
