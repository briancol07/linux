# Universisdad CTF: Red Team 

## Overview 

1. [Introduccion a Redes](./1-Introduccion-A-Redes/)
2. [Modelo OSI](./2-Modelo-OSI/)
3. [Direcciones IP](./3-Direcciones-IP/)
4. [Conceptos Importantes](./4-Conceptos-Importantes/)
5. [Fases de Pentesting ](./5-Fases-Pentesting/)
6. [Herramientas Escaneo](./6-Herramientas-Utiles-Escaneo/)
7. [Herramientas Vulnerabilidades](./7.Herramientas Vulnerabilidades/)
8. [Herramientas Obtener Acceso](./8.Herramientas Obtener Acceso/)
9. [Maquinas ](./9.Maquinas/)



