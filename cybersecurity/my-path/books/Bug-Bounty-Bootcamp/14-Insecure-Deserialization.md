# Insecure Deserialization 

Change program behaviour : Deserialize program

*Serialization*: Data converted into a format that allow to be save in a database 
*Deserialization*

Devs --> Trust --> User supplied serialization data
           |
           x 
           | 
        Can bypass or RCE: Remote code execution 

Pop Chain : Property oriented programming chain 
 |
similar
 |
ROP Chain: Return oriented programming ( Binary explotation ) 

> Tool : ysosrial




