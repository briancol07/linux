# Open Systems Interconnection (OSI)

## Introduction 

Standardised model which we use to demostrate the theory behind computer networking.
Compact TCP /IP model. To get initial understanding.

## Layers 

``` 
OSI ---- Application
    |--- Presentation
    |--- Session
    |--- Transport
    |--- Network
    |--- Data link 
    |--- Physical
```

### Layer 7 -- Application 

Provides networking options to programs running on a computer. 

### Layer6 -- Presentation

This layer receives data from the application layer. Format that app understands, but it's not necesarily in a standardised format that could be understood . 
This layer translates the data into a standardised format , can handle 

### Layer5 -- Session

When receives the data, it looks to see if it can set up a connection with the other computer across the network. if it can't then it sends back an error . If a session can be established then it's the job of the session layer to maintain it. 
unique to the communication in question. This is what allows you to make multiple requests to different endpoints simultaneously without all the data getting mixed up.
When the session layer has successfully logged a connection between the host and remote computer the data is passed down. 

### Layer4 -- Transport

1. Choose protocol over
  * TCP (Transmission Control Protocol)
  * UDP (User Datagram Protocol)

#### TCP

The transmission is connection-based : The connection between the computeris established and maintained for the duration of the request.
* Reliable transmission
* Data speed is acceptable 
* Constant communication

#### UDP

packets of data are essentially thrown at the receiving computerif it can't keep up then that's its problem. Used when speed is important 

### Layer3 -- Network

Responsible for locating the destination of your request. Take the ip address for the page and figures out the best route to take . Working with a Logical Addressing (ip addresses) like ipv4 format.

### Layer2 -- Data link

Focus on the physical addressing of the transmission. It receives a packet from the network layer (ip address fot the remote computer)  and adds in the physucal (MAC) address of the receiving endpoint. Inside every network enabled computer is a Network interface card (NIC)

MAC addresses are set by the manufacturer and literally burn into the card; they can't be changed. Although can be spoofed.

The data link also serves an important function when it receives data, as it checks the received information to make sure that it hasn't been corrupted durign transmission, which could well happen when the data is transmitted by layer1 

### Layer1 -- Physical 

The physical layer is right down to the hardware of the computer. This where the electrical pulses that make up data transfer over a network are sen and received. It's the job of the physical layer to convert the binary data of the transmission into signals and transmit them across the network, as well as receiving incoming signal and converting them back into binary data .
