# Intro Databases 

> SQL ( Structured Query Language ) 

## Database Management Systems ( DBMS )

Helps create, define , host and manage databases.

* Relational DBMS ( RDBMS ) 
* NoSQL
* Graph based 
* Key value stores 

* Ways to interact 
  * API ( Application Programming Interface ) 
  * Command line tools 
  * GUI

Feature | Description 
:------:|:-----------:
Concurrency | A real-world application might have multiples users interacting with a it simultaneously. A DBMS makes sure that these concurrent interactions succeed without corrupting or losing any data
Consistency | With so many concurrent interactions, the DBMS needs to ensure that the data remains consistent and valid throughout the database 
Security | DBMS provides fine-grained security controls through user authentication and permissions. This will prevent unauthorized viewing or editing of sensitive data 
Reliability | It is easy to backup databases and rolls them back to a previous state in case of data loss or a breach 
Structured Query | SQL simpliefies user interaction with the databases with an intuitive syntax supporting various operations 

## Architecture 

![DBMS](./img/DBMS-arch.png)

* Tier1 consist of client side applications such as websites or gui programs 
  * High-level interaction such as user login or commenting 
* Data passed to Tier2 through API calls or other request 
  * Middleware -> Interprets these events and puts them in a form required by the DBMS 
* Application Layer uses especific libraries and drivers based on the type of DBMS to interact 
  * DBMS receives queries from the middleware and performs the requested operations
  * Returs the requested data or error codes in the event of invalid queries 
