# Linux Basics and system startup

## Index 

* [## Boot Process ](#boot)
* [## BIOS - The first step ](#BIOS)
* [## Master Boot Record ( MBR )](### Master Boot Record ( MBR ))
* [## Boot loader in action ](#action)
* [## The initial RAM disk ](#ram)
* [## Text-mode login ](#text-mode)
* [## Linux Kernel  ](#kernel)
* [## /sbin/init and services ](#services)
* [## Startup alternatives ](#startup)
* [## Systemd features ](#systemd)
* [## FileSystems ](#fs)
* [## Partitions and FS ](#partitions)
* [## Filesystem Hierarchy standard ](#hierarchy)
* [## More about FHS ](#more)
* [## Choosing a linux distribution ](#choosing)
* [## Questions to ask when choosing a distribution ](#questions)

## Boot Process <a name="boot"></a>

Procedure for initializing the system. Consists of everything that happens from when the computer power is first switched on until the user interface is fully operational 

![boot](./img/boot.png)

## BIOS - The first step <a name="BIOS"></a>

When the computer is powered on, the basic input / output system ( BIOS ) initializes the HW, including the screen and keyboard and test the main memory. This process is called ( Power on self test ). 

The BIOS SW is stored on a read-only memory ( ROM ) chip on the motherboard. 

## Master Boot Record ( MBR ) 

BIOS --> Bootloader 

The Bootloader is usually stored o none of hte system's sotrage devices, such as a hard disk or SSD, either in the boot sector or the EFI partition  or EFI/UEFI systems.
Up to his stage the machine does not access any mass storage media. 

Information on the date, time and the most important peripherals are loaded from the CMOS vlues ( Battery)

A number of boot loaders exist for linux, the most common ones are GRUB ( Grand unified bootloades ), ISOLINUX ( removable media ) , DAS U-boot ( for booting on embedded devices / appliances ). 

When booting linux, the boot loader is responsible for loading the kernel image and the initial RAM disk or filesystem into memory 

![uefi](./img/uefi.png)

## Boot loader in action <a name="action"></a>

The bootloader resides at the first sector of the hard disk also known as the master boot record (MBR). The size of the MBR is just 512 bytes. 
In this stage the bootloader examines the partition table and finds a bootable partition. Once it finds a bootable partition, it then searches for the second stage. 

* GRUB: loads it into ram 
* EFI/UEFI: reads tis boot manager data and determine wich UEFI application is to be lunched 

The second stage boot loader resides under `/boot`. A splash screen is displayed, which allow us to choose which operating system and/or kernel to boot. After the OS and kernel are selected the boot loader loads the kernel of the operating system into RAM and passes control to it.

## The initial RAM disk <a name="ram"></a>

The initramfs filesystem image contains programs and binary files that perform all actios needed to mount the proper root filesystem, including providing the kernel funtionality required tro the specific filesystem that will be used on loading the device drivers for mass storage controllers, by taking advantage of the **dev** system. 

The mount program instruct the operating system that a filesystem is ready for use and associates it with a particular point in the overall hierarchy of the filesystem. 

If this is successful, the initramfs is cleared from RAM and the init program on the root filessytem (`/sbin/init`) is executed 

Init handles the mounting and pivoting over the final real root filesystem. If special HW drivers are needed before the mass storage can be accessed, they must be in the initramfs image.

![init](./img/initramfs.png)

## Text-mode login <a name="text-mode"></a>

Near the end, init starts a number of text mode login prompts, these enable you to type your username and password. 

## Linux Kernel  <a name="kernel"></a>

The boot loader loads kernel and an initial RAM-based file system (initramfs), so it can be used directly by the kernel 

When the kernel is laodade in RAM, it immediately initializes and configures the computer's memory and also configures all the hardware attached to the system. 

* Processors 
* I/O subsystems 
* Storage devices 

The kernel also loads some necessary user space application 

## /sbin/init and services <a name="services"></a>

Once the kernel has set up all its hardware mounted the root filesystem, the kernel runs `/sbin/init/`. This then becomes the initial process, which then starts other processes to get the system running. 

Other processes trace their origin ultimately to init, exception include the SO-called kernel processes. These are started by the kernel directly and their job is to manage internal operating system details. 

Init is responsible for keeping the system running and for shutting it down cleanl. One of its responsibilities is to act when necessary as a manager for all non-kernel processes; it cleans up after them upon completion and restart user login services as needed when user log in and out and other background system services.

In the old times was done using system V. This serial process called `sysVinit` had the system pass through a sequence of `runlevels` containing collections of scripts that start and stop services. Each level supported a different mode of running the system .

Now a days they migrate to systemd 

## Startup alternatives <a name="startup"></a>

SysVinit viewed things as a serial process, divided into a series of sequential stages. Each stage required completion befor the next could proceed. Parallel processing that could be done with the multiple processors or cores found on modern systems. 

Now we require methods with faster and enhanced capabilities. The older methods required rather complicated startup scripts, which were difficult to keep universal across distribution versions, kernel versions, architectures and types of systems. 
* Two manin alternatives 
  * Upstart 
  * Systemd 

## Systemd features <a name="systemd"></a>

System start faster with systemd, because replaces a serialized set of steps with aggressive parallelization techniques, which permits multiple services to be initiated simultaneously.

Shells scripts are replaced with simpler configuration files.
`/sbin/init` now points to `/lib/systemd/systemd` so it takes over the init process.

One command `systemctl` is used for most basic tasks. while we have not yet talked about working at the command line.

* Starting, stopping, restarting a service 
  * `sudo systemctl start|stop|restart httpd.service `
* Enabling or disabling a system service from starting up at system boot
  * `sudo systemctl enable|disable httpd.service`
* Checking on the status of a service 
  * `sudo systemctl status httpd.service`

## FileSystems <a name="fs"></a>

Embodiment of a mehtod of storing and organizing arbitrary collections of data in human-usable form

* Different types of FS suppported 
  * Conventional disk filesystems 
    * ext3, ext4, XFS, Btrfs, JFS, NTFS, vfat, exfat
  * Flash storage filesystems
    * ubifs, jffs2, yaffs
  * Database filesystem
  * Spercial purpose filesystems 
    * procfs, sysfs, tmpfs, squashfs, debugs, fuse 

## Partitions and FS <a name="partitions"></a>

A partition is a dedicated subsection of physical storage media.

This meant a physically contiguous portion of a hard disk.

A FS is just a mehtod of storing and accesing files 

Type | Windows | Linux 
:---:|:--------:|:----:
Partition | Disk1 | `/dev/sda1` 
Filesystem type | NTFS/VFAT | EXT3/EXT4/XFS/BTRFS
Mounting parameters | Driveletter | MountPoint
Base folder ( where os is stored ) | C:\ | / 

## Filesystem Hierarchy standard <a name="hierarchy"></a>

Standard layout called the filesystem Hierarchy standard ( FHS )

Linux uses the `/` character to separate path ( as sis Unix ) and does nto have drive letters. 
Removable media such USB drives and CDs and DVDs will show up as mounted at `/run/media/yourusername/disklabel` or `/media` 

![FHS](./img/FHS.jpg)

## More about FHS <a name="more"></a>

Are case-sensitive, so `/boot` != `/BOOT`.

## Choosing a linux distribution <a name="choosing"></a>

Some : 

Server | Desktop | Embedded 
-------|---------|---------
RHEL/CentOS | Ubuntu | Yocto 
Ubuntu Server| Fedora | open Embedded 
SLES/openSUSE | Debian | android 
Debian | Archlinux | --

## Questions to ask when choosing a distribution <a name="questions"></a>

* What is the main function of the system (server or desktop)
* What types of packager are important to the organization ? ( web, server , word processing ) 
* How much storage space is required, how much is available ? 
* How often are packages updated ? 
* How long is the support cycle for each release ? 
* Do you need kernel customization fro mthe vendor or a third party ? 
* What hardware are you running on ? 
* Do you need long-term stability ? or can you accept a more volatile cutting-edge system running the lastest software version?
