# Identificacion analisis versiones

## Index

* [## Que es la enumeracion de Servicios? ](#que)
* [## Deteccion de Versiones con Nmap ](#deteccion)
* [## Enumeracion con otras herramientas ](#enumeracion)

## Que es la enumeracion de Servicios? <a name="que"></a>

Es el proceso mediante el cual un pentester obtiene informacion detallada sobre los sercivios que estan en ejecucion en un puerto abierto.
Ademas de saber que servicios estan activos, tambien es fundamental identificar la version exacta de l software que esta corriendo en los puertos.

## Deteccion de Versiones con Nmap <a name="deteccion"></a>

Comando para detectar versiones de servicios `nmap -sV`, Si el puerto 22(SSH) esta abierto se detecta que en el sistema esta ejecutando una version antigua de openSSH, un pentester puede consultar la bbdd de CVE para identificar vulenrabilidades conocida y aplicar exploits.

## Enumeracion con otras herramientas <a name="enumeracion"></a>

Netcat es una herramienta poderosa para establecer conexiones manuales con servicios en puertos especificos. Esto es util cuando se desea interactuar directamente con un servicio para extraer informacion.

Comando de conexion manual con un servicio `nc`

Nikto es una herrameinta de escaneo web diseniada especificamente para detectar vulnerabilidades en servidores HTTP. Es ideal para descubrir configuraciones inseguras, archivos expuestos y versiones desactualizadas 

Comando basico de escaneo `nikto -h`
