# Networking Models

## Index 

* [## ISO/OSI Model ](#osi)
* [## TCP/IP model ](#tcp)
* [## OSI vs TCP ](#vs)
* [## Packet transfers ](#packet)

## ISO/OSI Model <a name="osi"></a>

> Open Systems Interconnection model 

Oftetn referred to as ISO/OSI layer model, is a reference model that can be used to describe and define the communication between systems.

## TCP/IP model <a name="tcp"></a>

> Transmission control protocol / Internet protocol

is a generic term for many network protocols. The protocols are responsible for the switching and transport of data packets on the internet 

ICMP ( Internet control message protocol ) and UDP (User Datagram protocol) belong to the protocol family 

![osi-tcp](./img/osi-tcp.png)

## OSI vs TCP <a name="vs"></a>

TCP/IP is a communication protocol that allows hosts to connect to the internet. It refers to the transmission control protocol used in and by applications on the internet.
In contrast to OSI, it allows a lightening of the rules that must be followed, provided that general guidelines are followed. Is a communication gateway between the network and end-users. The OSI model is ussually referred to as the reference model because it is newer and more widely used.

## Packet transfers <a name="packet"></a>

Devices in a layer exchange data in a different format called a protocol data unit (PDU). 

When we want to browse a website on the computer, the remote server software first pases the request data to the application layer, it is processed layer by layer, each layer performing its assigned functions.
The data is then transferred through the network's physical layer until the destination server or another device receives it. The data is routed through the layers again, with each layer performing tis assigned operations until the receiving software uses the data 


![packet](./img/packet.png)

During the transmission, each layer adds a header to the PDU from the upper layer which controls and identifies the packet. This process is called encapsulation. The header and the data together form the PDU for the next layer . The process continues to the physical layer or network layer, where the data is transmitted to the receiver.

The receiver reverses the process and unpacks the data on each layer with the header information. After that, the application finally uses the data, this continues until all data has been sent and received 

![transfer](./img/transfer.png)
