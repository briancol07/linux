# Sentencia case 

## Que es una sentencia case ?

La sentencia `case` en bash permite evaluar una variable o expresion y ejecutar un bloque de codigo basado en el valor de esa variable ( Funciona como if-elif ) .

## Sintaxis 

```
case variable in
    opción1)
        # Código a ejecutar si la variable coincide con opción1
        ;;
    opción2)
        # Código a ejecutar si la variable coincide con opción2
        ;;
    *)
        # Código por defecto si ninguna opción coincide
        ;;
esac
```
### Ejemplo 

```
#!/bin/bash
echo "Introduce un día de la semana (lunes, martes, etc.):"
read dia

case $dia in
    lunes)
        echo "Es lunes, empieza la semana."
        ;;
    viernes)
        echo "Es viernes, casi es fin de semana."
        ;;
    sábado|domingo)
        echo "Es fin de semana, disfruta tu descanso."
        ;;
    *)
        echo "No es un día válido."
        ;;
esac

```

```
#!/bin/bash
echo "¿Quieres continuar? (sí/no)"
read respuesta

case $respuesta in
    sí|Sí|YES)
        echo "Has elegido continuar."
        ;;
    no|NO|No)
        echo "Has elegido no continuar."
        ;;
    *)
        echo "Respuesta no válida."
        ;;
esac
```
### Uso de case con comandos 

Las sentencias case tambien pueden utilizarsse para manejar diferentes comando basodos en la entrada del usuario o en el resultado de una operacion 

```
#!/bin/bash
echo "¿Qué comando te gustaría ejecutar? (date, ls, pwd)"
read comando

case $comando in
    date)
        date
        ;;
    ls)
        ls -l
        ;;
    pwd)
        pwd
        ;;
    *)
        echo "Comando no reconocido."
        ;;
esac
```

## Caso por defecto 

`*)` Siempre incluir un bloque `*` para manejar entradas no validas o inesperadas 

## Tareas 

1. Crear un script que pida al usuario su pais y responda con el idioma oficial ( Por ejemplo si introduce espania responde con espaniol). Incluye almenos 4 paises y un caso por defecto para paises no incluidos 
2. Modifica el script de menu para que despues de mostrar la informacion, vuelva a mortrar el menu hasta que elija salir 

