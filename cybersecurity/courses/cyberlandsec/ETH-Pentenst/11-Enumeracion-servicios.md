# Enumeracion de servicios 

## Enumeracion de FTP <a name="ftp"></a>

FTP es un protocolo utilizado para la transferencia de archivos

* Enumeracion de acceso anonimo
  * Permite el acceso anonimo (Cualquier usuario puede iniciar sesion)
  * Comando para detectar acceso anonimo en FTP : `nmap -script ftp-anon`
* Explotaciohn de versiones obsoletas
  * Algunos permiten RCE, 
  * Detectar la version exacta de FTP con nmap es el primer paso apra planificar un ataque basado en exploits conocidos 

## Enumeracion SSH <a name="ssh"></a>

SSH es el protocolo estandar para la administracion remota segura de servidores. 

* Deteccion de Version SSH
  * `nmap -sV -p 22`
* Fuerza bruta en SSH
  * Si se detecta que ejecuta una version antigua de OpenSSH, y es suceptible a fuerza bruta .
  * Se podria usar Hydra o Medusa combinado con la informacion obtenida en nmap

## Enumeracion de HTTP <a name="http"></a>

HTTP es uno de los servicios mas comunes y expuesto a vulnerabilidades. 

* Configuraciones erroneas 
* Archivos sensibles / expuestos 
* Ejecutando versiones antiguas (Apache o Nginx)

Utilizando el scritp NSE(http-enum) se puede listar directorios y arhcivos sensibles que puedan estar expuestos en el servidor web . Comando : `nmap -script http-enum`

## Enumeracion de SMB <a name="smb"></a>

Es un protocolo utilizado en redes windows para compartir archivos e impresoras. Las versiones vulnerables de SMB, como SMBv1, han sido responsables de ataques masivos, como el de WannaCry

* Deteccion de vulnerabilidades
  * Comando `nmap -script smb-vuln*`


