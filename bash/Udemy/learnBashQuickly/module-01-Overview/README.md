# Module 1 


## Module overview

1. Create and run your first shell scripting
2. Convert your shell script into a bash script 
3. Editing your path Variables
4. Adding Comment 

## 1.1 

1. Create a new directory named scripts 
2. Inside scripts, create a file named hello.sh
3. Edit the file hello.sh to containt:
  * echo "Hello, World!"
4. Run the script hello.sh  

## Terminal vs Console vs Shell

### Terminal 

> Fisical terminal (old days), write commands and you see the output 

### Console 

> Connect switches,consoles and write, like a datacenter use a fisical console. 

### Shell 

> Command line interpreter, sw that undestand commands, Understands --> Interpret --> show output
process and understand

## Running the shell 

``` bash 
# other shell
# ShellName file 
# with bash 
bash hello.sh 
```

## 1.2 

1. Insert the shebang line ate the very beginning of your shell scripts 
  * #!/bin/bash 
2. make your script executable 
  * chmod u+x hello.sh
3. run your bash script 

## 1.3 

1. Use the export command:
  * export PATH:$PATH:/home/folders
  * to add your scripts directory to the path variables 
2. Run your script hello.sh




