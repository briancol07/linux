# Connect to ssh 

Load the OS in the raspy with the Raspberry imager because allows you to modify the ssh and set some names like `pi.local`

I am using a parrotOS , I have to install `avahi-daemon` to ve able to connect to the raspberry. 

## Install avahi-daemon

```
sudo apt update
sudo apt install avahi-daemon
```

### Start Avahi service

```
sudo systemctl start avahi-daemon
```
### Enable 

This make the command always available 
```
sudo systemctl enable avahi-daemon
```

## See Raspberry ip

This command make a ping to the raspberry to be able to see the ip address so we can connect .

```
ping pi.local
```

If you don't know the status of `avahi-daemon` use the following command `sudo systemctl status avahi-daemon
` 

## Connecto to ssh 

```
ssh pi@<IP_ADDRESS>


```

## My Case

```
pi.local
Name: portServer
```
