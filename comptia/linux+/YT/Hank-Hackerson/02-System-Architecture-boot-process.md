# System architecture boot process

## Index

* [## Understanding linux Directory structure & filesystem hierarchy ](#dir)
  * [### Root ](#root)
  * [### Bin (binaries) ](#bin)
  * [### Sbin (system Binaries) ](#sbim)
  * [### Etc (Configuration) ](#etc)
    * [#### Why is it important ? ](#important)
    * [#### Package managers ](#package)
  * [### Home (Your personal Space) ](#home)
  * [### Var (Variable directory) ](#var)
    * [#### Why is important? ](#why)
  * [### User programs & libraries (/usr) ](#usr)
    * [#### Why is important? ](#imp2)
  * [### OPT ( Optional software ) ](#opt)
  * [### DEV (Virtual device interface) ](#virtual)
    * [#### Why is important? ](#imp3)
  * [### TMP ( Temporary file system ) ](#tmp)
    * [#### Why is important ?  ](#imp4)
  * [### Other directories ](#other)
* [## Filesystem types and mount point ](#fs)
  * [### Ext4 (Fourth extended filesystem) ](#ext4)
  * [### xfs (X filesystem) ](#xfs)
  * [### NTFS (New technology file system) ](#ntfs)
  * [### FAT32 (File allocation table 32)](#fat)

## Understanding linux Directory structure & filesystem hierarchy <a name="dir"></a>

> Filesystem Hierarchy standard (FHS)o

Defines linux directory structure and what each folder is commonly used for

* Key directories 
  * `/` (root)
  * `/bin`, `/sbin`
  * `/etc`
  * `home`
  * `var`
  * `usr`
  * `opt`
  * `dev`
  * `tmp`

Come pre-installed

### Root <a name="root"></a>

* Top-Level directory  for the entire filesystem
* All of the other directories in FHS are in the root directory
  * Organized hierarchically 
* Owned by the root user 
* Stability & security (don't mess with it)

### Bin (binaries) <a name="bin"></a>

* Contains essential binary executables that are needed during the boot process or in single-user mode.
* Accessible to all users
  * `ls`
  * `cat`
  * `cp`
  * `mv`
  * `rm`

### Sbin (system Binaries) <a name="sbim"></a>

* Contains binary executables primarily used for system administration .
* Require root privilege to execute
  * `fdisk`: Partitions hard disks
  * `fsck`: Checks and repairs file systems
  * `init`: Initializes the system
  * `reboot`: Reboots the system
  * `shutdown` : shuts down the system 

Feature | `/bin` | `/sbin`
-------|---------|----------
User accessibility | all user | Root user
Purpose | Essential user commands | System adminsitration tools
Execution timing | Early boot process or single-user mode | System maintenance and administration 

### Etc (Configuration) <a name="etc"></a>

* Houses configuration files for many system services and apps.
* "Control Center"
* The contents of `/etc` can vary depending on the linux distro and installed software 
* /etc
  * `/passwd` : User account information
  * `/shadow` : User password hashes
  * `/group` : Group information
  * `/hosts` : Hostanme and IP address mappings
  * `/hostanme` : System hostname 
  * `/resolv.conf` : DNS resolver configuration
  * `/network/interfaces` : Network interface configuration (older system)
  * `/sysctl` : System kernel parameters
  * Service Configurations 
    * `/apache2` : Apache web server configuration
    * `/nginx` : Nginx web server configuration
    * `/mysql/my.cnf` : MySQL database configuration
    * `/postgresql/postgresql.conf` : PostgreSQL database configuration
    * `/ssh/sshd_config` : SSH server configuration
  * Package managers
    * `/apt/source.list` : APT package manager sources 
    * `/yum.repos.d` : YUM package manager repositories 


#### Why is it important ? <a name="important"></a>

* System behavior 
  * The configuration file in `/etc` directly influence how your system behaves 
* Security : 
  * Misconfigured files can pose security risks, so it's important to handle them with care 
* Customization
  * You can customize your system's behavior by editing these files
* Troubleshooting 
  * Many system problems can be resolved by modifying configuration files 

#### Package managers <a name="package"></a>

* Backup: Always back up any configuration files fore making changes 
* Permissions : Be mindful of file permissions, as incorrect permissions can lead to system instability 
* Syntax : Ensure that configuration files have correct syntax to avoid errors 
* Testing : Test changes in a controlled enviroment before deploying them to a production system 

### Home (Your personal Space) <a name="home"></a>

* Where user accoutns are stored 
* Primary location for user's files, documents, settings, and configs 
* Each user has their own subdirectory within 
  * user1 = `/home/user1`
* Key directories 
  * Documents
  * Downloads 
  * Music 
  * Pictures 
  * Public 
  * Videos
* Permission & ownership
  * User ownership : Each user's home directory is owned by that user 
  * Permissions : By default, only the user and the root user have full access to the home directory. 
    * This ensures privacy and security
  * Personalization: Users can customize their home directories to suit their preferences 
  * Data storage : Home directories are the primary storage location for user data 
  * Security : By default, home directories are protected from unauthorized access 
* Notable notes 
  * Backup regularly 
  * File permissions 
  * Storage limits 
  * Organization

### Var (Variable directory) <a name="var"></a>

* Stores variable data that changes frequently
* System logs, temporary files, mail spools, etc 
* Key directories 
  * `/log` : Stores system and application logs. 
    * these logs can be invaluable for troubleshooting and security for analysis 
  * `/mail`: stores incoming mail for users
  * `/spool` : Contains spools for various services, including print jobs, mail, and news 
  * `/lib` : Sotres state information for various services and applications 
  * `/tmp` : Stores temporary files created by various applications 
* Considerations
  * Regular Clean up (`/var/tmp`) should be cleaned regurlarly to prevent it from filling up
  * Log rotation : log files can grow large, so it's important to rotate them to save disk
  * Permissions 
  * Backup

#### Why is important? <a name="why"></a>

* System health monitoring 
* Security analysis 
* Service operations 
* Application state

### User programs & libraries (/usr) <a name="usr"></a>

> Stores user programs, libraries, and documentation 

* Key directories 
  * `/usr/bin`: Contains commonly used binaries accessible to all user
  * `/usr/sbin`: Contains system administration binaries, usually requiring root privileges 
  * `/usr/lib`: Stores shared libraries used by programs 
  * `/usr/local`: Used for locally installed software, often outside of the package manager 
  * `/usr/share`: Contains shared data files, such as documentation, icons, and configuration files 
  * `/usr/src`: Contains source code for various sytem utilities
* Key Points 
  * Read Only : To prevent accidental modifications 
  * Package management 
  * User access: while some files in `/usr` are accessible to all users, other may require root privileges to modify 

#### Why is important? <a name="imp2"></a>

* Program execution: The binaries in `/usr/bin` and `/usr/sbin` are essential for runnign programs 
* Library sharing : shared libraries in `/usr/lib` are used by multiple programs to reduce disk space and improve performance
* Software installation : Many software packages install their files in vairous subdirectories of `/usr`
* System documentation : The `/usr/share/doc` directory often contains documentation for system utilities and installed SW 

### OPT ( Optional software ) <a name="opt"></a>

Used to store addtional software packages that are not part of the base system 
Designed for optional software installations, often from third-party sources

* Why use ? 
  * Organization 
  * Isolation: It prevents conflits between different software packages 
  * Flexibility : Easy installation and removal of optional SW 

Example: `/opt/mysql` in this directory will contain the Software's binaries, libraries, configuration files and data.In `/etc` will only have the configuration file 

* Key points 
  * Ownership and permissions: The ownership and permissions of files and directories within `/opt` can vary depending on the SW
  * Package management: While some software packages can be installed using the system's package manager, other may require manual installation
  * Configuration : Configuration files for SW installed in `/opt` are often located within the SW's directory
  * Cleanup : when removing software installed in `/opt` it's important to remove all files and directories associated with the SW. 

### DEV (Virtual device interface) <a name="virtual"></a>

 A virtual filesystem that represents hardware devices as file. Allow the OS the interact with ahrdware devices using standard file operations

* `/dev/sda` `/dev/sdb` , etc : Represent hard disk drives
* `/dev/cdrom` : Represent a CD-ROM drive
* `/dev/ttyUSB0` `/dev/ttyUSB1`, etc : Represent USB serial ports 
* `/dev/null` : A special fiel that discars any data written to it 
* `/dev/zero` : A special file that produces an infinite stream of zeros 
* Key Points 
  * Dynamic Creation: Device files are often created dynamically when a device is plugged in or when the system boots 
  * Permissions: Access to device file may be restricted to root or specific users
  * Device Drivers: The correct device driver must be installed and loaded for a device to be accesible in `/dev`

#### Why is important? <a name="imp3"></a>

* Device abstraction: By treating devices as files, linux provides a consistent interface for interacting with various harware device.
* Device Drivers: which are SW components that control hardware devices, create a device files in `/dev`
* User interaction : Users can access and control devices through these device files, often using commands like dd or hdparm.

### TMP ( Temporary file system ) <a name="tmp"></a>

* Used to store temporary file created by various applications and system processes 
* Files are typically deleted when the application or process finishes or when the system is rebooted 
* Has write and execute permissions 
* Popular target for hackers 
* key points 
  * Automatic cleanup
  * Security  : Be careful with the files that contain sensitive information 
  * Manual cleanup

#### Why is important ?  <a name="imp4"></a>

* Temporary storage : It provides a convenient location for applications to store temporary files without cluttering the user's home directory or other permanent location
* System Processes
* User applications 

### Other directories <a name="other"></a>

* `/boot` : Stores boot loader files 
* `/dev` : Contains device files representing hardware device 
* `/lib` : Contains shared libraries used by programs 
* `/mnt` : Temporary mount point for removable media 
* `/proc` : Virtual filesystem providing info about the system processes 
* `/srv` : Contains data for services provided by the system
* `sys` : Virtual filesystem providing info about hte system's hardware

## Filesystem types and mount point <a name="fs"></a>

* Common filesystem 
  * ext4
  * xfs 
  * NTFS
  * Fat32

### Ext4 (Fourth extended filesystem) <a name="ext4"></a>

* Journaling file system for linux
  * Improves reliability by recording file system changes in a log before they are written to disk 
  * This ensure data integrity in case of system crashes or power failures 
* Designed as the succesor to ext3
* Large file support  
* Large fie system support 
* Enhance performance 
* Extensibility 

### xfs (X filesystem) <a name="xfs"></a>

* Journaling file system known for its high performance, sacability and reliability 
* Well-suited for large file systems and high-performance workloads 
* Key features 
  * High performance 
  * Large file system support (8 exabytes in size)
  * Reliability 
  * Scalability 
    * For servers with many users 
  * Flexibility
    * Online filesystem resizing and real-time defragmentation 
* Common use cases 
  * High-performance server 
  * NAS devices 
  * Virtual machines 

### NTFS (New technology file system) <a name="ntfs"></a>

* Primary file system used in moden windows OS
* Reliable, secure and performs well
  * Security : Offers robust security features, including access control lists (ACLs)
  * Reliability Uses a journaling file system 
  * Large file support 
* Other key features 
  * Compression : supports file and folder compression to save distk space
  * Encryption: Allow you to encrypt files and folders to protect sensitive data 
  * Hard links and symbolic links
* Common use cases
  * Windows OS
  * External Hard drives 
  * USB drives 

### FAT32 (File allocation table 32)<a name="fat"></a>

* Widely used in the early days of personal computing 
* Not rich as modern file systems . It remains popular due to its simplicity and broad compatibility
* Key features 
  * Simplicity 
  * Compability 
  * Read-write access
  * Limited file size 
  * Limited partition size 
* Common use cases
  * USB flash drives
  * Memory cards
  * External hard drives
