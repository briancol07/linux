# Busqueda y Reemplazo 

## Busqueda 

Con la / se escribre y luego la palabra, desde el cursor al final del archivo

```bash 
/ 
```

Con n me va a la siguiente occurencia pero con shift + n voy a la anterior 
Para buscar tambien el anterior al cursor se una el ? nos posiciona en el el anterior 

si quiero la palabra en la que estoy posicionado seria con el * ( shift + 8) 
con g + * nos busca las palabras similares 


## Reemplazo 

s -> Substitute 

en la linea actual 

```bash 

:s // 
```
Este comando me subtituye la ultima palabra que busque y puedo cambiar yo el texto

```bash 
:s //nuevo_palabra/ gc
```
La g y c al final son flags , la g de global y la c de confirmar, esto es para que lo cambie en todo el archivo 


```bash 
:%s//otracosa/g
```

Este sirve tambien para todo el archivo
si al final uso el flag de i (no case sensitive) no le importa si esta mayuscula o minuscula , encambio si pongo la I seria talcual esta escrito 
