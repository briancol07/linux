# Safeguarding your online privacy 

## Two factor Authentication

To add an extra layer of security for account login. Besides your username and password or personal identification number.
This authentication requires a second token to verify your identity:

* Physical object such as a credit card, mobile phone or fob.
* Biometric scan such as a fingerpirnt or facial and voice recognition
* Verification code sent via SMS or email 


## Open Authorization 

Open authorization (OAuth) is an open standard protocol that allows youto use your credential to access third-party applications without exposing your password

Have a login with:

* Google 
* Facebook
* etc 
