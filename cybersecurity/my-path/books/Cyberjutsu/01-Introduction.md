# Introduction

> Field Guide 

* Books: 
  * Bansenshukai ( The book of ninja ) 
  * The Secret tradition of shinobi
  * True Path of the ninja 

## For thinking 

```
Protect you castle ( network ) from the dangers posed by enemy ninja ( Cyber threat actors ) 
```

## Principle 

> The deepest principle of ninjutsu is to avoid where the enemy is attentive and strike where he is negligent 


## Meaning of shinobi 

The kanjis of this word 

Shinobi = Blade + Heart 
