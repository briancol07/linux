# Cybersecurity Devices and Technologies 

Ther is no sigle security appliance or piece of technology that will solve all the network security needs in an organization.

## Security Appliances

Security appliances can be standalone devices like a router or SW tools that are run on a network device.

### Routers 

While routers are primarily used to interconnect various network segments together, they usually also provide basic traffic filtering capabilities. This information can help you define which computers form a given network segment can communicate with which network segments.

### Firewalls 

Firewalls can look deeper into the network traffic itself and identify malicious behavior that has to be blocked. Firewalls can have sophisticated security policies applied to the traffic that is passing through them.

### Intrusion prevention System

IPS systems use a set of traffic signatures that match and block malicious traffic and attacks.

### Virtual private networks

VPN systems let remote employees use a secure encrypted tunnel from their mobile computer and securely connect back to the organization's network. VPN systems can also securely interconnect branch offices with the central office network.

### Antimalware or antivirus

These systems use signatures or behavioral analysis of application to identify and block malicious code form being executed. 


### Other security Devices 

Other security devices include web and email security appliances decryption devices, client access control servers and security management systems.

## Firewalls 

In computer networking, a firewall is designed to control or filter which communications are allowed in and which are allowed out of a device or network.

A firewall can be installed on a single computer with the purpose of protecting that one computer (host-based-firewall) or it can be a standalone network device that protects an entire network of computers and all of the host devices on that network (network-based-firewall)

### Network Layer  

This filters communications based on source and destination ip addresses

### Transport layer 

Filters communications based on source and destination data ports, as well as connection states

### Application layer 

Filter communication based on an application, program or service 

### Context aware layer firewall

Filters communications based on the user device, role application type and threat profile.

### Proxy Server 

Filters web content requests like URLs, domain names and media types 

### Reverse proxy server 

Placed in front of web servers,reverse proxy servers protect, hide offload and distribute access to web servers 

### Network address Translation (NAT)

This firewall hides or masquerades the private addresses of network hosts.

### Host-based

Filters ports and system service calls on a single computer operating system 

## Port Scanning 

In networking, each application running on a device is assigned an identifier called a port number. This port number is used on both ends of the transmission so that the right data is passed to the correct application.
Port scanning is a process of probing a computer server or other network host for open ports. It can be used maliciously as a reconnaissance tool to identify the operating system and services running on a computer or host, or it can be used harmlessly by a network adninistrator to verify network security policies on the network.

* Zenmap
* Nmap

## Intrusion Detection and Prevention systems 

Intrusion detection system (IDSs) and intrusion prevention sytems (IPSs) are security measures deployed on a network to detec and prevent malicious activities 

An IDS can either be a dedicated network device or one of several tools in a server, firewall or even a host computer operating system, that scans data against a database of rules or attack signatures, looking for malicious traffic.
If a match is detected,the IDS will log the detection and create an alert for a network administrator. the job of the IDS is to detec, log and report.

The Scanning performed by the IDS slows down the network (known as latency .To preven network delay an IDS is usually placed offline , separate form regular network traffic. Data is copied or mirrored by a switch and the forwarded to the IDS for offline detection.

An IPS can block or deny traffic based on a positive rule or signature match , One of the most well-known IPS/IDS systems is Snort 

## Real-Time Detection 

Detecting attacks in real time requires actively scanning for attacks using firewall and IDS/IPS network devices. Today, active scanning devices and SW must detect network anomalies using context based analysis and behavior detection.

DDoS is one of the biggest attack threats requiring real-time detection and response. For many organizations, regularly occurring DDoS attacks cripple internet server and network availability. 
These attacks are extremely difficult to defend against because the attack originate from hundreds, even thousands, of zombie host and the attacks appear as legitimate traffic.

## Protecting against malware

One way of defending agains zero day attacks and advanced persistent threats (APTs) is to use an enterprise level advance malware detection

* Advanced malware protection (AMP) threat grid 

This is client/server sW that can be deployed on host endpoints, as a standalone server or on other network security devices. It analyzes millions of files and correlates them against hundreds of millions of other analyzed malware artifacts for behaviors that reveal an APT.

### Secure Operations Center team 

The threat grid allows the cisco secure operations Center team to gather more accurate, actionable data.

### Incidence Response Team 

The Incidence response team therefore has acces to forensically sound information from which it can more quickly analyze and understand suspicious behaviors

### Threat Intelligence Team 

Using this analysis the threat intelligence team can proactively improve the organization's security infrastructure.

### Security infrastructure Engineering team 

Overall, the security infrastructure engineering team is able to consume and act on threat information faster, often is an automated way.

## Security Best Practices 

Many national and porfessional organizations have publshed lists of security best practices.Such NIST National Institute of Standards and Technology computer security resource center.

###  Perform a risk assessment 

Knowing and understanding the value of what you are protecting will help to justify security expenditures.

### Create a security policy 
  
Create a policy that clearly outlines the organization's rules, job roles andponsibilities and expecttions for employees

### Physical Security measures 

Restrict access to networking closets and server locations, as well as fire suppression

### Human resource security measures 

Background cheks should be completed for all employees

### Perform and test backups 

Back up information regularly and test data recovery from backups 

### Maintain security patches and updates 

Regularly update server, client and network device operating systems and programs 

### Employ Access controls 

Configure user roles and privilege levels as well as strong user authentication

### Regularly test incident response 

Emply an incident response team and test emergency responce scenarios 

### Implement a network monitoring, analytics and management tool

Choose a security monitoring solution that integrates with other technologies 

### Implement network security devices 

Use next generation routers, firewalls and other security appliances 

### Implement a comprehensive endpoint security solution

Use enterprise level antimalware and antivirus SW

### Educate Users 

Provide training to employees in security procedures 

One of the most widely known and respected organizations for cybersecurity training is the SANS Institute

### Encrypt data

Encrypt all sensitive organizational data, including email.
