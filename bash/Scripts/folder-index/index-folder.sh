#!/bin/bash 

while read -r line; do 
  file=$line"README.md"
  if [ ! -e $file ]; then
    continue
  fi
 echo "* [$line](./$line)"
done < <(ls -d */)
