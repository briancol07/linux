# Buffers , ventanas y pestanias 

> Haremos todo esto desde el modo normal

Debe asegurarse que la opcion set hidden est peresnet en tu archivo vimrc. sin este ajuste cada vez que cambies a un buffer, vim te pedira que guarde el archivo.

Esto de .vimrc se vera mas adelante por ahora no darle tanta importancia 

## Buffers 

> Un buffer es un espacio de memoria en el que peudes escribir y editar algun texto. 

Cuando abres un archivo, su contenido estara ligado a un buffer. Asi por cada archivo que abras 


### Mostrar los buffers 

```vim 
:buffers
:b 
:ls 
:files
```

### Desplazarse entre buffers
 
```vim 
" salta al siguiente buffer 
:bnext 
:bn
" Salta al anterior 
:bprevious 
:bp
``` 


```vim
"salta a un buffer en especifico 
:buffer + nombreArchivo
:b 
" Puede autocompletar con tab 
```

Los buffers tiene numero entonces se puede ir saltando entre ellos sin saber el nombre 


```vim 
" salta a un buffer por numero 
:buffer + n 
:b + n 
"ejemplo 
:b 2 
```

### Saltar  la antigua posicion y a la nueva posicion 

```vim 
<Ctrl - O>
<Ctrl - I>
" Ir al buffer previamente editado 
<Ctrl - ^> 
```

## Eliminar buffers

Una vez creado, el buffer permanecera en tu lista, para eliminarlo 

```vim 
:bdelete 
:bd 3
```

## Saliendo de vim 

Cerrar todos los buffers a la vez 

```vim 
:qall
" sin guardar los cambios
:qall!
" Guardar y salir 
:wqall
```

## Ventanas 

> Ventana es el medio por el cual ves el buffer 

```vim
" For help 
:h window
```

```vim 
" Hace un split de ventana horizontal
:split fileName
" vertical split
:vsplit fileName
```

### Navegar etnre ventanas 

```vim 
<Ctrl-w h> " Mueve el cursor a la ventana de la izquierda 
<Ctrl-w j> " Mueve el cursor a la ventena inferior
<Ctrl-w k> " Mueve el cursor a ventana superior
<Ctrl-w l> " Mueve el cursor a la ventana de la derecha
```

### Cerrar ventana 

```vim
<Ctrl-w c>
:quit
```

### Atajos 

```vim

<Ctrl-w v> "Abre un nueva division vertical 
<Ctrl-w s> "Abre un nueva division horizontal 
<Ctrl-w c> "Cierra una ventana 
<Ctrl-w o> "Hace que la ventana sea principial y cierra las otras 
```

## Pestanias

> Una pestania en una collecion de ventanas 

```vim 
" Abrir una nueva pestania
:tabnew fileName 
" Cierra la pestania actual
:tabclose 
"--------------------------
:tabnext     " Ir a la proxima pestania
:tabprevious " Ir a la prestania previa 
:tablast     " Ir a la ultima pestania 
:tabfirst    " Ir a la primer perstania 

gt          " Siguiente 
gT          " Pestantia anterios 
3gt         " Ir a la tercer pestanis sig
```
