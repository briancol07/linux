# Proxies 

## Index 

* [## Dedicated Proxy / Forward proxy ](#dedicated)
* [## Reverse Proxy ](#reverse)
* [## (Non-) Transparent Proxy ](#transparent)

A proxy is when a device or service sits in the middle of a conenction and act as a mediator. The mediator is the critical piece of information because it means the device in the middle must be able to inspect the contents of the traffic. Without the ability to be a mediator, the device is technically a gateway, not a proxy. 

> A gateway is a connecting point of any network that helps it to connect with differnet networks o

In gateways he does not read the message , instead as a mediator he reads it ( Understand the message) . 

If you have trouble remembering this proxies will almost alwyas operate at layer 7 of the OSI Model.

* Dedicated Proxy / Forward Proxy
* Reverse proxy 
* Transparent proxy 

## Dedicated Proxy / Forward proxy <a name="dedicated"></a>

The forward proxy, is what most people imagine a proxy to be. A clietn makes a request to a computer and that computer carries out the request.

In some corporate network you must go through a proxy (or web filter). This can be an incredibly powerful line of defense against malware, as not only does it need to bypass the web filter, but it would also need to be proxy aware or use a non-traditional C2

* Winsock (Native Windows API)
  * Internet explorer
  * Edge 
  * Chrome 
  * All obey to system proxy 
* Libcurl
  * Firefox 
  * Same code on any operating system 

![forward](./img/forward.png)

## Reverse Proxy <a name="reverse"></a>

Is the Reverse of forward proxy. Instead of being designed to filter outgoing requests, it filter incoming ones. The most common goal with a reverse proxy, is to listen on an address and forward it to a closed-off network 

Penetration testers will configure reverse proxies on infected endpoints. The infected endpoint will liste on a port and send any client that connects to hte port back to the attcker through the infected endpoint. 

* Bypass firewalls 
* Evade logging 

Some organizations may have IDS (Instrusion detection systems), watching external web request. If the attacker gians access to the organization over SSH, a reverse proxy can send web request through the SSH-tunnel and evade the IDS.

* ModSecurity 
  * Web application Firewal (WAF)

This inspect web request for malicious content and block the request if it is malicious.

![reverse](./img/reverse.png)

## (Non-) Transparent Proxy <a name="transparent"></a>

* transparently 
* Non - transparently

With a transparent proxy, the client doesn't know about its existence. The transparent proxy intercepts the client's communication request to the internet and acts as a subsitute instance. To the outside, the transparent proxy, like the non-transparent proxy, acts as a communication partner. 

With the non-transparent proxy we must informed about its existence. For this purpose, we and the software we want to use are given a special proxy configuration that ensures that traffic to the internet is first addressed to the proxy. 

If the configuration does not exist, we cannot communicate via the proxy. 
