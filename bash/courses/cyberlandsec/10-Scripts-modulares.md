# Script modulares en Bash

## Que es un script Modular ?

Un script modular es un script que esta compuesto por varias funciones o secciones que trabajan juntas para realizar una tarea mayor. 

* Ventajas 
  * Reutilizacion
  * Mantenibilidad 
  * Legibilidad 

## Modulariazcion con funciones 

Una de las formas mas simples de modularizar un script es utilizando funciones, para que cada tarea este claramente separada.

## Modularizacion con archivos externos 

Otra forma es dividir el codigo en varios archivos. Esto es util cuando tienes funciones o secciones de codigo que quieres reutilizar en varios scripts. Puedes crear un archivo de funciones y luego importarlo.

Bash permite cargar otros archivos dentro de un script utilizando el comando `source` o `.` 

```
source archivo_externo.sh
. archivo_externo.sh
```

Se puede crear un archivo de funciones y luego otro principal que con el comando source lo llames arriba de todo

## Buenas practicas al modularizar 

1. Separa la logica en funciones
2. Utiliza archivos externos 
3. Documenta los modulos 
4. Evita la duplicacion de codigo 
5 Manten la modularizacion logica 

## Tarea 

1. Crea un archivo que contenga funciones para realizar operaciones matematicas ( suma resta multiplicacion division ) . Luego, crea un script separado que importe esas funciones y permita al usuario realizar diferentes operaciones 
2. Modifica el script apra que la funcion de suma pueda ser utilizada en otros sripts sin tenes que duplicar codigo 
