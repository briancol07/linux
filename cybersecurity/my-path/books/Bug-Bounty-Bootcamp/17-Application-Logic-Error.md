# Application logic errors 

> Business logic vulnerabilities

* No illegal HTTP request
* Use Logic flow that can result in Negative consequences 
* Broken Access control ( BAC ) 
  * Occurs when access control in an application is improperly implemented an it can by bypassed by an attacker
* Exposed admin panels 
* Directory traversal vulnerabilities ( PATH Traversal ) 
  * View 
  * Modify 
  * Execute files -> they shouldn't 

## Prevention 

* Test -> Functionalities
* Granular Access 
* Check consistent access 
  * API
  * Web 
  * Mobile 

## Hunting 

1. Learn about the target 
2. Intercept request while browsing 
3. Think Outside the box 
