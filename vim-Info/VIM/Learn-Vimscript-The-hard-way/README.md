# Learn Vimscript the hard way 

## Links 

* [The hard way](https://learnvimscriptthehardway.stevelosh.com/)

## Table of content

1. Prerequisites
2. Echoing Messages
3. Setting Options
4. Basic Mapping
5. Modal Mapping
6. Strict Mapping
7. Leaders
8. Editing Your Vimrc
9. Abbreviations
10. More Mappings
11. Training Your Fingers
12. Buffer-Local Options and Mappings
13. Autocommands
14. Buffer-Local Abbreviations
15. Autocommand Groups
16. Operator-Pending Mappings
17. More Operator-Pending Mappings
18. Status Lines
19. Responsible Coding
20. Variables
21. Variable Scoping
22. Conditionals
23. Comparisons
24. Functions
25. Function Arguments
26. Numbers
27. Strings
28. String Functions
29. Execute
30. Normal
31. Execute Normal!
32. Basic Regular Expressions
33. Case Study: Grep Operator, Part One
34. Case Study: Grep Operator, Part Two
35. Case Study: Grep Operator, Part Three
36. Lists
37. Looping
38. Dictionaries
39. Toggling
40. Functional Programming
41. Paths
42. Creating a Full Plugin
43. Plugin Layout in the Dark Ages
44. A New Hope: Plugin Layout with Pathogen
45. Detecting Filetypes
46. Basic Syntax Highlighting
47. Advanced Syntax Highlighting
48. Even More Advanced Syntax Highlighting
49. Basic Folding
50. Advanced Folding
51. Section Movement Theory
52. Potion Section Movement
53. External Commands
54. Autoloading
55. Documentation
56. Distribution
57. What Now?

