# Routing 

## Index 

* [## What is a router? ](#router)
  * [### How does a router work?](#work)
  * [### Hops ](#hops)
  * [### Diference between switching, routing & flooding ? ](#difference)
* [## Routing Table ](#routingTable)
  * [### Destination ](#destination)
  * [### Gateway ](#gateway)
  * [### Genmask ](#genmask)
  * [### Flags ](#flags)
  * [### Iface ](#iface)
* [## Path of a Packet ](#path)
  * [### Travel outside ](#outside)
* [## Routing protocols ](#protocols)
  * [### Convergence ](#convergence)
* [## Distance Vector Protocols ](#distance)
* [## Link state Protocol ](#state)
* [## Border Gateway protocol ](#border)

## What is a router? <a name="router"></a>

Enables machines on a network to communicate with each other as well as other networks. 
On a typical router, you will have LAN ports, Also an internet uplink port that connect you to the internet.

The router decides where our network packets go and which ones come in. It routes our packets between multiple networks to get from it'ssource host to it's destination host.

### How does a router work?<a name="work"></a>

Like a mail delivery,

When we route packets, they use similar address "routes", such as to get to network A, Send these packets to network B. When we don't have a route set for that, we have a default route that our packets will use. These routes are set on a routing table that oru system uses to navigate us across networks.

### Hops <a name="hops"></a>

As Packets move across netoworks, they travel in hops, a hop is how we roughly measure the distance that the packet must travel to get fro mthe source to the destination. Each hop is an intermediate device like the routers that we must pass through.

### Diference between switching, routing & flooding ? <a name="difference"></a>

* Switching is basically receiving, processing and forwarding data to the destionation device.
* Routing : Process of creating the routing table,so that we can do switching better 
* Flooding: If a router don't know which way to send a packet than every incoming packet is sent through every outgoing link except the one it arrived on.

## Routing Table <a name="routingTable"></a>

See the routing table `sudo route -n`

### Destination <a name="destination"></a>

In the first field, we have a destination IP address, this says that any packet that tries to go to this network. Example goes through Ethernet cable (eth0)

If we have addresses of 0.0.0.0 this means that no address is specified or it's unknown. Iwan to send a packet to IP address 151.123.43.6 our routing table doesn't know where that goes, so it denotes it as 0.0.0.0 and therefore routes ourpacket to the gateway 

### Gateway <a name="gateway"></a>

If we are sending a packet that is not on the same network, it will be sent to this gateway address, which is aptly name as being a gateway to another network

### Genmask <a name="genmask"></a>

This is the subnet mask, used to figure out what IP addressess match what destination 

### Flags <a name="flags"></a>

* UG network is UP and is a gateway
* U Network is up 

### Iface <a name="iface"></a>
  * This is the interface that our packet will be going out of eth0 usually stands forthe first Ethernet device on your systems 

## Path of a Packet <a name="path"></a>

1. First the local machine will compare the destination IP address to see if it's in the same subnet by looking at its subnet mask
2. When packets are sent they need to have a source MAC address, destination MAC, source IP and destination IP, at this point we do not know the destination MAC
3. To get the destination host, we use ARP to broadcast a request on the local network to find the mac address of the destination host
4. Now the packet can be successfully sent 

### Travel outside <a name="outside"></a>

1. Fist the local machine will compare the destination IP, since its outside of our network, it does not see MAC of the destination hsot. And we can't use ARP because is a boradcast to locally connected hosts 
2. So our packet now looks at the routing table , it doesn't knwo the address of the destination IP, so it sends it out to the default gateway (another router). SO now our packet contains our source IP destination IP and source MAC. Mac are only reached through the same network. SO what does it do? it sends an ARP request to get the MAC address of the default gateway.
3. The router looks at the packet and confirms the destination MAC, but it's not the final destiantion IP, so it keeps looking at the routing table to forward the packet to another IP that can help the packet move along to its destiantion. Everytime the packet moves, it strips the old source and destination MAC and updates the packet with the new source and destination MAC
4. Once the packet get forwarded to the same network, we use ARP to find the final destination MAC
5. During this process, our packet doesn't change the source or destination IP.

## Routing protocols <a name="protocols"></a>

Are used to help our system adapt to network changes. It learns of different routes, builds them in the routing table and then routes our packets through that way. 

* There are two primary routing procol types 
  * Distance vector protocols 
  * Link state protocols 

### Convergence <a name="convergence"></a>

When using routing protocols, routers communicate with other routers to collect and exchange information about the network, qwhen htey agree on how a network should look, every routing table maps out the complete topology of the network thus "converging".When something occurs in the network topology, the convergence will temporarily break until all routers are aware of this change.

## Distance Vector Protocols <a name="distance"></a>

Determine the path of other networks using the hop count a packet takes across the network. So it takes the one with the least amount of hops 

Great for small networks, when networks start to scale it takes longer for the routers to converge because it periodically sends the entire routing table out to every router.

Another downside to distance vector protocols is efficiency, it chooses routes that are closer in hops, but it may not always choose the most efficient route.

* Protocol
  * RIP (Routing information Protocol)
    * It boradcast the routingtable to every router in the network every 30s.
    * For large network, this cna take some serious juice to pull off, because of that RIP limits it's hop count to 15

## Link state Protocol <a name="state"></a>

Are great for large scale networks, they are more complex than distance vectors protocol. 
They converge quickly because instead of periodically sending out the whole routing table, they only send updates to neighboring routes.
They use a different algorithm to calculate the shortest path first and construct their network topology in the fomr of a graph to show which routers are connected to other routers.

* Protocol 
  * OSPF (Open Shortest Path first) 
    * It doesn't have a hop limit

## Border Gateway protocol <a name="border"></a>

* Protocol BGP

How the internet runs. It's used to collect and exchange routing informattion among autonomous systems ( Internet service provider, company, university, organization). Without BGP they would not know how to talk to each other, they would just be siloed off.
