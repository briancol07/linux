# linux

`(*・‿・)ノ⌒*:･ﾟ✧`

## Index 

* [## Books ](#books)
* [## Useful links ](#links)
* [## Taking notes ](#notes)
* [## Some Distros ](#some-distros)
* [## Folders ](#folders)

> This contains a mix of different types of subjects 


## Books <a name="books"></a>

* The linux Command line 
  * William E.Shott,jr.
* Notes for Professionals
* UNIX AND LINUX SYSTEM ADMINISTRATION HANDBOOK


## Useful links <a name="links"></a>

* [Process in background](https://linuxhandbook.com/run-process-background/)
* [Linux dev Setup](https://www.youtube.com/watch?v=hKGPH9C-EFc)
* [Linux administration](https://www.youtube.com/watch?v=1HqKJKnX6zk)
* [Linux Kernel Development YT](https://www.youtube.com/watch?v=WvByZ104jPY)


## Taking notes <a name="notes"></a>

* [Note-taking with Zettelkasten](https://www.youtube.com/watch?v=o1NJYnZCfmY&t=187s)

## Some Distros <a name="some-distros"></a>

* [light-weight](https://linuxsimply.com/best-linux-distros-for-old-laptops/)

## Folders <a name="folders"></a>

* [azure/](./azure/)
* [bash/](./bash/)
* [cybersecurity/](./cybersecurity/)
* [DevOps/](./DevOps/)
* [dotfiles/](./dotfiles/)
* [Grep/](./Grep/)
* [notes/](./notes/)
* [overTheWire/](./overTheWire/)
* [Raspy/](./Raspy/)
* [Sed/](./Sed/)
* [TLCL/](./TLCL/)
* [UbuntuServer/](./UbuntuServer/)
* [vim-Info/](./vim-Info/)
