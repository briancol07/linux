# Init

## Index 

* [## System V Overview ](#overview)
* [## System V service ](#service)
* [## Upstart Overview ](#upstart)
* [## Upstart jobs ](#jobs)
* [## Systemd overview ](#systemd)
* [## Systemd Goals ](#goals)
* [## Power states ](#power-states)

## System V Overview <a name="overview"></a>

The main purpose of init is to start and stop essential processes on the FS ( filesystem )

* Implementations
  * System V
  * Upstart
  * Systemd 

> To find out, check the `/etc/inittab`

SysV starts and stops processes sequentially, so let's say if you wanted to start up a service named foo-a, you have to make sure foo-a is already running. SysV does that with scritps, these scripts start and stop services for us ( we can write our own scripts ) 

The Pros is that it's relatively easy to solve dependencies, however performance isn't great because usually one thing is starting or stopping at a time 

* It have run levels 0-6
  0. Shutdown 
  1. Single user mode 
  2. Multiuser mode without networking 
  3. Multiuser mode with networking 
  4. Unused
  5. Multiuser mode with networking and GUI 
  6. Reboot 

When the system starts up, it lookd to see what runlevel you are in and executes scritps located inside that runlevel configuration `/etc/rc.d/rc[runlevel number].d` or `/etc/init.d`

Scripts that start with S(Start) or K(kill) will run on startup and shutdown 

To find out what runlevel your machine is booting into, you can see the default runlevel in the `/etc/inittab` file 

System V is getteing replaced. 

## System V service <a name="service"></a>

* Commands 
  * List Services: `service --status-all`
  * Start a service: `sudo service networking start`
  * Stop a service: `sudo service networking stop`
  * Restar a service: `sudo service networking restart` 

These commands aren't specific to sys V init systems, you can use these commands to manage upstart service as well . 

## Upstart Overview <a name="upstart"></a>

Init implementation on Ubuntu for a while. Was created to improve upon the issues with SysV.
To find out if you are using Upstart, if you have a `/usr/share/upstart` directory. 

Jobs are the actions that upstart performs and events are message that are received from other processs to trigger jobs. 

Inisde the job configurations, it'll include information on how to start jobs and when to start jobs 

* Way that upstart work 
  1. First, it loads up the job configuration from `/etc/init`
  2. Once a startup even occurs, it will run jobs triggered by that event 
  3. These jobs will make new events and then those events will trigger more jobs 
  4. Upstart continues to do this until it completes all the necessary jobs 

## Upstart jobs <a name="jobs"></a>

There is no easy way to see where an event or job originated, you have to poke arround the job configuration in `/etc/init`.

* View especific job ` initctl status networking `
* Manually start a job `sudo initctl start networking`
* Manually stop a job `sudo initctl stop networking`
* Manually restart a job `sudo initctl restart networking`
* Manually emit an event `sudo initctl emit some_event`

## Systemd overview <a name="systemd"></a>

If you have `/usr/lib/systemd` you're most likely using systemd 

Uses goals to get your system up and running. 

1. First, systemd loads its configurations files, usually located in `etc/systemd/system` or `/usr/lib/systemd/system`
2. Then it determines its boot goal, which is usually default target
3. System figures out the dependencies of the boot target and activates them 

* Targets 
  * Poweroff.target - Shutdown system
  * Rescue.target - Single user mode 
  * Mutli-user.target - multiuser with networking 
  * Graphical.target - multiuser with networking and GUI 
  * Reboot.target - restart

The default boot goal of default.target usually points to the graphical.target

The main object that systemd works with are known as units. Systemd doesn't just stop and start services, it can mount filesystem, moitor your network sockets, etc and because of that robustness it has differents types of units 

* Units 
  * Service units 
    * These are the services we've been starting and stopping, these unit files end in `.service`
  * Mount units 
    * These mount filesystem, these unit files end in `.mount`
  * Target Units
    * These group together other units, the files end in `.target`

## Systemd Goals <a name="goals"></a>


Here is a basic service unit file : `foobar.service`
```
[Unit]
Description=My Foobar
Before=bar.target
[Service]
ExecStart=/usr/bin/foobar
[Install]
WantedBy=multi-user.target
```

* List Units : `systemctl list-units`
* View status of units: ` systemctl status networking.service`
* Start a service: `sudo systemctl start networking.service`
* Stop a service: `sudo systemctl stop networking.service`
* Restart a service: `sudo systemctl restart networking.service`
* Enable a unit: `sudo systemctl enable networking.service`
* Disable a unit: `sudo systemctl disable networking.service`

## Power states <a name="power-states"></a>

* To shutdown your system: `sudo shutdown -h now`
  * Can use flag `-h +2` to indicate time ( this is 2 minutes to shutdown )
  * Restart `sudo shutdown -r now`
* Reboot: `sudo reboot`
