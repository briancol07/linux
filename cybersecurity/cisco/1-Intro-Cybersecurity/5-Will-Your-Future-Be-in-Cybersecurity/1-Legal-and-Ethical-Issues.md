# Legan and Ethica Issues

## Legal issues in Cybersecurity 

In order to protect against attacks, cybersecurity professionals must have the same skills as the attackers. However, cybersecurity professionals use their skills within the bounds of the law 

### Personal legal issues 

At work or home, you may have the opportunity and skills to hack another person's computer or network. But there is an old saying, 'just because you can does not mean you should'. 

Cybesecurity professional develop many skills, which can be used positively or illegally. There is always a huge demand for those who choose to put their cyber skill to good use within legal bounds 

### Coporate legal issues 

Most countries have cybersecurity laws in place, which businesses and organization must abide by.

In some cases, if you break cybersecurity laws while doing your job, the organization may be punished and you could lose your job. In other cases, you could be prosecuted, fined and possibly sentenced.

### International law and cybersecurity 

International cybersecurity law is a constantly evolving field. Cyber attacks take plae in cyberspace, an electronic space created, maintained and owned by both the public and private entities. 
There are no traditional geographic  boundaries in cyberspace. To further complicate issues, it is much easier to mask the source of a attack in cyberwarfare than in conventional warfare 


## Ethical Issues in Cybersecurity 

Ask yourself the following questins to help you decide on the best course of action 

* Is it legal?
* Does your action comply with company policy 
* Will your action be favorable for the company and its stakeholdes ?
* Would it be okay if everyone in the company took this action?
* Would the outcome of your action represent the company in a positive light in a new headline ?

## Corporate

Many professional IT organizations such as the Information Systems Security Association (ISSA) have published codes of Ethics to help guide employee actions and behaviors 

