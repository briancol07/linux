# API Hacking 

> Application Programming Interface 

Programs to communicate with each other ( set of rules ) or components 

* Action or communicate sensitive information 

URL is a request -> api endpoint 

* Response json or xml 
  * Key value param 
* REST API 
  * Representational state transfer 
* Structure for queries 
* Use HTTP methods 
* SOAP API 
  * Architecutre less common 
  * XML Data 
  * Service
    * Web services description language ( WSDL ) 
      * Describe API and how to access  ( .wsdl , ?wsdl) 
* Graphql API's 
  * Request the precise resource field they need 
  * Hve custom query language 
  * Queries 
    * Fetch data 
  * Mutation 
    * Create 
    * Update 
    * Delete 
* API-centric application ( app build using API ) 
  * Client side request and render using API calls 

## Hunting 

* Tools 
  * Postman 
  * Swagger 
  * Graphql playground 
  * ZAP
* API Enumeration refers to the process of identifying as many of the API's endpoint as you can (test them) 
* Testing rate limit 
* Technical bugs 

