if exists('*termopen')
    " Only run this code if termopen is available
    call termopen('bash')
else
    echo "termopen not available"
endif

if exists('*popup_create')
    " Only run this code if popup_create is available
    call popup_create('This is a floating window', {})
else
    echo "popup_create not available"
endif

