# Installing Kubernetes 

## Index 

* [## Configuration ](#Configuration)
* [## Infrastructure for kubernetes installation ](#infra)
* [## Installing local learning clusters ](#learning)
* [## Installing Production cluster with deployment tools ](#tools)
  * [### Kubeadm ](#kubeadm)
  * [### Kubespray ](#kubespray)
  * [### Kops ](#kops)
* [## Production Clusters from Certified Solutions Providers ](#prod)

## Configuration <a name="Configuration"></a>

Can be installed using different cluster configurations. 

* All-in-One-Single-Node installation 
  * All the control plane and worker components are installed and running on asingle-node
  * While it is useful for learning, development, and testing. Not recommended for production purposes
* Single-control plane and Multi-worker installation 
  * Single-control plane node running a stacked etcd instance.
  * Multiple worker nodes can be managed by the control plane node
* Single-control plane with Single-node etcd and multi-worker installation
  * We have a single-control plane node with an external etcd instance 
  * Multiple worker nodes can be managed by the control plane node
* Multi-control Plane and multi-worker installation 
  * We have multiple control plane nodes configured for High-availability (HA) with each control plane node running a stacked etcd instance 
  * The etcd instances are also configured in an HA etcd cluster and multiple worker nodes can me manged by the HA control plane 
* Multi-Control plane with multi-node etcd, and multi-worker installation 
  * We have multiple control plane nodes configured in HA mode with each control plane node paired with an externeal etcd instance. 
  * The external etcd instances are also configured in an HA etcd cluster 
  * Multiple worker nodes can be managed by the HA control plane.
  * This is the most advanced cluster configuration recommended for production environments 

## Infrastructure for kubernetes installation <a name="infra"></a>

* Should we set up kubernetes on bare metal, public cloud, private or hybrid cloud?
* Which underlying OS should we use? 
  * Linux Distribution
    * Red hat based 
    * Debian based
    * Windows 
  * Which networking solution (CNI) shoud we use 

## Installing local learning clusters <a name="learning"></a>

* [Minikube](https://minikube.sigs.k8s.io/docs/)
  * Single and multiple node local kubernetes cluster, recommended for a learning environment deployed on a single host
* [Kind](https://kind.sigs.k8s.io/)
  * Multi-node kubernetes cluster deployed in docker containers acting as Kubernetes nodes, recommended for learning environments 
* [Docker Desktop](https://www.docker.com/products/docker-desktop)
  * Including a local kubernetes cluster for docker users 
* [Podman Desktop](https://podman-desktop.io/)
  * Including kubernetes integration for podman users 
* [MicroK8s](https://microk8s.io/)
  * Local and cloud kubernetes cluster for developers and production from canonical.
* [K3S](https://k3s.io/)
  * Lightweight kubernetes cluster for local, cloud, edge, IoT deployments.
 
## Installing Production cluster with deployment tools <a name="tools"></a>

### Kubeadm <a name="kubeadm"></a>

Is a first-class citizen of the kubernetes ecosystem. It is a secure and recommended method to bootstrap a multi-node production ready HA kubernetes cluster, on-premises or in the cloud.  Also can a signle-node cluster for learning. It has a set of building blocks to set up the cluster. 

### Kubespray <a name="kubespray"></a>

Allow us to install HA production ready kubernetes cluster on AWS, GCP, Azure, OpenStack, VSphere or bare metal kubespray is based on ansible, and is available on most linux distribution.

### Kops <a name="kops"></a>

Enables us to create, upgrade and maitain production-grade, HA kubernetes cluster, from the commmand line. It can provision the required infrastructure as well. Currently, AWS and GCE are officially supported. Support for digitalocean and openstack in beta.

## Production Clusters from Certified Solutions Providers <a name="prod"></a>

* Hosted solutions
  * Alibaba Cloud container Service for kubernetes (ACK)
  * Amazon elastic kubernetes Service (EKS)
  * Azure Kubernetes Service (AKS)
  * ETC 
* Partners 
  * Aqua Security 
  * Canonical
  * D2IQ
  * Dell technologies consulting
  * ETC
* Turnkey cloud solution
  * Linode Kubernetes engine 
  * Nirmata managed kubernetes
  * Nutanix Karbon
  * Vultr kubernetes Engine
