# Debugging 



you can debug a part or all your bash script 

Debugging is running the script

## All the code 

### Way 1 adding -x in shebang line

 ``` 
#!/bin/bash -x 
 ```

### Way 2 adding -x calling the program

``` 
bash -x giga2mega.sh 7
```
This will show all the data and how it move

## One part 

we usea a command: 

```bash 

set -x 
# commands to test
set +x
```

## Also you can run a script from another 
