# Introduction to SQL injections 

Web applications usually use user-input when retrieving data, When a user uses the search functions to search for other users, their search input is passed to the web applications which uses the input to search within the database

## What is an Injection? 

Injection occurs when an application misinterprets user input as actual code rather than a string, changing the code flow and executing it. This can occur by sescaping user-input bonds by injecting a special character like (`'`), and then writing code to be executed like javaScript code or SQL in SQL injections. Unles the user input is sanitized, it's very likely to execute the injected code and run it.

> Sanitization : Refers to the removal of any special characters in user-input, in order to break any injection attempts.

## SQL injection 

An SQL injection occurs when user-input is imputted into the SQL query stringwithout properly sanitizing or filtering the input. 

We can add asingle quote (`'`), wich will end the user-input field, and after it we can write actual SQL code 

```
'%1'; DROP TABLE users;'
```
Can give an error `'%1'; DROP TABLE users;'`

In this case, we had only one trailing character, as our input from the search query was near the end of the SQL query. However, the user input usually goes in the middle of the SQL query and the rest of the original SQL query comes after it 

## Types of SQL injections 

![SQLi](./img/Types-of-SQLi.png)

In simple cases, the output of both the intende and the new query may be printed directly on the front end, and can read it . This is known as In-band SQLi , has two types Union based and Error based.

More complicated cases we may not get the output printed , so we may utilize SQL logic to retrieve the output character by character . This is know as Blinkd SQLi also two type Boolean Based and time Based .

Type | Description 
-----|------------
Union Based | We may have to specify the exact location, so the query will give the output there 
Error Based | It's used when we can get error in the front-end, and make SQL error that returns the output of our query
Boolean Based | We can use SQL Conditional to control whether the page returns any output at all
Time Based | We use SQL conditional statements that delay page reponse, like Sleep() function 

The last one , we may not have direct access to the output whatsoever, so we may have to direct th e output to a remote location, this is known as Out-of-Band SQL

