# What is XSS ? 

XSS allow an atttacker to execute arbitrary client-side code on a victim's browser XSS can be used for phishing, exfiltrating data, account takeover and more. 

Control behavior: in js 

## How does XSS work? 

* An attacker inserts a malicious script (or a payload) into the victim's browser 
* When the victim encoutners the script it executes in the victim's browser 
* A malicious payload is able to perform any action that the vitim is able to perform 
* If the victim has special privileges it can be a serous vulnerability 

## Types 


| Reflected XSS | Stored XSS | DOM XSS |
|:---------:|:---------:|:---------:|
  Payload is injected form the victim's request. The victim must clik a malicious link or navigate to an attacker-controlled property| Payload is stored server-side and can be triggered by a victim with no interaction outisde of the application| The vulnerability is in the clinet-side code versus the server-side injection is still typically from the victim's request|

## Reflected XSS 

```
https://example.com/page?param=<payload>
```

Places where insert html

## Stored XSS 

Also known as persistent XSS

* It stores the payload in the database 
* The payload is reflected back to the user when visiting a particular page 
* One of the most famous stored XSS is in Tweetdeck 

> The payload is stored server-side and can be triggered by a victim without any interacion outside of using the application


## DOM XSS

Happens when javascript reflects data from an attacker controlled reource ( within the URL ) and passes it to a function later on 

Consider the following piece of code that could be exploited by linking the victim to the following URL

```
https://example.com/#send-transaction<div/class="header__wrap"><a/href=javascript:alert(0)><h1>pwn3d</h1></a><img/src=//unskid.me/dist/jesus.gif></div>
```
