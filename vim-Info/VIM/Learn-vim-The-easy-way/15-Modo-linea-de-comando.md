# El modo de linea de comando 

## Entrar y salir del modo linea de comando 

Es un modo en si mismo, como cualquier otro ( normal,visual,etc). Cuando estas en este modo, el cursor se situa en la parte inferior de la pantalla donde se pueden escribir comandos. 

* Comandos 
  * Patrones de busqueda (`/ ?`)
  * Comandos de la linea de comandos (`:`)
  * Comandos externos (`!`)

## Repitiendo el comando previo 

Puedes repetir tanto el comando o externo ejecutando `@:`

## Atajos de teclado del modo linea de comandos 


Uso | Atajo 
:----:|:------:
Usando flechas | izq o der
Moviendote una palaba | `shift+(der|izq)`
Comienzo de linea | `ctrl + B`
Fin de linea | `ctrl + E `
Elimina un carcter | `ctrl + H`
Elimina una palabra | `ctrl + W`
Elimina la linea entera | ` ctrl + U`
Editar como si fuere archivo normal | `Ctrl + f `

## Registro y autocompletado

Si tenes alguna cosa guardada en el registro puedes usar `Ctrl + R a ` Todo lo que puedes hacer desde los registros en el modo normal, tambien puede hacerlo en el modo linea de comandos , tambien insertar la palabra debajo del cursor utiliza `ctrl + R Ctrl + A ` o la linea `ctrl + R ctrl + L` o el archivo `ctrl + R ctrl + F ` 

Autocomplete con tab tambien funciona , y `shift + tab` para la opcion previa ( same as `ctrl + n` or `ctrl + p`


## La ventana del hisotrial y la ventan de la linea de comandos 

Para abrir historial de la linea de comandos ejecuta `:his :` de manera predeterminada son 50 pero se puede modificar a 100 ` set history=100`

## Mas comandos para la linea de comandos 

```
:h ex-cmd-index 
:h :index
```




