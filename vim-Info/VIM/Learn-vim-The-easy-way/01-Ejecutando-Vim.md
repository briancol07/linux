# Ejecutando Vim 

## Instalacion 

* ver 
  * [web](https://www.vim.org/download.php)
  * [github](https://github.com/vim/vim)

## Comando Vim 

```vim 
vim
```
Aparecera una pantalla de bienvenida. Iniciara en el modo normal en el cual no podras escribir amenos que se cambie al modo de insercion (presione i)

```vim 
i
```

Para volver al modo normal se debera presionar esc, las teclas especiales las escribiremos con <>

```vim
<Esc>
```

## Saliendo de Vim 

Hay varias maneras de salir de vim, los dos puntos se utilizan para realizar comandos que mas adelante veremos pero por ahora los utilizaremos para salir 

``` vim
:quit 
:q 
```

## Guardando un fichero 

Utilizaremos nuevamente los dos punto mas otro comando que en este caso sera write o su forma abreviada w.

```vim 
:write 
:w 
```

### Guardarlo con un nombre 

```vim 
:w archivo.txt
```

Esto lo guardara como un archivo txt 

### Guardar y salir 

Aca se realizara una secuencia de guardar y luego salir 

```vim 
:wq
```

### Salir sin guardar 

```vim 
:q!
```

El signo fuerza el cierre 

## Ayuda 

Vim viene con un "manual" o pagina de ayuda que se puede acceder desde el mismo 

```vim 
:help {comando}
:h
```

Este es un ejemplo de las maneras de salid de vim. Como sabemos cual es cual, no lo sabemos pones una palabra y luego vas haciendo \<Tab\> y nos mostrara las distintas opciones 

```vim 
:h write-quit
```

## Abriendo un archivo 

Para abrir un solo archivo 

```vim 
vim hola1.txt
```

Abrir varios archivos no necesariamente tienen que tener la misma extension 

```bash
vim hola1.txt hola2.txt hola3.txt
```

Estos archivos se abren en buffers separados (luego los veremos en profundidad) 

## Argumentos 

### Version 

Para saber la version de vim que tenemos 

```bash 
vim --version
```
o desde el propio vim 

```vim 
:version
```


### Historial 
Ver el historial de comandos utilizados 

```vim 
:history
```

Si se queire abrir un archivo e inmediatamente ejecutar un comando, podras hacerlo aniadendio al comando vim la opcion + {cmd} 

### Substitute

Sustituir un texto con el comando 

```vim
:s
```

```bash 
vim +%s/donut/rosquilla/g hola.txt
```

Tambien se peude stackear 

```bash 
vim +%s/donut/rosquilla/g +%s/rosquilla/huevos/g hola.txt
```

Otra opcion sera con el comando de vim -c, en el cual tambien se pueden stackear  

```bash 
vim -c %s/donut/rosquilla/g hola
```

## Abriendo multiples ventanas 

Puedes ejecutar vim en ventanas divididas de manera horizontal y vertical con las opciones -o y -O respectivamente 

Abre con dos ventanas horizontales 
```bash 
vim -o2
```
Abre con 5 horizontales y ocupa las 2 primeras 

```bash 
vim -o5 hola1.txt hola2.txt
```

## Suspender 

Si necesitas suspender la ejecucion de vim mientras estas a la mitad podes presionar Ctrl - z , o tambien ejecutar los comandos 

```vim 
:stop
:suspend
```
Para volver a ejecutar ejecutar el comanod fg 

## Arancando vim de la manera mas inteligente

Ejemplo redirigir la salida de un comando a vim 

```bash 
ls -l | vim -
```
para aprender mas sobre vim usar el manual de linux 

```bash 
man vim
```
