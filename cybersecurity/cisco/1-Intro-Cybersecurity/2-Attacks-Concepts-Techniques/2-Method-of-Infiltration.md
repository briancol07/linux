# Method of Infiltration

## Social Engineering 

Is the manipulation of people into performing actions or divulging confidential information.

### pretexting 

This is whe an attacker calls an individual and lies to them in an attempt to agin access to privileged data.
For example, pretending to need a person's personal or financial data in order to confirm their identity.

### Tailgating 

This is when an attacker quickly follows an authorized person into a asecure, physical location


### Something for something (quid pro quo)

This is when an attacker requests personal information from a person in exchange for something, like a free gift.

## Denial of Service

**Dos** attacks are a type of network attack that is relatively simple to carry out, Results in some sort of interruption of network service to users, devices or applications 

### Overwhelming qyantity of traffic

> This is when a network, host or application is sent an enormous amout of data at a rate which it cannot handle. This causes a slowdown in transmission or response, or the device or service to crash.

### Maliciouly formatted packets 

A packet is a collection of data that flows between a source and a receiver computer or application over a network, such as the internet. When a maliciously formatted packet is sent, the receiver will be unable to handle it.

Dos attacks are considered a major risk because they can easily interrupt communication and cause significant loss of time and money.

## Distributed Dos 

DDoS is similar to a DoS but originates from multiple coordinated sources.

* An attacker builds a network(botnet) of infected hosts called zombies, which are controlled by handler systems.
* The zombie computer will constantly scan and infect more hosts, creating more and more zombies.
* When ready, the hacker will instruct the handler systems to make the botnet of zombies carry out a DDoS attack 

## Botnet 

A bot computer is typically infected by visiting an unsafe website or opening an infected email attachment or infected media file. Abotnet is a group of bots, connected through the internet, that can be controlled by a malicious individual or group, It can have tens of thousands, or even hundreds of thousands. Typically controlled thorugh a command and control server.

1. Infectd boyts try to communicate with a command and control host on the internet.
2. The Cisco firewall botnet filter is a feature that detects traffic coming from devices infected with the malicious botnet code.
3. The cloud-based Cisco Security Intelligence Operations (SIO) service pushes down update filters to the firewall that match traffic from new known botnets.
4. Alerts fo out to Cisco's internal security team to notify them about the infected devices that are generating malicious traffic so that they cn prevent, mitigate and remedy these

##  On-Path Attacks 

Intercept or modify communicaitons between two devices, such as a web browser and a web server, wither to collect information from or to impersonta one of the devices.
This type of attack is also referred to as **man in the middle** or **man in the mobile**.

### Man in the middle (MITM)

This happens when a cybercriminal takes control of a device without the user's knowledge. Whith this level of access, an attacker can intercept and capture user information before it is sent to its intended destination. Used to steal financial information.

### Man in the mobile (MITMO)

A variation of MITM, is a type of attack used to take control over a user's mobile device. When infected, the mobile device is instructed to exfiltrate user-sensitive infromation and send it to the attackers. ZeuS is one example of malware package with MitMo capabilities. It allows attackers to quietly capture two-step verification SMS messages that are sent to users.

## SEO Poisoning 

Attackert take advantage of popular seatch terms and use SEO to push malicious sites higher up the ranks of search result.

## Passwords Attacks 

### Password spraying 

This technique attempts to gian access to a system by 'spraying' a few commonly used password across a large number of accounts. This technique allows the perpetrator to reamin undetected as they avoid frequent account lockouts.

### Dictionary attacks 

A hacker systematically tries every word in a dictionary or a list of commonly used words as a password in an attempt to break into a password-protected account.

### Brute-force Attacks

The simplest and most commonly used way of gaining access to a password-protected site, brute-force attacks see an attacker using all possible combinations of letters, numbers and symbols in the password space until thy get it right .

### Rainbow attack 

Passwords in a computer system are not stored as plain text, but as hashed values (numerical values that uniquely identify data). A rainbow table is a large dictionary of precomputed hashes and the passwords from which they ere calculated.

Unlike a brute-force attack that has to calculate each hash, a rainbow attack compares the hash of a password with those stored in the rainbow table. When an attacker finds a match, they identify the pasword used to create the hash.

### Traffic interception

Plain text or unencypted passwords can be easily read by other humans and machines by intercepting communications.
If you store a password in clear, readable text, anyone who has access to your account or device, whether authorized or unauthorized, can read it.

## Advanced Persistent Threats 

Attackers also achieve infiltration through advanced persistent threats (APTs) a multi-phase, long term stealthy and advanced operation against a specific target. For these reasons, an individual attacker often lacks the skill set, resources or persistence to perform APTs
Its main purpose is to deploy customized malware on one or more of the target's systems and remains thre undetected.
