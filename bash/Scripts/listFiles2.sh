#!/bin/bash 

# Print with the format 
printScreen () {
  if [ -z "$2" ];then
    echo "* [$1](./$1)" | sed 's/://g'
  else
    echo "* [$1](./$2/$1)" | sed 's/://g' 
  fi
}

makeArray(){
  if [ -z "$1" ];then
    echo $(ls *)
  else
    echo $(ls $1)
  fi
}


iterateArray(){
  arrayFolder=()
  array=$(makeArray)

  for item in ${array[@]}; do
    if [[ -d $item ]];then 
      arrayFolder += $item
    else
      printScreen $item
    fi 
  done
  for item in ${arrayFolder[@]};do 
    arrayAux==$(makeArray $item)
    printScreen $item 
    for file in ${arrayAux[@]}; do
      if [[ -d $item ]];then 
        arrayFolder += $item
        continue
      else
        printScreen $item $file 
      fi 
    done
  done
}


echo "-----------------------------"

array=$(makeArray)
for item in ${array[@]}; do
  echo $item | sed 's/://g'
done

echo "-----------------------------"

iterateArray

