# Funciones 

## Que es una funcion? 

Una funcion es un bloque de codigo que tiene un nombre y puedes ejecutarlo cuando lo necesites, simplemente llamando a ese nombre 

* Nos permiten 
  * Reutilizar Codigo
  * Organizar el script 
  * Reducir errores 

## Estructura basica 

```
nombre_de_la_funcion() {
    # Código a ejecutar cuando se llama a la función
}
```

### Ejemplo 

```
#!/bin/bash

saludar() {
    echo "¡Hola, bienvenido al curso de Bash scripting!"
}

# Llamada a la función
saludar
```

## Paso de argumentos a funciones 

Los argumentos se pasan a la funcion al llamarla y dentro de la funcion puedes acceder a ellos 

* `$1` Es el primer argumento 
* `$2` el segundo 

```
#!/bin/bash

saludar() {
    echo "¡Hola, $1! Bienvenido al curso de Bash scripting."
}

# Llamada a la función con un argumento
saludar "Rocío"
```

Se pueden tantos argumentos como necesites 

## uso de variables locales 

Dentro de una funcion se pueden declarar variables locales, y solo existiran dentro de esa funcion, con el keyword `local`

```
#!/bin/bash

calcular_suma() {
    local suma=$(( $1 + $2 ))
    echo "La suma de $1 y $2 es: $suma"
}

# Llamada a la función
calcular_suma 5 10
```

## Devolver Valores desde una funcion 

Las funciones no pueden devolver valores directamente como en otros lenguajes programacion. Pero se puede devolver codigo de estado ( exit status ) utilizando el comando return o puedes imprimir el vlaor que quieres devolver y capturarlo en una variable 

### Ejemplo codigo de estado 

```
#!/bin/bash

comprobar_numero() {
    if [ $1 -gt 10 ]; then
        return 0  # Éxito
    else
        return 1  # Error
    fi
}

comprobar_numero 15
if [ $? -eq 0 ]; then
    echo "El número es mayor que 10"
else
    echo "El número es menor o igual a 10"
fi
```

### Ejemplo capturando valor 

```
#!/bin/bash

multiplicar() {
    echo $(( $1 * $2 ))
}

resultado=$(multiplicar 5 4)
echo "El resultado de la multiplicación es: $resultado"
```

## Funciones recursivas bash 

Una funcion puede llamarse a si misma , ejemplo factorial 

```
#!/bin/bash

factorial() {
    if [ $1 -le 1 ]; then
        echo 1
    else
        local anterior=$(factorial $(( $1 - 1 )))
        echo $(( $1 * anterior ))
    fi
}

# Calcular el factorial de 5
resultado=$(factorial 5)
echo "El factorial de 5 es: $resultado"
```

## Tarea de esta leccion 

1. Crear una funcion que reciba tres numeros como argumentos y devuelva el mayor de los tres 
2. Crea una funcion recursiva que calcule la suma de los numero del 1 al n ( donde n sea un argumento pasado a la funcion ) 

