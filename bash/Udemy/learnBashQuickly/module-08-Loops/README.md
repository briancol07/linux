# Module 8 Loops 

> Help to do something over and over

## Overview 

1. for loops in bash
  * C-style for loops
  * List/Range for loops 
2. While loops  
3. Until loops
4. Traversing array elements 
5. Using break and continue statement
6. Infinite loops 

## 1. C-stryle

``` 
for((initialize; condition; increment)); do 
  [commands]
done 
``` 

## List/Range for loops 

``` 
for item in [list]; do 
  [commands]
done

for i in {0..9}; do 
  echo "$i"
done
```

## 2. While loop

```
while [ condition ]; do
  [Commands]
done
```
## 3. Until Loop

> The until loop will run while the condition is false 

```
until [ condition ]; do 
  [commands]
done
```

## 4. Traversing array elements 



## 5. Using break and continue statement

> Loop control statement 

break will end the loop 
continue will omit that cicle 


## 6. Infinite loops 

It's not bad if you have a control statement. 
