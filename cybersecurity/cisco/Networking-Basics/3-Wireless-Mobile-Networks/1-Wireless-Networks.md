# Wireless Networks 

## Other wireless networks 

* Global Positioning System (GPS) 
  * Uses satellites to transmit signals 
  * Accuracy of within 10 meters 
* Wi-Fi
  * Enable phones to connect to local networks and internet 
  * Network access point 
  * Sometimes there are guest or public access hotspots 
    * Hotspot is an area where Wi-Fi signals are available 
* Bluetooth 
  * Low-power, shorter range wireless technology 
  * Can transmit data and voice 
* Near Fiel Communication (NFC) 
  * Enables data to be exchange by devices that are very close
  * Uses electromagnetic fields to transmit data 
