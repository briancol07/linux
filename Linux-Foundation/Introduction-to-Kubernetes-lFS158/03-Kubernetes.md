# Kubernetes 

## Index 

* [## What is kubernetes? ](#kubernetes)
* [## Kubernetes Features ](#features)
* [## Why use Kubernetes? ](#why)
* [## Cloud Native Computing FOundation (CNCF) ](#cncf)

## What is kubernetes? <a name="kubernetes"></a>

* [Website](https://kubernetes.io/)

> Open-soruce system for automating deployment, scaling and management of containerized applications 

Kubernetes is highly inspired by the google Borg System, a container and workload orchestrator for tis global operations. 

## Kubernetes Features <a name="features"></a>

* Automatic bin packing 
  * Automatically schedules containers based on resource need and constraints, to maximize utilization without sacrificing availability 
* Designed for extensibility 
  * Can extend with new custom features without modifying the upstream source code
* Self-healing 
  * Automatically replaces and reschedules containers from failed nodes.
  * It terminates and then restarts containers that become unresponsive to health, checks, based on exiting rules/policy.
* Horizontal scaling 
  * Kubernetes scales applications manually or automatically based on CPU or custom metrics utilization 
* Service discovery and load balancing 
  * Containers receive IP addresses from kubernetes, while it assigns a single domain name system (DNS) name to a set of containers to aid in load-balancing requests across the containers of the set 
* Additional features 
  * Automated rollout and roolbacks 
  * Secret and configuration management 
  * Storage orchestration 
    * Mounts SW-defined storage (SDS) solution to containers from local storage, external cloud providers, distributed storage or network storage systems 
  * Batch execution
  * IPv4/IPv6 dual stack 
* Support PaaS specific features such as:
  * Application deployment 
  * Scaling 
  * Load balancing 
  * Monitoring / logging and alerting solutions
* Alpha / Beta 
  * RBAC : role based access control 

## Why use Kubernetes? <a name="why"></a>

It can be deployed in many environments such as local or remote virtual machines, bare metal or in public/private/hybrid/multi-cloud setups

## Cloud Native Computing FOundation (CNCF) <a name="cncf"></a>

Is one of the largest sub-projects hosted by the linux foundation, ams to accelerate the adoption of containers, microservices, and cloud-native applications 

* Provides a neutral home for kubernetes trademark and enforces proper usage 
* Provides license scanning of core and vendor code
* Creates and maintains open source learning 
* Manages a SW conformance
* Actively markets kubernetes 
* Supports ad hoc activities
* Spnsors conferences and meetup event 
