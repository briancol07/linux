# Kubernetes Architecture 

## Architecture <a name="architecture"></a>

At high level is a cluster of compute systems categorized by their distinct roles 

* One or more control plane nodes
* One or more worker nodes (optional, but recommended)

![architecture](./img/architecture.png)

## Control Plane node overview <a name="control-plane"></a>

Provides a running environment for the control plane agents responsibles for managing the state of a kubernetes cluste, and it is the brain behind all operations inside the cluster. 

The controlplane components are agents with very distinct roles in the cluster's management. In order to communicate with the kubernetes cluster, users send request to the control plane via CLI, Web UI dashboard, or an API

If the control plane is lost we may introduce downtime, causing service disruption to clients, with possible loss of business. To ensure the control plane's fault tolerance, nodes replicas can be added to the cluster, configured in High-Availability (HA) mode.
This type of configuration adds resilency to the cluster's control plan, should the active control plane node fail.

To persist the kubernetes cluster's state, all cluster configuration data is saved to an distributed key-values store which only holds cluster state related data. The key-value store may be configured on the control plane node (stacked topology), or on its dedicated hots (external topology) to help reduce the chances of data store loss by decoupling it from other control plane agents.

In the stacked key-value store topology, HA control plane node replicas ensure the key-value store's resiliency as well. However, that is no the case with external key-value store topology, Where the dedicated key-value store host have to be separately replicated for HA, a configuration that introduces the need for additional hardware, hence additional operational costs.

## Control plane node Componenets <a name="components"></a>

* API server
* Scheduler 
* Controller managers 
* Key-value data store 

### API Server <a name="api-server"></a>

Administrative tasks are coordinated by the kube-apiserver, a central control plane component running on the control plane node. 

Intercepts RESTful calls from users, administrators, developers, operators and external agents, Then validates and processes them.

* Reads the Kubernetes cluster's current state from the key-value store and after call execution.
* The API server is the only control plane component to talk to the key-value store, both to read from and to save kubernetes cluster state information (acting as middle interface for any other contol plane agent inquiring about cluster state).
* The API server is higly configurable and customizable. It can scale horizontally, but it also supports the addition of custom secondary API sever, a configration the primary API server into a proxy to all secondary, custom API Servers, routing all incoming Restfull Calls to them based on custom defined rules. 


### Scheduler <a name="scheduler"></a>

The role of Kube-scheduler is to assign new workload objects such as pods encapsulating containers, to nodes.

```
Scheduler -> API Server --> Key-value
     <----state-----|
     |---Decision--->
```

The scheduler also receives from the API Server the new workload object's requirements which are part of its configuration data. 

Also takes into account Quality of Service (QoS) requirements, data locality, affinity, anti-affinity, taints, tolerations, cluster topology, etc.

Once all the cluster data is availabe the scheduling algorithms filters the nodes with predicates to isolate the possible node candidates which then are scored with priorities in order to select the one node that satisfies all the requirement for hosting the new workload. The outcome of the decision process is communicated back to the API Server. Which then deegates the workload deployment with other control plane agents 

The scheduler is highly configurable and customizable through scheduling policies, plugins, and profiles. Additional custom schedulers are also supported, then the object's configuration data should include the name of the custom scheduler expected to make the scheduling decision for that particular object; if no such data is included, the default scheduler is selected instead.

Is really importan in a multi-node kubernetes cluster, while in a single-node kubernetes cluster possibly used for learning and development purposes, the scheduler's job is quite simple.

### Controller Manager <a name="controller"></a>

To regulate the state of the cluster, are watch-loop processes continuously running and comparing the cluster's state (with the API-server).

In case of a mismatch, corrective action is taken in the cluster until its current state matches the desired state 

Run controllers or operators responsible to act when nodes become unavailable to ensure container pod counts are as expected, to create endpoints, service accounts and API access tokens 

### Key-value store data <a name="key-value"></a>

* [etcd](https://etcd.io/)

Open source project , strongly consistent, distributed key-value data store used to persist a kubernetes cluster's stte. Append data to it, never replaced. 
Obsolete data is compacted (or shredded) periodically to minimize the size of the data store 

Only API-sever is able to communicate with etcd data store.

* Provides 
  * Snapshot save 
  * restore capabilities 

Some kuberenetes cluster bootstrapping tool, such as kubeadm, by default, provision stacked etcd control plane nodes, where the data store runs alongside and shares resources with the other control plane componenets on the same control plane node.

![stacked](./img/stacked.png)

For data store isolation from the control plane components, the bootstrapping process can be configured for an external etcd topology, where the data store is provisioned on a dedicated separate host, thus reducing the chances of an etcd failure.


![external](./img/external.png)

Both support HA configuration, based on the [Raft Consensus algorithms](https://raft.github.io/) which allows a collection of mahcines to work as a coherent groupt that can survive failure of some of its members. Any node can be treated as a leader . 
Keep in mind however that the leader/followers hierarchy is distinct from the primary/secondary hierarchy, meaning that neither node is favored for the leader role, and neither node outranks other nodes.
A leader will remain active until it fails, at which point in time a new leader is elected by the group of healthy followers .

## Worker Node <a name="worker node"></a>

Provides a running environment for client applications. These applications are microservices running as application containers. In Kubernetes the application containers are encapsulated in Pods, controlled by the cluster control plane agent running on the control plane node.

A pods is the smallest schedullign work unit in kubernetes. It is a logical collection of one or more containers scheduled together and the collection can be started stopped and rescheduled as single unit of work

## Worker Node Components <a name="node-components"></a>

### Container Runtime <a name="runtime"></a>

Although kubernetes is described as a container orchestration engine, it lacks the capability to directly handle and run containers. 

In Order to manage a container's lifecycle, requires a container runtime on the node where a Pod and its containers are to be both control plane and worker. 

The recommendation is to run the kubernetes control plane components as containers, hence the necesity of a runtime on the control plane nodes 

* Several container runtimes 
  * CRI-O
  * Containerd
  * Docker engine 
  * Mirantis Container Runtime 

### Node Agent - Kubelet <a name="kubelet"></a>

Agent running on each node, control plane and workers, and it communicates tiwh the control plane. It receives pod definitions, primarly from the API-server and interacts with the container runtime on the node to run containers associated with the Pod.  Also monitors the heald and resources.

The kubelet connects to container runtime through a plugin based interface- the [CRI](https://github.com/kubernetes/community/blob/master/contributors/devel/sig-node/container-runtime-interface.md). Consist of protocol buffers, gRPC API, libraries and additional specifications and tools 

![kubelet](./img/kubelet.png)

* CRI Implements 2 services 
  * ImageService
    * Responsible for all the iamge-related operations
  * RuntimeService 
    * Responsible for all the pod and container related operations 

### Kubelet - CRI shims <a name="cri-shims"></a>

Kubelet agent supported only a couple of container runtimes, 

* Docker engine 
* rkt 

Kubernetes adopted a decoupled and flexible method to integrate with various container runtimes without the need to recompile its source code. Any container runtime that implements the CRI could be used by kubernetes to manage containers 

Shims are container runtime interfacer (CRI) implementations, interface or adapters, specific for each container runtime supported by kubernetes 

#### cri-containerd <a name="cri-containerd"></a>

Allows containers to be directy created and managed with containerd at kubelet's request

![cri-containerd](./img/cri-containerd.png)

#### CRI-O <a name="crio-o"></a>

Enables the use of any open container initiative (OCI) compatible runtime with kubernetes, such as runC.

![crio](./img/crio.png)

#### Dockershim <a name="dockershim"></a>

Not longer suppoer by kubernetes. So they introduces cri-dockerd that would ensure that the docker engine will continue to be a continer runtime option for kubernetes. In addition to the mirantis container runtime (MCR).

![dockershim](./img/dockershim.png)

### Proxy - kube-proxy <a name="kube-proxy"></a>

Is the netwrok agent whcih runs on each node, control plane and workers, responsible for dynamic updates and maintenance of all networking rules on the node. 

The kube proxy is responsble for TCP, UDP and SCTP stream forwarding or random forwarding across a set of Pod backends of an application, and it implements forwarding rules defined by users through Service API objects 

Operates in conjuntcion with the iptables of the node, iptables is a firewall utility created for the linux os that can be managed by users through CLI utility of the same name. The iptalbes utility is available for and pre-installed on many linux distributions 

### Add-ons <a name="add-ons"></a>

Are cluster features and functionallity not yet availabe in kubernetes, therefore implemented through 3rd-party plugins and services 

* DNS 
  * To assign DNS records to kubernetes objects and resources
* Dashboard 
  * Web UI
* Monitoring 
  * Collects metrics 
* Loggign 
  * Collects cluster-level container logs 
* Device Pluggins 
  * For system HW resources such as GPU, FPGA, High performance NIC

## Networking Challenges <a name="networking"></a>

Decoupled microservices based applications rely heavily on networking in order to mimic the tight- coupling once available in the monolithic era.

* Container-to-Container communication inside pods
* Pod-to-pod communication on the same node and across cluster nodes
* Service-to-Pod communication within the same namespace and across cluster namespeace 
* External-to-service communication for clients to access applications in a cluster 

### Container-to-container communication inside pods <a name="ctc"></a>

A container runtime creates an isolated network space for each container it starts. On linux this isolated network space is referred to as a network namespace. A netwrok namespace can be shared across containers, or with the host operating system. 

When a grouping of containers defined by a pod is started, a special infrastructure pause container is initialized by the container runtime for the sole purpose of creating a network namespace for the pod. All Additional containers, created through user request, running inisde the pods will share the pause container's network namespace so that they can talk to each other via localhost

### Pod to pod communication across nodes <a name="ptp"></a>

In a kubernetes cluter pods, groups of containers, are scheduled on nodes in a nearly unpredictable fashion. Regardless of their host nod pods are expected to be able to communicate with all other pods in the cluster . 

The kubernetes network model aims to reduce complexity and it treats pods as VMs on an network, where each vm is quipped with a network interface. This is called IP-per-Pod and ensures pod-to-pod communication.

They sahre the pod's netwok namespace and must coordiante ports assignments inside the pod jus as appication would on a VM

Howeber containers are integrated with the overall kubernetes networking model thorugh the use of the container network interface (CNI supported by CNI plugins 

![cni](./img/CNI.png)

The container runtime offloads the IP assignment to CNI, which connets to the underlying configured plugin. Once the IP address is given by the respective plugin, CNI forwards it back to the requested container runtime 

### External to pod communication<a name="etp"></a>

Kubernetes enables external accessibility through services complex encapsulations of network routing rule definitions stored in iptables on cluster nodes and implemented by kub-proxy agents. By exposing services to external world with the aid of kube-proxy applicatiohns become accessible from outside the cluster over a virtual IP address and a dedicated port number.
