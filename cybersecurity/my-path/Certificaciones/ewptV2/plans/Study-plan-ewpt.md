# Study plan 

## Mayo

* Semana 3-4: Fundamentos de Pentesting Web
    * Objetivo: Familiarizarse con los conceptos básicos de pentesting web.
    * Martes 1-3 horas.
      * Curso gratuito de OWASP (OWASP Top 10).
      * Recursos: OWASP Top 10.
      * Introducción a OWASP y OWASP Top 10.
        * Recurso: OWASP Top 10.
        * Tarea: Leer y comprender las primeras 3 vulnerabilidades del OWASP Top 10.
    * Sabado 1-3 horas.
      * Continuar con OWASP Top 10.
      * Tarea: Leer y comprender las siguientes 4 vulnerabilidades del OWASP Top 10.
    * Domingo 1-3 horas.
      * Finalizar OWASP Top 10.
      * Tarea: Leer y comprender las últimas 3 vulnerabilidades del OWASP Top 10.
    * Otros días: 30 minutos.
      * Leer y tomar notas del documento de OWASP Top 10.

# Junio

* Semana 1-2: Herramientas de Pentesting Web
  * Objetivo: Dominar el uso de herramientas como Burp Suite, SQLMap, OWASP ZAP.
  * Martes, Sábado y Domingo: 1-3 horas.
    * Tutoriales de Burp Suite en PortSwigger Academy (gratuito).
    * Recursos: PortSwigger Web Security Academy.
  * Martes 
    * Introducción a Burp Suite y su interfaz.
    * Recurso: PortSwigger Web Security Academy.
    * Tarea: Completar el módulo introductorio de Burp Suite.
  * Sabado 
    * Configuración y uso básico de Burp Suite.
    * Tarea: Realizar el laboratorio de "Proxy" y "Target" en PortSwigger Academy.
  * Domingo 
    * Herramientas de Burp Suite: Repeater, Intruder, Scanner.
    * Tarea: Completar los laboratorios de "Repeater" y "Intruder".
  * Otros días: 30 minutos.
    * Practicar con Burp Suite en sitios de prueba como bWAPP.
* Semana 3-4: Herramientas de Pentesting Web
  * Objetivo: Continuar con herramientas como SQLMap y OWASP ZAP.
  * Martes, Sábado y Domingo: 1-3 horas.
    * Tutoriales de SQLMap y OWASP ZAP.
    * Recursos: Documentación oficial de SQLMap y OWASP ZAP.
  * Martes 
    * Introducción a SQLMap.
    * Recurso: Documentación oficial de SQLMap.
    * Tarea: Realizar tutorial básico de SQLMap.
  * Sabado 
    * Introducción a OWASP ZAP.
    * Recurso: OWASP ZAP.
    * Tarea: Completar tutorial introductorio de OWASP ZAP.
  * Domingo
    * Uso avanzado de SQLMap y OWASP ZAP.
    * Tarea: Realizar laboratorios avanzados y ejemplos prácticos.
  * Otros días: 30 minutos.
    * Práctica en entornos de prueba.

## Julio

* Semana 1-2: Metodologías de Pentesting
  * Objetivo: Conocer las metodologías de pentesting.
  * Martes, Sábado y Domingo: 1-3 horas.
    * Leer la OWASP Testing Guide.
    * Recursos: OWASP Testing Guide.
  * Martes 
    * Introducción a OWASP Testing Guide.
    * Recurso: OWASP Testing Guide.
    * Tarea: Leer capítulos 1 y 2 (Introducción y Metodología).
  * Sabado 
    * Técnicas de testeo según OWASP.
    * Tarea: Leer capítulos 3 y 4 (Information Gathering y Configuration Management Testing).
  * Domingo
    * Continuar con técnicas de testeo.
    * Tarea: Leer capítulos 5 y 6 (Identity Management Testing y Authentication Testing).
  * Otros días: 30 minutos.
    * Resumir los capítulos y aplicar la teoría.
    * Resumir los capítulos leídos y tomar notas de los puntos clave.
* Semana 3-4: Metodologías de Pentesting
  * Objetivo: Continuar con metodologías de pentesting.
  * Martes, Sábado y Domingo: 1-3 horas.
  * Lectura y práctica de la OSSTMM (Open Source Security Testing Methodology Manual).
    * Recursos: OSSTMM (gratis).
  * Martes 
    * Introducción a OSSTMM.
    * Recurso: OSSTMM.
    * Tarea: Leer el capítulo introductorio de OSSTMM.
  * Sabado 
    * Análisis de Seguridad según OSSTMM.
    * Tarea: Leer y resumir las secciones sobre controles y auditorías.
  * Domingo
    * Aplicación práctica de OSSTMM.
    * Tarea: Realizar ejercicios prácticos basados en OSSTMM.
  * Otros días: 30 minutos.
    * Resumir y aplicar la teoría.
    * Resumir y aplicar la teoría en ejercicios cortos.

## Agosto

* Semana 1-2: Explotación Avanzada
  * Objetivo: Practicar explotación de vulnerabilidades avanzadas.
  * Martes, Sábado y Domingo: 1-3 horas.
    * Curso en línea de PentesterLab (pago).
    * Recursos:  PentesterLab.
    * Martes 
      * Introducción a los laboratorios de PentesterLab.
      * Recurso: PentesterLab (pago).
      * Tarea: Completar los primeros laboratorios de nivel básico.
    * Sabado 
      * Continuar con laboratorios intermedios.
      * Tarea: Completar laboratorios de inyección SQL y XSS avanzados.
    * Domingorsos: 
      * Realizar laboratorios de vulnerabilidades avanzadas.
      * Tarea: Completar laboratorios sobre deserialización y RCE.
  * Otros días: 30 minutos.
    * Laboratorios de Hack The Box (algunos gratuitos).
    * Recursos: Hack The Box.
* Semana 3-4: Explotación Avanzada
  * Objetivo: Continuar con explotación avanzada.
  * Martes, Sábado y Domingo: 1-3 horas.
    * Continuar con PentesterLab y Hack The Box.
    * Martes 
      * Desafíos de Hack The Box.
      * Tarea: Resolver una máquina fácil a intermedia.
    * Sabado 
      * Desafíos avanzados de TryHackMe.
      * Recurso: TryHackMe (algunos gratuitos).
      * Tarea: Completar un laboratorio avanzado.
    * Domingo 
      * Continuar con Hack The Box y TryHackMe.
      * Tarea:
    * Otros días: 30 minutos.
      * Practicar y repasar técnicas aprendidas en semanas anteriores.

## Septiembre

* Semana 1-2: Preparación para el Examen
  * Objetivo: Familiarizarse con el formato del examen y tipo de preguntas.
  * Martes, Sábado y Domingo: 1-3 horas.
    * Exámenes de práctica y ejemplos de eWPT.
    * Recursos: Foros de discusión y grupos de estudio (e.g., Reddit, Discord).
  * Martes 
    * Exámenes de práctica.
    * Recurso: Foros de discusión y grupos de estudio.
    * Tarea: Realizar un examen de práctica completo. 
  * Sabado 
    * Revisar respuestas de exámenes de práctica.
    * Tarea: Analizar y corregir errores cometidos.
  * Domingo
    * Continuar con exámenes de práctica.
    * Tarea: Realizar otro examen de práctica.
  * Otros días: 30 minutos.
    * Leer experiencias de otros candidatos y notas de estudio.
* Semana 3-4: Revisión y Práctica Intensiva
  * Objetivo: Revisión de todos los temas y práctica intensiva.
  * Martes, Sábado y Domingo: 1-3 horas.
    * Revisión de notas y laboratorios de prueba.
    * Martes 
      * Revisión de notas y conceptos clave.
      * Tarea: Repasar OWASP Top 10 y metodologías de pentesting.
    * Sabado 
      * Práctica intensiva en laboratorios.
      * Tarea: Resolver laboratorios pendientes o revisitar antiguos.
    * Domingo
      * Simulación de examen.
      * Tarea: Realizar una simulación completa del examen.
    * Otros días: 30 minutos.
      * Reforzar áreas débiles y practicar informes de vulnerabilidades.

## Octubre

* Semana 1-2: Simulación de Examen
  * Objetivo: Realizar simulaciones completas del examen.
  * Martes, Sábado y Domingo: 1-3 horas.
    * Simulaciones de exámenes y autoevaluaciones.
    * Martes 
      * Simulación de examen.
      * Tarea: Realizar una simulación de examen bajo condiciones similares.
    * Sabado 
      * Revisar resultados de la simulación.
      * Tarea: Analizar y corregir errores.
    * Domingo
      * Otra simulación de examen.
      * Tarea: Realizar una segunda simulación completa.
    * Otros días: 30 minutos.
      * Repaso de conceptos clave.
* Semana 3-4: Ajustes Finales y Revisión
  * Objetivo: Realizar ajustes finales y revisión de conceptos.
  * Martes, Sábado y Domingo: 1-3 horas.
    * Revisión de todos los temas y simulaciones.
    * Martes 
    * Sabado 
    * Domingo* Otros días: 30 minutos.
    * Relajación y técnicas de manejo del estrés pre-examen.

## Noviembre

* Todo el mes: Revisión y Preparación Final
  * Objetivo: Consolidar conocimientos y estar preparado para el examen.
  * Martes, Sábado y Domingo: 1-3 horas.
    * Revisar áreas específicas según necesidad.
  * Martes 
  * Sabado 
  * Domingo* Otros días: 30 minutos.
    * Mantenerse actualizado y enfocado.

# Recursos Adicionales (Gratuitos y Pagos)

* Gratuitos:
  * OWASP Top 10
  * PortSwigger Web Security Academy
  * OSSTMM
  * Hack The Box (algunos laboratorios gratuitos)
  * TryHackMe (algunos laboratorios gratuitos)
* Pagos:
  * PentesterLab
  * Cursos de pentesting en Udemy
  * Exámenes de práctica y recursos premium en eLearnSecurity
