# OSI model

## Index

* [## Layers ](#layer)

Was to create a reference model that enables the communicaiton of didfferent technical systems, via various devices and technologies and provides compatibility .

The layers represent phases in the establishment of each connection through which the sent packets pass. The standard was created to trace how a connection is structured and established visually.


## Layers <a name="layer"></a>

Layer | Function 
:----:|:-------:
7 - Application | This layer controls the input and output of data and providesthe application functions 
6 - Presentation | The task is to transfer the system-dependent presentation of data into a form independent of the application
5 - Session | Controls the logical connection between two systems and prevents, ex, connection breakdonw or other problems
4 - Transport | Is used for end-to-end control of the transferred data. Can detect and avoid congestions situations and segment data streams 
3 - Network | Connections are established in circuit-switched networks, and data packets are forwarded in packet-switched networks. Data is transmitted over the entire network from the sender to receiver
2 - Data link | Enable reliable and error-free transmissions on the respective medium. The bitstreams from layer 1 are divided into blocks or frames 
1 - Physical | The transmission takes place on wired or wireless transmisison lines

Layer 2-4 are transport oriented , 5-7 are application oriented. In each layer, precisely defined tasks are performed,and the interfaces to the neighboring layers are precisely described. 
Each one offers services for use to the layer directly above it. To make these the layer uses the service of the layer below it and performs the task of its layer. 

If the system communicate all 7 layer are run at least twice. When an application sends a packet to the other system, the system works the layers shown above from 7 down to layer 1, and the receiving system unpacks the received packet from layer 1 up to layer 7. 
