# The Three dimensions 

## 1. Security Principles 

The first dimension of the cybersecurity, identifies the goals to protect cyberspace. 
The foundational principles:

* Triad (CIA)
  * Confidentiality 
    * Prevents the disclosure of information to unauthorized people 
  * Integrity 
    * Refers to the accuracy consistency and trustworthiness 
  * Availability 
    * Ensures that information is accesible by authorized users when needed 

All of data, provide a focus which enables the cybersecurity expert to prioritize actions 

## 2. Data States

The cyberspace domain contains a considerble amount of critcally important data.

* Data States 
  * Data in Transit 
  * Data at rest or in storage 
  * Data in process 

## 3. Safeguards 

Bases of our defenses in order to protect data and infrastructure 

* Technology 
* Policy 
* Practices 
* Improving training and awereness in people

##  CIA Triad

### Confidentiality 

To accomplish confidentiality without using encryption, tokenization is a substitution technique. 

Tokenization can preserve the data format (type and length), which makes it useful for databases and card payment processing.

### DRM (Digital rights management) 

Protects copyrighted material like music albums, films or books. Cannot be copy without the decryption key.

IRM is used in email and other files relevant to the activities and communications of an organization that need to be shared with others.

## Data Integrity 

Integrity is the accuracy,m consistency and trustworthiness of data across its entire lifecycle

Data undergoes several operations such as capture , storage, retrieval, update and transfer and must remain unaltered by unauthorized entities during all these operations 

Banks need more data integrity than a social media platform 

### In a healthcare organization (Critical)

 Data integrity might be a matter of life or death. Presciption information must be accurate. Therefore all data is continuously validated, tested and verified. 

### In an E-Commerce or analytics-based organization (High level) 

Transaccion and customer accounts must be accurate. All data is validated and verified at frequent intervals 

###  Online sales (mid level)

Online sales and search engines collect data that has been publicly posted. Little verificaion is performed and data is not completely trustworthy 

### Blogs/ Forums / Personal pages (low level)

Public opinion and open contribution. Data may not be verified at all and there is a low level of trust in the content 

## Availability 

Availability refer to the need to maintain availability of information whenever it is needed. Cyberattacks and system failures cna prevent access to information, system and services. 

* Method to ensure 
  * System redundacy 
  * System backup
  * Increased system resilency 
  * Equipment maintenance 
  * OS updates and patches 
  * Proactive recovery plans 
