# Part 1 

## Basic Command 

* pwd 
  * Prints out the working directory 
* cd 
  * Change directory 
* ls 
  * list files in the directory 
* mkdir 
  * Make a directory
* rmdir 
  * Remove a directory  
* touch 
  * Create empty files
* rm 
  * Deletes a file of files
* date
  * Prints out the current date


