# Gestion de errores 

## Importancia de la gestion de errores 

La gestion de errores te permite anticiparte a estos problemas, manejar las situaciones inesperadas y asegurar que el script falle de manera controlada, o mejor aun que el script pueda continuar sin interrumpirse por completo 

* Cosas que pueden salir mal 
  * Archivos que no existen 
  * Permisos insuficientes 
  * Comandos que fallan 
  * Errores en la entrada del usuario 


## Codigos de salida ( exit codes ) 

Cada comando que ejecutar en bash devuelve un codigo de salida. Este codigo indica si el comando se ejecuto correctamente o si hubo algun error 

* Codigo de salida = 0 : se ejecuto correctamente 
* Distinto de 0 indica error 

Para acceder al exit code del ultimo comando `$?`

```
#!/bin/bash

mkdir /directorio/nuevo
if [ $? -eq 0 ]; then
    echo "Directorio creado con éxito."
else
    echo "Error al crear el directorio."
fi
```

## Uso de lcomando `set -e` para parar el script ante errores 

El comando `set -e` en bash hace que el script se detenga automaticamente cuando cualquier comando devuelve un codigo de error 

```
#!/bin/bash
set -e

echo "Creando un directorio..."
mkdir /directorio/nuevo

echo "Accediendo al directorio..."
cd /directorio/nuevo

echo "Todo salió bien."
```

## El comando `trap` para capturar seniales y errores 

El comando trap te permite capturar seniales del sistema o errores especificos y ejecutar un bloque de codigo cuando se procduce un evento. Es util para limpiar o tomar medidas en caso de error 

```
trap 'comando_a_ejecutar' SIGNAL
```
* Signal seria la senial que se quiere capturar un `exit` o `err` 

```
#!/bin/bash

trap 'echo "Error: Ocurrió un fallo en el script."' ERR

echo "Intentando acceder a un directorio que no existe..."
cd /directorio_inexistente

echo "Este mensaje no se mostrará si hay un error."
```

## Comprobar comandos con `&&` o `||`

Se pueden usar esos operadores para manejar errores de manera condicional

* `&&` ejecuta el segundo comando solo si el primero tiene exito
* `||` Ejecuta el comando solo si el primero falla 

```
#!/bin/bash

echo "Creando directorio..."
mkdir /directorio/nuevo && echo "Directorio creado con éxito." || echo "Error al crear el directorio."
```

## Manejo de errores complejos 

```
#!/bin/bash
set -e
trap 'echo "Error: Fallo en el script. Realizando limpieza..."; rm -f /tmp/archivo_temporal' ERR

echo "Creando archivo temporal..."
touch /tmp/archivo_temporal

echo "Modificando permisos del archivo temporal..."
chmod 777 /tmp/archivo_temporal

echo "Intentando acceder a un directorio inexistente..."
cd /directorio_inexistente

echo "Este mensaje no se mostrará si ocurre un error."
```

## Tareas 

1. Crea un script que copie un archivo de un directorio a otro. Si la copia falla, captura el error y elimina el archivo de destino ( si fue parcialmente copiado) 
2. Modifica un script existente para que utilice `set -e` y `trap paramanejar errores y limpiar recursos al terminar 
