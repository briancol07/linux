# Creating the perfect BB automation 

* [Link](https://labs.detectify.com/ethical-hacking/hakluke-creating-the-perfect-bug-bounty-automation/)

## Attempt 1 Bash 

* Get subdomains and check for HTTP responses
* Brute force all subdomains 
* Check all domains for open git repositories 
* Others 

> Files are slow 

## Attempt 2 Djando 

> Or other framework 

* Data Storage 
  * Sort of database 
  * Questions to answer
    * Programs that belong 
    * Ports open or a specific port
    * Vulns in those subdomains 

> Idea can be make a datalake 

* See
  * [Nuclei](https://github.com/projectdiscovery/nuclei) 
  * [Django](https://docs.djangoproject.com/en/3.2/howto/custom-management-commands/)


## Attempt 3 Golang & Unix Philosophy

* Piping tools into one to another 
* Unix Philosophy + horizontal Scalling , independent parts 
* A system to : 
  * Store and retrieve data from DB
  * Manage Scalling out jobs 
  * Scan for vulneravilities 
  * Send notificactions when something is discover 

> Consume alot and money 

## Attempt 4 Cloud native 

* Workers -> Docker containers 
  * Aws Fargate 
  * Aws Elastic container ( ECS )
* EFS : Share files to all containers 
* Amazon simple queue service ( SQS ) : automate jobs 
  * Dead letter queue ( DLQ ) is utilized to discover jobs that are failling 
* Amazon Aurora : RRDB

## Conclusion 

* Educate yourself with technologies that exist 
* Planning ahead ( Infrastructure and code ) 

### Other things 

* [RabbitMQ](https://www.rabbitmq.com/)
* [Interlace](https://github.com/codingo/Interlace)
* [HTTPx](https://github.com/projectdiscovery/httpx)
