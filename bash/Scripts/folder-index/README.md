# Folder Index 

Is a script that allow me to create an index for each folder in the directoy.
So I can navigate each folder from the first readme 

## Idea 

- [ ] Grep files and extract the name and create the string 
- [ ] One option is to avoid img folder 
- [ ] Error handler if there is no README file 

```
Directory 
  |- img
  |- Folder1 
  \- Folder2
```

### Result 

```
* [Folder1](./folder1/Readme)
* [Folder2](./folder2/Readme)
* [img](./img/Readme)
```

## Links 

* [Stackoverflow-exist-file](https://stackoverflow.com/questions/638975/how-do-i-tell-if-a-file-does-not-exist-in-bash)
