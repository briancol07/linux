# Introduction to BIOS 

> Basic Input/Output System 

* It is the first software to run on a PC when the power is turned on 
* It is firmware software that establishes the base behavior of the pc 
* It sets the HW configurations of the sytem
* It tells the PC what HW to initialize and what the priority of that HW is. 
* It point the sytem to the files that are needed to boot the operating system. 

## More BIOS basics 

* Stored in nonvolatile random access memory (NVRAM) 
* It is designed and coded for a specific motherboard
* BIOS updates can add functionality to a system that wasn't originally present.

## How to access the BIOS ?

System stability may have been compromised by the addition of a new component. Some people adjust settings in the BIOS to improve performance. Most importantly, the BIOS may need to be entered to help in the system recovery process.

## How to update the BIOS?

* Warning 
  * The BIOS is required in order for the system to function, making a mistake in updating the BIOS may have serious consequences on your system.
* Why the BIOS may need to be udpdated 
  * To extend the productive life of the system by adding additional functionality to the system 
  * To resolve issues that where unknown at the time the BIOS was created 
* The process needed to update
  * The motherboard/system manufacturer is responsible for developing the BIOS update process 
  * Follow their instructions to the letter to avoid a catastrophic failure

