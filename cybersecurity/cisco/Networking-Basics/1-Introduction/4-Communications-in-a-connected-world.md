# Communications in a connected world 

## Network type 

The internet is not owned, it's a worldwide collection of interconnected networks, cooperating with each other to exchange information. 

* Telephone wires 
* Fiber-optic cables
* Wireless transmissions 
* Satellite links 

## Data Transmission 

* Personal Data:
  * Volunteered data 
    * this is created and explicitly shared by individuals
    * Social network 
      * Videos 
      * Pictures 
  * Observed data 
    * This is capture by recording the actions of individuals 
  * Inferred data
    * This is data such as a credit score 

* Methods of signal transmission 
  * Electrical signals 
  * Optical signals 
  * Wireless signals 


## Banddith and throughput 

Bandwidth is the capacity of a medium to carry data, digital bandwidth measures the amount of data that can flow from one place to another ina given amount of time 

* Kbps 
* Mbps 
* Gbps 

Throughput does not usually match the specified bandwith

* Factors 
  * The amount of data being sent and received over the connection 
  * The latency created by the number of network devices encountered between the source and destination 

Latency refers to the amout of time including delays, for data to travel from one given point to another.
